#include "main.h"


struct netif *echo_netif;

int main(){

	Xil_Out32(XPAR_AXI_GPIO_0_BASEADDR, 70000);
	init_mb(); /* Инициализация Микроблэйза */

	Xil_Out32(XPAR_AXI_GPIO_0_BASEADDR + 83, 70000);

	Xil_Out32(XPAR_AXI_GPIO_0_BASEADDR, 70000);


	while(true){

		/* Передаем измеренные данные на ПК*/
		SendMeasurementDataToPC();
		/* Если DMA передал из ПЛИС в DDR спектр одного азимутального направления то можно его передать по сети */
		SendRawDataToPC();
		/* Прием данных по сети */
		xemacif_input(echo_netif);
	}
}

