#include "xadc.h"

XSysMon SysMonInst;	/* System Monitor driver instance */
XSysMon_Config *ConfigPtr;
XSysMon *SysMonInstPtr = &SysMonInst;
/*-------------------XADC----------------------*/
//u32 V_temp_Raw_Data;
//u16 Vaux_01_RawData;
//u32 Vaux_09_RawData;
//float Temp_FPGA;
//float Vaux_01_Data;
//float Vaux_10_Data;

int XADC_Init(){

	xil_printf_BLE("INFO: XADC enable channel 1,9 \r\n");

	ConfigPtr = XSysMon_LookupConfig(SYSMON_DEVICE_ID);
	if (ConfigPtr == NULL)
		return XST_FAILURE;

	XSysMon_CfgInitialize(SysMonInstPtr, ConfigPtr, ConfigPtr->BaseAddress);

	int Status = XSysMon_SelfTest(SysMonInstPtr);
	if (Status != XST_SUCCESS)
		return XST_FAILURE;


	XSysMon_SetSequencerMode(SysMonInstPtr, XSM_SEQ_MODE_SAFE);
	XSysMon_SetAlarmEnables(SysMonInstPtr, 0x0);
	XSysMon_SetAvg(SysMonInstPtr, XSM_AVG_16_SAMPLES);

	Status = XSysMon_SetSeqInputMode(SysMonInstPtr, XSM_SEQ_CH_TEMP |
													XSM_SEQ_CH_AUX00);
	if (Status != XST_SUCCESS)
		return XST_FAILURE;


	Status = XSysMon_SetSeqAcqTime(SysMonInstPtr, XSM_SEQ_CH_AUX15 |
												  XSM_SEQ_CH_TEMP |
												  XSM_SEQ_CH_AUX00);
	if (Status != XST_SUCCESS)
		return XST_FAILURE;


	Status = XSysMon_SetSeqAvgEnables(SysMonInstPtr, XSM_SEQ_CH_TEMP |
													 XSM_SEQ_CH_VCCINT |
													 XSM_SEQ_CH_VCCAUX |
													 XSM_SEQ_CH_AUX01 |
													 XSM_SEQ_CH_AUX09 |
													 //XSM_SEQ_CH_AUX00 |
													 //XSM_SEQ_CH_AUX02 |
													 //XSM_SEQ_CH_AUX10 |
													 //XSM_SEQ_CH_AUX15 |
													 XSM_SEQ_CH_CALIB);
	if (Status != XST_SUCCESS)
		return XST_FAILURE;


	Status = XSysMon_SetSeqChEnables(SysMonInstPtr, XSM_SEQ_CH_TEMP |
													XSM_SEQ_CH_VCCINT |
													XSM_SEQ_CH_VCCAUX |
													XSM_SEQ_CH_AUX01 |
													XSM_SEQ_CH_AUX09 |
													//XSM_SEQ_CH_AUX00 |
													//XSM_SEQ_CH_AUX02 |
													//XSM_SEQ_CH_AUX10 |
													//XSM_SEQ_CH_AUX15 |
													XSM_SEQ_CH_CALIB);
	if (Status != XST_SUCCESS)
		return XST_FAILURE;


	XSysMon_SetAdcClkDivisor(SysMonInstPtr, 32);
	XSysMon_SetCalibEnables(SysMonInstPtr, XSM_CFR1_CAL_PS_GAIN_OFFSET_MASK | XSM_CFR1_CAL_ADC_GAIN_OFFSET_MASK);
	XSysMon_SetSequencerMode(SysMonInstPtr, XSM_SEQ_MODE_CONTINPASS);

	XSysMon_GetStatus(SysMonInstPtr); /* Clear the old status */
	while ((XSysMon_GetStatus(SysMonInstPtr) & XSM_SR_EOS_MASK) != XSM_SR_EOS_MASK)
	{
	}

//	u32 V_temp_Raw_Data;
//	u16 Vaux_01_RawData;
//	u32 Vaux_09_RawData;
//	float Temp_FPGA;
//	float Vaux_01_Data;
//	float Vaux_10_Data;
//
//	printf("\r\n<---System_Monitor--->\r\n");
//	//--------------------------------------------------------------------------------------------------------------------------
//	V_temp_Raw_Data = XSysMon_GetAdcData(SysMonInstPtr, XSM_CH_TEMP);
//	Temp_FPGA = XSysMon_RawToTemperature(V_temp_Raw_Data);
//	printf("INFO: temperature FPGA    : %d.%d Grad  \r\n", (int)(Temp_FPGA), SysMonFractionToInt(Temp_FPGA));
//	//--------------------------------------------------------------------------------------------------------------------------
//	Vaux_09_RawData = XSysMon_GetAdcData(SysMonInstPtr, 16 + 9);
//	Vaux_10_Data = RawToVoltage(Vaux_09_RawData);
//	//printf("INFO: VCCAUX_10:%0d.%03d Volts            \r\n", (int)(Vaux_10_Data), SysMonFractionToInt(Vaux_10_Data));
//	printf("INFO: temperature LT3086 : %d.%d Grad   \r\n", (int)(float)(Vaux_09_RawData / (float)655.535), SysMonFractionToInt((float)(Vaux_09_RawData / (float)655.535)));
//	//--------------------------------------------------------------------------------------------------------------------------
//	Vaux_01_RawData = XSysMon_GetAdcData(SysMonInstPtr, 16 + 1);
//	Vaux_01_Data = RawToVoltage(Vaux_01_RawData);
//	//printf("INFO: Raw: %x                   \r\n", Vaux_02_RawData);
//	printf("INFO: Vcc_DetPower           : %0d.%03d Volts \r\n", (int)(Vaux_01_Data), SysMonFractionToInt(Vaux_01_Data));


//	Vdet_Param.param.param_value = 56;
//	Vdet_Param.param.param_count = 5;
//	Vdet_Param.param.param_index = 1;
//	Vdet_Param.param.param_type = PARAM_TYPE_REAL32;
//	strncpy(Vdet_Param.param.param_id,    "Vdet", strlen("Vdet")+1);
//	strncpy(Vdet_Param.param.param_group, "TransmiterGroup",  strlen("TransmiterGroup")+1);
//	Vdet_Param.param.param_type = 1;
//	Vdet_Param.setter = (paramFuncSet)setterIP;
//	Vdet_Param.getter = (paramFuncGet)getter_Vdet;
//
//	addParam(&Vdet_Param);

	xil_printf_BLE("INFO: XADC init completed \r\n");
	return XST_SUCCESS;
}
u16 XADC_Read(u8 channel){

	XSysMon_GetStatus(SysMonInstPtr); // Clear the old status
	while ((XSysMon_GetStatus(SysMonInstPtr) & XSM_SR_EOS_MASK) != XSM_SR_EOS_MASK){}
	return XSysMon_GetAdcData(SysMonInstPtr, channel);
}
int SysMonFractionToInt(float FloatNum){

	float Temp;

	Temp = FloatNum;
	if (FloatNum < 0)
	{
		Temp = -(FloatNum);
	}

	return (((int)((Temp - (float)((int)Temp)) * (10000000.0f))));
}



