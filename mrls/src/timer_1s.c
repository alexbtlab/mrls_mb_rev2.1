#include "timer_1s.h"

mavlink_gps_raw_t 	msgTx_gps;
mavlink_amplifier_t msgTx_amp;
mavlink_drive_t 	msgTx_drive;
mavlink_power_t	 	msgTx_power;

static void timer_handler(void *p);

void init_timer_1s(){

	/* set the number of cycles the timer counts before interrupting */
	/* 100 Mhz clock => .01us for 1 clk tick. For 100ms, 10000000 clk ticks need to elapse  */
	XTmrCtr_SetLoadReg(XPAR_AXI_TIMER_2_BASEADDR, 0, 100000000);
	//XTmrCtr_GetLoadReg(XPAR_AXI_TIMER_2_BASEADDR, 0);
	/* reset the timers, and clear interrupts */
	XTmrCtr_SetControlStatusReg(XPAR_AXI_TIMER_2_BASEADDR, 0, XTC_CSR_INT_OCCURED_MASK | XTC_CSR_LOAD_MASK );

	/* start the timers */
	XTmrCtr_SetControlStatusReg(XPAR_AXI_TIMER_2_BASEADDR, 0,
			   XTC_CSR_ENABLE_TMR_MASK | XTC_CSR_ENABLE_INT_MASK
			| XTC_CSR_AUTO_RELOAD_MASK | XTC_CSR_DOWN_COUNT_MASK);

	/* Register Timer handler */
	XIntc_RegisterHandler(XPAR_INTC_0_BASEADDR,
			XPAR_MICROBLAZE_0_AXI_INTC_AXI_TIMER_2_INTERRUPT_INTR,
			(XInterruptHandler)timer_handler,
			0);
}
static void timer_handler(void *p){

	timer_1s_callback();

	/* Load timer, clear interrupt bit */
	XTmrCtr_SetControlStatusReg(XPAR_AXI_TIMER_2_BASEADDR, 0,
			XTC_CSR_INT_OCCURED_MASK
			| XTC_CSR_LOAD_MASK);

	XTmrCtr_SetControlStatusReg(XPAR_AXI_TIMER_2_BASEADDR, 0,
			XTC_CSR_ENABLE_TMR_MASK
			| XTC_CSR_ENABLE_INT_MASK
			| XTC_CSR_AUTO_RELOAD_MASK
			| XTC_CSR_DOWN_COUNT_MASK);

	XIntc_AckIntr(XPAR_INTC_0_BASEADDR, PLATFORM_TIMER_2_INTERRUPT_MASK);
}

extern bool  flag_HB_done_to_transmit;
extern bool  flag_XADC_Data_Done;

void timer_1s_callback(){


	//flag_XADC_Data_Done = true;




	//printf("INFO: Vcc_DetPower           : %0d.%03d Volts \r\n", (int)(Vaux_01_Data), SysMonFractionToInt(Vaux_01_Data));

	//	flag_HB_done_to_transmit = true;





//	mavlink_message_t 	msgTx;
////	char bufTx_tim1s[128];
////
//u16 len_hb;
//
//
//
//	mavlink_msg_heartbeat_pack(SYS_ID, COMPONENT_ID, &msgTx, state_mrls, 1);
//
//	 len_hb = mavlink_msg_to_send_buffer((uint8_t *)bufTx, &msgTx);
//	 //xil_printf("%d\r\n", len_hb);
//
//	if(my_udp_send((char *)bufTx, len_hb, &ip_addr_broadcast) == ERR_UDP){
//		xil_printf("ERR: UDP my_udp_send failed\n");
//	}
//
//
////
//	//ready = true;
/////*------ GPS RAW ---------------------------------------------------------------*/
//	msgTx_gps.time = sys_tick;
//	msgTx_gps.lat  = 557825400;
//	msgTx_gps.lon  = 375819660;
//	mavlink_msg_gps_raw_encode(SYS_ID, COMPONENT_ID, &msgTx, &msgTx_gps);
//	u16 len_gps = mavlink_msg_to_send_buffer((uint8_t *)bufTx, &msgTx);
//	my_udp_send(bufTx, len_gps, &ip_addr_broadcast);
///*------ AMPLIFIER -------------------------------------------------------------*/
//	msgTx_amp.time   = sys_tick;
//	msgTx_amp.power  = 1;
//	msgTx_amp.temp   = 2;
//	mavlink_msg_amplifier_encode(SYS_ID, COMPONENT_ID, &msgTx, &msgTx_amp);
//	u16 len_amp = mavlink_msg_to_send_buffer((uint8_t *)bufTx, &msgTx);
//	my_udp_send(bufTx, len_amp, &ip_addr_broadcast);
///*------ DRIVE -----------------------------------------------------------------*/
//	msgTx_drive.time   = sys_tick;
//	msgTx_drive.speed  = 3;
//	msgTx_drive.temp   = 4;
//	mavlink_msg_drive_encode(SYS_ID, COMPONENT_ID, &msgTx, &msgTx_drive);
//	u16 len_drive = mavlink_msg_to_send_buffer((uint8_t *)bufTx, &msgTx);
//	my_udp_send(bufTx, len_drive, &ip_addr_broadcast);
///*------ POWER -----------------------------------------------------------------*/
//	msgTx_power.time  = sys_tick;
//	msgTx_power.value = 5;
//	mavlink_msg_power_encode(SYS_ID, COMPONENT_ID, &msgTx, &msgTx_power);
//	u16 len_power = mavlink_msg_to_send_buffer((uint8_t *)bufTx, &msgTx);
//	my_udp_send(bufTx, len_power, &ip_addr_broadcast);
}


