#include "uart.h"

XUartLite UartLiteInst;
XUartLite UartLiteInst_BLE;
INTC IntcInstance;
#define SIZE_BUF_FROM_JEKA 256
char buff_UART[SIZE_BUF_FROM_JEKA] = {0,};
char buff_UART_BLE[SIZE_BUF_FROM_JEKA] = {0,};
volatile u16 cntByte = 0;



u64 lon_dec;
u64 lat_dec;

char  end[128];
 char*  p_end;

u8 sat_dec;
//u8 latitude_cnt_data = 0;
//u8 longitude_cnt_data = 0;
u8 satelite_cnt_data = 0;

float lat_done = 0;
float lon_done = 0;
float sat_done = 0;

float lat_sum = 0;
float lon_sum = 0;
float sat_sum = 0;
   u8 lat_cnt = 0;
   u8 lon_cnt = 0;
   u8 sat_cnt = 0;

   float lat = 0;
   float lon = 0;
   uint8_t Sat;

   static char del_str[7] = {0,};
   static char adc_str[7] = {0,};

void UartLiteSendHandler_BLE(void *CallBackRef, unsigned int EventData){}
void UartLiteSendHandler(void *CallBackRef, unsigned int EventData){}
void parse_data_from_jeka(){

    char  end[128];
    char*  p_end = end;

	static char lat_degrees_str[2] = {0,};
	static char lat_minutes_str[7] = {0,};
	static char lon_degrees_str[3] = {0,};
	static char lon_minutes_str[7] = {0,};
	static char sat_str[7] = {0,};

	cntByte = 0;

	/*------LATITUDE---------------------------------------*/

		while (FIND_STR('L','a','t'))				// Ищем Latitude
		{
			if(cntByte != SIZE_BUF_FROM_JEKA)		cntByte++;	// Опасно!! можем обратится к несуществующему элементу массива
			else									return;		// Если не нашли вываливаемся и не парсим мусор
		}
			// Из ГГ°MM.ммм' / DD°MM.mmm'	в	ГГ.гггггг° / DD.dddddd°
			strncpy((char *)lat_degrees_str, 		BUFF_UART(LAT_DEGREES_POS),			LAT_SIZE_DEGREES_CELL);
			strncpy((char *)lat_minutes_str, 		BUFF_UART(LAT_MINUTE_POS),			LAT_SIZE_MINUTE_CELL);

			float lat_minutes = (float)strtod(lat_minutes_str, &p_end)/60;
			float lat_degrees = (float)strtod(lat_degrees_str, &p_end);
			lat = lat_degrees + lat_minutes;

//			if(lat != 0){
//				lat_sum += lat;
//				lat_cnt++;
//			}
//			else{
//				lat_cnt = 0;
//			}

			//xil_printf_BLE("lat:%d.%d ", (int)lat,	SysMonFractionToInt(lat));
	/*----LON------------------------------------------------------------------------------*/
		while ( FIND_STR('L','o','n'))				// Ищем Longitude
		{
			if(cntByte != SIZE_BUF_FROM_JEKA)		cntByte++;	// Опасно!! можем обратится к несуществующему элементу массива
			else									return;		// Если не нашли вываливаемся и не парсим мусор
		}
		// Из ГГ°MM.ммм' / DD°MM.mmm'	в	ГГ.гггггг° / DD.dddddd°
		strncpy((char *)lon_degrees_str, 		BUFF_UART(LON_DEGREES_POS),			LON_SIZE_DEGREES_CELL);
		strncpy((char *)lon_minutes_str, 		BUFF_UART(LON_MINUTE_POS),			LON_SIZE_MINUTE_CELL);

		float lon_minutes = (float)strtod(lon_minutes_str, &p_end)/60;
		float lon_degrees = (float)strtod(lon_degrees_str, &p_end);
		lon = lon_degrees + lon_minutes;

//		if(lon != 0){
//			lon_sum += lon;
//			lon_cnt++;
//		}
//		else{
//			lon_cnt = 0;
//		}
		//xil_printf_BLE("lon:%d.%d ", (int)lon,	SysMonFractionToInt(lon));
	/*------SATELLITE-----------------------------------------------------*/
		while (!( (buff_UART[cntByte]     == 'S')  &&		// Ищем количество спутников
				  (buff_UART[cntByte + 1] == 'a')  &&
				  (buff_UART[cntByte + 2] == 't')))
		{
			if(cntByte != SIZE_BUF_FROM_JEKA)		cntByte++;
			else									return;
		}

		memcpy((void *)sat_str, (void *)&buff_UART[cntByte+4],  2);
		//sat = strtol ((char *)sat_str, &p_end, 10);

//		if(sat != 0){
//			sat_sum += sat;
//			sat_cnt++;
//		}
//		else{
//			sat_cnt = 0;
//		}

		//xil_printf_BLE("Sat:%d\r\n", (int)sat);
	/*------ClearBuff-----------------------------------------------------*/
		memset((void *)buff_UART, 0, SIZE_BUF_FROM_JEKA+1);

		GPS_data_Recieved_Done = true;

//		if(sat_cnt >= 5 && lon_cnt >= 5 && lat_cnt >= 5){
//			lat_done = lat_sum/(float)lat_cnt;
//			lon_done = lon_sum/(float)lon_cnt;
//			sat_done = sat_sum/(float)sat_cnt;
//
//			xil_printf_BLE("INFO_GPS: ");
//			xil_printf_BLE("lat:%d.%d    ", (int)lat_done,	SysMonFractionToInt(lat_done));
//			xil_printf_BLE("lon:%d.%d\r\n", (int)lon_done,	SysMonFractionToInt(lon_done));
//
//			XIntc_Disable(intcp, XPAR_INTC_0_UARTLITE_0_VEC_ID);
//			XIntc_Disable(intcp, XPAR_INTC_0_UARTLITE_1_VEC_ID);
//
//			GPS_data_Recieved_Done = true;
//
//		}
}
void parse_data_from_vetal(){

	static char lat_degrees_str[2] = {0,};
	static char lat_minutes_str[7] = {0,};
	static char lon_degrees_str[3] = {0,};
	static char lon_minutes_str[7] = {0,};
	static char sat_str[7]         = {0,};

	cntByte = 0;

	while (FIND_STR('G','G','A'))				// Ищем Latitude
	{
		if(cntByte != SIZE_BUF_FROM_JEKA)		cntByte++;	// Опасно!! можем обратится к несуществующему элементу массива
		else									return;		// Если не нашли вываливаемся и не парсим мусор
	}

	strncpy((char *)lat_degrees_str, 		BUFF_UART(14),			2);
	strncpy((char *)lat_minutes_str, 		BUFF_UART(16),			8);
	strncpy((char *)lon_degrees_str, 		BUFF_UART(28),			2);
	strncpy((char *)lon_minutes_str, 		BUFF_UART(30),			8);
	strncpy((char *)sat_str, 		        BUFF_UART(43),			2);

	float lat_degrees = strtod(lat_degrees_str, &p_end);
	float lat_minutes = strtod(lat_minutes_str, &p_end)/60 ;
	float lon_minutes = strtod(lon_minutes_str, &p_end)/60;
	float lon_degrees = strtod(lon_degrees_str, &p_end);
	      Sat         = strtol(sat_str, &p_end, 10);

	lon = lon_degrees + lon_minutes;
	lat = lat_degrees + lat_minutes;

	GPS_data_Recieved_Done = true;
}
void UartLiteRecvHandler(void *CallBackRef, unsigned int EventData){

	buff_UART[cntByte] = XUartLite_RecvByte(XPAR_AXI_UARTLITE_0_BASEADDR);	// Принимаем по байту

	if((buff_UART[cntByte] == '\n'))	parse_data_from_vetal();
	else								cntByte++;
}
#include "string.h"

void UartLiteRecvHandler_BLE(void *CallBackRef, unsigned int EventData){

	u16 del = 0;
    u16 adc = 0;
    char  end[128];
    char*  p_end = end;
    u16 delay;

    buff_UART_BLE[cntByte] = XUartLite_RecvByte(XPAR_AXI_UARTLITE_BLE_BASEADDR);	// Принимаем по байту

		if((buff_UART_BLE[cntByte-1] == '\r') && (buff_UART_BLE[cntByte] == '\n'))	{
			buff_UART_BLE[cntByte+1] = 0;
			cntByte = 0;
			xil_printf_BLE(":%s\r\n", &buff_UART_BLE[0]);	//Возвращаем то что приняли
			if(strncmp(buff_UART_BLE, "ADC:", 4) == 0){

				delay = strtol(&buff_UART_BLE[4], &p_end, 10);
				xil_printf_BLE("delay:%d\r\n", delay);
//				AD9508_writeData(0x17, delay);
//				AD9508_writeData(0x1D, delay);
//				AD9508_writeData(0x23, delay);
//				AD9508_writeData(0x29, delay);
				Xil_Out32(XPAR_AXI_GPIO_0_BASEADDR, delay);
			}

			HMC769_setAtten(strtol ((char *)buff_UART_BLE, &p_end, 10));
			//AD9508_writeData(0x17, strtol ((char *)buff_UART, &p_end, 10));
//			Xil_Out32(XPAR_AXI_DELAYCTRL_0_S00_AXI_BASEADDR, strtol ((char *)buff_UART_BLE, &p_end, 10));
			//HMC769_setAtten(strtol ((char *)buff_UART, &p_end, 10));

//			Xil_Out32(XPAR_AXI_DELAYCTRL_0_S00_AXI_BASEADDR, strtol ((char *)buff_UART, &p_end, 10));
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//			if(strncmp("adc", buff_UART, 3) == 0){
//
//				memcpy((void *)adc_str, (void *)&buff_UART[cntByte+3],  2);
//				adc = strtol ((char *)adc_str, &p_end, 10);
//				AD9508_writeData(0x17, adc);
//			}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//			if(strncmp("del", buff_UART, 3) == 0){
//
//				memcpy((void *)del_str, (void *)&buff_UART[cntByte+3],  2);
//				del = strtol ((char *)del_str, &p_end, 10);
//
//				Xil_Out32(XPAR_AXI_DELAYCTRL_0_S00_AXI_BASEADDR, del);
//			}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





//			while (!( (buff_UART[cntByte]     == 'd')  &&		// Ищем количество спутников
//					  (buff_UART[cntByte + 1] == 'e')  &&
//					  (buff_UART[cntByte + 2] == 'l')))
//			{
//				if(cntByte != SIZE_BUF_FROM_JEKA)		cntByte++;
//				else									return;
//			}
//
//			memcpy((void *)del_str, (void *)&buff_UART[cntByte+3],  2);
//			del = strtol ((char *)del_str, &p_end, 10);
//			Xil_Out32(XPAR_AXI_DELAYCTRL_0_S00_AXI_BASEADDR, strtol ((char *)buff_UART, &p_end, 10));
//
//			xil_printf_BLE("del:%d\r\n", del);	//Возвращаем то что приняли




//			Xil_Out32(XPAR_AXI_DELAYCTRL_0_S00_AXI_BASEADDR, strtol ((char *)buff_UART, &p_end, 10));
//
//			//Xil_Out32(XPAR_AXI_GPIO_2_BASEADDR, strtol ((char *)buff_UART, &p_end, 10));
//			//Xil_Out32(XPAR_AXI_GPIO_5_BASEADDR, strtol ((char *)buff_UART, &p_end, 10));			// shift_adc_start
//			AD9508_writeData(0x17, strtol ((char *)buff_UART, &p_end, 10));
			//memset((void *)buff_UART, 0, SIZE_BUF_FROM_JEKA+1);
		}
		else								cntByte++;										// Продолжаем принимать... Еще не конец строки
}



