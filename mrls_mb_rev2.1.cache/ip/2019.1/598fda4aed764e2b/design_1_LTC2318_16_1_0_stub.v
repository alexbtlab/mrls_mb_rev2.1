// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Oct 13 18:21:43 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_LTC2318_16_1_0_stub.v
// Design      : design_1_LTC2318_16_1_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "LTC2387_16,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk_data_out, clk_data_out_inv, ADC_DATA_OUT, 
  ADC_DATA, adc_dax_p, adc_dax_n, adc_dbx_p, adc_dbx_n, adc_dcox_p, adc_dcox_n, fpga_clk, clk_100, 
  adc_clkx_p, adc_clkx_n, polar, NUM_READ_1514, adc_da_ILA, adc_db_ILA, adc_clk_ILA, 
  adc_dco_delayed_ILA, adc_clk_delayed_ILA, cnt100_ILA, cnt_pos_dco_ILA, cnt_neg_dco_ILA, 
  clk_200)
/* synthesis syn_black_box black_box_pad_pin="clk_data_out,clk_data_out_inv,ADC_DATA_OUT[15:0],ADC_DATA[15:0],adc_dax_p,adc_dax_n,adc_dbx_p,adc_dbx_n,adc_dcox_p,adc_dcox_n,fpga_clk,clk_100,adc_clkx_p,adc_clkx_n,polar,NUM_READ_1514[2:0],adc_da_ILA,adc_db_ILA,adc_clk_ILA,adc_dco_delayed_ILA,adc_clk_delayed_ILA,cnt100_ILA[2:0],cnt_pos_dco_ILA[1:0],cnt_neg_dco_ILA[1:0],clk_200" */;
  output clk_data_out;
  output clk_data_out_inv;
  output [15:0]ADC_DATA_OUT;
  output [15:0]ADC_DATA;
  input adc_dax_p;
  input adc_dax_n;
  input adc_dbx_p;
  input adc_dbx_n;
  input adc_dcox_p;
  input adc_dcox_n;
  input fpga_clk;
  input clk_100;
  output adc_clkx_p;
  output adc_clkx_n;
  input polar;
  input [2:0]NUM_READ_1514;
  output adc_da_ILA;
  output adc_db_ILA;
  output adc_clk_ILA;
  output adc_dco_delayed_ILA;
  output adc_clk_delayed_ILA;
  output [2:0]cnt100_ILA;
  output [1:0]cnt_pos_dco_ILA;
  output [1:0]cnt_neg_dco_ILA;
  input clk_200;
endmodule
