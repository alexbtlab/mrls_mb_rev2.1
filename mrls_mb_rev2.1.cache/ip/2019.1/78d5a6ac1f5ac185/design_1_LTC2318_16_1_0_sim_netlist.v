// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Fri Sep 25 15:02:40 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_LTC2318_16_1_0_sim_netlist.v
// Design      : design_1_LTC2318_16_1_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_LTC2387
   (adc_clkx_p,
    adc_clkx_n,
    adc_clk_ILA,
    adc_da_ILA,
    adc_dco_ILA,
    adc_dco_delayed_ILA,
    cnt_pos_dco_ILA,
    cnt_neg_dco_ILA,
    cnt100_ILA,
    adc_dax_p,
    adc_dax_n,
    adc_dbx_p,
    adc_dbx_n,
    adc_dcox_p,
    adc_dcox_n,
    clk_200,
    fpga_clk,
    clk_100);
  output adc_clkx_p;
  output adc_clkx_n;
  output adc_clk_ILA;
  output adc_da_ILA;
  output adc_dco_ILA;
  output adc_dco_delayed_ILA;
  output [1:0]cnt_pos_dco_ILA;
  output [1:0]cnt_neg_dco_ILA;
  output [3:0]cnt100_ILA;
  input adc_dax_p;
  input adc_dax_n;
  input adc_dbx_p;
  input adc_dbx_n;
  input adc_dcox_p;
  input adc_dcox_n;
  input clk_200;
  input fpga_clk;
  input clk_100;

  wire adc_clk_ILA;
  wire adc_clkx_n;
  wire adc_clkx_p;
  wire adc_da_ILA;
  wire adc_dax_n;
  wire adc_dax_p;
  wire adc_dbx_n;
  wire adc_dbx_p;
  wire adc_dco_ILA;
  wire adc_dco_delayed_ILA;
  wire adc_dcox_n;
  wire adc_dcox_p;
  wire clk_100;
  wire clk_200;
  wire [3:0]cnt100_ILA;
  wire \cnt_neg_dco[0]_i_1_n_0 ;
  wire \cnt_neg_dco[1]_i_1_n_0 ;
  wire [1:0]cnt_neg_dco_ILA;
  wire \cnt_pos_dco[0]_i_1_n_0 ;
  wire \cnt_pos_dco[1]_i_1_n_0 ;
  wire [1:0]cnt_pos_dco_ILA;
  wire fpga_clk;
  wire [3:0]p_0_in;
  wire NLW_IBUFDS_adc_db_O_UNCONNECTED;
  wire NLW_IDELAYCTRL_inst_RDY_UNCONNECTED;
  wire NLW_IDELAYE2_adc_if2_ch1_dco_IDATAIN_UNCONNECTED;
  wire [4:0]NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEIN_UNCONNECTED;
  wire [4:0]NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEOUT_UNCONNECTED;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0600)) 
    \/i_ 
       (.I0(cnt100_ILA[1]),
        .I1(cnt100_ILA[2]),
        .I2(cnt100_ILA[3]),
        .I3(clk_100),
        .O(adc_clk_ILA));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_da
       (.I(adc_dax_p),
        .IB(adc_dax_n),
        .O(adc_da_ILA));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_db
       (.I(adc_dbx_p),
        .IB(adc_dbx_n),
        .O(NLW_IBUFDS_adc_db_O_UNCONNECTED));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_dco
       (.I(adc_dcox_p),
        .IB(adc_dcox_n),
        .O(adc_dco_ILA));
  (* BOX_TYPE = "PRIMITIVE" *) 
  IDELAYCTRL #(
    .SIM_DEVICE("7SERIES")) 
    IDELAYCTRL_inst
       (.RDY(NLW_IDELAYCTRL_inst_RDY_UNCONNECTED),
        .REFCLK(clk_200),
        .RST(1'b0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SIM_DELAY_D = "0" *) 
  IDELAYE2 #(
    .CINVCTRL_SEL("FALSE"),
    .DELAY_SRC("DATAIN"),
    .HIGH_PERFORMANCE_MODE("TRUE"),
    .IDELAY_TYPE("FIXED"),
    .IDELAY_VALUE(13),
    .IS_C_INVERTED(1'b0),
    .IS_DATAIN_INVERTED(1'b0),
    .IS_IDATAIN_INVERTED(1'b0),
    .PIPE_SEL("FALSE"),
    .REFCLK_FREQUENCY(200.000000),
    .SIGNAL_PATTERN("CLOCK")) 
    IDELAYE2_adc_if2_ch1_dco
       (.C(clk_200),
        .CE(1'b0),
        .CINVCTRL(1'b0),
        .CNTVALUEIN(NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEIN_UNCONNECTED[4:0]),
        .CNTVALUEOUT(NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEOUT_UNCONNECTED[4:0]),
        .DATAIN(adc_dco_ILA),
        .DATAOUT(adc_dco_delayed_ILA),
        .IDATAIN(NLW_IDELAYE2_adc_if2_ch1_dco_IDATAIN_UNCONNECTED),
        .INC(1'b0),
        .LD(1'b0),
        .LDPIPEEN(1'b0),
        .REGRST(1'b0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    OBUFDS_adc_clk
       (.I(adc_clk_ILA),
        .O(adc_clkx_p),
        .OB(adc_clkx_n));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt100[0]_i_1 
       (.I0(cnt100_ILA[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \cnt100[1]_i_1 
       (.I0(cnt100_ILA[0]),
        .I1(cnt100_ILA[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \cnt100[2]_i_1 
       (.I0(cnt100_ILA[0]),
        .I1(cnt100_ILA[1]),
        .I2(cnt100_ILA[2]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \cnt100[3]_i_1 
       (.I0(cnt100_ILA[1]),
        .I1(cnt100_ILA[0]),
        .I2(cnt100_ILA[2]),
        .I3(cnt100_ILA[3]),
        .O(p_0_in[3]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[0] 
       (.C(clk_100),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(cnt100_ILA[0]),
        .R(fpga_clk));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[1] 
       (.C(clk_100),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(cnt100_ILA[1]),
        .R(fpga_clk));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[2] 
       (.C(clk_100),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(cnt100_ILA[2]),
        .R(fpga_clk));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[3] 
       (.C(clk_100),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(cnt100_ILA[3]),
        .R(fpga_clk));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \cnt_neg_dco[0]_i_1 
       (.I0(cnt_neg_dco_ILA[0]),
        .I1(fpga_clk),
        .O(\cnt_neg_dco[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h06)) 
    \cnt_neg_dco[1]_i_1 
       (.I0(cnt_neg_dco_ILA[1]),
        .I1(cnt_neg_dco_ILA[0]),
        .I2(fpga_clk),
        .O(\cnt_neg_dco[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_neg_dco_reg[0] 
       (.C(adc_dco_delayed_ILA),
        .CE(1'b1),
        .D(\cnt_neg_dco[0]_i_1_n_0 ),
        .Q(cnt_neg_dco_ILA[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_neg_dco_reg[1] 
       (.C(adc_dco_delayed_ILA),
        .CE(1'b1),
        .D(\cnt_neg_dco[1]_i_1_n_0 ),
        .Q(cnt_neg_dco_ILA[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \cnt_pos_dco[0]_i_1 
       (.I0(cnt_pos_dco_ILA[0]),
        .I1(fpga_clk),
        .O(\cnt_pos_dco[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h06)) 
    \cnt_pos_dco[1]_i_1 
       (.I0(cnt_pos_dco_ILA[1]),
        .I1(cnt_pos_dco_ILA[0]),
        .I2(fpga_clk),
        .O(\cnt_pos_dco[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_pos_dco_reg[0] 
       (.C(adc_dco_delayed_ILA),
        .CE(1'b1),
        .D(\cnt_pos_dco[0]_i_1_n_0 ),
        .Q(cnt_pos_dco_ILA[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_pos_dco_reg[1] 
       (.C(adc_dco_delayed_ILA),
        .CE(1'b1),
        .D(\cnt_pos_dco[1]_i_1_n_0 ),
        .Q(cnt_pos_dco_ILA[1]),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_LTC2318_16_1_0,LTC2387,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "LTC2387,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (ADC_DATA_OUT,
    adc_dax_p,
    adc_dax_n,
    adc_dbx_p,
    adc_dbx_n,
    adc_dcox_p,
    adc_dcox_n,
    fpga_clk,
    clk_100,
    adc_clkx_p,
    adc_clkx_n,
    adc_da_ILA,
    adc_dco_ILA,
    adc_dco_delayed_ILA,
    adc_clk_ILA,
    cnt100_ILA,
    cnt_pos_dco_ILA,
    cnt_neg_dco_ILA,
    clk_200);
  output [15:0]ADC_DATA_OUT;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dax p" *) input adc_dax_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dax n" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_dax, SV_INTERFACE true" *) input adc_dax_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dbx p" *) input adc_dbx_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dbx n" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_dbx, SV_INTERFACE true" *) input adc_dbx_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dcox p" *) input adc_dcox_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dcox n" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_dcox, SV_INTERFACE true" *) input adc_dcox_n;
  input fpga_clk;
  input clk_100;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_clkx p" *) output adc_clkx_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_clkx n" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_clkx, SV_INTERFACE true" *) output adc_clkx_n;
  output adc_da_ILA;
  output adc_dco_ILA;
  output adc_dco_delayed_ILA;
  output adc_clk_ILA;
  output [3:0]cnt100_ILA;
  output [1:0]cnt_pos_dco_ILA;
  output [1:0]cnt_neg_dco_ILA;
  input clk_200;

  wire \<const0> ;
  (* SLEW = "SLOW" *) wire adc_clk_ILA;
  (* SLEW = "SLOW" *) wire adc_clkx_n;
  (* SLEW = "SLOW" *) wire adc_clkx_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_da_ILA;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dax_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dax_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dbx_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dbx_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dco_ILA;
  wire adc_dco_delayed_ILA;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dcox_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dcox_p;
  wire clk_100;
  wire clk_200;
  wire [3:0]cnt100_ILA;
  wire [1:0]cnt_neg_dco_ILA;
  wire [1:0]cnt_pos_dco_ILA;
  wire fpga_clk;

  assign ADC_DATA_OUT[15] = \<const0> ;
  assign ADC_DATA_OUT[14] = \<const0> ;
  assign ADC_DATA_OUT[13] = \<const0> ;
  assign ADC_DATA_OUT[12] = \<const0> ;
  assign ADC_DATA_OUT[11] = \<const0> ;
  assign ADC_DATA_OUT[10] = \<const0> ;
  assign ADC_DATA_OUT[9] = \<const0> ;
  assign ADC_DATA_OUT[8] = \<const0> ;
  assign ADC_DATA_OUT[7] = \<const0> ;
  assign ADC_DATA_OUT[6] = \<const0> ;
  assign ADC_DATA_OUT[5] = \<const0> ;
  assign ADC_DATA_OUT[4] = \<const0> ;
  assign ADC_DATA_OUT[3] = \<const0> ;
  assign ADC_DATA_OUT[2] = \<const0> ;
  assign ADC_DATA_OUT[1] = \<const0> ;
  assign ADC_DATA_OUT[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_LTC2387 inst
       (.adc_clk_ILA(adc_clk_ILA),
        .adc_clkx_n(adc_clkx_n),
        .adc_clkx_p(adc_clkx_p),
        .adc_da_ILA(adc_da_ILA),
        .adc_dax_n(adc_dax_n),
        .adc_dax_p(adc_dax_p),
        .adc_dbx_n(adc_dbx_n),
        .adc_dbx_p(adc_dbx_p),
        .adc_dco_ILA(adc_dco_ILA),
        .adc_dco_delayed_ILA(adc_dco_delayed_ILA),
        .adc_dcox_n(adc_dcox_n),
        .adc_dcox_p(adc_dcox_p),
        .clk_100(clk_100),
        .clk_200(clk_200),
        .cnt100_ILA(cnt100_ILA),
        .cnt_neg_dco_ILA(cnt_neg_dco_ILA),
        .cnt_pos_dco_ILA(cnt_pos_dco_ILA),
        .fpga_clk(fpga_clk));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
