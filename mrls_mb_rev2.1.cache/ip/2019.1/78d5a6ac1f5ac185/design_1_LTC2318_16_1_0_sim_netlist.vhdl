-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Fri Sep 25 15:02:40 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_LTC2318_16_1_0_sim_netlist.vhdl
-- Design      : design_1_LTC2318_16_1_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_LTC2387 is
  port (
    adc_clkx_p : out STD_LOGIC;
    adc_clkx_n : out STD_LOGIC;
    adc_clk_ILA : out STD_LOGIC;
    adc_da_ILA : out STD_LOGIC;
    adc_dco_ILA : out STD_LOGIC;
    adc_dco_delayed_ILA : out STD_LOGIC;
    cnt_pos_dco_ILA : out STD_LOGIC_VECTOR ( 1 downto 0 );
    cnt_neg_dco_ILA : out STD_LOGIC_VECTOR ( 1 downto 0 );
    cnt100_ILA : out STD_LOGIC_VECTOR ( 3 downto 0 );
    adc_dax_p : in STD_LOGIC;
    adc_dax_n : in STD_LOGIC;
    adc_dbx_p : in STD_LOGIC;
    adc_dbx_n : in STD_LOGIC;
    adc_dcox_p : in STD_LOGIC;
    adc_dcox_n : in STD_LOGIC;
    clk_200 : in STD_LOGIC;
    fpga_clk : in STD_LOGIC;
    clk_100 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_LTC2387;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_LTC2387 is
  signal \^adc_clk_ila\ : STD_LOGIC;
  signal \^adc_dco_ila\ : STD_LOGIC;
  signal \^adc_dco_delayed_ila\ : STD_LOGIC;
  signal \^cnt100_ila\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \cnt_neg_dco[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_neg_dco[1]_i_1_n_0\ : STD_LOGIC;
  signal \^cnt_neg_dco_ila\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \cnt_pos_dco[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_pos_dco[1]_i_1_n_0\ : STD_LOGIC;
  signal \^cnt_pos_dco_ila\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal p_0_in : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_IBUFDS_adc_db_O_UNCONNECTED : STD_LOGIC;
  signal NLW_IDELAYCTRL_inst_RDY_UNCONNECTED : STD_LOGIC;
  signal NLW_IDELAYE2_adc_if2_ch1_dco_IDATAIN_UNCONNECTED : STD_LOGIC;
  signal NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEIN_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \/i_\ : label is "soft_lutpair0";
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of IBUFDS_adc_da : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of IBUFDS_adc_da : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_da : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of IBUFDS_adc_da : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_db : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_db : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_db : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_db : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_dco : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_dco : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_dco : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_dco : label is "AUTO";
  attribute BOX_TYPE of IDELAYCTRL_inst : label is "PRIMITIVE";
  attribute BOX_TYPE of IDELAYE2_adc_if2_ch1_dco : label is "PRIMITIVE";
  attribute SIM_DELAY_D : integer;
  attribute SIM_DELAY_D of IDELAYE2_adc_if2_ch1_dco : label is 0;
  attribute BOX_TYPE of OBUFDS_adc_clk : label is "PRIMITIVE";
  attribute CAPACITANCE of OBUFDS_adc_clk : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of OBUFDS_adc_clk : label is "OBUFDS";
  attribute SOFT_HLUTNM of \cnt100[1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \cnt100[2]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \cnt100[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \cnt_neg_dco[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \cnt_neg_dco[1]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \cnt_pos_dco[0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \cnt_pos_dco[1]_i_1\ : label is "soft_lutpair1";
begin
  adc_clk_ILA <= \^adc_clk_ila\;
  adc_dco_ILA <= \^adc_dco_ila\;
  adc_dco_delayed_ILA <= \^adc_dco_delayed_ila\;
  cnt100_ILA(3 downto 0) <= \^cnt100_ila\(3 downto 0);
  cnt_neg_dco_ILA(1 downto 0) <= \^cnt_neg_dco_ila\(1 downto 0);
  cnt_pos_dco_ILA(1 downto 0) <= \^cnt_pos_dco_ila\(1 downto 0);
\/i_\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0600"
    )
        port map (
      I0 => \^cnt100_ila\(1),
      I1 => \^cnt100_ila\(2),
      I2 => \^cnt100_ila\(3),
      I3 => clk_100,
      O => \^adc_clk_ila\
    );
IBUFDS_adc_da: unisim.vcomponents.IBUFDS
     port map (
      I => adc_dax_p,
      IB => adc_dax_n,
      O => adc_da_ILA
    );
IBUFDS_adc_db: unisim.vcomponents.IBUFDS
     port map (
      I => adc_dbx_p,
      IB => adc_dbx_n,
      O => NLW_IBUFDS_adc_db_O_UNCONNECTED
    );
IBUFDS_adc_dco: unisim.vcomponents.IBUFDS
     port map (
      I => adc_dcox_p,
      IB => adc_dcox_n,
      O => \^adc_dco_ila\
    );
IDELAYCTRL_inst: unisim.vcomponents.IDELAYCTRL
    generic map(
      SIM_DEVICE => "7SERIES"
    )
        port map (
      RDY => NLW_IDELAYCTRL_inst_RDY_UNCONNECTED,
      REFCLK => clk_200,
      RST => '0'
    );
IDELAYE2_adc_if2_ch1_dco: unisim.vcomponents.IDELAYE2
    generic map(
      CINVCTRL_SEL => "FALSE",
      DELAY_SRC => "DATAIN",
      HIGH_PERFORMANCE_MODE => "TRUE",
      IDELAY_TYPE => "FIXED",
      IDELAY_VALUE => 13,
      IS_C_INVERTED => '0',
      IS_DATAIN_INVERTED => '0',
      IS_IDATAIN_INVERTED => '0',
      PIPE_SEL => "FALSE",
      REFCLK_FREQUENCY => 200.000000,
      SIGNAL_PATTERN => "CLOCK"
    )
        port map (
      C => clk_200,
      CE => '0',
      CINVCTRL => '0',
      CNTVALUEIN(4 downto 0) => NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEIN_UNCONNECTED(4 downto 0),
      CNTVALUEOUT(4 downto 0) => NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEOUT_UNCONNECTED(4 downto 0),
      DATAIN => \^adc_dco_ila\,
      DATAOUT => \^adc_dco_delayed_ila\,
      IDATAIN => NLW_IDELAYE2_adc_if2_ch1_dco_IDATAIN_UNCONNECTED,
      INC => '0',
      LD => '0',
      LDPIPEEN => '0',
      REGRST => '0'
    );
OBUFDS_adc_clk: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => \^adc_clk_ila\,
      O => adc_clkx_p,
      OB => adc_clkx_n
    );
\cnt100[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^cnt100_ila\(0),
      O => p_0_in(0)
    );
\cnt100[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^cnt100_ila\(0),
      I1 => \^cnt100_ila\(1),
      O => p_0_in(1)
    );
\cnt100[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^cnt100_ila\(0),
      I1 => \^cnt100_ila\(1),
      I2 => \^cnt100_ila\(2),
      O => p_0_in(2)
    );
\cnt100[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \^cnt100_ila\(1),
      I1 => \^cnt100_ila\(0),
      I2 => \^cnt100_ila\(2),
      I3 => \^cnt100_ila\(3),
      O => p_0_in(3)
    );
\cnt100_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100,
      CE => '1',
      D => p_0_in(0),
      Q => \^cnt100_ila\(0),
      R => fpga_clk
    );
\cnt100_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100,
      CE => '1',
      D => p_0_in(1),
      Q => \^cnt100_ila\(1),
      R => fpga_clk
    );
\cnt100_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100,
      CE => '1',
      D => p_0_in(2),
      Q => \^cnt100_ila\(2),
      R => fpga_clk
    );
\cnt100_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100,
      CE => '1',
      D => p_0_in(3),
      Q => \^cnt100_ila\(3),
      R => fpga_clk
    );
\cnt_neg_dco[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^cnt_neg_dco_ila\(0),
      I1 => fpga_clk,
      O => \cnt_neg_dco[0]_i_1_n_0\
    );
\cnt_neg_dco[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"06"
    )
        port map (
      I0 => \^cnt_neg_dco_ila\(1),
      I1 => \^cnt_neg_dco_ila\(0),
      I2 => fpga_clk,
      O => \cnt_neg_dco[1]_i_1_n_0\
    );
\cnt_neg_dco_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \^adc_dco_delayed_ila\,
      CE => '1',
      D => \cnt_neg_dco[0]_i_1_n_0\,
      Q => \^cnt_neg_dco_ila\(0),
      R => '0'
    );
\cnt_neg_dco_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \^adc_dco_delayed_ila\,
      CE => '1',
      D => \cnt_neg_dco[1]_i_1_n_0\,
      Q => \^cnt_neg_dco_ila\(1),
      R => '0'
    );
\cnt_pos_dco[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^cnt_pos_dco_ila\(0),
      I1 => fpga_clk,
      O => \cnt_pos_dco[0]_i_1_n_0\
    );
\cnt_pos_dco[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"06"
    )
        port map (
      I0 => \^cnt_pos_dco_ila\(1),
      I1 => \^cnt_pos_dco_ila\(0),
      I2 => fpga_clk,
      O => \cnt_pos_dco[1]_i_1_n_0\
    );
\cnt_pos_dco_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^adc_dco_delayed_ila\,
      CE => '1',
      D => \cnt_pos_dco[0]_i_1_n_0\,
      Q => \^cnt_pos_dco_ila\(0),
      R => '0'
    );
\cnt_pos_dco_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^adc_dco_delayed_ila\,
      CE => '1',
      D => \cnt_pos_dco[1]_i_1_n_0\,
      Q => \^cnt_pos_dco_ila\(1),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    ADC_DATA_OUT : out STD_LOGIC_VECTOR ( 15 downto 0 );
    adc_dax_p : in STD_LOGIC;
    adc_dax_n : in STD_LOGIC;
    adc_dbx_p : in STD_LOGIC;
    adc_dbx_n : in STD_LOGIC;
    adc_dcox_p : in STD_LOGIC;
    adc_dcox_n : in STD_LOGIC;
    fpga_clk : in STD_LOGIC;
    clk_100 : in STD_LOGIC;
    adc_clkx_p : out STD_LOGIC;
    adc_clkx_n : out STD_LOGIC;
    adc_da_ILA : out STD_LOGIC;
    adc_dco_ILA : out STD_LOGIC;
    adc_dco_delayed_ILA : out STD_LOGIC;
    adc_clk_ILA : out STD_LOGIC;
    cnt100_ILA : out STD_LOGIC_VECTOR ( 3 downto 0 );
    cnt_pos_dco_ILA : out STD_LOGIC_VECTOR ( 1 downto 0 );
    cnt_neg_dco_ILA : out STD_LOGIC_VECTOR ( 1 downto 0 );
    clk_200 : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_LTC2318_16_1_0,LTC2387,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "LTC2387,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of adc_clkx_n : signal is "bt.local:interface:diff:1.0 adc_clkx n";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of adc_clkx_n : signal is "XIL_INTERFACENAME adc_clkx, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of adc_clkx_p : signal is "bt.local:interface:diff:1.0 adc_clkx p";
  attribute X_INTERFACE_INFO of adc_dax_n : signal is "bt.local:interface:diff:1.0 adc_dax n";
  attribute X_INTERFACE_PARAMETER of adc_dax_n : signal is "XIL_INTERFACENAME adc_dax, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of adc_dax_p : signal is "bt.local:interface:diff:1.0 adc_dax p";
  attribute X_INTERFACE_INFO of adc_dbx_n : signal is "bt.local:interface:diff:1.0 adc_dbx n";
  attribute X_INTERFACE_PARAMETER of adc_dbx_n : signal is "XIL_INTERFACENAME adc_dbx, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of adc_dbx_p : signal is "bt.local:interface:diff:1.0 adc_dbx p";
  attribute X_INTERFACE_INFO of adc_dcox_n : signal is "bt.local:interface:diff:1.0 adc_dcox n";
  attribute X_INTERFACE_PARAMETER of adc_dcox_n : signal is "XIL_INTERFACENAME adc_dcox, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of adc_dcox_p : signal is "bt.local:interface:diff:1.0 adc_dcox p";
begin
  ADC_DATA_OUT(15) <= \<const0>\;
  ADC_DATA_OUT(14) <= \<const0>\;
  ADC_DATA_OUT(13) <= \<const0>\;
  ADC_DATA_OUT(12) <= \<const0>\;
  ADC_DATA_OUT(11) <= \<const0>\;
  ADC_DATA_OUT(10) <= \<const0>\;
  ADC_DATA_OUT(9) <= \<const0>\;
  ADC_DATA_OUT(8) <= \<const0>\;
  ADC_DATA_OUT(7) <= \<const0>\;
  ADC_DATA_OUT(6) <= \<const0>\;
  ADC_DATA_OUT(5) <= \<const0>\;
  ADC_DATA_OUT(4) <= \<const0>\;
  ADC_DATA_OUT(3) <= \<const0>\;
  ADC_DATA_OUT(2) <= \<const0>\;
  ADC_DATA_OUT(1) <= \<const0>\;
  ADC_DATA_OUT(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_LTC2387
     port map (
      adc_clk_ILA => adc_clk_ILA,
      adc_clkx_n => adc_clkx_n,
      adc_clkx_p => adc_clkx_p,
      adc_da_ILA => adc_da_ILA,
      adc_dax_n => adc_dax_n,
      adc_dax_p => adc_dax_p,
      adc_dbx_n => adc_dbx_n,
      adc_dbx_p => adc_dbx_p,
      adc_dco_ILA => adc_dco_ILA,
      adc_dco_delayed_ILA => adc_dco_delayed_ILA,
      adc_dcox_n => adc_dcox_n,
      adc_dcox_p => adc_dcox_p,
      clk_100 => clk_100,
      clk_200 => clk_200,
      cnt100_ILA(3 downto 0) => cnt100_ILA(3 downto 0),
      cnt_neg_dco_ILA(1 downto 0) => cnt_neg_dco_ILA(1 downto 0),
      cnt_pos_dco_ILA(1 downto 0) => cnt_pos_dco_ILA(1 downto 0),
      fpga_clk => fpga_clk
    );
end STRUCTURE;
