-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Sep 29 20:32:10 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_mrls_top_0_1_sim_netlist.vhdl
-- Design      : design_1_mrls_top_0_1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_get_data is
  port (
    adc_data_if1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    adc_if1_ch1_clk : out STD_LOGIC;
    fpga_clk_buf_if1 : in STD_LOGIC;
    adc_clk_100MHz_if1 : in STD_LOGIC;
    adc_if1_ch1_dco : in STD_LOGIC;
    ch_num : in STD_LOGIC_VECTOR ( 1 downto 0 );
    adc_if2_ch2_dco : in STD_LOGIC;
    adc_if1_ch1_da : in STD_LOGIC;
    adc_if2_ch2_da : in STD_LOGIC;
    adc_if1_ch1_db : in STD_LOGIC;
    adc_if2_ch2_db : in STD_LOGIC;
    data_5_reg_0 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_get_data;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_get_data is
  signal \adc_data_reg[15]_i_1__0_n_0\ : STD_LOGIC;
  signal \adc_data_reg[15]_i_2_n_0\ : STD_LOGIC;
  signal \adc_reading_allowed_reg_i_1__0_n_0\ : STD_LOGIC;
  signal \adc_reading_allowed_reg_i_2__0_n_0\ : STD_LOGIC;
  signal adc_reading_allowed_reg_reg_n_0 : STD_LOGIC;
  signal \data_0_i_1__0_n_0\ : STD_LOGIC;
  signal data_0_reg_n_0 : STD_LOGIC;
  signal \data_10_i_1__0_n_0\ : STD_LOGIC;
  signal data_10_reg_n_0 : STD_LOGIC;
  signal \data_11_i_1__0_n_0\ : STD_LOGIC;
  signal data_11_reg_n_0 : STD_LOGIC;
  signal \data_12_i_1__0_n_0\ : STD_LOGIC;
  signal data_12_reg_n_0 : STD_LOGIC;
  signal \data_13_i_1__0_n_0\ : STD_LOGIC;
  signal data_13_reg_n_0 : STD_LOGIC;
  signal \data_14_i_1__0_n_0\ : STD_LOGIC;
  signal \data_14_i_2__0_n_0\ : STD_LOGIC;
  signal data_14_reg_n_0 : STD_LOGIC;
  signal \data_15_i_1__0_n_0\ : STD_LOGIC;
  signal data_15_reg_n_0 : STD_LOGIC;
  signal data_1_i_1_n_0 : STD_LOGIC;
  signal data_1_reg_n_0 : STD_LOGIC;
  signal \data_2_i_1__0_n_0\ : STD_LOGIC;
  signal data_2_i_2_n_0 : STD_LOGIC;
  signal data_2_reg_n_0 : STD_LOGIC;
  signal \data_3_i_1__0_n_0\ : STD_LOGIC;
  signal data_3_i_2_n_0 : STD_LOGIC;
  signal data_3_reg_n_0 : STD_LOGIC;
  signal \data_4_i_1__0_n_0\ : STD_LOGIC;
  signal data_4_reg_n_0 : STD_LOGIC;
  signal \data_5_i_1__0_n_0\ : STD_LOGIC;
  signal data_5_reg_n_0 : STD_LOGIC;
  signal \data_6_i_1__0_n_0\ : STD_LOGIC;
  signal data_6_reg_n_0 : STD_LOGIC;
  signal \data_7_i_1__0_n_0\ : STD_LOGIC;
  signal data_7_reg_n_0 : STD_LOGIC;
  signal \data_8_i_1__0_n_0\ : STD_LOGIC;
  signal data_8_reg_n_0 : STD_LOGIC;
  signal \data_9_i_1__0_n_0\ : STD_LOGIC;
  signal data_9_reg_n_0 : STD_LOGIC;
  signal \dco_neg_num[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \dco_neg_num[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \dco_neg_num_reg_n_0_[0]\ : STD_LOGIC;
  signal \dco_neg_num_reg_n_0_[1]\ : STD_LOGIC;
  signal \dco_pos_num[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \dco_pos_num[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \dco_pos_num_reg_n_0_[0]\ : STD_LOGIC;
  signal \dco_pos_num_reg_n_0_[1]\ : STD_LOGIC;
  signal \even_odd_frame_i_1__0_n_0\ : STD_LOGIC;
  signal even_odd_frame_prev_neg_reg_n_0 : STD_LOGIC;
  signal even_odd_frame_prev_pos_reg_n_0 : STD_LOGIC;
  signal even_odd_frame_reg_n_0 : STD_LOGIC;
  signal fpga_clk_prev : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 7 downto 1 );
  signal tconv_cnt_reg : STD_LOGIC;
  signal \tconv_cnt_reg[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \tconv_cnt_reg[7]_i_3__0_n_0\ : STD_LOGIC;
  signal tconv_cnt_reg_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \adc_reading_allowed_reg_i_2__0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \data_14_i_2__0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \data_2_i_1__0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \data_3_i_1__0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \data_6_i_1__0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \data_7_i_1__0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \dco_neg_num[0]_i_1__0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \dco_neg_num[1]_i_1__0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \dco_pos_num[0]_i_1__0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \dco_pos_num[1]_i_1__0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[0]_i_1__0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[1]_i_1__0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[2]_i_1__0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[3]_i_1__0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[4]_i_1__0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[7]_i_2__0\ : label is "soft_lutpair5";
begin
OBUFDS_adc_if1_ch1_clk_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => adc_clk_100MHz_if1,
      I1 => adc_reading_allowed_reg_reg_n_0,
      O => adc_if1_ch1_clk
    );
\adc_data_reg[15]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \dco_neg_num_reg_n_0_[1]\,
      I1 => \dco_neg_num_reg_n_0_[0]\,
      O => \adc_data_reg[15]_i_1__0_n_0\
    );
\adc_data_reg[15]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => adc_if1_ch1_dco,
      I1 => ch_num(0),
      I2 => ch_num(1),
      I3 => adc_if2_ch2_dco,
      O => \adc_data_reg[15]_i_2_n_0\
    );
\adc_data_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => \adc_data_reg[15]_i_1__0_n_0\,
      D => data_0_reg_n_0,
      Q => adc_data_if1(0),
      R => '0'
    );
\adc_data_reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => \adc_data_reg[15]_i_1__0_n_0\,
      D => data_10_reg_n_0,
      Q => adc_data_if1(10),
      R => '0'
    );
\adc_data_reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => \adc_data_reg[15]_i_1__0_n_0\,
      D => data_11_reg_n_0,
      Q => adc_data_if1(11),
      R => '0'
    );
\adc_data_reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => \adc_data_reg[15]_i_1__0_n_0\,
      D => data_12_reg_n_0,
      Q => adc_data_if1(12),
      R => '0'
    );
\adc_data_reg_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => \adc_data_reg[15]_i_1__0_n_0\,
      D => data_13_reg_n_0,
      Q => adc_data_if1(13),
      R => '0'
    );
\adc_data_reg_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => \adc_data_reg[15]_i_1__0_n_0\,
      D => data_14_reg_n_0,
      Q => adc_data_if1(14),
      R => '0'
    );
\adc_data_reg_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => \adc_data_reg[15]_i_1__0_n_0\,
      D => data_15_reg_n_0,
      Q => adc_data_if1(15),
      R => '0'
    );
\adc_data_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => \adc_data_reg[15]_i_1__0_n_0\,
      D => data_1_reg_n_0,
      Q => adc_data_if1(1),
      R => '0'
    );
\adc_data_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => \adc_data_reg[15]_i_1__0_n_0\,
      D => data_2_reg_n_0,
      Q => adc_data_if1(2),
      R => '0'
    );
\adc_data_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => \adc_data_reg[15]_i_1__0_n_0\,
      D => data_3_reg_n_0,
      Q => adc_data_if1(3),
      R => '0'
    );
\adc_data_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => \adc_data_reg[15]_i_1__0_n_0\,
      D => data_4_reg_n_0,
      Q => adc_data_if1(4),
      R => '0'
    );
\adc_data_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => \adc_data_reg[15]_i_1__0_n_0\,
      D => data_5_reg_n_0,
      Q => adc_data_if1(5),
      R => '0'
    );
\adc_data_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => \adc_data_reg[15]_i_1__0_n_0\,
      D => data_6_reg_n_0,
      Q => adc_data_if1(6),
      R => '0'
    );
\adc_data_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => \adc_data_reg[15]_i_1__0_n_0\,
      D => data_7_reg_n_0,
      Q => adc_data_if1(7),
      R => '0'
    );
\adc_data_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => \adc_data_reg[15]_i_1__0_n_0\,
      D => data_8_reg_n_0,
      Q => adc_data_if1(8),
      R => '0'
    );
\adc_data_reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => \adc_data_reg[15]_i_1__0_n_0\,
      D => data_9_reg_n_0,
      Q => adc_data_if1(9),
      R => '0'
    );
\adc_reading_allowed_reg_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAEAAA8AA"
    )
        port map (
      I0 => adc_reading_allowed_reg_reg_n_0,
      I1 => tconv_cnt_reg_reg(2),
      I2 => tconv_cnt_reg_reg(3),
      I3 => tconv_cnt_reg_reg(0),
      I4 => tconv_cnt_reg_reg(1),
      I5 => \adc_reading_allowed_reg_i_2__0_n_0\,
      O => \adc_reading_allowed_reg_i_1__0_n_0\
    );
\adc_reading_allowed_reg_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => tconv_cnt_reg_reg(7),
      I1 => tconv_cnt_reg_reg(6),
      I2 => tconv_cnt_reg_reg(4),
      I3 => tconv_cnt_reg_reg(5),
      O => \adc_reading_allowed_reg_i_2__0_n_0\
    );
adc_reading_allowed_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if1,
      CE => '1',
      D => \adc_reading_allowed_reg_i_1__0_n_0\,
      Q => adc_reading_allowed_reg_reg_n_0,
      R => '0'
    );
\data_0_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8FFFFFFB8000000"
    )
        port map (
      I0 => adc_if1_ch1_db,
      I1 => data_5_reg_0,
      I2 => adc_if2_ch2_db,
      I3 => \dco_pos_num_reg_n_0_[0]\,
      I4 => \dco_pos_num_reg_n_0_[1]\,
      I5 => data_0_reg_n_0,
      O => \data_0_i_1__0_n_0\
    );
data_0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => \data_0_i_1__0_n_0\,
      Q => data_0_reg_n_0,
      R => '0'
    );
\data_10_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEFF00020200"
    )
        port map (
      I0 => data_2_i_2_n_0,
      I1 => \dco_neg_num_reg_n_0_[0]\,
      I2 => \dco_neg_num_reg_n_0_[1]\,
      I3 => even_odd_frame_prev_neg_reg_n_0,
      I4 => even_odd_frame_reg_n_0,
      I5 => data_10_reg_n_0,
      O => \data_10_i_1__0_n_0\
    );
data_10_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => \data_10_i_1__0_n_0\,
      Q => data_10_reg_n_0,
      R => '0'
    );
\data_11_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEFF00020200"
    )
        port map (
      I0 => data_3_i_2_n_0,
      I1 => \dco_neg_num_reg_n_0_[0]\,
      I2 => \dco_neg_num_reg_n_0_[1]\,
      I3 => even_odd_frame_prev_neg_reg_n_0,
      I4 => even_odd_frame_reg_n_0,
      I5 => data_11_reg_n_0,
      O => \data_11_i_1__0_n_0\
    );
data_11_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => \data_11_i_1__0_n_0\,
      Q => data_11_reg_n_0,
      R => '0'
    );
\data_12_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEFF00020200"
    )
        port map (
      I0 => data_2_i_2_n_0,
      I1 => \dco_pos_num_reg_n_0_[0]\,
      I2 => \dco_pos_num_reg_n_0_[1]\,
      I3 => even_odd_frame_prev_pos_reg_n_0,
      I4 => even_odd_frame_reg_n_0,
      I5 => data_12_reg_n_0,
      O => \data_12_i_1__0_n_0\
    );
data_12_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => \data_12_i_1__0_n_0\,
      Q => data_12_reg_n_0,
      R => '0'
    );
\data_13_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEFF00020200"
    )
        port map (
      I0 => data_3_i_2_n_0,
      I1 => \dco_pos_num_reg_n_0_[0]\,
      I2 => \dco_pos_num_reg_n_0_[1]\,
      I3 => even_odd_frame_prev_pos_reg_n_0,
      I4 => even_odd_frame_reg_n_0,
      I5 => data_13_reg_n_0,
      O => \data_13_i_1__0_n_0\
    );
data_13_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => \data_13_i_1__0_n_0\,
      Q => data_13_reg_n_0,
      R => '0'
    );
\data_14_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => data_2_i_2_n_0,
      I1 => \adc_reading_allowed_reg_i_2__0_n_0\,
      I2 => \data_14_i_2__0_n_0\,
      I3 => tconv_cnt_reg_reg(3),
      I4 => tconv_cnt_reg_reg(2),
      I5 => data_14_reg_n_0,
      O => \data_14_i_1__0_n_0\
    );
\data_14_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => tconv_cnt_reg_reg(1),
      I1 => tconv_cnt_reg_reg(0),
      O => \data_14_i_2__0_n_0\
    );
data_14_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if1,
      CE => '1',
      D => \data_14_i_1__0_n_0\,
      Q => data_14_reg_n_0,
      R => '0'
    );
\data_15_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => data_3_i_2_n_0,
      I1 => \adc_reading_allowed_reg_i_2__0_n_0\,
      I2 => \data_14_i_2__0_n_0\,
      I3 => tconv_cnt_reg_reg(3),
      I4 => tconv_cnt_reg_reg(2),
      I5 => data_15_reg_n_0,
      O => \data_15_i_1__0_n_0\
    );
data_15_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if1,
      CE => '1',
      D => \data_15_i_1__0_n_0\,
      Q => data_15_reg_n_0,
      R => '0'
    );
data_1_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8FFFFFFB8000000"
    )
        port map (
      I0 => adc_if1_ch1_da,
      I1 => data_5_reg_0,
      I2 => adc_if2_ch2_da,
      I3 => \dco_pos_num_reg_n_0_[0]\,
      I4 => \dco_pos_num_reg_n_0_[1]\,
      I5 => data_1_reg_n_0,
      O => data_1_i_1_n_0
    );
data_1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => data_1_i_1_n_0,
      Q => data_1_reg_n_0,
      R => '0'
    );
\data_2_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => data_2_i_2_n_0,
      I1 => \dco_neg_num_reg_n_0_[1]\,
      I2 => \dco_neg_num_reg_n_0_[0]\,
      I3 => data_2_reg_n_0,
      O => \data_2_i_1__0_n_0\
    );
data_2_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => adc_if1_ch1_db,
      I1 => ch_num(0),
      I2 => ch_num(1),
      I3 => adc_if2_ch2_db,
      O => data_2_i_2_n_0
    );
data_2_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => \data_2_i_1__0_n_0\,
      Q => data_2_reg_n_0,
      R => '0'
    );
\data_3_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => data_3_i_2_n_0,
      I1 => \dco_neg_num_reg_n_0_[1]\,
      I2 => \dco_neg_num_reg_n_0_[0]\,
      I3 => data_3_reg_n_0,
      O => \data_3_i_1__0_n_0\
    );
data_3_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => adc_if1_ch1_da,
      I1 => ch_num(0),
      I2 => ch_num(1),
      I3 => adc_if2_ch2_da,
      O => data_3_i_2_n_0
    );
data_3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => \data_3_i_1__0_n_0\,
      Q => data_3_reg_n_0,
      R => '0'
    );
\data_4_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB8FF0000B800"
    )
        port map (
      I0 => adc_if1_ch1_db,
      I1 => data_5_reg_0,
      I2 => adc_if2_ch2_db,
      I3 => \dco_pos_num_reg_n_0_[1]\,
      I4 => \dco_pos_num_reg_n_0_[0]\,
      I5 => data_4_reg_n_0,
      O => \data_4_i_1__0_n_0\
    );
data_4_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => \data_4_i_1__0_n_0\,
      Q => data_4_reg_n_0,
      R => '0'
    );
\data_5_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB8FF0000B800"
    )
        port map (
      I0 => adc_if1_ch1_da,
      I1 => data_5_reg_0,
      I2 => adc_if2_ch2_da,
      I3 => \dco_pos_num_reg_n_0_[1]\,
      I4 => \dco_pos_num_reg_n_0_[0]\,
      I5 => data_5_reg_n_0,
      O => \data_5_i_1__0_n_0\
    );
data_5_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => \data_5_i_1__0_n_0\,
      Q => data_5_reg_n_0,
      R => '0'
    );
\data_6_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => data_2_i_2_n_0,
      I1 => \dco_neg_num_reg_n_0_[1]\,
      I2 => \dco_neg_num_reg_n_0_[0]\,
      I3 => data_6_reg_n_0,
      O => \data_6_i_1__0_n_0\
    );
data_6_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => \data_6_i_1__0_n_0\,
      Q => data_6_reg_n_0,
      R => '0'
    );
\data_7_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => data_3_i_2_n_0,
      I1 => \dco_neg_num_reg_n_0_[1]\,
      I2 => \dco_neg_num_reg_n_0_[0]\,
      I3 => data_7_reg_n_0,
      O => \data_7_i_1__0_n_0\
    );
data_7_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => \data_7_i_1__0_n_0\,
      Q => data_7_reg_n_0,
      R => '0'
    );
\data_8_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB8FF0000B800"
    )
        port map (
      I0 => adc_if1_ch1_db,
      I1 => data_5_reg_0,
      I2 => adc_if2_ch2_db,
      I3 => \dco_pos_num_reg_n_0_[0]\,
      I4 => \dco_pos_num_reg_n_0_[1]\,
      I5 => data_8_reg_n_0,
      O => \data_8_i_1__0_n_0\
    );
data_8_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => \data_8_i_1__0_n_0\,
      Q => data_8_reg_n_0,
      R => '0'
    );
\data_9_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB8FF0000B800"
    )
        port map (
      I0 => adc_if1_ch1_da,
      I1 => data_5_reg_0,
      I2 => adc_if2_ch2_da,
      I3 => \dco_pos_num_reg_n_0_[0]\,
      I4 => \dco_pos_num_reg_n_0_[1]\,
      I5 => data_9_reg_n_0,
      O => \data_9_i_1__0_n_0\
    );
data_9_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => \data_9_i_1__0_n_0\,
      Q => data_9_reg_n_0,
      R => '0'
    );
\dco_neg_num[0]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4FF4"
    )
        port map (
      I0 => \dco_neg_num_reg_n_0_[0]\,
      I1 => \dco_neg_num_reg_n_0_[1]\,
      I2 => even_odd_frame_prev_neg_reg_n_0,
      I3 => even_odd_frame_reg_n_0,
      O => \dco_neg_num[0]_i_1__0_n_0\
    );
\dco_neg_num[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0990"
    )
        port map (
      I0 => even_odd_frame_reg_n_0,
      I1 => even_odd_frame_prev_neg_reg_n_0,
      I2 => \dco_neg_num_reg_n_0_[0]\,
      I3 => \dco_neg_num_reg_n_0_[1]\,
      O => \dco_neg_num[1]_i_1__0_n_0\
    );
\dco_neg_num_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => \dco_neg_num[0]_i_1__0_n_0\,
      Q => \dco_neg_num_reg_n_0_[0]\,
      R => '0'
    );
\dco_neg_num_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => \dco_neg_num[1]_i_1__0_n_0\,
      Q => \dco_neg_num_reg_n_0_[1]\,
      R => '0'
    );
\dco_pos_num[0]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4FF4"
    )
        port map (
      I0 => \dco_pos_num_reg_n_0_[0]\,
      I1 => \dco_pos_num_reg_n_0_[1]\,
      I2 => even_odd_frame_prev_pos_reg_n_0,
      I3 => even_odd_frame_reg_n_0,
      O => \dco_pos_num[0]_i_1__0_n_0\
    );
\dco_pos_num[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0990"
    )
        port map (
      I0 => even_odd_frame_reg_n_0,
      I1 => even_odd_frame_prev_pos_reg_n_0,
      I2 => \dco_pos_num_reg_n_0_[0]\,
      I3 => \dco_pos_num_reg_n_0_[1]\,
      O => \dco_pos_num[1]_i_1__0_n_0\
    );
\dco_pos_num_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => \dco_pos_num[0]_i_1__0_n_0\,
      Q => \dco_pos_num_reg_n_0_[0]\,
      R => '0'
    );
\dco_pos_num_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => \dco_pos_num[1]_i_1__0_n_0\,
      Q => \dco_pos_num_reg_n_0_[1]\,
      R => '0'
    );
\even_odd_frame_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000010"
    )
        port map (
      I0 => tconv_cnt_reg_reg(0),
      I1 => tconv_cnt_reg_reg(3),
      I2 => tconv_cnt_reg_reg(2),
      I3 => tconv_cnt_reg_reg(1),
      I4 => \adc_reading_allowed_reg_i_2__0_n_0\,
      I5 => even_odd_frame_reg_n_0,
      O => \even_odd_frame_i_1__0_n_0\
    );
even_odd_frame_prev_neg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => even_odd_frame_reg_n_0,
      Q => even_odd_frame_prev_neg_reg_n_0,
      R => '0'
    );
even_odd_frame_prev_pos_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2_n_0\,
      CE => '1',
      D => even_odd_frame_reg_n_0,
      Q => even_odd_frame_prev_pos_reg_n_0,
      R => '0'
    );
even_odd_frame_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if1,
      CE => '1',
      D => \even_odd_frame_i_1__0_n_0\,
      Q => even_odd_frame_reg_n_0,
      R => '0'
    );
fpga_clk_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if1,
      CE => '1',
      D => fpga_clk_buf_if1,
      Q => fpga_clk_prev,
      R => '0'
    );
\tconv_cnt_reg[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => tconv_cnt_reg_reg(0),
      O => \tconv_cnt_reg[0]_i_1__0_n_0\
    );
\tconv_cnt_reg[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => tconv_cnt_reg_reg(0),
      I1 => tconv_cnt_reg_reg(1),
      O => p_0_in(1)
    );
\tconv_cnt_reg[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => tconv_cnt_reg_reg(1),
      I1 => tconv_cnt_reg_reg(0),
      I2 => tconv_cnt_reg_reg(2),
      O => p_0_in(2)
    );
\tconv_cnt_reg[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => tconv_cnt_reg_reg(0),
      I1 => tconv_cnt_reg_reg(1),
      I2 => tconv_cnt_reg_reg(2),
      I3 => tconv_cnt_reg_reg(3),
      O => p_0_in(3)
    );
\tconv_cnt_reg[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => tconv_cnt_reg_reg(2),
      I1 => tconv_cnt_reg_reg(1),
      I2 => tconv_cnt_reg_reg(0),
      I3 => tconv_cnt_reg_reg(3),
      I4 => tconv_cnt_reg_reg(4),
      O => p_0_in(4)
    );
\tconv_cnt_reg[5]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => tconv_cnt_reg_reg(3),
      I1 => tconv_cnt_reg_reg(0),
      I2 => tconv_cnt_reg_reg(1),
      I3 => tconv_cnt_reg_reg(2),
      I4 => tconv_cnt_reg_reg(4),
      I5 => tconv_cnt_reg_reg(5),
      O => p_0_in(5)
    );
\tconv_cnt_reg[6]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tconv_cnt_reg[7]_i_3__0_n_0\,
      I1 => tconv_cnt_reg_reg(6),
      O => p_0_in(6)
    );
\tconv_cnt_reg[7]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => fpga_clk_buf_if1,
      I1 => fpga_clk_prev,
      O => tconv_cnt_reg
    );
\tconv_cnt_reg[7]_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \tconv_cnt_reg[7]_i_3__0_n_0\,
      I1 => tconv_cnt_reg_reg(6),
      I2 => tconv_cnt_reg_reg(7),
      O => p_0_in(7)
    );
\tconv_cnt_reg[7]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => tconv_cnt_reg_reg(5),
      I1 => tconv_cnt_reg_reg(3),
      I2 => tconv_cnt_reg_reg(0),
      I3 => tconv_cnt_reg_reg(1),
      I4 => tconv_cnt_reg_reg(2),
      I5 => tconv_cnt_reg_reg(4),
      O => \tconv_cnt_reg[7]_i_3__0_n_0\
    );
\tconv_cnt_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if1,
      CE => '1',
      D => \tconv_cnt_reg[0]_i_1__0_n_0\,
      Q => tconv_cnt_reg_reg(0),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if1,
      CE => '1',
      D => p_0_in(1),
      Q => tconv_cnt_reg_reg(1),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if1,
      CE => '1',
      D => p_0_in(2),
      Q => tconv_cnt_reg_reg(2),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if1,
      CE => '1',
      D => p_0_in(3),
      Q => tconv_cnt_reg_reg(3),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if1,
      CE => '1',
      D => p_0_in(4),
      Q => tconv_cnt_reg_reg(4),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if1,
      CE => '1',
      D => p_0_in(5),
      Q => tconv_cnt_reg_reg(5),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if1,
      CE => '1',
      D => p_0_in(6),
      Q => tconv_cnt_reg_reg(6),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if1,
      CE => '1',
      D => p_0_in(7),
      Q => tconv_cnt_reg_reg(7),
      R => tconv_cnt_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_get_data_0 is
  port (
    ch_num_0_sp_1 : out STD_LOGIC;
    adc_data_if2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    adc_if2_ch1_clk : out STD_LOGIC;
    fpga_clk_buf_if2 : in STD_LOGIC;
    adc_clk_100MHz_if2 : in STD_LOGIC;
    adc_if2_ch1_dco : in STD_LOGIC;
    ch_num : in STD_LOGIC_VECTOR ( 1 downto 0 );
    adc_if1_ch2_dco : in STD_LOGIC;
    adc_if2_ch1_da : in STD_LOGIC;
    adc_if1_ch2_da : in STD_LOGIC;
    adc_if2_ch1_db : in STD_LOGIC;
    adc_if1_ch2_db : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_get_data_0 : entity is "adc_get_data";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_get_data_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_get_data_0 is
  signal \adc_data_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \adc_data_reg[15]_i_2__0_n_0\ : STD_LOGIC;
  signal adc_data_temp : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal adc_reading_allowed_reg_i_1_n_0 : STD_LOGIC;
  signal adc_reading_allowed_reg_i_2_n_0 : STD_LOGIC;
  signal adc_reading_allowed_reg_reg_n_0 : STD_LOGIC;
  signal ch_num_0_sn_1 : STD_LOGIC;
  signal data_0_i_1_n_0 : STD_LOGIC;
  signal data_10_i_1_n_0 : STD_LOGIC;
  signal data_11_i_1_n_0 : STD_LOGIC;
  signal data_12_i_1_n_0 : STD_LOGIC;
  signal data_13_i_1_n_0 : STD_LOGIC;
  signal data_14_i_1_n_0 : STD_LOGIC;
  signal data_14_i_2_n_0 : STD_LOGIC;
  signal data_15_i_1_n_0 : STD_LOGIC;
  signal \data_1_i_1__0_n_0\ : STD_LOGIC;
  signal data_2_i_1_n_0 : STD_LOGIC;
  signal \data_2_i_2__0_n_0\ : STD_LOGIC;
  signal data_3_i_1_n_0 : STD_LOGIC;
  signal \data_3_i_2__0_n_0\ : STD_LOGIC;
  signal data_4_i_1_n_0 : STD_LOGIC;
  signal data_5_i_1_n_0 : STD_LOGIC;
  signal data_6_i_1_n_0 : STD_LOGIC;
  signal data_7_i_1_n_0 : STD_LOGIC;
  signal data_8_i_1_n_0 : STD_LOGIC;
  signal data_9_i_1_n_0 : STD_LOGIC;
  signal \dco_neg_num[0]_i_1_n_0\ : STD_LOGIC;
  signal \dco_neg_num[1]_i_1_n_0\ : STD_LOGIC;
  signal \dco_neg_num_reg_n_0_[0]\ : STD_LOGIC;
  signal \dco_neg_num_reg_n_0_[1]\ : STD_LOGIC;
  signal \dco_pos_num[0]_i_1_n_0\ : STD_LOGIC;
  signal \dco_pos_num[1]_i_1_n_0\ : STD_LOGIC;
  signal \dco_pos_num_reg_n_0_[0]\ : STD_LOGIC;
  signal \dco_pos_num_reg_n_0_[1]\ : STD_LOGIC;
  signal even_odd_frame : STD_LOGIC;
  signal even_odd_frame_i_1_n_0 : STD_LOGIC;
  signal even_odd_frame_prev_neg : STD_LOGIC;
  signal even_odd_frame_prev_pos : STD_LOGIC;
  signal fpga_clk_prev : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 7 downto 1 );
  signal tconv_cnt_reg : STD_LOGIC;
  signal \tconv_cnt_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \tconv_cnt_reg[7]_i_3_n_0\ : STD_LOGIC;
  signal tconv_cnt_reg_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of adc_reading_allowed_reg_i_2 : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of data_0_i_2 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of data_14_i_2 : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of data_2_i_1 : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \data_2_i_2__0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of data_3_i_1 : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of data_6_i_1 : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of data_7_i_1 : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \dco_neg_num[0]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \dco_neg_num[1]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \dco_pos_num[0]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \dco_pos_num[1]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[0]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[1]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[2]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[3]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[4]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[7]_i_2\ : label is "soft_lutpair14";
begin
  ch_num_0_sp_1 <= ch_num_0_sn_1;
OBUFDS_adc_if2_ch1_clk_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => adc_clk_100MHz_if2,
      I1 => adc_reading_allowed_reg_reg_n_0,
      O => adc_if2_ch1_clk
    );
\adc_data_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \dco_neg_num_reg_n_0_[1]\,
      I1 => \dco_neg_num_reg_n_0_[0]\,
      O => \adc_data_reg[15]_i_1_n_0\
    );
\adc_data_reg[15]_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => adc_if2_ch1_dco,
      I1 => ch_num(0),
      I2 => ch_num(1),
      I3 => adc_if1_ch2_dco,
      O => \adc_data_reg[15]_i_2__0_n_0\
    );
\adc_data_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(0),
      Q => adc_data_if2(0),
      R => '0'
    );
\adc_data_reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(10),
      Q => adc_data_if2(10),
      R => '0'
    );
\adc_data_reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(11),
      Q => adc_data_if2(11),
      R => '0'
    );
\adc_data_reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(12),
      Q => adc_data_if2(12),
      R => '0'
    );
\adc_data_reg_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(13),
      Q => adc_data_if2(13),
      R => '0'
    );
\adc_data_reg_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(14),
      Q => adc_data_if2(14),
      R => '0'
    );
\adc_data_reg_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(15),
      Q => adc_data_if2(15),
      R => '0'
    );
\adc_data_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(1),
      Q => adc_data_if2(1),
      R => '0'
    );
\adc_data_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(2),
      Q => adc_data_if2(2),
      R => '0'
    );
\adc_data_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(3),
      Q => adc_data_if2(3),
      R => '0'
    );
\adc_data_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(4),
      Q => adc_data_if2(4),
      R => '0'
    );
\adc_data_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(5),
      Q => adc_data_if2(5),
      R => '0'
    );
\adc_data_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(6),
      Q => adc_data_if2(6),
      R => '0'
    );
\adc_data_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(7),
      Q => adc_data_if2(7),
      R => '0'
    );
\adc_data_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(8),
      Q => adc_data_if2(8),
      R => '0'
    );
\adc_data_reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(9),
      Q => adc_data_if2(9),
      R => '0'
    );
adc_reading_allowed_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAEAAA8AA"
    )
        port map (
      I0 => adc_reading_allowed_reg_reg_n_0,
      I1 => tconv_cnt_reg_reg(2),
      I2 => tconv_cnt_reg_reg(3),
      I3 => tconv_cnt_reg_reg(0),
      I4 => tconv_cnt_reg_reg(1),
      I5 => adc_reading_allowed_reg_i_2_n_0,
      O => adc_reading_allowed_reg_i_1_n_0
    );
adc_reading_allowed_reg_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => tconv_cnt_reg_reg(7),
      I1 => tconv_cnt_reg_reg(6),
      I2 => tconv_cnt_reg_reg(4),
      I3 => tconv_cnt_reg_reg(5),
      O => adc_reading_allowed_reg_i_2_n_0
    );
adc_reading_allowed_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if2,
      CE => '1',
      D => adc_reading_allowed_reg_i_1_n_0,
      Q => adc_reading_allowed_reg_reg_n_0,
      R => '0'
    );
data_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8FFFFFFB8000000"
    )
        port map (
      I0 => adc_if2_ch1_db,
      I1 => ch_num_0_sn_1,
      I2 => adc_if1_ch2_db,
      I3 => \dco_pos_num_reg_n_0_[0]\,
      I4 => \dco_pos_num_reg_n_0_[1]\,
      I5 => adc_data_temp(0),
      O => data_0_i_1_n_0
    );
data_0_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ch_num(0),
      I1 => ch_num(1),
      O => ch_num_0_sn_1
    );
data_0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => data_0_i_1_n_0,
      Q => adc_data_temp(0),
      R => '0'
    );
data_10_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEFF00020200"
    )
        port map (
      I0 => \data_2_i_2__0_n_0\,
      I1 => \dco_neg_num_reg_n_0_[0]\,
      I2 => \dco_neg_num_reg_n_0_[1]\,
      I3 => even_odd_frame_prev_neg,
      I4 => even_odd_frame,
      I5 => adc_data_temp(10),
      O => data_10_i_1_n_0
    );
data_10_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => data_10_i_1_n_0,
      Q => adc_data_temp(10),
      R => '0'
    );
data_11_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEFF00020200"
    )
        port map (
      I0 => \data_3_i_2__0_n_0\,
      I1 => \dco_neg_num_reg_n_0_[0]\,
      I2 => \dco_neg_num_reg_n_0_[1]\,
      I3 => even_odd_frame_prev_neg,
      I4 => even_odd_frame,
      I5 => adc_data_temp(11),
      O => data_11_i_1_n_0
    );
data_11_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => data_11_i_1_n_0,
      Q => adc_data_temp(11),
      R => '0'
    );
data_12_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEFF00020200"
    )
        port map (
      I0 => \data_2_i_2__0_n_0\,
      I1 => \dco_pos_num_reg_n_0_[0]\,
      I2 => \dco_pos_num_reg_n_0_[1]\,
      I3 => even_odd_frame_prev_pos,
      I4 => even_odd_frame,
      I5 => adc_data_temp(12),
      O => data_12_i_1_n_0
    );
data_12_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => data_12_i_1_n_0,
      Q => adc_data_temp(12),
      R => '0'
    );
data_13_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEFF00020200"
    )
        port map (
      I0 => \data_3_i_2__0_n_0\,
      I1 => \dco_pos_num_reg_n_0_[0]\,
      I2 => \dco_pos_num_reg_n_0_[1]\,
      I3 => even_odd_frame_prev_pos,
      I4 => even_odd_frame,
      I5 => adc_data_temp(13),
      O => data_13_i_1_n_0
    );
data_13_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => data_13_i_1_n_0,
      Q => adc_data_temp(13),
      R => '0'
    );
data_14_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => \data_2_i_2__0_n_0\,
      I1 => adc_reading_allowed_reg_i_2_n_0,
      I2 => data_14_i_2_n_0,
      I3 => tconv_cnt_reg_reg(3),
      I4 => tconv_cnt_reg_reg(2),
      I5 => adc_data_temp(14),
      O => data_14_i_1_n_0
    );
data_14_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => tconv_cnt_reg_reg(1),
      I1 => tconv_cnt_reg_reg(0),
      O => data_14_i_2_n_0
    );
data_14_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if2,
      CE => '1',
      D => data_14_i_1_n_0,
      Q => adc_data_temp(14),
      R => '0'
    );
data_15_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => \data_3_i_2__0_n_0\,
      I1 => adc_reading_allowed_reg_i_2_n_0,
      I2 => data_14_i_2_n_0,
      I3 => tconv_cnt_reg_reg(3),
      I4 => tconv_cnt_reg_reg(2),
      I5 => adc_data_temp(15),
      O => data_15_i_1_n_0
    );
data_15_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if2,
      CE => '1',
      D => data_15_i_1_n_0,
      Q => adc_data_temp(15),
      R => '0'
    );
\data_1_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8FFFFFFB8000000"
    )
        port map (
      I0 => adc_if2_ch1_da,
      I1 => ch_num_0_sn_1,
      I2 => adc_if1_ch2_da,
      I3 => \dco_pos_num_reg_n_0_[0]\,
      I4 => \dco_pos_num_reg_n_0_[1]\,
      I5 => adc_data_temp(1),
      O => \data_1_i_1__0_n_0\
    );
data_1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => \data_1_i_1__0_n_0\,
      Q => adc_data_temp(1),
      R => '0'
    );
data_2_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \data_2_i_2__0_n_0\,
      I1 => \dco_neg_num_reg_n_0_[1]\,
      I2 => \dco_neg_num_reg_n_0_[0]\,
      I3 => adc_data_temp(2),
      O => data_2_i_1_n_0
    );
\data_2_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => adc_if2_ch1_db,
      I1 => ch_num(0),
      I2 => ch_num(1),
      I3 => adc_if1_ch2_db,
      O => \data_2_i_2__0_n_0\
    );
data_2_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => data_2_i_1_n_0,
      Q => adc_data_temp(2),
      R => '0'
    );
data_3_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => \data_3_i_2__0_n_0\,
      I1 => \dco_neg_num_reg_n_0_[1]\,
      I2 => \dco_neg_num_reg_n_0_[0]\,
      I3 => adc_data_temp(3),
      O => data_3_i_1_n_0
    );
\data_3_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => adc_if2_ch1_da,
      I1 => ch_num(0),
      I2 => ch_num(1),
      I3 => adc_if1_ch2_da,
      O => \data_3_i_2__0_n_0\
    );
data_3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => data_3_i_1_n_0,
      Q => adc_data_temp(3),
      R => '0'
    );
data_4_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB8FF0000B800"
    )
        port map (
      I0 => adc_if2_ch1_db,
      I1 => ch_num_0_sn_1,
      I2 => adc_if1_ch2_db,
      I3 => \dco_pos_num_reg_n_0_[1]\,
      I4 => \dco_pos_num_reg_n_0_[0]\,
      I5 => adc_data_temp(4),
      O => data_4_i_1_n_0
    );
data_4_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => data_4_i_1_n_0,
      Q => adc_data_temp(4),
      R => '0'
    );
data_5_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB8FF0000B800"
    )
        port map (
      I0 => adc_if2_ch1_da,
      I1 => ch_num_0_sn_1,
      I2 => adc_if1_ch2_da,
      I3 => \dco_pos_num_reg_n_0_[1]\,
      I4 => \dco_pos_num_reg_n_0_[0]\,
      I5 => adc_data_temp(5),
      O => data_5_i_1_n_0
    );
data_5_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => data_5_i_1_n_0,
      Q => adc_data_temp(5),
      R => '0'
    );
data_6_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => \data_2_i_2__0_n_0\,
      I1 => \dco_neg_num_reg_n_0_[1]\,
      I2 => \dco_neg_num_reg_n_0_[0]\,
      I3 => adc_data_temp(6),
      O => data_6_i_1_n_0
    );
data_6_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => data_6_i_1_n_0,
      Q => adc_data_temp(6),
      R => '0'
    );
data_7_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => \data_3_i_2__0_n_0\,
      I1 => \dco_neg_num_reg_n_0_[1]\,
      I2 => \dco_neg_num_reg_n_0_[0]\,
      I3 => adc_data_temp(7),
      O => data_7_i_1_n_0
    );
data_7_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => data_7_i_1_n_0,
      Q => adc_data_temp(7),
      R => '0'
    );
data_8_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB8FF0000B800"
    )
        port map (
      I0 => adc_if2_ch1_db,
      I1 => ch_num_0_sn_1,
      I2 => adc_if1_ch2_db,
      I3 => \dco_pos_num_reg_n_0_[0]\,
      I4 => \dco_pos_num_reg_n_0_[1]\,
      I5 => adc_data_temp(8),
      O => data_8_i_1_n_0
    );
data_8_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => data_8_i_1_n_0,
      Q => adc_data_temp(8),
      R => '0'
    );
data_9_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFB8FF0000B800"
    )
        port map (
      I0 => adc_if2_ch1_da,
      I1 => ch_num_0_sn_1,
      I2 => adc_if1_ch2_da,
      I3 => \dco_pos_num_reg_n_0_[0]\,
      I4 => \dco_pos_num_reg_n_0_[1]\,
      I5 => adc_data_temp(9),
      O => data_9_i_1_n_0
    );
data_9_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => data_9_i_1_n_0,
      Q => adc_data_temp(9),
      R => '0'
    );
\dco_neg_num[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4FF4"
    )
        port map (
      I0 => \dco_neg_num_reg_n_0_[0]\,
      I1 => \dco_neg_num_reg_n_0_[1]\,
      I2 => even_odd_frame_prev_neg,
      I3 => even_odd_frame,
      O => \dco_neg_num[0]_i_1_n_0\
    );
\dco_neg_num[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0990"
    )
        port map (
      I0 => even_odd_frame,
      I1 => even_odd_frame_prev_neg,
      I2 => \dco_neg_num_reg_n_0_[0]\,
      I3 => \dco_neg_num_reg_n_0_[1]\,
      O => \dco_neg_num[1]_i_1_n_0\
    );
\dco_neg_num_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => \dco_neg_num[0]_i_1_n_0\,
      Q => \dco_neg_num_reg_n_0_[0]\,
      R => '0'
    );
\dco_neg_num_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => \dco_neg_num[1]_i_1_n_0\,
      Q => \dco_neg_num_reg_n_0_[1]\,
      R => '0'
    );
\dco_pos_num[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4FF4"
    )
        port map (
      I0 => \dco_pos_num_reg_n_0_[0]\,
      I1 => \dco_pos_num_reg_n_0_[1]\,
      I2 => even_odd_frame_prev_pos,
      I3 => even_odd_frame,
      O => \dco_pos_num[0]_i_1_n_0\
    );
\dco_pos_num[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0990"
    )
        port map (
      I0 => even_odd_frame,
      I1 => even_odd_frame_prev_pos,
      I2 => \dco_pos_num_reg_n_0_[0]\,
      I3 => \dco_pos_num_reg_n_0_[1]\,
      O => \dco_pos_num[1]_i_1_n_0\
    );
\dco_pos_num_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => \dco_pos_num[0]_i_1_n_0\,
      Q => \dco_pos_num_reg_n_0_[0]\,
      R => '0'
    );
\dco_pos_num_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => \dco_pos_num[1]_i_1_n_0\,
      Q => \dco_pos_num_reg_n_0_[1]\,
      R => '0'
    );
even_odd_frame_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000010"
    )
        port map (
      I0 => tconv_cnt_reg_reg(0),
      I1 => tconv_cnt_reg_reg(3),
      I2 => tconv_cnt_reg_reg(2),
      I3 => tconv_cnt_reg_reg(1),
      I4 => adc_reading_allowed_reg_i_2_n_0,
      I5 => even_odd_frame,
      O => even_odd_frame_i_1_n_0
    );
even_odd_frame_prev_neg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => even_odd_frame,
      Q => even_odd_frame_prev_neg,
      R => '0'
    );
even_odd_frame_prev_pos_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \adc_data_reg[15]_i_2__0_n_0\,
      CE => '1',
      D => even_odd_frame,
      Q => even_odd_frame_prev_pos,
      R => '0'
    );
even_odd_frame_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if2,
      CE => '1',
      D => even_odd_frame_i_1_n_0,
      Q => even_odd_frame,
      R => '0'
    );
fpga_clk_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if2,
      CE => '1',
      D => fpga_clk_buf_if2,
      Q => fpga_clk_prev,
      R => '0'
    );
\tconv_cnt_reg[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => tconv_cnt_reg_reg(0),
      O => \tconv_cnt_reg[0]_i_1_n_0\
    );
\tconv_cnt_reg[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => tconv_cnt_reg_reg(0),
      I1 => tconv_cnt_reg_reg(1),
      O => p_0_in(1)
    );
\tconv_cnt_reg[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => tconv_cnt_reg_reg(1),
      I1 => tconv_cnt_reg_reg(0),
      I2 => tconv_cnt_reg_reg(2),
      O => p_0_in(2)
    );
\tconv_cnt_reg[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => tconv_cnt_reg_reg(0),
      I1 => tconv_cnt_reg_reg(1),
      I2 => tconv_cnt_reg_reg(2),
      I3 => tconv_cnt_reg_reg(3),
      O => p_0_in(3)
    );
\tconv_cnt_reg[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => tconv_cnt_reg_reg(2),
      I1 => tconv_cnt_reg_reg(1),
      I2 => tconv_cnt_reg_reg(0),
      I3 => tconv_cnt_reg_reg(3),
      I4 => tconv_cnt_reg_reg(4),
      O => p_0_in(4)
    );
\tconv_cnt_reg[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => tconv_cnt_reg_reg(3),
      I1 => tconv_cnt_reg_reg(0),
      I2 => tconv_cnt_reg_reg(1),
      I3 => tconv_cnt_reg_reg(2),
      I4 => tconv_cnt_reg_reg(4),
      I5 => tconv_cnt_reg_reg(5),
      O => p_0_in(5)
    );
\tconv_cnt_reg[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tconv_cnt_reg[7]_i_3_n_0\,
      I1 => tconv_cnt_reg_reg(6),
      O => p_0_in(6)
    );
\tconv_cnt_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => fpga_clk_buf_if2,
      I1 => fpga_clk_prev,
      O => tconv_cnt_reg
    );
\tconv_cnt_reg[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \tconv_cnt_reg[7]_i_3_n_0\,
      I1 => tconv_cnt_reg_reg(6),
      I2 => tconv_cnt_reg_reg(7),
      O => p_0_in(7)
    );
\tconv_cnt_reg[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => tconv_cnt_reg_reg(5),
      I1 => tconv_cnt_reg_reg(3),
      I2 => tconv_cnt_reg_reg(0),
      I3 => tconv_cnt_reg_reg(1),
      I4 => tconv_cnt_reg_reg(2),
      I5 => tconv_cnt_reg_reg(4),
      O => \tconv_cnt_reg[7]_i_3_n_0\
    );
\tconv_cnt_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if2,
      CE => '1',
      D => \tconv_cnt_reg[0]_i_1_n_0\,
      Q => tconv_cnt_reg_reg(0),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if2,
      CE => '1',
      D => p_0_in(1),
      Q => tconv_cnt_reg_reg(1),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if2,
      CE => '1',
      D => p_0_in(2),
      Q => tconv_cnt_reg_reg(2),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if2,
      CE => '1',
      D => p_0_in(3),
      Q => tconv_cnt_reg_reg(3),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if2,
      CE => '1',
      D => p_0_in(4),
      Q => tconv_cnt_reg_reg(4),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if2,
      CE => '1',
      D => p_0_in(5),
      Q => tconv_cnt_reg_reg(5),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if2,
      CE => '1',
      D => p_0_in(6),
      Q => tconv_cnt_reg_reg(6),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_clk_100MHz_if2,
      CE => '1',
      D => p_0_in(7),
      Q => tconv_cnt_reg_reg(7),
      R => tconv_cnt_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mrls_top is
  port (
    ch_num : in STD_LOGIC_VECTOR ( 1 downto 0 );
    clk_10MHz_inverted : in STD_LOGIC;
    fpga_clk_buf_if1 : in STD_LOGIC;
    fpga_clk_buf_if2 : in STD_LOGIC;
    adc_clk_100MHz_if1 : in STD_LOGIC;
    adc_clk_100MHz_if2 : in STD_LOGIC;
    clk_100MHz : in STD_LOGIC;
    fpga_clk : in STD_LOGIC;
    adc_if2_ch1_clk_p : out STD_LOGIC;
    adc_if2_ch1_clk_n : out STD_LOGIC;
    adc_if2_ch1_dco_p : in STD_LOGIC;
    adc_if2_ch1_dco_n : in STD_LOGIC;
    adc_if2_ch1_da_p : in STD_LOGIC;
    adc_if2_ch1_da_n : in STD_LOGIC;
    adc_if2_ch1_db_p : in STD_LOGIC;
    adc_if2_ch1_db_n : in STD_LOGIC;
    adc_if1_ch1_clk_p : out STD_LOGIC;
    adc_if1_ch1_clk_n : out STD_LOGIC;
    adc_if1_ch1_dco_p : in STD_LOGIC;
    adc_if1_ch1_dco_n : in STD_LOGIC;
    adc_if1_ch1_da_p : in STD_LOGIC;
    adc_if1_ch1_da_n : in STD_LOGIC;
    adc_if1_ch1_db_p : in STD_LOGIC;
    adc_if1_ch1_db_n : in STD_LOGIC;
    adc_if1_ch2_dco_p : in STD_LOGIC;
    adc_if1_ch2_dco_n : in STD_LOGIC;
    adc_if1_ch2_da_p : in STD_LOGIC;
    adc_if1_ch2_da_n : in STD_LOGIC;
    adc_if1_ch2_db_p : in STD_LOGIC;
    adc_if1_ch2_db_n : in STD_LOGIC;
    adc_if2_ch2_dco_p : in STD_LOGIC;
    adc_if2_ch2_dco_n : in STD_LOGIC;
    adc_if2_ch2_da_p : in STD_LOGIC;
    adc_if2_ch2_da_n : in STD_LOGIC;
    adc_if2_ch2_db_p : in STD_LOGIC;
    adc_if2_ch2_db_n : in STD_LOGIC;
    adc_data_if1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    adc_data_if2 : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mrls_top;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mrls_top is
  signal adc_if1_ch1_clk : STD_LOGIC;
  signal adc_if1_ch1_da : STD_LOGIC;
  signal adc_if1_ch1_db : STD_LOGIC;
  signal adc_if1_ch1_dco : STD_LOGIC;
  signal adc_if1_ch2_da : STD_LOGIC;
  signal adc_if1_ch2_db : STD_LOGIC;
  signal adc_if1_ch2_dco : STD_LOGIC;
  signal adc_if2_ch1_clk : STD_LOGIC;
  signal adc_if2_ch1_da : STD_LOGIC;
  signal adc_if2_ch1_db : STD_LOGIC;
  signal adc_if2_ch1_dco : STD_LOGIC;
  signal adc_if2_ch2_da : STD_LOGIC;
  signal adc_if2_ch2_db : STD_LOGIC;
  signal adc_if2_ch2_dco : STD_LOGIC;
  signal adc_if2_get_data_n_0 : STD_LOGIC;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of IBUFDS_adc_da_if1_ch1 : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of IBUFDS_adc_da_if1_ch1 : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_da_if1_ch1 : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of IBUFDS_adc_da_if1_ch1 : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_da_if1_ch2 : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_da_if1_ch2 : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_da_if1_ch2 : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_da_if1_ch2 : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_da_if2_ch1 : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_da_if2_ch1 : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_da_if2_ch1 : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_da_if2_ch1 : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_da_if2_ch2 : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_da_if2_ch2 : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_da_if2_ch2 : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_da_if2_ch2 : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_if1_ch1_db : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_if1_ch1_db : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_if1_ch1_db : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_if1_ch1_db : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_if1_ch1_dco : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_if1_ch1_dco : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_if1_ch1_dco : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_if1_ch1_dco : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_if1_ch2_db : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_if1_ch2_db : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_if1_ch2_db : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_if1_ch2_db : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_if1_ch2_dco : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_if1_ch2_dco : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_if1_ch2_dco : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_if1_ch2_dco : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_if2_ch1_db : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_if2_ch1_db : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_if2_ch1_db : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_if2_ch1_db : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_if2_ch1_dco : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_if2_ch1_dco : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_if2_ch1_dco : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_if2_ch1_dco : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_if2_ch2_db : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_if2_ch2_db : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_if2_ch2_db : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_if2_ch2_db : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_if2_ch2_dco : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_if2_ch2_dco : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_if2_ch2_dco : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_if2_ch2_dco : label is "AUTO";
  attribute BOX_TYPE of OBUFDS_adc_if1_ch1_clk : label is "PRIMITIVE";
  attribute CAPACITANCE of OBUFDS_adc_if1_ch1_clk : label is "DONT_CARE";
  attribute BOX_TYPE of OBUFDS_adc_if2_ch1_clk : label is "PRIMITIVE";
  attribute CAPACITANCE of OBUFDS_adc_if2_ch1_clk : label is "DONT_CARE";
begin
IBUFDS_adc_da_if1_ch1: unisim.vcomponents.IBUFDS
     port map (
      I => adc_if1_ch1_da_p,
      IB => adc_if1_ch1_da_n,
      O => adc_if1_ch1_da
    );
IBUFDS_adc_da_if1_ch2: unisim.vcomponents.IBUFDS
     port map (
      I => adc_if1_ch2_da_p,
      IB => adc_if1_ch2_da_n,
      O => adc_if1_ch2_da
    );
IBUFDS_adc_da_if2_ch1: unisim.vcomponents.IBUFDS
     port map (
      I => adc_if2_ch1_da_p,
      IB => adc_if2_ch1_da_n,
      O => adc_if2_ch1_da
    );
IBUFDS_adc_da_if2_ch2: unisim.vcomponents.IBUFDS
     port map (
      I => adc_if2_ch2_da_p,
      IB => adc_if2_ch2_da_n,
      O => adc_if2_ch2_da
    );
IBUFDS_adc_if1_ch1_db: unisim.vcomponents.IBUFDS
     port map (
      I => adc_if1_ch1_db_p,
      IB => adc_if1_ch1_db_n,
      O => adc_if1_ch1_db
    );
IBUFDS_adc_if1_ch1_dco: unisim.vcomponents.IBUFDS
     port map (
      I => adc_if1_ch1_dco_p,
      IB => adc_if1_ch1_dco_n,
      O => adc_if1_ch1_dco
    );
IBUFDS_adc_if1_ch2_db: unisim.vcomponents.IBUFDS
     port map (
      I => adc_if1_ch2_db_p,
      IB => adc_if1_ch2_db_n,
      O => adc_if1_ch2_db
    );
IBUFDS_adc_if1_ch2_dco: unisim.vcomponents.IBUFDS
     port map (
      I => adc_if1_ch2_dco_p,
      IB => adc_if1_ch2_dco_n,
      O => adc_if1_ch2_dco
    );
IBUFDS_adc_if2_ch1_db: unisim.vcomponents.IBUFDS
     port map (
      I => adc_if2_ch1_db_p,
      IB => adc_if2_ch1_db_n,
      O => adc_if2_ch1_db
    );
IBUFDS_adc_if2_ch1_dco: unisim.vcomponents.IBUFDS
     port map (
      I => adc_if2_ch1_dco_p,
      IB => adc_if2_ch1_dco_n,
      O => adc_if2_ch1_dco
    );
IBUFDS_adc_if2_ch2_db: unisim.vcomponents.IBUFDS
     port map (
      I => adc_if2_ch2_db_p,
      IB => adc_if2_ch2_db_n,
      O => adc_if2_ch2_db
    );
IBUFDS_adc_if2_ch2_dco: unisim.vcomponents.IBUFDS
     port map (
      I => adc_if2_ch2_dco_p,
      IB => adc_if2_ch2_dco_n,
      O => adc_if2_ch2_dco
    );
OBUFDS_adc_if1_ch1_clk: unisim.vcomponents.OBUFDS
     port map (
      I => adc_if1_ch1_clk,
      O => adc_if1_ch1_clk_p,
      OB => adc_if1_ch1_clk_n
    );
OBUFDS_adc_if2_ch1_clk: unisim.vcomponents.OBUFDS
     port map (
      I => adc_if2_ch1_clk,
      O => adc_if2_ch1_clk_p,
      OB => adc_if2_ch1_clk_n
    );
adc_if1_get_data: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_get_data
     port map (
      adc_clk_100MHz_if1 => adc_clk_100MHz_if1,
      adc_data_if1(15 downto 0) => adc_data_if1(15 downto 0),
      adc_if1_ch1_clk => adc_if1_ch1_clk,
      adc_if1_ch1_da => adc_if1_ch1_da,
      adc_if1_ch1_db => adc_if1_ch1_db,
      adc_if1_ch1_dco => adc_if1_ch1_dco,
      adc_if2_ch2_da => adc_if2_ch2_da,
      adc_if2_ch2_db => adc_if2_ch2_db,
      adc_if2_ch2_dco => adc_if2_ch2_dco,
      ch_num(1 downto 0) => ch_num(1 downto 0),
      data_5_reg_0 => adc_if2_get_data_n_0,
      fpga_clk_buf_if1 => fpga_clk_buf_if1
    );
adc_if2_get_data: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_get_data_0
     port map (
      adc_clk_100MHz_if2 => adc_clk_100MHz_if2,
      adc_data_if2(15 downto 0) => adc_data_if2(15 downto 0),
      adc_if1_ch2_da => adc_if1_ch2_da,
      adc_if1_ch2_db => adc_if1_ch2_db,
      adc_if1_ch2_dco => adc_if1_ch2_dco,
      adc_if2_ch1_clk => adc_if2_ch1_clk,
      adc_if2_ch1_da => adc_if2_ch1_da,
      adc_if2_ch1_db => adc_if2_ch1_db,
      adc_if2_ch1_dco => adc_if2_ch1_dco,
      ch_num(1 downto 0) => ch_num(1 downto 0),
      ch_num_0_sp_1 => adc_if2_get_data_n_0,
      fpga_clk_buf_if2 => fpga_clk_buf_if2
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    ch_num : in STD_LOGIC_VECTOR ( 1 downto 0 );
    clk_10MHz_inverted : in STD_LOGIC;
    fpga_clk_buf_if1 : in STD_LOGIC;
    fpga_clk_buf_if2 : in STD_LOGIC;
    adc_clk_100MHz_if1 : in STD_LOGIC;
    adc_clk_100MHz_if2 : in STD_LOGIC;
    clk_100MHz : in STD_LOGIC;
    fpga_clk : in STD_LOGIC;
    adc_if2_ch1_clk_p : out STD_LOGIC;
    adc_if2_ch1_clk_n : out STD_LOGIC;
    adc_if2_ch1_dco_p : in STD_LOGIC;
    adc_if2_ch1_dco_n : in STD_LOGIC;
    adc_if2_ch1_da_p : in STD_LOGIC;
    adc_if2_ch1_da_n : in STD_LOGIC;
    adc_if2_ch1_db_p : in STD_LOGIC;
    adc_if2_ch1_db_n : in STD_LOGIC;
    adc_if2_ch2_dco_p : in STD_LOGIC;
    adc_if2_ch2_dco_n : in STD_LOGIC;
    adc_if2_ch2_da_p : in STD_LOGIC;
    adc_if2_ch2_da_n : in STD_LOGIC;
    adc_if2_ch2_db_p : in STD_LOGIC;
    adc_if2_ch2_db_n : in STD_LOGIC;
    adc_data_if1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    adc_data_if2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    adc_if1_ch1_clk_p : out STD_LOGIC;
    adc_if1_ch1_clk_n : out STD_LOGIC;
    adc_if1_ch1_dco_p : in STD_LOGIC;
    adc_if1_ch1_dco_n : in STD_LOGIC;
    adc_if1_ch1_da_p : in STD_LOGIC;
    adc_if1_ch1_da_n : in STD_LOGIC;
    adc_if1_ch1_db_p : in STD_LOGIC;
    adc_if1_ch1_db_n : in STD_LOGIC;
    adc_if1_ch2_dco_p : in STD_LOGIC;
    adc_if1_ch2_dco_n : in STD_LOGIC;
    adc_if1_ch2_da_p : in STD_LOGIC;
    adc_if1_ch2_da_n : in STD_LOGIC;
    adc_if1_ch2_db_p : in STD_LOGIC;
    adc_if1_ch2_db_n : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_mrls_top_0_1,mrls_top,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "package_project";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "mrls_top,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mrls_top
     port map (
      adc_clk_100MHz_if1 => adc_clk_100MHz_if1,
      adc_clk_100MHz_if2 => adc_clk_100MHz_if2,
      adc_data_if1(15 downto 0) => adc_data_if1(15 downto 0),
      adc_data_if2(15 downto 0) => adc_data_if2(15 downto 0),
      adc_if1_ch1_clk_n => adc_if1_ch1_clk_n,
      adc_if1_ch1_clk_p => adc_if1_ch1_clk_p,
      adc_if1_ch1_da_n => adc_if1_ch1_da_n,
      adc_if1_ch1_da_p => adc_if1_ch1_da_p,
      adc_if1_ch1_db_n => adc_if1_ch1_db_n,
      adc_if1_ch1_db_p => adc_if1_ch1_db_p,
      adc_if1_ch1_dco_n => adc_if1_ch1_dco_n,
      adc_if1_ch1_dco_p => adc_if1_ch1_dco_p,
      adc_if1_ch2_da_n => adc_if1_ch2_da_n,
      adc_if1_ch2_da_p => adc_if1_ch2_da_p,
      adc_if1_ch2_db_n => adc_if1_ch2_db_n,
      adc_if1_ch2_db_p => adc_if1_ch2_db_p,
      adc_if1_ch2_dco_n => adc_if1_ch2_dco_n,
      adc_if1_ch2_dco_p => adc_if1_ch2_dco_p,
      adc_if2_ch1_clk_n => adc_if2_ch1_clk_n,
      adc_if2_ch1_clk_p => adc_if2_ch1_clk_p,
      adc_if2_ch1_da_n => adc_if2_ch1_da_n,
      adc_if2_ch1_da_p => adc_if2_ch1_da_p,
      adc_if2_ch1_db_n => adc_if2_ch1_db_n,
      adc_if2_ch1_db_p => adc_if2_ch1_db_p,
      adc_if2_ch1_dco_n => adc_if2_ch1_dco_n,
      adc_if2_ch1_dco_p => adc_if2_ch1_dco_p,
      adc_if2_ch2_da_n => adc_if2_ch2_da_n,
      adc_if2_ch2_da_p => adc_if2_ch2_da_p,
      adc_if2_ch2_db_n => adc_if2_ch2_db_n,
      adc_if2_ch2_db_p => adc_if2_ch2_db_p,
      adc_if2_ch2_dco_n => adc_if2_ch2_dco_n,
      adc_if2_ch2_dco_p => adc_if2_ch2_dco_p,
      ch_num(1 downto 0) => ch_num(1 downto 0),
      clk_100MHz => clk_100MHz,
      clk_10MHz_inverted => clk_10MHz_inverted,
      fpga_clk => fpga_clk,
      fpga_clk_buf_if1 => fpga_clk_buf_if1,
      fpga_clk_buf_if2 => fpga_clk_buf_if2
    );
end STRUCTURE;
