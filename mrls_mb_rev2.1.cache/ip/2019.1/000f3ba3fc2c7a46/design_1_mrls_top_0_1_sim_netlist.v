// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Sep 29 20:32:10 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_mrls_top_0_1_sim_netlist.v
// Design      : design_1_mrls_top_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_get_data
   (adc_data_if1,
    adc_if1_ch1_clk,
    fpga_clk_buf_if1,
    adc_clk_100MHz_if1,
    adc_if1_ch1_dco,
    ch_num,
    adc_if2_ch2_dco,
    adc_if1_ch1_da,
    adc_if2_ch2_da,
    adc_if1_ch1_db,
    adc_if2_ch2_db,
    data_5_reg_0);
  output [15:0]adc_data_if1;
  output adc_if1_ch1_clk;
  input fpga_clk_buf_if1;
  input adc_clk_100MHz_if1;
  input adc_if1_ch1_dco;
  input [1:0]ch_num;
  input adc_if2_ch2_dco;
  input adc_if1_ch1_da;
  input adc_if2_ch2_da;
  input adc_if1_ch1_db;
  input adc_if2_ch2_db;
  input data_5_reg_0;

  wire adc_clk_100MHz_if1;
  wire [15:0]adc_data_if1;
  wire \adc_data_reg[15]_i_1__0_n_0 ;
  wire \adc_data_reg[15]_i_2_n_0 ;
  wire adc_if1_ch1_clk;
  wire adc_if1_ch1_da;
  wire adc_if1_ch1_db;
  wire adc_if1_ch1_dco;
  wire adc_if2_ch2_da;
  wire adc_if2_ch2_db;
  wire adc_if2_ch2_dco;
  wire adc_reading_allowed_reg_i_1__0_n_0;
  wire adc_reading_allowed_reg_i_2__0_n_0;
  wire adc_reading_allowed_reg_reg_n_0;
  wire [1:0]ch_num;
  wire data_0_i_1__0_n_0;
  wire data_0_reg_n_0;
  wire data_10_i_1__0_n_0;
  wire data_10_reg_n_0;
  wire data_11_i_1__0_n_0;
  wire data_11_reg_n_0;
  wire data_12_i_1__0_n_0;
  wire data_12_reg_n_0;
  wire data_13_i_1__0_n_0;
  wire data_13_reg_n_0;
  wire data_14_i_1__0_n_0;
  wire data_14_i_2__0_n_0;
  wire data_14_reg_n_0;
  wire data_15_i_1__0_n_0;
  wire data_15_reg_n_0;
  wire data_1_i_1_n_0;
  wire data_1_reg_n_0;
  wire data_2_i_1__0_n_0;
  wire data_2_i_2_n_0;
  wire data_2_reg_n_0;
  wire data_3_i_1__0_n_0;
  wire data_3_i_2_n_0;
  wire data_3_reg_n_0;
  wire data_4_i_1__0_n_0;
  wire data_4_reg_n_0;
  wire data_5_i_1__0_n_0;
  wire data_5_reg_0;
  wire data_5_reg_n_0;
  wire data_6_i_1__0_n_0;
  wire data_6_reg_n_0;
  wire data_7_i_1__0_n_0;
  wire data_7_reg_n_0;
  wire data_8_i_1__0_n_0;
  wire data_8_reg_n_0;
  wire data_9_i_1__0_n_0;
  wire data_9_reg_n_0;
  wire \dco_neg_num[0]_i_1__0_n_0 ;
  wire \dco_neg_num[1]_i_1__0_n_0 ;
  wire \dco_neg_num_reg_n_0_[0] ;
  wire \dco_neg_num_reg_n_0_[1] ;
  wire \dco_pos_num[0]_i_1__0_n_0 ;
  wire \dco_pos_num[1]_i_1__0_n_0 ;
  wire \dco_pos_num_reg_n_0_[0] ;
  wire \dco_pos_num_reg_n_0_[1] ;
  wire even_odd_frame_i_1__0_n_0;
  wire even_odd_frame_prev_neg_reg_n_0;
  wire even_odd_frame_prev_pos_reg_n_0;
  wire even_odd_frame_reg_n_0;
  wire fpga_clk_buf_if1;
  wire fpga_clk_prev;
  wire [7:1]p_0_in;
  wire tconv_cnt_reg;
  wire \tconv_cnt_reg[0]_i_1__0_n_0 ;
  wire \tconv_cnt_reg[7]_i_3__0_n_0 ;
  wire [7:0]tconv_cnt_reg_reg;

  LUT2 #(
    .INIT(4'h8)) 
    OBUFDS_adc_if1_ch1_clk_i_1
       (.I0(adc_clk_100MHz_if1),
        .I1(adc_reading_allowed_reg_reg_n_0),
        .O(adc_if1_ch1_clk));
  LUT2 #(
    .INIT(4'h8)) 
    \adc_data_reg[15]_i_1__0 
       (.I0(\dco_neg_num_reg_n_0_[1] ),
        .I1(\dco_neg_num_reg_n_0_[0] ),
        .O(\adc_data_reg[15]_i_1__0_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \adc_data_reg[15]_i_2 
       (.I0(adc_if1_ch1_dco),
        .I1(ch_num[0]),
        .I2(ch_num[1]),
        .I3(adc_if2_ch2_dco),
        .O(\adc_data_reg[15]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[0] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(\adc_data_reg[15]_i_1__0_n_0 ),
        .D(data_0_reg_n_0),
        .Q(adc_data_if1[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[10] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(\adc_data_reg[15]_i_1__0_n_0 ),
        .D(data_10_reg_n_0),
        .Q(adc_data_if1[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[11] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(\adc_data_reg[15]_i_1__0_n_0 ),
        .D(data_11_reg_n_0),
        .Q(adc_data_if1[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[12] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(\adc_data_reg[15]_i_1__0_n_0 ),
        .D(data_12_reg_n_0),
        .Q(adc_data_if1[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[13] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(\adc_data_reg[15]_i_1__0_n_0 ),
        .D(data_13_reg_n_0),
        .Q(adc_data_if1[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[14] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(\adc_data_reg[15]_i_1__0_n_0 ),
        .D(data_14_reg_n_0),
        .Q(adc_data_if1[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[15] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(\adc_data_reg[15]_i_1__0_n_0 ),
        .D(data_15_reg_n_0),
        .Q(adc_data_if1[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[1] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(\adc_data_reg[15]_i_1__0_n_0 ),
        .D(data_1_reg_n_0),
        .Q(adc_data_if1[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[2] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(\adc_data_reg[15]_i_1__0_n_0 ),
        .D(data_2_reg_n_0),
        .Q(adc_data_if1[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[3] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(\adc_data_reg[15]_i_1__0_n_0 ),
        .D(data_3_reg_n_0),
        .Q(adc_data_if1[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[4] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(\adc_data_reg[15]_i_1__0_n_0 ),
        .D(data_4_reg_n_0),
        .Q(adc_data_if1[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[5] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(\adc_data_reg[15]_i_1__0_n_0 ),
        .D(data_5_reg_n_0),
        .Q(adc_data_if1[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[6] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(\adc_data_reg[15]_i_1__0_n_0 ),
        .D(data_6_reg_n_0),
        .Q(adc_data_if1[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[7] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(\adc_data_reg[15]_i_1__0_n_0 ),
        .D(data_7_reg_n_0),
        .Q(adc_data_if1[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[8] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(\adc_data_reg[15]_i_1__0_n_0 ),
        .D(data_8_reg_n_0),
        .Q(adc_data_if1[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[9] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(\adc_data_reg[15]_i_1__0_n_0 ),
        .D(data_9_reg_n_0),
        .Q(adc_data_if1[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAAAAAAAEAAA8AA)) 
    adc_reading_allowed_reg_i_1__0
       (.I0(adc_reading_allowed_reg_reg_n_0),
        .I1(tconv_cnt_reg_reg[2]),
        .I2(tconv_cnt_reg_reg[3]),
        .I3(tconv_cnt_reg_reg[0]),
        .I4(tconv_cnt_reg_reg[1]),
        .I5(adc_reading_allowed_reg_i_2__0_n_0),
        .O(adc_reading_allowed_reg_i_1__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    adc_reading_allowed_reg_i_2__0
       (.I0(tconv_cnt_reg_reg[7]),
        .I1(tconv_cnt_reg_reg[6]),
        .I2(tconv_cnt_reg_reg[4]),
        .I3(tconv_cnt_reg_reg[5]),
        .O(adc_reading_allowed_reg_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    adc_reading_allowed_reg_reg
       (.C(adc_clk_100MHz_if1),
        .CE(1'b1),
        .D(adc_reading_allowed_reg_i_1__0_n_0),
        .Q(adc_reading_allowed_reg_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hB8FFFFFFB8000000)) 
    data_0_i_1__0
       (.I0(adc_if1_ch1_db),
        .I1(data_5_reg_0),
        .I2(adc_if2_ch2_db),
        .I3(\dco_pos_num_reg_n_0_[0] ),
        .I4(\dco_pos_num_reg_n_0_[1] ),
        .I5(data_0_reg_n_0),
        .O(data_0_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_0_reg
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(data_0_i_1__0_n_0),
        .Q(data_0_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFEFEFF00020200)) 
    data_10_i_1__0
       (.I0(data_2_i_2_n_0),
        .I1(\dco_neg_num_reg_n_0_[0] ),
        .I2(\dco_neg_num_reg_n_0_[1] ),
        .I3(even_odd_frame_prev_neg_reg_n_0),
        .I4(even_odd_frame_reg_n_0),
        .I5(data_10_reg_n_0),
        .O(data_10_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_10_reg
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(data_10_i_1__0_n_0),
        .Q(data_10_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFEFEFF00020200)) 
    data_11_i_1__0
       (.I0(data_3_i_2_n_0),
        .I1(\dco_neg_num_reg_n_0_[0] ),
        .I2(\dco_neg_num_reg_n_0_[1] ),
        .I3(even_odd_frame_prev_neg_reg_n_0),
        .I4(even_odd_frame_reg_n_0),
        .I5(data_11_reg_n_0),
        .O(data_11_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_11_reg
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(data_11_i_1__0_n_0),
        .Q(data_11_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFEFEFF00020200)) 
    data_12_i_1__0
       (.I0(data_2_i_2_n_0),
        .I1(\dco_pos_num_reg_n_0_[0] ),
        .I2(\dco_pos_num_reg_n_0_[1] ),
        .I3(even_odd_frame_prev_pos_reg_n_0),
        .I4(even_odd_frame_reg_n_0),
        .I5(data_12_reg_n_0),
        .O(data_12_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_12_reg
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(data_12_i_1__0_n_0),
        .Q(data_12_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFEFEFF00020200)) 
    data_13_i_1__0
       (.I0(data_3_i_2_n_0),
        .I1(\dco_pos_num_reg_n_0_[0] ),
        .I2(\dco_pos_num_reg_n_0_[1] ),
        .I3(even_odd_frame_prev_pos_reg_n_0),
        .I4(even_odd_frame_reg_n_0),
        .I5(data_13_reg_n_0),
        .O(data_13_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_13_reg
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(data_13_i_1__0_n_0),
        .Q(data_13_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFEFFFF00020000)) 
    data_14_i_1__0
       (.I0(data_2_i_2_n_0),
        .I1(adc_reading_allowed_reg_i_2__0_n_0),
        .I2(data_14_i_2__0_n_0),
        .I3(tconv_cnt_reg_reg[3]),
        .I4(tconv_cnt_reg_reg[2]),
        .I5(data_14_reg_n_0),
        .O(data_14_i_1__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h7)) 
    data_14_i_2__0
       (.I0(tconv_cnt_reg_reg[1]),
        .I1(tconv_cnt_reg_reg[0]),
        .O(data_14_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_14_reg
       (.C(adc_clk_100MHz_if1),
        .CE(1'b1),
        .D(data_14_i_1__0_n_0),
        .Q(data_14_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFEFFFF00020000)) 
    data_15_i_1__0
       (.I0(data_3_i_2_n_0),
        .I1(adc_reading_allowed_reg_i_2__0_n_0),
        .I2(data_14_i_2__0_n_0),
        .I3(tconv_cnt_reg_reg[3]),
        .I4(tconv_cnt_reg_reg[2]),
        .I5(data_15_reg_n_0),
        .O(data_15_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_15_reg
       (.C(adc_clk_100MHz_if1),
        .CE(1'b1),
        .D(data_15_i_1__0_n_0),
        .Q(data_15_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hB8FFFFFFB8000000)) 
    data_1_i_1
       (.I0(adc_if1_ch1_da),
        .I1(data_5_reg_0),
        .I2(adc_if2_ch2_da),
        .I3(\dco_pos_num_reg_n_0_[0] ),
        .I4(\dco_pos_num_reg_n_0_[1] ),
        .I5(data_1_reg_n_0),
        .O(data_1_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_1_reg
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(data_1_i_1_n_0),
        .Q(data_1_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    data_2_i_1__0
       (.I0(data_2_i_2_n_0),
        .I1(\dco_neg_num_reg_n_0_[1] ),
        .I2(\dco_neg_num_reg_n_0_[0] ),
        .I3(data_2_reg_n_0),
        .O(data_2_i_1__0_n_0));
  LUT4 #(
    .INIT(16'hFB08)) 
    data_2_i_2
       (.I0(adc_if1_ch1_db),
        .I1(ch_num[0]),
        .I2(ch_num[1]),
        .I3(adc_if2_ch2_db),
        .O(data_2_i_2_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_2_reg
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(data_2_i_1__0_n_0),
        .Q(data_2_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    data_3_i_1__0
       (.I0(data_3_i_2_n_0),
        .I1(\dco_neg_num_reg_n_0_[1] ),
        .I2(\dco_neg_num_reg_n_0_[0] ),
        .I3(data_3_reg_n_0),
        .O(data_3_i_1__0_n_0));
  LUT4 #(
    .INIT(16'hFB08)) 
    data_3_i_2
       (.I0(adc_if1_ch1_da),
        .I1(ch_num[0]),
        .I2(ch_num[1]),
        .I3(adc_if2_ch2_da),
        .O(data_3_i_2_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_3_reg
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(data_3_i_1__0_n_0),
        .Q(data_3_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFB8FF0000B800)) 
    data_4_i_1__0
       (.I0(adc_if1_ch1_db),
        .I1(data_5_reg_0),
        .I2(adc_if2_ch2_db),
        .I3(\dco_pos_num_reg_n_0_[1] ),
        .I4(\dco_pos_num_reg_n_0_[0] ),
        .I5(data_4_reg_n_0),
        .O(data_4_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_4_reg
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(data_4_i_1__0_n_0),
        .Q(data_4_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFB8FF0000B800)) 
    data_5_i_1__0
       (.I0(adc_if1_ch1_da),
        .I1(data_5_reg_0),
        .I2(adc_if2_ch2_da),
        .I3(\dco_pos_num_reg_n_0_[1] ),
        .I4(\dco_pos_num_reg_n_0_[0] ),
        .I5(data_5_reg_n_0),
        .O(data_5_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_5_reg
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(data_5_i_1__0_n_0),
        .Q(data_5_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hEF20)) 
    data_6_i_1__0
       (.I0(data_2_i_2_n_0),
        .I1(\dco_neg_num_reg_n_0_[1] ),
        .I2(\dco_neg_num_reg_n_0_[0] ),
        .I3(data_6_reg_n_0),
        .O(data_6_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_6_reg
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(data_6_i_1__0_n_0),
        .Q(data_6_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hEF20)) 
    data_7_i_1__0
       (.I0(data_3_i_2_n_0),
        .I1(\dco_neg_num_reg_n_0_[1] ),
        .I2(\dco_neg_num_reg_n_0_[0] ),
        .I3(data_7_reg_n_0),
        .O(data_7_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_7_reg
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(data_7_i_1__0_n_0),
        .Q(data_7_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFB8FF0000B800)) 
    data_8_i_1__0
       (.I0(adc_if1_ch1_db),
        .I1(data_5_reg_0),
        .I2(adc_if2_ch2_db),
        .I3(\dco_pos_num_reg_n_0_[0] ),
        .I4(\dco_pos_num_reg_n_0_[1] ),
        .I5(data_8_reg_n_0),
        .O(data_8_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_8_reg
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(data_8_i_1__0_n_0),
        .Q(data_8_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFB8FF0000B800)) 
    data_9_i_1__0
       (.I0(adc_if1_ch1_da),
        .I1(data_5_reg_0),
        .I2(adc_if2_ch2_da),
        .I3(\dco_pos_num_reg_n_0_[0] ),
        .I4(\dco_pos_num_reg_n_0_[1] ),
        .I5(data_9_reg_n_0),
        .O(data_9_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_9_reg
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(data_9_i_1__0_n_0),
        .Q(data_9_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h4FF4)) 
    \dco_neg_num[0]_i_1__0 
       (.I0(\dco_neg_num_reg_n_0_[0] ),
        .I1(\dco_neg_num_reg_n_0_[1] ),
        .I2(even_odd_frame_prev_neg_reg_n_0),
        .I3(even_odd_frame_reg_n_0),
        .O(\dco_neg_num[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h0990)) 
    \dco_neg_num[1]_i_1__0 
       (.I0(even_odd_frame_reg_n_0),
        .I1(even_odd_frame_prev_neg_reg_n_0),
        .I2(\dco_neg_num_reg_n_0_[0] ),
        .I3(\dco_neg_num_reg_n_0_[1] ),
        .O(\dco_neg_num[1]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \dco_neg_num_reg[0] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(\dco_neg_num[0]_i_1__0_n_0 ),
        .Q(\dco_neg_num_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \dco_neg_num_reg[1] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(\dco_neg_num[1]_i_1__0_n_0 ),
        .Q(\dco_neg_num_reg_n_0_[1] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h4FF4)) 
    \dco_pos_num[0]_i_1__0 
       (.I0(\dco_pos_num_reg_n_0_[0] ),
        .I1(\dco_pos_num_reg_n_0_[1] ),
        .I2(even_odd_frame_prev_pos_reg_n_0),
        .I3(even_odd_frame_reg_n_0),
        .O(\dco_pos_num[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h0990)) 
    \dco_pos_num[1]_i_1__0 
       (.I0(even_odd_frame_reg_n_0),
        .I1(even_odd_frame_prev_pos_reg_n_0),
        .I2(\dco_pos_num_reg_n_0_[0] ),
        .I3(\dco_pos_num_reg_n_0_[1] ),
        .O(\dco_pos_num[1]_i_1__0_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dco_pos_num_reg[0] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(\dco_pos_num[0]_i_1__0_n_0 ),
        .Q(\dco_pos_num_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \dco_pos_num_reg[1] 
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(\dco_pos_num[1]_i_1__0_n_0 ),
        .Q(\dco_pos_num_reg_n_0_[1] ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFEF00000010)) 
    even_odd_frame_i_1__0
       (.I0(tconv_cnt_reg_reg[0]),
        .I1(tconv_cnt_reg_reg[3]),
        .I2(tconv_cnt_reg_reg[2]),
        .I3(tconv_cnt_reg_reg[1]),
        .I4(adc_reading_allowed_reg_i_2__0_n_0),
        .I5(even_odd_frame_reg_n_0),
        .O(even_odd_frame_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    even_odd_frame_prev_neg_reg
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(even_odd_frame_reg_n_0),
        .Q(even_odd_frame_prev_neg_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    even_odd_frame_prev_pos_reg
       (.C(\adc_data_reg[15]_i_2_n_0 ),
        .CE(1'b1),
        .D(even_odd_frame_reg_n_0),
        .Q(even_odd_frame_prev_pos_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    even_odd_frame_reg
       (.C(adc_clk_100MHz_if1),
        .CE(1'b1),
        .D(even_odd_frame_i_1__0_n_0),
        .Q(even_odd_frame_reg_n_0),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    fpga_clk_prev_reg
       (.C(adc_clk_100MHz_if1),
        .CE(1'b1),
        .D(fpga_clk_buf_if1),
        .Q(fpga_clk_prev),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \tconv_cnt_reg[0]_i_1__0 
       (.I0(tconv_cnt_reg_reg[0]),
        .O(\tconv_cnt_reg[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \tconv_cnt_reg[1]_i_1__0 
       (.I0(tconv_cnt_reg_reg[0]),
        .I1(tconv_cnt_reg_reg[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \tconv_cnt_reg[2]_i_1__0 
       (.I0(tconv_cnt_reg_reg[1]),
        .I1(tconv_cnt_reg_reg[0]),
        .I2(tconv_cnt_reg_reg[2]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \tconv_cnt_reg[3]_i_1__0 
       (.I0(tconv_cnt_reg_reg[0]),
        .I1(tconv_cnt_reg_reg[1]),
        .I2(tconv_cnt_reg_reg[2]),
        .I3(tconv_cnt_reg_reg[3]),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \tconv_cnt_reg[4]_i_1__0 
       (.I0(tconv_cnt_reg_reg[2]),
        .I1(tconv_cnt_reg_reg[1]),
        .I2(tconv_cnt_reg_reg[0]),
        .I3(tconv_cnt_reg_reg[3]),
        .I4(tconv_cnt_reg_reg[4]),
        .O(p_0_in[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \tconv_cnt_reg[5]_i_1__0 
       (.I0(tconv_cnt_reg_reg[3]),
        .I1(tconv_cnt_reg_reg[0]),
        .I2(tconv_cnt_reg_reg[1]),
        .I3(tconv_cnt_reg_reg[2]),
        .I4(tconv_cnt_reg_reg[4]),
        .I5(tconv_cnt_reg_reg[5]),
        .O(p_0_in[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \tconv_cnt_reg[6]_i_1__0 
       (.I0(\tconv_cnt_reg[7]_i_3__0_n_0 ),
        .I1(tconv_cnt_reg_reg[6]),
        .O(p_0_in[6]));
  LUT2 #(
    .INIT(4'h2)) 
    \tconv_cnt_reg[7]_i_1__0 
       (.I0(fpga_clk_buf_if1),
        .I1(fpga_clk_prev),
        .O(tconv_cnt_reg));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \tconv_cnt_reg[7]_i_2__0 
       (.I0(\tconv_cnt_reg[7]_i_3__0_n_0 ),
        .I1(tconv_cnt_reg_reg[6]),
        .I2(tconv_cnt_reg_reg[7]),
        .O(p_0_in[7]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \tconv_cnt_reg[7]_i_3__0 
       (.I0(tconv_cnt_reg_reg[5]),
        .I1(tconv_cnt_reg_reg[3]),
        .I2(tconv_cnt_reg_reg[0]),
        .I3(tconv_cnt_reg_reg[1]),
        .I4(tconv_cnt_reg_reg[2]),
        .I5(tconv_cnt_reg_reg[4]),
        .O(\tconv_cnt_reg[7]_i_3__0_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[0] 
       (.C(adc_clk_100MHz_if1),
        .CE(1'b1),
        .D(\tconv_cnt_reg[0]_i_1__0_n_0 ),
        .Q(tconv_cnt_reg_reg[0]),
        .R(tconv_cnt_reg));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[1] 
       (.C(adc_clk_100MHz_if1),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(tconv_cnt_reg_reg[1]),
        .R(tconv_cnt_reg));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[2] 
       (.C(adc_clk_100MHz_if1),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(tconv_cnt_reg_reg[2]),
        .R(tconv_cnt_reg));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[3] 
       (.C(adc_clk_100MHz_if1),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(tconv_cnt_reg_reg[3]),
        .R(tconv_cnt_reg));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[4] 
       (.C(adc_clk_100MHz_if1),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(tconv_cnt_reg_reg[4]),
        .R(tconv_cnt_reg));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[5] 
       (.C(adc_clk_100MHz_if1),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(tconv_cnt_reg_reg[5]),
        .R(tconv_cnt_reg));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[6] 
       (.C(adc_clk_100MHz_if1),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(tconv_cnt_reg_reg[6]),
        .R(tconv_cnt_reg));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[7] 
       (.C(adc_clk_100MHz_if1),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(tconv_cnt_reg_reg[7]),
        .R(tconv_cnt_reg));
endmodule

(* ORIG_REF_NAME = "adc_get_data" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_get_data_0
   (ch_num_0_sp_1,
    adc_data_if2,
    adc_if2_ch1_clk,
    fpga_clk_buf_if2,
    adc_clk_100MHz_if2,
    adc_if2_ch1_dco,
    ch_num,
    adc_if1_ch2_dco,
    adc_if2_ch1_da,
    adc_if1_ch2_da,
    adc_if2_ch1_db,
    adc_if1_ch2_db);
  output ch_num_0_sp_1;
  output [15:0]adc_data_if2;
  output adc_if2_ch1_clk;
  input fpga_clk_buf_if2;
  input adc_clk_100MHz_if2;
  input adc_if2_ch1_dco;
  input [1:0]ch_num;
  input adc_if1_ch2_dco;
  input adc_if2_ch1_da;
  input adc_if1_ch2_da;
  input adc_if2_ch1_db;
  input adc_if1_ch2_db;

  wire adc_clk_100MHz_if2;
  wire [15:0]adc_data_if2;
  wire \adc_data_reg[15]_i_1_n_0 ;
  wire \adc_data_reg[15]_i_2__0_n_0 ;
  wire [15:0]adc_data_temp;
  wire adc_if1_ch2_da;
  wire adc_if1_ch2_db;
  wire adc_if1_ch2_dco;
  wire adc_if2_ch1_clk;
  wire adc_if2_ch1_da;
  wire adc_if2_ch1_db;
  wire adc_if2_ch1_dco;
  wire adc_reading_allowed_reg_i_1_n_0;
  wire adc_reading_allowed_reg_i_2_n_0;
  wire adc_reading_allowed_reg_reg_n_0;
  wire [1:0]ch_num;
  wire ch_num_0_sn_1;
  wire data_0_i_1_n_0;
  wire data_10_i_1_n_0;
  wire data_11_i_1_n_0;
  wire data_12_i_1_n_0;
  wire data_13_i_1_n_0;
  wire data_14_i_1_n_0;
  wire data_14_i_2_n_0;
  wire data_15_i_1_n_0;
  wire data_1_i_1__0_n_0;
  wire data_2_i_1_n_0;
  wire data_2_i_2__0_n_0;
  wire data_3_i_1_n_0;
  wire data_3_i_2__0_n_0;
  wire data_4_i_1_n_0;
  wire data_5_i_1_n_0;
  wire data_6_i_1_n_0;
  wire data_7_i_1_n_0;
  wire data_8_i_1_n_0;
  wire data_9_i_1_n_0;
  wire \dco_neg_num[0]_i_1_n_0 ;
  wire \dco_neg_num[1]_i_1_n_0 ;
  wire \dco_neg_num_reg_n_0_[0] ;
  wire \dco_neg_num_reg_n_0_[1] ;
  wire \dco_pos_num[0]_i_1_n_0 ;
  wire \dco_pos_num[1]_i_1_n_0 ;
  wire \dco_pos_num_reg_n_0_[0] ;
  wire \dco_pos_num_reg_n_0_[1] ;
  wire even_odd_frame;
  wire even_odd_frame_i_1_n_0;
  wire even_odd_frame_prev_neg;
  wire even_odd_frame_prev_pos;
  wire fpga_clk_buf_if2;
  wire fpga_clk_prev;
  wire [7:1]p_0_in;
  wire tconv_cnt_reg;
  wire \tconv_cnt_reg[0]_i_1_n_0 ;
  wire \tconv_cnt_reg[7]_i_3_n_0 ;
  wire [7:0]tconv_cnt_reg_reg;

  assign ch_num_0_sp_1 = ch_num_0_sn_1;
  LUT2 #(
    .INIT(4'h8)) 
    OBUFDS_adc_if2_ch1_clk_i_1
       (.I0(adc_clk_100MHz_if2),
        .I1(adc_reading_allowed_reg_reg_n_0),
        .O(adc_if2_ch1_clk));
  LUT2 #(
    .INIT(4'h8)) 
    \adc_data_reg[15]_i_1 
       (.I0(\dco_neg_num_reg_n_0_[1] ),
        .I1(\dco_neg_num_reg_n_0_[0] ),
        .O(\adc_data_reg[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \adc_data_reg[15]_i_2__0 
       (.I0(adc_if2_ch1_dco),
        .I1(ch_num[0]),
        .I2(ch_num[1]),
        .I3(adc_if1_ch2_dco),
        .O(\adc_data_reg[15]_i_2__0_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[0] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[0]),
        .Q(adc_data_if2[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[10] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[10]),
        .Q(adc_data_if2[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[11] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[11]),
        .Q(adc_data_if2[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[12] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[12]),
        .Q(adc_data_if2[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[13] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[13]),
        .Q(adc_data_if2[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[14] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[14]),
        .Q(adc_data_if2[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[15] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[15]),
        .Q(adc_data_if2[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[1] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[1]),
        .Q(adc_data_if2[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[2] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[2]),
        .Q(adc_data_if2[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[3] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[3]),
        .Q(adc_data_if2[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[4] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[4]),
        .Q(adc_data_if2[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[5] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[5]),
        .Q(adc_data_if2[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[6] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[6]),
        .Q(adc_data_if2[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[7] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[7]),
        .Q(adc_data_if2[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[8] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[8]),
        .Q(adc_data_if2[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[9] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[9]),
        .Q(adc_data_if2[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAAAAAAAEAAA8AA)) 
    adc_reading_allowed_reg_i_1
       (.I0(adc_reading_allowed_reg_reg_n_0),
        .I1(tconv_cnt_reg_reg[2]),
        .I2(tconv_cnt_reg_reg[3]),
        .I3(tconv_cnt_reg_reg[0]),
        .I4(tconv_cnt_reg_reg[1]),
        .I5(adc_reading_allowed_reg_i_2_n_0),
        .O(adc_reading_allowed_reg_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    adc_reading_allowed_reg_i_2
       (.I0(tconv_cnt_reg_reg[7]),
        .I1(tconv_cnt_reg_reg[6]),
        .I2(tconv_cnt_reg_reg[4]),
        .I3(tconv_cnt_reg_reg[5]),
        .O(adc_reading_allowed_reg_i_2_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    adc_reading_allowed_reg_reg
       (.C(adc_clk_100MHz_if2),
        .CE(1'b1),
        .D(adc_reading_allowed_reg_i_1_n_0),
        .Q(adc_reading_allowed_reg_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hB8FFFFFFB8000000)) 
    data_0_i_1
       (.I0(adc_if2_ch1_db),
        .I1(ch_num_0_sn_1),
        .I2(adc_if1_ch2_db),
        .I3(\dco_pos_num_reg_n_0_[0] ),
        .I4(\dco_pos_num_reg_n_0_[1] ),
        .I5(adc_data_temp[0]),
        .O(data_0_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h2)) 
    data_0_i_2
       (.I0(ch_num[0]),
        .I1(ch_num[1]),
        .O(ch_num_0_sn_1));
  FDRE #(
    .INIT(1'b0)) 
    data_0_reg
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(data_0_i_1_n_0),
        .Q(adc_data_temp[0]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFEFEFF00020200)) 
    data_10_i_1
       (.I0(data_2_i_2__0_n_0),
        .I1(\dco_neg_num_reg_n_0_[0] ),
        .I2(\dco_neg_num_reg_n_0_[1] ),
        .I3(even_odd_frame_prev_neg),
        .I4(even_odd_frame),
        .I5(adc_data_temp[10]),
        .O(data_10_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_10_reg
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(data_10_i_1_n_0),
        .Q(adc_data_temp[10]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFEFEFF00020200)) 
    data_11_i_1
       (.I0(data_3_i_2__0_n_0),
        .I1(\dco_neg_num_reg_n_0_[0] ),
        .I2(\dco_neg_num_reg_n_0_[1] ),
        .I3(even_odd_frame_prev_neg),
        .I4(even_odd_frame),
        .I5(adc_data_temp[11]),
        .O(data_11_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_11_reg
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(data_11_i_1_n_0),
        .Q(adc_data_temp[11]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFEFEFF00020200)) 
    data_12_i_1
       (.I0(data_2_i_2__0_n_0),
        .I1(\dco_pos_num_reg_n_0_[0] ),
        .I2(\dco_pos_num_reg_n_0_[1] ),
        .I3(even_odd_frame_prev_pos),
        .I4(even_odd_frame),
        .I5(adc_data_temp[12]),
        .O(data_12_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_12_reg
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(data_12_i_1_n_0),
        .Q(adc_data_temp[12]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFEFEFF00020200)) 
    data_13_i_1
       (.I0(data_3_i_2__0_n_0),
        .I1(\dco_pos_num_reg_n_0_[0] ),
        .I2(\dco_pos_num_reg_n_0_[1] ),
        .I3(even_odd_frame_prev_pos),
        .I4(even_odd_frame),
        .I5(adc_data_temp[13]),
        .O(data_13_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_13_reg
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(data_13_i_1_n_0),
        .Q(adc_data_temp[13]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFEFFFF00020000)) 
    data_14_i_1
       (.I0(data_2_i_2__0_n_0),
        .I1(adc_reading_allowed_reg_i_2_n_0),
        .I2(data_14_i_2_n_0),
        .I3(tconv_cnt_reg_reg[3]),
        .I4(tconv_cnt_reg_reg[2]),
        .I5(adc_data_temp[14]),
        .O(data_14_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h7)) 
    data_14_i_2
       (.I0(tconv_cnt_reg_reg[1]),
        .I1(tconv_cnt_reg_reg[0]),
        .O(data_14_i_2_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_14_reg
       (.C(adc_clk_100MHz_if2),
        .CE(1'b1),
        .D(data_14_i_1_n_0),
        .Q(adc_data_temp[14]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFEFFFF00020000)) 
    data_15_i_1
       (.I0(data_3_i_2__0_n_0),
        .I1(adc_reading_allowed_reg_i_2_n_0),
        .I2(data_14_i_2_n_0),
        .I3(tconv_cnt_reg_reg[3]),
        .I4(tconv_cnt_reg_reg[2]),
        .I5(adc_data_temp[15]),
        .O(data_15_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_15_reg
       (.C(adc_clk_100MHz_if2),
        .CE(1'b1),
        .D(data_15_i_1_n_0),
        .Q(adc_data_temp[15]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hB8FFFFFFB8000000)) 
    data_1_i_1__0
       (.I0(adc_if2_ch1_da),
        .I1(ch_num_0_sn_1),
        .I2(adc_if1_ch2_da),
        .I3(\dco_pos_num_reg_n_0_[0] ),
        .I4(\dco_pos_num_reg_n_0_[1] ),
        .I5(adc_data_temp[1]),
        .O(data_1_i_1__0_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_1_reg
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(data_1_i_1__0_n_0),
        .Q(adc_data_temp[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    data_2_i_1
       (.I0(data_2_i_2__0_n_0),
        .I1(\dco_neg_num_reg_n_0_[1] ),
        .I2(\dco_neg_num_reg_n_0_[0] ),
        .I3(adc_data_temp[2]),
        .O(data_2_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    data_2_i_2__0
       (.I0(adc_if2_ch1_db),
        .I1(ch_num[0]),
        .I2(ch_num[1]),
        .I3(adc_if1_ch2_db),
        .O(data_2_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_2_reg
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(data_2_i_1_n_0),
        .Q(adc_data_temp[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    data_3_i_1
       (.I0(data_3_i_2__0_n_0),
        .I1(\dco_neg_num_reg_n_0_[1] ),
        .I2(\dco_neg_num_reg_n_0_[0] ),
        .I3(adc_data_temp[3]),
        .O(data_3_i_1_n_0));
  LUT4 #(
    .INIT(16'hFB08)) 
    data_3_i_2__0
       (.I0(adc_if2_ch1_da),
        .I1(ch_num[0]),
        .I2(ch_num[1]),
        .I3(adc_if1_ch2_da),
        .O(data_3_i_2__0_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_3_reg
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(data_3_i_1_n_0),
        .Q(adc_data_temp[3]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFB8FF0000B800)) 
    data_4_i_1
       (.I0(adc_if2_ch1_db),
        .I1(ch_num_0_sn_1),
        .I2(adc_if1_ch2_db),
        .I3(\dco_pos_num_reg_n_0_[1] ),
        .I4(\dco_pos_num_reg_n_0_[0] ),
        .I5(adc_data_temp[4]),
        .O(data_4_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_4_reg
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(data_4_i_1_n_0),
        .Q(adc_data_temp[4]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFB8FF0000B800)) 
    data_5_i_1
       (.I0(adc_if2_ch1_da),
        .I1(ch_num_0_sn_1),
        .I2(adc_if1_ch2_da),
        .I3(\dco_pos_num_reg_n_0_[1] ),
        .I4(\dco_pos_num_reg_n_0_[0] ),
        .I5(adc_data_temp[5]),
        .O(data_5_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_5_reg
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(data_5_i_1_n_0),
        .Q(adc_data_temp[5]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'hEF20)) 
    data_6_i_1
       (.I0(data_2_i_2__0_n_0),
        .I1(\dco_neg_num_reg_n_0_[1] ),
        .I2(\dco_neg_num_reg_n_0_[0] ),
        .I3(adc_data_temp[6]),
        .O(data_6_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_6_reg
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(data_6_i_1_n_0),
        .Q(adc_data_temp[6]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'hEF20)) 
    data_7_i_1
       (.I0(data_3_i_2__0_n_0),
        .I1(\dco_neg_num_reg_n_0_[1] ),
        .I2(\dco_neg_num_reg_n_0_[0] ),
        .I3(adc_data_temp[7]),
        .O(data_7_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_7_reg
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(data_7_i_1_n_0),
        .Q(adc_data_temp[7]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFB8FF0000B800)) 
    data_8_i_1
       (.I0(adc_if2_ch1_db),
        .I1(ch_num_0_sn_1),
        .I2(adc_if1_ch2_db),
        .I3(\dco_pos_num_reg_n_0_[0] ),
        .I4(\dco_pos_num_reg_n_0_[1] ),
        .I5(adc_data_temp[8]),
        .O(data_8_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_8_reg
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(data_8_i_1_n_0),
        .Q(adc_data_temp[8]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFB8FF0000B800)) 
    data_9_i_1
       (.I0(adc_if2_ch1_da),
        .I1(ch_num_0_sn_1),
        .I2(adc_if1_ch2_da),
        .I3(\dco_pos_num_reg_n_0_[0] ),
        .I4(\dco_pos_num_reg_n_0_[1] ),
        .I5(adc_data_temp[9]),
        .O(data_9_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_9_reg
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(data_9_i_1_n_0),
        .Q(adc_data_temp[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'h4FF4)) 
    \dco_neg_num[0]_i_1 
       (.I0(\dco_neg_num_reg_n_0_[0] ),
        .I1(\dco_neg_num_reg_n_0_[1] ),
        .I2(even_odd_frame_prev_neg),
        .I3(even_odd_frame),
        .O(\dco_neg_num[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'h0990)) 
    \dco_neg_num[1]_i_1 
       (.I0(even_odd_frame),
        .I1(even_odd_frame_prev_neg),
        .I2(\dco_neg_num_reg_n_0_[0] ),
        .I3(\dco_neg_num_reg_n_0_[1] ),
        .O(\dco_neg_num[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \dco_neg_num_reg[0] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(\dco_neg_num[0]_i_1_n_0 ),
        .Q(\dco_neg_num_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \dco_neg_num_reg[1] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(\dco_neg_num[1]_i_1_n_0 ),
        .Q(\dco_neg_num_reg_n_0_[1] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h4FF4)) 
    \dco_pos_num[0]_i_1 
       (.I0(\dco_pos_num_reg_n_0_[0] ),
        .I1(\dco_pos_num_reg_n_0_[1] ),
        .I2(even_odd_frame_prev_pos),
        .I3(even_odd_frame),
        .O(\dco_pos_num[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h0990)) 
    \dco_pos_num[1]_i_1 
       (.I0(even_odd_frame),
        .I1(even_odd_frame_prev_pos),
        .I2(\dco_pos_num_reg_n_0_[0] ),
        .I3(\dco_pos_num_reg_n_0_[1] ),
        .O(\dco_pos_num[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dco_pos_num_reg[0] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(\dco_pos_num[0]_i_1_n_0 ),
        .Q(\dco_pos_num_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \dco_pos_num_reg[1] 
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(\dco_pos_num[1]_i_1_n_0 ),
        .Q(\dco_pos_num_reg_n_0_[1] ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFEF00000010)) 
    even_odd_frame_i_1
       (.I0(tconv_cnt_reg_reg[0]),
        .I1(tconv_cnt_reg_reg[3]),
        .I2(tconv_cnt_reg_reg[2]),
        .I3(tconv_cnt_reg_reg[1]),
        .I4(adc_reading_allowed_reg_i_2_n_0),
        .I5(even_odd_frame),
        .O(even_odd_frame_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    even_odd_frame_prev_neg_reg
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(even_odd_frame),
        .Q(even_odd_frame_prev_neg),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    even_odd_frame_prev_pos_reg
       (.C(\adc_data_reg[15]_i_2__0_n_0 ),
        .CE(1'b1),
        .D(even_odd_frame),
        .Q(even_odd_frame_prev_pos),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    even_odd_frame_reg
       (.C(adc_clk_100MHz_if2),
        .CE(1'b1),
        .D(even_odd_frame_i_1_n_0),
        .Q(even_odd_frame),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    fpga_clk_prev_reg
       (.C(adc_clk_100MHz_if2),
        .CE(1'b1),
        .D(fpga_clk_buf_if2),
        .Q(fpga_clk_prev),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \tconv_cnt_reg[0]_i_1 
       (.I0(tconv_cnt_reg_reg[0]),
        .O(\tconv_cnt_reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \tconv_cnt_reg[1]_i_1 
       (.I0(tconv_cnt_reg_reg[0]),
        .I1(tconv_cnt_reg_reg[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \tconv_cnt_reg[2]_i_1 
       (.I0(tconv_cnt_reg_reg[1]),
        .I1(tconv_cnt_reg_reg[0]),
        .I2(tconv_cnt_reg_reg[2]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \tconv_cnt_reg[3]_i_1 
       (.I0(tconv_cnt_reg_reg[0]),
        .I1(tconv_cnt_reg_reg[1]),
        .I2(tconv_cnt_reg_reg[2]),
        .I3(tconv_cnt_reg_reg[3]),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \tconv_cnt_reg[4]_i_1 
       (.I0(tconv_cnt_reg_reg[2]),
        .I1(tconv_cnt_reg_reg[1]),
        .I2(tconv_cnt_reg_reg[0]),
        .I3(tconv_cnt_reg_reg[3]),
        .I4(tconv_cnt_reg_reg[4]),
        .O(p_0_in[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \tconv_cnt_reg[5]_i_1 
       (.I0(tconv_cnt_reg_reg[3]),
        .I1(tconv_cnt_reg_reg[0]),
        .I2(tconv_cnt_reg_reg[1]),
        .I3(tconv_cnt_reg_reg[2]),
        .I4(tconv_cnt_reg_reg[4]),
        .I5(tconv_cnt_reg_reg[5]),
        .O(p_0_in[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \tconv_cnt_reg[6]_i_1 
       (.I0(\tconv_cnt_reg[7]_i_3_n_0 ),
        .I1(tconv_cnt_reg_reg[6]),
        .O(p_0_in[6]));
  LUT2 #(
    .INIT(4'h2)) 
    \tconv_cnt_reg[7]_i_1 
       (.I0(fpga_clk_buf_if2),
        .I1(fpga_clk_prev),
        .O(tconv_cnt_reg));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \tconv_cnt_reg[7]_i_2 
       (.I0(\tconv_cnt_reg[7]_i_3_n_0 ),
        .I1(tconv_cnt_reg_reg[6]),
        .I2(tconv_cnt_reg_reg[7]),
        .O(p_0_in[7]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \tconv_cnt_reg[7]_i_3 
       (.I0(tconv_cnt_reg_reg[5]),
        .I1(tconv_cnt_reg_reg[3]),
        .I2(tconv_cnt_reg_reg[0]),
        .I3(tconv_cnt_reg_reg[1]),
        .I4(tconv_cnt_reg_reg[2]),
        .I5(tconv_cnt_reg_reg[4]),
        .O(\tconv_cnt_reg[7]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[0] 
       (.C(adc_clk_100MHz_if2),
        .CE(1'b1),
        .D(\tconv_cnt_reg[0]_i_1_n_0 ),
        .Q(tconv_cnt_reg_reg[0]),
        .R(tconv_cnt_reg));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[1] 
       (.C(adc_clk_100MHz_if2),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(tconv_cnt_reg_reg[1]),
        .R(tconv_cnt_reg));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[2] 
       (.C(adc_clk_100MHz_if2),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(tconv_cnt_reg_reg[2]),
        .R(tconv_cnt_reg));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[3] 
       (.C(adc_clk_100MHz_if2),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(tconv_cnt_reg_reg[3]),
        .R(tconv_cnt_reg));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[4] 
       (.C(adc_clk_100MHz_if2),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(tconv_cnt_reg_reg[4]),
        .R(tconv_cnt_reg));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[5] 
       (.C(adc_clk_100MHz_if2),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(tconv_cnt_reg_reg[5]),
        .R(tconv_cnt_reg));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[6] 
       (.C(adc_clk_100MHz_if2),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(tconv_cnt_reg_reg[6]),
        .R(tconv_cnt_reg));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[7] 
       (.C(adc_clk_100MHz_if2),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(tconv_cnt_reg_reg[7]),
        .R(tconv_cnt_reg));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_mrls_top_0_1,mrls_top,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "package_project" *) 
(* X_CORE_INFO = "mrls_top,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (ch_num,
    clk_10MHz_inverted,
    fpga_clk_buf_if1,
    fpga_clk_buf_if2,
    adc_clk_100MHz_if1,
    adc_clk_100MHz_if2,
    clk_100MHz,
    fpga_clk,
    adc_if2_ch1_clk_p,
    adc_if2_ch1_clk_n,
    adc_if2_ch1_dco_p,
    adc_if2_ch1_dco_n,
    adc_if2_ch1_da_p,
    adc_if2_ch1_da_n,
    adc_if2_ch1_db_p,
    adc_if2_ch1_db_n,
    adc_if2_ch2_dco_p,
    adc_if2_ch2_dco_n,
    adc_if2_ch2_da_p,
    adc_if2_ch2_da_n,
    adc_if2_ch2_db_p,
    adc_if2_ch2_db_n,
    adc_data_if1,
    adc_data_if2,
    adc_if1_ch1_clk_p,
    adc_if1_ch1_clk_n,
    adc_if1_ch1_dco_p,
    adc_if1_ch1_dco_n,
    adc_if1_ch1_da_p,
    adc_if1_ch1_da_n,
    adc_if1_ch1_db_p,
    adc_if1_ch1_db_n,
    adc_if1_ch2_dco_p,
    adc_if1_ch2_dco_n,
    adc_if1_ch2_da_p,
    adc_if1_ch2_da_n,
    adc_if1_ch2_db_p,
    adc_if1_ch2_db_n);
  input [1:0]ch_num;
  input clk_10MHz_inverted;
  input fpga_clk_buf_if1;
  input fpga_clk_buf_if2;
  input adc_clk_100MHz_if1;
  input adc_clk_100MHz_if2;
  input clk_100MHz;
  input fpga_clk;
  output adc_if2_ch1_clk_p;
  output adc_if2_ch1_clk_n;
  input adc_if2_ch1_dco_p;
  input adc_if2_ch1_dco_n;
  input adc_if2_ch1_da_p;
  input adc_if2_ch1_da_n;
  input adc_if2_ch1_db_p;
  input adc_if2_ch1_db_n;
  input adc_if2_ch2_dco_p;
  input adc_if2_ch2_dco_n;
  input adc_if2_ch2_da_p;
  input adc_if2_ch2_da_n;
  input adc_if2_ch2_db_p;
  input adc_if2_ch2_db_n;
  output [15:0]adc_data_if1;
  output [15:0]adc_data_if2;
  output adc_if1_ch1_clk_p;
  output adc_if1_ch1_clk_n;
  input adc_if1_ch1_dco_p;
  input adc_if1_ch1_dco_n;
  input adc_if1_ch1_da_p;
  input adc_if1_ch1_da_n;
  input adc_if1_ch1_db_p;
  input adc_if1_ch1_db_n;
  input adc_if1_ch2_dco_p;
  input adc_if1_ch2_dco_n;
  input adc_if1_ch2_da_p;
  input adc_if1_ch2_da_n;
  input adc_if1_ch2_db_p;
  input adc_if1_ch2_db_n;

  wire adc_clk_100MHz_if1;
  wire adc_clk_100MHz_if2;
  wire [15:0]adc_data_if1;
  wire [15:0]adc_data_if2;
  (* IOSTANDARD = "LVDS_25" *) (* SLEW = "FAST" *) wire adc_if1_ch1_clk_n;
  (* IOSTANDARD = "LVDS_25" *) (* SLEW = "FAST" *) wire adc_if1_ch1_clk_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if1_ch1_da_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if1_ch1_da_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if1_ch1_db_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if1_ch1_db_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if1_ch1_dco_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if1_ch1_dco_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if1_ch2_da_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if1_ch2_da_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if1_ch2_db_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if1_ch2_db_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if1_ch2_dco_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if1_ch2_dco_p;
  (* IOSTANDARD = "LVDS_25" *) (* SLEW = "FAST" *) wire adc_if2_ch1_clk_n;
  (* IOSTANDARD = "LVDS_25" *) (* SLEW = "FAST" *) wire adc_if2_ch1_clk_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if2_ch1_da_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if2_ch1_da_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if2_ch1_db_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if2_ch1_db_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if2_ch1_dco_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if2_ch1_dco_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if2_ch2_da_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if2_ch2_da_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if2_ch2_db_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if2_ch2_db_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if2_ch2_dco_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR = 0 *) (* IOSTANDARD = "LVDS_25" *) wire adc_if2_ch2_dco_p;
  wire [1:0]ch_num;
  wire clk_100MHz;
  wire clk_10MHz_inverted;
  wire fpga_clk;
  wire fpga_clk_buf_if1;
  wire fpga_clk_buf_if2;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mrls_top inst
       (.adc_clk_100MHz_if1(adc_clk_100MHz_if1),
        .adc_clk_100MHz_if2(adc_clk_100MHz_if2),
        .adc_data_if1(adc_data_if1),
        .adc_data_if2(adc_data_if2),
        .adc_if1_ch1_clk_n(adc_if1_ch1_clk_n),
        .adc_if1_ch1_clk_p(adc_if1_ch1_clk_p),
        .adc_if1_ch1_da_n(adc_if1_ch1_da_n),
        .adc_if1_ch1_da_p(adc_if1_ch1_da_p),
        .adc_if1_ch1_db_n(adc_if1_ch1_db_n),
        .adc_if1_ch1_db_p(adc_if1_ch1_db_p),
        .adc_if1_ch1_dco_n(adc_if1_ch1_dco_n),
        .adc_if1_ch1_dco_p(adc_if1_ch1_dco_p),
        .adc_if1_ch2_da_n(adc_if1_ch2_da_n),
        .adc_if1_ch2_da_p(adc_if1_ch2_da_p),
        .adc_if1_ch2_db_n(adc_if1_ch2_db_n),
        .adc_if1_ch2_db_p(adc_if1_ch2_db_p),
        .adc_if1_ch2_dco_n(adc_if1_ch2_dco_n),
        .adc_if1_ch2_dco_p(adc_if1_ch2_dco_p),
        .adc_if2_ch1_clk_n(adc_if2_ch1_clk_n),
        .adc_if2_ch1_clk_p(adc_if2_ch1_clk_p),
        .adc_if2_ch1_da_n(adc_if2_ch1_da_n),
        .adc_if2_ch1_da_p(adc_if2_ch1_da_p),
        .adc_if2_ch1_db_n(adc_if2_ch1_db_n),
        .adc_if2_ch1_db_p(adc_if2_ch1_db_p),
        .adc_if2_ch1_dco_n(adc_if2_ch1_dco_n),
        .adc_if2_ch1_dco_p(adc_if2_ch1_dco_p),
        .adc_if2_ch2_da_n(adc_if2_ch2_da_n),
        .adc_if2_ch2_da_p(adc_if2_ch2_da_p),
        .adc_if2_ch2_db_n(adc_if2_ch2_db_n),
        .adc_if2_ch2_db_p(adc_if2_ch2_db_p),
        .adc_if2_ch2_dco_n(adc_if2_ch2_dco_n),
        .adc_if2_ch2_dco_p(adc_if2_ch2_dco_p),
        .ch_num(ch_num),
        .clk_100MHz(clk_100MHz),
        .clk_10MHz_inverted(clk_10MHz_inverted),
        .fpga_clk(fpga_clk),
        .fpga_clk_buf_if1(fpga_clk_buf_if1),
        .fpga_clk_buf_if2(fpga_clk_buf_if2));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mrls_top
   (ch_num,
    clk_10MHz_inverted,
    fpga_clk_buf_if1,
    fpga_clk_buf_if2,
    adc_clk_100MHz_if1,
    adc_clk_100MHz_if2,
    clk_100MHz,
    fpga_clk,
    adc_if2_ch1_clk_p,
    adc_if2_ch1_clk_n,
    adc_if2_ch1_dco_p,
    adc_if2_ch1_dco_n,
    adc_if2_ch1_da_p,
    adc_if2_ch1_da_n,
    adc_if2_ch1_db_p,
    adc_if2_ch1_db_n,
    adc_if1_ch1_clk_p,
    adc_if1_ch1_clk_n,
    adc_if1_ch1_dco_p,
    adc_if1_ch1_dco_n,
    adc_if1_ch1_da_p,
    adc_if1_ch1_da_n,
    adc_if1_ch1_db_p,
    adc_if1_ch1_db_n,
    adc_if1_ch2_dco_p,
    adc_if1_ch2_dco_n,
    adc_if1_ch2_da_p,
    adc_if1_ch2_da_n,
    adc_if1_ch2_db_p,
    adc_if1_ch2_db_n,
    adc_if2_ch2_dco_p,
    adc_if2_ch2_dco_n,
    adc_if2_ch2_da_p,
    adc_if2_ch2_da_n,
    adc_if2_ch2_db_p,
    adc_if2_ch2_db_n,
    adc_data_if1,
    adc_data_if2);
  input [1:0]ch_num;
  input clk_10MHz_inverted;
  input fpga_clk_buf_if1;
  input fpga_clk_buf_if2;
  input adc_clk_100MHz_if1;
  input adc_clk_100MHz_if2;
  input clk_100MHz;
  input fpga_clk;
  output adc_if2_ch1_clk_p;
  output adc_if2_ch1_clk_n;
  input adc_if2_ch1_dco_p;
  input adc_if2_ch1_dco_n;
  input adc_if2_ch1_da_p;
  input adc_if2_ch1_da_n;
  input adc_if2_ch1_db_p;
  input adc_if2_ch1_db_n;
  output adc_if1_ch1_clk_p;
  output adc_if1_ch1_clk_n;
  input adc_if1_ch1_dco_p;
  input adc_if1_ch1_dco_n;
  input adc_if1_ch1_da_p;
  input adc_if1_ch1_da_n;
  input adc_if1_ch1_db_p;
  input adc_if1_ch1_db_n;
  input adc_if1_ch2_dco_p;
  input adc_if1_ch2_dco_n;
  input adc_if1_ch2_da_p;
  input adc_if1_ch2_da_n;
  input adc_if1_ch2_db_p;
  input adc_if1_ch2_db_n;
  input adc_if2_ch2_dco_p;
  input adc_if2_ch2_dco_n;
  input adc_if2_ch2_da_p;
  input adc_if2_ch2_da_n;
  input adc_if2_ch2_db_p;
  input adc_if2_ch2_db_n;
  output [15:0]adc_data_if1;
  output [15:0]adc_data_if2;

  wire adc_clk_100MHz_if1;
  wire adc_clk_100MHz_if2;
  wire [15:0]adc_data_if1;
  wire [15:0]adc_data_if2;
  wire adc_if1_ch1_clk;
  wire adc_if1_ch1_clk_n;
  wire adc_if1_ch1_clk_p;
  wire adc_if1_ch1_da;
  wire adc_if1_ch1_da_n;
  wire adc_if1_ch1_da_p;
  wire adc_if1_ch1_db;
  wire adc_if1_ch1_db_n;
  wire adc_if1_ch1_db_p;
  wire adc_if1_ch1_dco;
  wire adc_if1_ch1_dco_n;
  wire adc_if1_ch1_dco_p;
  wire adc_if1_ch2_da;
  wire adc_if1_ch2_da_n;
  wire adc_if1_ch2_da_p;
  wire adc_if1_ch2_db;
  wire adc_if1_ch2_db_n;
  wire adc_if1_ch2_db_p;
  wire adc_if1_ch2_dco;
  wire adc_if1_ch2_dco_n;
  wire adc_if1_ch2_dco_p;
  wire adc_if2_ch1_clk;
  wire adc_if2_ch1_clk_n;
  wire adc_if2_ch1_clk_p;
  wire adc_if2_ch1_da;
  wire adc_if2_ch1_da_n;
  wire adc_if2_ch1_da_p;
  wire adc_if2_ch1_db;
  wire adc_if2_ch1_db_n;
  wire adc_if2_ch1_db_p;
  wire adc_if2_ch1_dco;
  wire adc_if2_ch1_dco_n;
  wire adc_if2_ch1_dco_p;
  wire adc_if2_ch2_da;
  wire adc_if2_ch2_da_n;
  wire adc_if2_ch2_da_p;
  wire adc_if2_ch2_db;
  wire adc_if2_ch2_db_n;
  wire adc_if2_ch2_db_p;
  wire adc_if2_ch2_dco;
  wire adc_if2_ch2_dco_n;
  wire adc_if2_ch2_dco_p;
  wire adc_if2_get_data_n_0;
  wire [1:0]ch_num;
  wire fpga_clk_buf_if1;
  wire fpga_clk_buf_if2;

  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_da_if1_ch1
       (.I(adc_if1_ch1_da_p),
        .IB(adc_if1_ch1_da_n),
        .O(adc_if1_ch1_da));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_da_if1_ch2
       (.I(adc_if1_ch2_da_p),
        .IB(adc_if1_ch2_da_n),
        .O(adc_if1_ch2_da));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_da_if2_ch1
       (.I(adc_if2_ch1_da_p),
        .IB(adc_if2_ch1_da_n),
        .O(adc_if2_ch1_da));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_da_if2_ch2
       (.I(adc_if2_ch2_da_p),
        .IB(adc_if2_ch2_da_n),
        .O(adc_if2_ch2_da));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_if1_ch1_db
       (.I(adc_if1_ch1_db_p),
        .IB(adc_if1_ch1_db_n),
        .O(adc_if1_ch1_db));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_if1_ch1_dco
       (.I(adc_if1_ch1_dco_p),
        .IB(adc_if1_ch1_dco_n),
        .O(adc_if1_ch1_dco));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_if1_ch2_db
       (.I(adc_if1_ch2_db_p),
        .IB(adc_if1_ch2_db_n),
        .O(adc_if1_ch2_db));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_if1_ch2_dco
       (.I(adc_if1_ch2_dco_p),
        .IB(adc_if1_ch2_dco_n),
        .O(adc_if1_ch2_dco));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_if2_ch1_db
       (.I(adc_if2_ch1_db_p),
        .IB(adc_if2_ch1_db_n),
        .O(adc_if2_ch1_db));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_if2_ch1_dco
       (.I(adc_if2_ch1_dco_p),
        .IB(adc_if2_ch1_dco_n),
        .O(adc_if2_ch1_dco));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_if2_ch2_db
       (.I(adc_if2_ch2_db_p),
        .IB(adc_if2_ch2_db_n),
        .O(adc_if2_ch2_db));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_if2_ch2_dco
       (.I(adc_if2_ch2_dco_p),
        .IB(adc_if2_ch2_dco_n),
        .O(adc_if2_ch2_dco));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  OBUFDS OBUFDS_adc_if1_ch1_clk
       (.I(adc_if1_ch1_clk),
        .O(adc_if1_ch1_clk_p),
        .OB(adc_if1_ch1_clk_n));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  OBUFDS OBUFDS_adc_if2_ch1_clk
       (.I(adc_if2_ch1_clk),
        .O(adc_if2_ch1_clk_p),
        .OB(adc_if2_ch1_clk_n));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_get_data adc_if1_get_data
       (.adc_clk_100MHz_if1(adc_clk_100MHz_if1),
        .adc_data_if1(adc_data_if1),
        .adc_if1_ch1_clk(adc_if1_ch1_clk),
        .adc_if1_ch1_da(adc_if1_ch1_da),
        .adc_if1_ch1_db(adc_if1_ch1_db),
        .adc_if1_ch1_dco(adc_if1_ch1_dco),
        .adc_if2_ch2_da(adc_if2_ch2_da),
        .adc_if2_ch2_db(adc_if2_ch2_db),
        .adc_if2_ch2_dco(adc_if2_ch2_dco),
        .ch_num(ch_num),
        .data_5_reg_0(adc_if2_get_data_n_0),
        .fpga_clk_buf_if1(fpga_clk_buf_if1));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_adc_get_data_0 adc_if2_get_data
       (.adc_clk_100MHz_if2(adc_clk_100MHz_if2),
        .adc_data_if2(adc_data_if2),
        .adc_if1_ch2_da(adc_if1_ch2_da),
        .adc_if1_ch2_db(adc_if1_ch2_db),
        .adc_if1_ch2_dco(adc_if1_ch2_dco),
        .adc_if2_ch1_clk(adc_if2_ch1_clk),
        .adc_if2_ch1_da(adc_if2_ch1_da),
        .adc_if2_ch1_db(adc_if2_ch1_db),
        .adc_if2_ch1_dco(adc_if2_ch1_dco),
        .ch_num(ch_num),
        .ch_num_0_sp_1(adc_if2_get_data_n_0),
        .fpga_clk_buf_if2(fpga_clk_buf_if2));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
