-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Sep 29 20:32:10 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_mrls_top_0_1_stub.vhdl
-- Design      : design_1_mrls_top_0_1
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    ch_num : in STD_LOGIC_VECTOR ( 1 downto 0 );
    clk_10MHz_inverted : in STD_LOGIC;
    fpga_clk_buf_if1 : in STD_LOGIC;
    fpga_clk_buf_if2 : in STD_LOGIC;
    adc_clk_100MHz_if1 : in STD_LOGIC;
    adc_clk_100MHz_if2 : in STD_LOGIC;
    clk_100MHz : in STD_LOGIC;
    fpga_clk : in STD_LOGIC;
    adc_if2_ch1_clk_p : out STD_LOGIC;
    adc_if2_ch1_clk_n : out STD_LOGIC;
    adc_if2_ch1_dco_p : in STD_LOGIC;
    adc_if2_ch1_dco_n : in STD_LOGIC;
    adc_if2_ch1_da_p : in STD_LOGIC;
    adc_if2_ch1_da_n : in STD_LOGIC;
    adc_if2_ch1_db_p : in STD_LOGIC;
    adc_if2_ch1_db_n : in STD_LOGIC;
    adc_if2_ch2_dco_p : in STD_LOGIC;
    adc_if2_ch2_dco_n : in STD_LOGIC;
    adc_if2_ch2_da_p : in STD_LOGIC;
    adc_if2_ch2_da_n : in STD_LOGIC;
    adc_if2_ch2_db_p : in STD_LOGIC;
    adc_if2_ch2_db_n : in STD_LOGIC;
    adc_data_if1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    adc_data_if2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    adc_if1_ch1_clk_p : out STD_LOGIC;
    adc_if1_ch1_clk_n : out STD_LOGIC;
    adc_if1_ch1_dco_p : in STD_LOGIC;
    adc_if1_ch1_dco_n : in STD_LOGIC;
    adc_if1_ch1_da_p : in STD_LOGIC;
    adc_if1_ch1_da_n : in STD_LOGIC;
    adc_if1_ch1_db_p : in STD_LOGIC;
    adc_if1_ch1_db_n : in STD_LOGIC;
    adc_if1_ch2_dco_p : in STD_LOGIC;
    adc_if1_ch2_dco_n : in STD_LOGIC;
    adc_if1_ch2_da_p : in STD_LOGIC;
    adc_if1_ch2_da_n : in STD_LOGIC;
    adc_if1_ch2_db_p : in STD_LOGIC;
    adc_if1_ch2_db_n : in STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "ch_num[1:0],clk_10MHz_inverted,fpga_clk_buf_if1,fpga_clk_buf_if2,adc_clk_100MHz_if1,adc_clk_100MHz_if2,clk_100MHz,fpga_clk,adc_if2_ch1_clk_p,adc_if2_ch1_clk_n,adc_if2_ch1_dco_p,adc_if2_ch1_dco_n,adc_if2_ch1_da_p,adc_if2_ch1_da_n,adc_if2_ch1_db_p,adc_if2_ch1_db_n,adc_if2_ch2_dco_p,adc_if2_ch2_dco_n,adc_if2_ch2_da_p,adc_if2_ch2_da_n,adc_if2_ch2_db_p,adc_if2_ch2_db_n,adc_data_if1[15:0],adc_data_if2[15:0],adc_if1_ch1_clk_p,adc_if1_ch1_clk_n,adc_if1_ch1_dco_p,adc_if1_ch1_dco_n,adc_if1_ch1_da_p,adc_if1_ch1_da_n,adc_if1_ch1_db_p,adc_if1_ch1_db_n,adc_if1_ch2_dco_p,adc_if1_ch2_dco_n,adc_if1_ch2_da_p,adc_if1_ch2_da_n,adc_if1_ch2_db_p,adc_if1_ch2_db_n";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "mrls_top,Vivado 2019.1";
begin
end;
