// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Sep 29 20:32:10 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_mrls_top_0_1_stub.v
// Design      : design_1_mrls_top_0_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "mrls_top,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(ch_num, clk_10MHz_inverted, fpga_clk_buf_if1, 
  fpga_clk_buf_if2, adc_clk_100MHz_if1, adc_clk_100MHz_if2, clk_100MHz, fpga_clk, 
  adc_if2_ch1_clk_p, adc_if2_ch1_clk_n, adc_if2_ch1_dco_p, adc_if2_ch1_dco_n, 
  adc_if2_ch1_da_p, adc_if2_ch1_da_n, adc_if2_ch1_db_p, adc_if2_ch1_db_n, 
  adc_if2_ch2_dco_p, adc_if2_ch2_dco_n, adc_if2_ch2_da_p, adc_if2_ch2_da_n, 
  adc_if2_ch2_db_p, adc_if2_ch2_db_n, adc_data_if1, adc_data_if2, adc_if1_ch1_clk_p, 
  adc_if1_ch1_clk_n, adc_if1_ch1_dco_p, adc_if1_ch1_dco_n, adc_if1_ch1_da_p, 
  adc_if1_ch1_da_n, adc_if1_ch1_db_p, adc_if1_ch1_db_n, adc_if1_ch2_dco_p, 
  adc_if1_ch2_dco_n, adc_if1_ch2_da_p, adc_if1_ch2_da_n, adc_if1_ch2_db_p, 
  adc_if1_ch2_db_n)
/* synthesis syn_black_box black_box_pad_pin="ch_num[1:0],clk_10MHz_inverted,fpga_clk_buf_if1,fpga_clk_buf_if2,adc_clk_100MHz_if1,adc_clk_100MHz_if2,clk_100MHz,fpga_clk,adc_if2_ch1_clk_p,adc_if2_ch1_clk_n,adc_if2_ch1_dco_p,adc_if2_ch1_dco_n,adc_if2_ch1_da_p,adc_if2_ch1_da_n,adc_if2_ch1_db_p,adc_if2_ch1_db_n,adc_if2_ch2_dco_p,adc_if2_ch2_dco_n,adc_if2_ch2_da_p,adc_if2_ch2_da_n,adc_if2_ch2_db_p,adc_if2_ch2_db_n,adc_data_if1[15:0],adc_data_if2[15:0],adc_if1_ch1_clk_p,adc_if1_ch1_clk_n,adc_if1_ch1_dco_p,adc_if1_ch1_dco_n,adc_if1_ch1_da_p,adc_if1_ch1_da_n,adc_if1_ch1_db_p,adc_if1_ch1_db_n,adc_if1_ch2_dco_p,adc_if1_ch2_dco_n,adc_if1_ch2_da_p,adc_if1_ch2_da_n,adc_if1_ch2_db_p,adc_if1_ch2_db_n" */;
  input [1:0]ch_num;
  input clk_10MHz_inverted;
  input fpga_clk_buf_if1;
  input fpga_clk_buf_if2;
  input adc_clk_100MHz_if1;
  input adc_clk_100MHz_if2;
  input clk_100MHz;
  input fpga_clk;
  output adc_if2_ch1_clk_p;
  output adc_if2_ch1_clk_n;
  input adc_if2_ch1_dco_p;
  input adc_if2_ch1_dco_n;
  input adc_if2_ch1_da_p;
  input adc_if2_ch1_da_n;
  input adc_if2_ch1_db_p;
  input adc_if2_ch1_db_n;
  input adc_if2_ch2_dco_p;
  input adc_if2_ch2_dco_n;
  input adc_if2_ch2_da_p;
  input adc_if2_ch2_da_n;
  input adc_if2_ch2_db_p;
  input adc_if2_ch2_db_n;
  output [15:0]adc_data_if1;
  output [15:0]adc_data_if2;
  output adc_if1_ch1_clk_p;
  output adc_if1_ch1_clk_n;
  input adc_if1_ch1_dco_p;
  input adc_if1_ch1_dco_n;
  input adc_if1_ch1_da_p;
  input adc_if1_ch1_da_n;
  input adc_if1_ch1_db_p;
  input adc_if1_ch1_db_n;
  input adc_if1_ch2_dco_p;
  input adc_if1_ch2_dco_n;
  input adc_if1_ch2_da_p;
  input adc_if1_ch2_da_n;
  input adc_if1_ch2_db_p;
  input adc_if1_ch2_db_n;
endmodule
