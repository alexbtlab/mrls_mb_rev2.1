-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Thu Sep 17 13:52:04 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_SELECTOR_0_0_sim_netlist.vhdl
-- Design      : design_1_ADC_SELECTOR_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_SELECTOR_v1_0 is
  port (
    ADC_DATA : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ADC_DATA_IF2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ADC_DATA_IF1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    CH_SEL : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_SELECTOR_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_SELECTOR_v1_0 is
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ADC_DATA[0]_INST_0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \ADC_DATA[10]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \ADC_DATA[11]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \ADC_DATA[12]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \ADC_DATA[13]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \ADC_DATA[14]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \ADC_DATA[15]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \ADC_DATA[1]_INST_0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \ADC_DATA[2]_INST_0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \ADC_DATA[3]_INST_0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \ADC_DATA[4]_INST_0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \ADC_DATA[5]_INST_0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \ADC_DATA[6]_INST_0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \ADC_DATA[7]_INST_0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \ADC_DATA[8]_INST_0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \ADC_DATA[9]_INST_0\ : label is "soft_lutpair4";
begin
\ADC_DATA[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ADC_DATA_IF2(0),
      I1 => ADC_DATA_IF1(0),
      I2 => CH_SEL,
      O => ADC_DATA(0)
    );
\ADC_DATA[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ADC_DATA_IF2(10),
      I1 => ADC_DATA_IF1(10),
      I2 => CH_SEL,
      O => ADC_DATA(10)
    );
\ADC_DATA[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ADC_DATA_IF2(11),
      I1 => ADC_DATA_IF1(11),
      I2 => CH_SEL,
      O => ADC_DATA(11)
    );
\ADC_DATA[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ADC_DATA_IF2(12),
      I1 => ADC_DATA_IF1(12),
      I2 => CH_SEL,
      O => ADC_DATA(12)
    );
\ADC_DATA[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ADC_DATA_IF2(13),
      I1 => ADC_DATA_IF1(13),
      I2 => CH_SEL,
      O => ADC_DATA(13)
    );
\ADC_DATA[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ADC_DATA_IF2(14),
      I1 => ADC_DATA_IF1(14),
      I2 => CH_SEL,
      O => ADC_DATA(14)
    );
\ADC_DATA[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ADC_DATA_IF2(15),
      I1 => ADC_DATA_IF1(15),
      I2 => CH_SEL,
      O => ADC_DATA(15)
    );
\ADC_DATA[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ADC_DATA_IF2(1),
      I1 => ADC_DATA_IF1(1),
      I2 => CH_SEL,
      O => ADC_DATA(1)
    );
\ADC_DATA[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ADC_DATA_IF2(2),
      I1 => ADC_DATA_IF1(2),
      I2 => CH_SEL,
      O => ADC_DATA(2)
    );
\ADC_DATA[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ADC_DATA_IF2(3),
      I1 => ADC_DATA_IF1(3),
      I2 => CH_SEL,
      O => ADC_DATA(3)
    );
\ADC_DATA[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ADC_DATA_IF2(4),
      I1 => ADC_DATA_IF1(4),
      I2 => CH_SEL,
      O => ADC_DATA(4)
    );
\ADC_DATA[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ADC_DATA_IF2(5),
      I1 => ADC_DATA_IF1(5),
      I2 => CH_SEL,
      O => ADC_DATA(5)
    );
\ADC_DATA[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ADC_DATA_IF2(6),
      I1 => ADC_DATA_IF1(6),
      I2 => CH_SEL,
      O => ADC_DATA(6)
    );
\ADC_DATA[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ADC_DATA_IF2(7),
      I1 => ADC_DATA_IF1(7),
      I2 => CH_SEL,
      O => ADC_DATA(7)
    );
\ADC_DATA[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ADC_DATA_IF2(8),
      I1 => ADC_DATA_IF1(8),
      I2 => CH_SEL,
      O => ADC_DATA(8)
    );
\ADC_DATA[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => ADC_DATA_IF2(9),
      I1 => ADC_DATA_IF1(9),
      I2 => CH_SEL,
      O => ADC_DATA(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    ADC_CLK_IF1 : in STD_LOGIC;
    ADC_CLK_IF2 : in STD_LOGIC;
    ADC_DATA_IF1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ADC_DATA_IF2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    CH_SEL : in STD_LOGIC;
    ADC_DATA : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ADC_CLK : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ADC_SELECTOR_0_0,ADC_SELECTOR_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ADC_SELECTOR_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
begin
ADC_CLK_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => ADC_CLK_IF2,
      I1 => CH_SEL,
      I2 => ADC_CLK_IF1,
      O => ADC_CLK
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_SELECTOR_v1_0
     port map (
      ADC_DATA(15 downto 0) => ADC_DATA(15 downto 0),
      ADC_DATA_IF1(15 downto 0) => ADC_DATA_IF1(15 downto 0),
      ADC_DATA_IF2(15 downto 0) => ADC_DATA_IF2(15 downto 0),
      CH_SEL => CH_SEL
    );
end STRUCTURE;
