-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Thu Sep 17 13:52:04 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_SELECTOR_0_0_stub.vhdl
-- Design      : design_1_ADC_SELECTOR_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    ADC_CLK_IF1 : in STD_LOGIC;
    ADC_CLK_IF2 : in STD_LOGIC;
    ADC_DATA_IF1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ADC_DATA_IF2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    CH_SEL : in STD_LOGIC;
    ADC_DATA : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ADC_CLK : out STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "ADC_CLK_IF1,ADC_CLK_IF2,ADC_DATA_IF1[15:0],ADC_DATA_IF2[15:0],CH_SEL,ADC_DATA[15:0],ADC_CLK";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "ADC_SELECTOR_v1_0,Vivado 2019.1";
begin
end;
