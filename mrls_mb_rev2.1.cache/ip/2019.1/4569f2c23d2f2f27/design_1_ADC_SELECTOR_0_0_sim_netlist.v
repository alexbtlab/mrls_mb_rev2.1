// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Thu Sep 17 13:52:04 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_SELECTOR_0_0_sim_netlist.v
// Design      : design_1_ADC_SELECTOR_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_SELECTOR_v1_0
   (ADC_DATA,
    ADC_DATA_IF2,
    ADC_DATA_IF1,
    CH_SEL);
  output [15:0]ADC_DATA;
  input [15:0]ADC_DATA_IF2;
  input [15:0]ADC_DATA_IF1;
  input CH_SEL;

  wire [15:0]ADC_DATA;
  wire [15:0]ADC_DATA_IF1;
  wire [15:0]ADC_DATA_IF2;
  wire CH_SEL;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \ADC_DATA[0]_INST_0 
       (.I0(ADC_DATA_IF2[0]),
        .I1(ADC_DATA_IF1[0]),
        .I2(CH_SEL),
        .O(ADC_DATA[0]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \ADC_DATA[10]_INST_0 
       (.I0(ADC_DATA_IF2[10]),
        .I1(ADC_DATA_IF1[10]),
        .I2(CH_SEL),
        .O(ADC_DATA[10]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \ADC_DATA[11]_INST_0 
       (.I0(ADC_DATA_IF2[11]),
        .I1(ADC_DATA_IF1[11]),
        .I2(CH_SEL),
        .O(ADC_DATA[11]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \ADC_DATA[12]_INST_0 
       (.I0(ADC_DATA_IF2[12]),
        .I1(ADC_DATA_IF1[12]),
        .I2(CH_SEL),
        .O(ADC_DATA[12]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \ADC_DATA[13]_INST_0 
       (.I0(ADC_DATA_IF2[13]),
        .I1(ADC_DATA_IF1[13]),
        .I2(CH_SEL),
        .O(ADC_DATA[13]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \ADC_DATA[14]_INST_0 
       (.I0(ADC_DATA_IF2[14]),
        .I1(ADC_DATA_IF1[14]),
        .I2(CH_SEL),
        .O(ADC_DATA[14]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \ADC_DATA[15]_INST_0 
       (.I0(ADC_DATA_IF2[15]),
        .I1(ADC_DATA_IF1[15]),
        .I2(CH_SEL),
        .O(ADC_DATA[15]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \ADC_DATA[1]_INST_0 
       (.I0(ADC_DATA_IF2[1]),
        .I1(ADC_DATA_IF1[1]),
        .I2(CH_SEL),
        .O(ADC_DATA[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \ADC_DATA[2]_INST_0 
       (.I0(ADC_DATA_IF2[2]),
        .I1(ADC_DATA_IF1[2]),
        .I2(CH_SEL),
        .O(ADC_DATA[2]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \ADC_DATA[3]_INST_0 
       (.I0(ADC_DATA_IF2[3]),
        .I1(ADC_DATA_IF1[3]),
        .I2(CH_SEL),
        .O(ADC_DATA[3]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \ADC_DATA[4]_INST_0 
       (.I0(ADC_DATA_IF2[4]),
        .I1(ADC_DATA_IF1[4]),
        .I2(CH_SEL),
        .O(ADC_DATA[4]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \ADC_DATA[5]_INST_0 
       (.I0(ADC_DATA_IF2[5]),
        .I1(ADC_DATA_IF1[5]),
        .I2(CH_SEL),
        .O(ADC_DATA[5]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \ADC_DATA[6]_INST_0 
       (.I0(ADC_DATA_IF2[6]),
        .I1(ADC_DATA_IF1[6]),
        .I2(CH_SEL),
        .O(ADC_DATA[6]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \ADC_DATA[7]_INST_0 
       (.I0(ADC_DATA_IF2[7]),
        .I1(ADC_DATA_IF1[7]),
        .I2(CH_SEL),
        .O(ADC_DATA[7]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \ADC_DATA[8]_INST_0 
       (.I0(ADC_DATA_IF2[8]),
        .I1(ADC_DATA_IF1[8]),
        .I2(CH_SEL),
        .O(ADC_DATA[8]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \ADC_DATA[9]_INST_0 
       (.I0(ADC_DATA_IF2[9]),
        .I1(ADC_DATA_IF1[9]),
        .I2(CH_SEL),
        .O(ADC_DATA[9]));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_ADC_SELECTOR_0_0,ADC_SELECTOR_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "ADC_SELECTOR_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (ADC_CLK_IF1,
    ADC_CLK_IF2,
    ADC_DATA_IF1,
    ADC_DATA_IF2,
    CH_SEL,
    ADC_DATA,
    ADC_CLK);
  input ADC_CLK_IF1;
  input ADC_CLK_IF2;
  input [15:0]ADC_DATA_IF1;
  input [15:0]ADC_DATA_IF2;
  input CH_SEL;
  output [15:0]ADC_DATA;
  output ADC_CLK;

  wire ADC_CLK;
  wire ADC_CLK_IF1;
  wire ADC_CLK_IF2;
  wire [15:0]ADC_DATA;
  wire [15:0]ADC_DATA_IF1;
  wire [15:0]ADC_DATA_IF2;
  wire CH_SEL;

  LUT3 #(
    .INIT(8'hB8)) 
    ADC_CLK_INST_0
       (.I0(ADC_CLK_IF2),
        .I1(CH_SEL),
        .I2(ADC_CLK_IF1),
        .O(ADC_CLK));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_SELECTOR_v1_0 inst
       (.ADC_DATA(ADC_DATA),
        .ADC_DATA_IF1(ADC_DATA_IF1),
        .ADC_DATA_IF2(ADC_DATA_IF2),
        .CH_SEL(CH_SEL));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
