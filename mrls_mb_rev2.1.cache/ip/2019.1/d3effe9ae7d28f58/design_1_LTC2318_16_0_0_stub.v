// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Mon Sep 14 13:09:51 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_LTC2318_16_0_0_stub.v
// Design      : design_1_LTC2318_16_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "LTC2387_16,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk_data_out, clk_data_out_inv, ADC_DATA_OUT, 
  adc_dax_p, adc_dax_n, adc_dbx_p, adc_dbx_n, fpga_clk, clk_100, adc_clkx_p, adc_clkx_n, 
  adc_clk_inv_ila, adc_da_ila, adc_db_ila, adc_clk_ila, cnt100_ila, cnt_pos_dco_ila, 
  cnt_neg_dco_ila)
/* synthesis syn_black_box black_box_pad_pin="clk_data_out,clk_data_out_inv,ADC_DATA_OUT[15:0],adc_dax_p,adc_dax_n,adc_dbx_p,adc_dbx_n,fpga_clk,clk_100,adc_clkx_p,adc_clkx_n,adc_clk_inv_ila,adc_da_ila,adc_db_ila,adc_clk_ila,cnt100_ila[2:0],cnt_pos_dco_ila[1:0],cnt_neg_dco_ila[1:0]" */;
  output clk_data_out;
  output clk_data_out_inv;
  output [15:0]ADC_DATA_OUT;
  input adc_dax_p;
  input adc_dax_n;
  input adc_dbx_p;
  input adc_dbx_n;
  input fpga_clk;
  input clk_100;
  output adc_clkx_p;
  output adc_clkx_n;
  output adc_clk_inv_ila;
  output adc_da_ila;
  output adc_db_ila;
  output adc_clk_ila;
  output [2:0]cnt100_ila;
  output [1:0]cnt_pos_dco_ila;
  output [1:0]cnt_neg_dco_ila;
endmodule
