-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Fri Sep 25 16:50:46 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_LTC2318_16_0_0_stub.vhdl
-- Design      : design_1_LTC2318_16_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    clk_data : out STD_LOGIC;
    ADC_DATA_OUT : out STD_LOGIC_VECTOR ( 15 downto 0 );
    adc_dax_p : in STD_LOGIC;
    adc_dax_n : in STD_LOGIC;
    adc_dbx_p : in STD_LOGIC;
    adc_dbx_n : in STD_LOGIC;
    adc_dcox_p : in STD_LOGIC;
    adc_dcox_n : in STD_LOGIC;
    fpga_clk : in STD_LOGIC;
    clk_100 : in STD_LOGIC;
    adc_clkx_p : out STD_LOGIC;
    adc_clkx_n : out STD_LOGIC;
    fpga_clk_ILA : out STD_LOGIC;
    adc_da_ILA : out STD_LOGIC;
    adc_dco_ILA : out STD_LOGIC;
    adc_dco_delayed_ILA : out STD_LOGIC;
    adc_clk_ILA : out STD_LOGIC;
    cnt100_ILA : out STD_LOGIC_VECTOR ( 3 downto 0 );
    cnt_pos_dco_ILA : out STD_LOGIC_VECTOR ( 1 downto 0 );
    cnt_neg_dco_ILA : out STD_LOGIC_VECTOR ( 1 downto 0 );
    clk_200 : in STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_data,ADC_DATA_OUT[15:0],adc_dax_p,adc_dax_n,adc_dbx_p,adc_dbx_n,adc_dcox_p,adc_dcox_n,fpga_clk,clk_100,adc_clkx_p,adc_clkx_n,fpga_clk_ILA,adc_da_ILA,adc_dco_ILA,adc_dco_delayed_ILA,adc_clk_ILA,cnt100_ILA[3:0],cnt_pos_dco_ILA[1:0],cnt_neg_dco_ILA[1:0],clk_200";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "LTC2387,Vivado 2019.1";
begin
end;
