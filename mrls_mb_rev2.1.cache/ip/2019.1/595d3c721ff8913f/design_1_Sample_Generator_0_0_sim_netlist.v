// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Mon Sep 14 17:57:13 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_Sample_Generator_0_0_sim_netlist.v
// Design      : design_1_Sample_Generator_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0
   (m00_axis_tdata,
    m00_axis_tvalid,
    m00_axis_tlast,
    m00_axis_tready,
    m00_axis_aresetn,
    FrameSize,
    data_clk,
    m00_axis_aclk,
    data_in_IF1,
    data_in_IF2);
  output [31:0]m00_axis_tdata;
  output m00_axis_tvalid;
  output m00_axis_tlast;
  input m00_axis_tready;
  input m00_axis_aresetn;
  input [15:0]FrameSize;
  input data_clk;
  input m00_axis_aclk;
  input [15:0]data_in_IF1;
  input [15:0]data_in_IF2;

  wire [15:0]FrameSize;
  wire clk_W10_R100;
  wire [12:0]cnt_clk_10;
  wire cnt_clk_10_w0_carry__0_i_1_n_0;
  wire cnt_clk_10_w0_carry__0_i_2_n_0;
  wire cnt_clk_10_w0_carry__0_i_3_n_0;
  wire cnt_clk_10_w0_carry__0_i_4_n_0;
  wire cnt_clk_10_w0_carry__0_i_5_n_0;
  wire cnt_clk_10_w0_carry__0_i_6_n_0;
  wire cnt_clk_10_w0_carry__0_i_7_n_0;
  wire cnt_clk_10_w0_carry__0_i_8_n_0;
  wire cnt_clk_10_w0_carry__0_n_0;
  wire cnt_clk_10_w0_carry__0_n_1;
  wire cnt_clk_10_w0_carry__0_n_2;
  wire cnt_clk_10_w0_carry__0_n_3;
  wire cnt_clk_10_w0_carry_i_1_n_0;
  wire cnt_clk_10_w0_carry_i_2_n_0;
  wire cnt_clk_10_w0_carry_i_3_n_0;
  wire cnt_clk_10_w0_carry_i_4_n_0;
  wire cnt_clk_10_w0_carry_i_5_n_0;
  wire cnt_clk_10_w0_carry_i_6_n_0;
  wire cnt_clk_10_w0_carry_i_7_n_0;
  wire cnt_clk_10_w0_carry_i_8_n_0;
  wire cnt_clk_10_w0_carry_n_0;
  wire cnt_clk_10_w0_carry_n_1;
  wire cnt_clk_10_w0_carry_n_2;
  wire cnt_clk_10_w0_carry_n_3;
  wire cnt_clk_10_w1_carry__0_i_1_n_0;
  wire cnt_clk_10_w1_carry__0_i_2_n_0;
  wire cnt_clk_10_w1_carry__0_n_2;
  wire cnt_clk_10_w1_carry__0_n_3;
  wire cnt_clk_10_w1_carry_i_1_n_0;
  wire cnt_clk_10_w1_carry_i_2_n_0;
  wire cnt_clk_10_w1_carry_i_3_n_0;
  wire cnt_clk_10_w1_carry_i_4_n_0;
  wire cnt_clk_10_w1_carry_n_0;
  wire cnt_clk_10_w1_carry_n_1;
  wire cnt_clk_10_w1_carry_n_2;
  wire cnt_clk_10_w1_carry_n_3;
  wire \cnt_clk_10_w[0]_i_1_n_0 ;
  wire \cnt_clk_10_w[0]_i_2_n_0 ;
  wire \cnt_clk_10_w[0]_i_4_n_0 ;
  wire \cnt_clk_10_w_reg[0]_i_3_n_0 ;
  wire \cnt_clk_10_w_reg[0]_i_3_n_1 ;
  wire \cnt_clk_10_w_reg[0]_i_3_n_2 ;
  wire \cnt_clk_10_w_reg[0]_i_3_n_3 ;
  wire \cnt_clk_10_w_reg[0]_i_3_n_4 ;
  wire \cnt_clk_10_w_reg[0]_i_3_n_5 ;
  wire \cnt_clk_10_w_reg[0]_i_3_n_6 ;
  wire \cnt_clk_10_w_reg[0]_i_3_n_7 ;
  wire \cnt_clk_10_w_reg[12]_i_1_n_1 ;
  wire \cnt_clk_10_w_reg[12]_i_1_n_2 ;
  wire \cnt_clk_10_w_reg[12]_i_1_n_3 ;
  wire \cnt_clk_10_w_reg[12]_i_1_n_4 ;
  wire \cnt_clk_10_w_reg[12]_i_1_n_5 ;
  wire \cnt_clk_10_w_reg[12]_i_1_n_6 ;
  wire \cnt_clk_10_w_reg[12]_i_1_n_7 ;
  wire \cnt_clk_10_w_reg[4]_i_1_n_0 ;
  wire \cnt_clk_10_w_reg[4]_i_1_n_1 ;
  wire \cnt_clk_10_w_reg[4]_i_1_n_2 ;
  wire \cnt_clk_10_w_reg[4]_i_1_n_3 ;
  wire \cnt_clk_10_w_reg[4]_i_1_n_4 ;
  wire \cnt_clk_10_w_reg[4]_i_1_n_5 ;
  wire \cnt_clk_10_w_reg[4]_i_1_n_6 ;
  wire \cnt_clk_10_w_reg[4]_i_1_n_7 ;
  wire \cnt_clk_10_w_reg[8]_i_1_n_0 ;
  wire \cnt_clk_10_w_reg[8]_i_1_n_1 ;
  wire \cnt_clk_10_w_reg[8]_i_1_n_2 ;
  wire \cnt_clk_10_w_reg[8]_i_1_n_3 ;
  wire \cnt_clk_10_w_reg[8]_i_1_n_4 ;
  wire \cnt_clk_10_w_reg[8]_i_1_n_5 ;
  wire \cnt_clk_10_w_reg[8]_i_1_n_6 ;
  wire \cnt_clk_10_w_reg[8]_i_1_n_7 ;
  wire \cnt_clk_10_w_reg_n_0_[13] ;
  wire \cnt_clk_10_w_reg_n_0_[14] ;
  wire \cnt_clk_10_w_reg_n_0_[15] ;
  wire data_clk;
  wire [15:0]data_in_IF1;
  wire [15:0]data_in_IF2;
  wire [31:0]data_in_ram;
  wire [12:0]i_addr;
  wire \i_addr[12]_i_1_n_0 ;
  wire i_write;
  wire i_write_i_1_n_0;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire m00_axis_tlast_w0_carry__0_i_1_n_0;
  wire m00_axis_tlast_w0_carry__0_i_2_n_0;
  wire m00_axis_tlast_w0_carry__0_n_0;
  wire m00_axis_tlast_w0_carry__0_n_1;
  wire m00_axis_tlast_w0_carry__0_n_2;
  wire m00_axis_tlast_w0_carry__0_n_3;
  wire m00_axis_tlast_w0_carry__1_n_2;
  wire m00_axis_tlast_w0_carry__1_n_3;
  wire m00_axis_tlast_w0_carry_i_1_n_0;
  wire m00_axis_tlast_w0_carry_i_2_n_0;
  wire m00_axis_tlast_w0_carry_i_3_n_0;
  wire m00_axis_tlast_w0_carry_i_4_n_0;
  wire m00_axis_tlast_w0_carry_n_0;
  wire m00_axis_tlast_w0_carry_n_1;
  wire m00_axis_tlast_w0_carry_n_2;
  wire m00_axis_tlast_w0_carry_n_3;
  wire m00_axis_tlast_w_i_1_n_0;
  wire m00_axis_tready;
  wire m00_axis_tvalid;
  wire m00_axis_tvalid_w1;
  wire m00_axis_tvalid_w12_out;
  wire m00_axis_tvalid_w1_carry__0_i_1_n_0;
  wire m00_axis_tvalid_w1_carry__0_i_2_n_0;
  wire m00_axis_tvalid_w1_carry__0_i_3_n_0;
  wire m00_axis_tvalid_w1_carry__0_i_4_n_0;
  wire m00_axis_tvalid_w1_carry__0_i_5_n_0;
  wire m00_axis_tvalid_w1_carry__0_i_6_n_0;
  wire m00_axis_tvalid_w1_carry__0_i_7_n_0;
  wire m00_axis_tvalid_w1_carry__0_i_8_n_0;
  wire m00_axis_tvalid_w1_carry__0_n_0;
  wire m00_axis_tvalid_w1_carry__0_n_1;
  wire m00_axis_tvalid_w1_carry__0_n_2;
  wire m00_axis_tvalid_w1_carry__0_n_3;
  wire m00_axis_tvalid_w1_carry__1_i_1_n_0;
  wire m00_axis_tvalid_w1_carry__1_i_2_n_0;
  wire m00_axis_tvalid_w1_carry__1_i_3_n_0;
  wire m00_axis_tvalid_w1_carry__1_i_4_n_0;
  wire m00_axis_tvalid_w1_carry__1_n_0;
  wire m00_axis_tvalid_w1_carry__1_n_1;
  wire m00_axis_tvalid_w1_carry__1_n_2;
  wire m00_axis_tvalid_w1_carry__1_n_3;
  wire m00_axis_tvalid_w1_carry__2_i_2_n_0;
  wire m00_axis_tvalid_w1_carry__2_i_3_n_0;
  wire m00_axis_tvalid_w1_carry__2_i_4_n_0;
  wire m00_axis_tvalid_w1_carry__2_n_1;
  wire m00_axis_tvalid_w1_carry__2_n_2;
  wire m00_axis_tvalid_w1_carry__2_n_3;
  wire m00_axis_tvalid_w1_carry_i_1_n_0;
  wire m00_axis_tvalid_w1_carry_i_2_n_0;
  wire m00_axis_tvalid_w1_carry_i_3_n_0;
  wire m00_axis_tvalid_w1_carry_i_4_n_0;
  wire m00_axis_tvalid_w1_carry_i_5_n_0;
  wire m00_axis_tvalid_w1_carry_i_6_n_0;
  wire m00_axis_tvalid_w1_carry_i_7_n_0;
  wire m00_axis_tvalid_w1_carry_i_8_n_0;
  wire m00_axis_tvalid_w1_carry_n_0;
  wire m00_axis_tvalid_w1_carry_n_1;
  wire m00_axis_tvalid_w1_carry_n_2;
  wire m00_axis_tvalid_w1_carry_n_3;
  wire [31:1]m00_axis_tvalid_w2;
  wire m00_axis_tvalid_w2_carry__0_i_1_n_0;
  wire m00_axis_tvalid_w2_carry__0_i_2_n_0;
  wire m00_axis_tvalid_w2_carry__0_i_3_n_0;
  wire m00_axis_tvalid_w2_carry__0_i_4_n_0;
  wire m00_axis_tvalid_w2_carry__0_n_0;
  wire m00_axis_tvalid_w2_carry__0_n_1;
  wire m00_axis_tvalid_w2_carry__0_n_2;
  wire m00_axis_tvalid_w2_carry__0_n_3;
  wire m00_axis_tvalid_w2_carry__1_i_1_n_0;
  wire m00_axis_tvalid_w2_carry__1_i_2_n_0;
  wire m00_axis_tvalid_w2_carry__1_i_3_n_0;
  wire m00_axis_tvalid_w2_carry__1_i_4_n_0;
  wire m00_axis_tvalid_w2_carry__1_n_0;
  wire m00_axis_tvalid_w2_carry__1_n_1;
  wire m00_axis_tvalid_w2_carry__1_n_2;
  wire m00_axis_tvalid_w2_carry__1_n_3;
  wire m00_axis_tvalid_w2_carry__2_i_1_n_0;
  wire m00_axis_tvalid_w2_carry__2_i_2_n_0;
  wire m00_axis_tvalid_w2_carry__2_i_3_n_0;
  wire m00_axis_tvalid_w2_carry__2_n_0;
  wire m00_axis_tvalid_w2_carry__2_n_2;
  wire m00_axis_tvalid_w2_carry__2_n_3;
  wire m00_axis_tvalid_w2_carry_i_1_n_0;
  wire m00_axis_tvalid_w2_carry_i_2_n_0;
  wire m00_axis_tvalid_w2_carry_i_3_n_0;
  wire m00_axis_tvalid_w2_carry_i_4_n_0;
  wire m00_axis_tvalid_w2_carry_n_0;
  wire m00_axis_tvalid_w2_carry_n_1;
  wire m00_axis_tvalid_w2_carry_n_2;
  wire m00_axis_tvalid_w2_carry_n_3;
  wire m00_axis_tvalid_w_i_1_n_0;
  wire m00_axis_tvalid_w_i_2_n_0;
  wire m00_axis_tvalid_w_i_3_n_0;
  wire m00_axis_tvalid_w_i_4_n_0;
  wire m00_axis_tvalid_w_i_5_n_0;
  wire memory_array_reg_7_i_2_n_0;
  wire memory_array_reg_7_i_3_n_0;
  wire memory_array_reg_7_i_4_n_0;
  wire p_0_in;
  wire [3:0]NLW_cnt_clk_10_w0_carry_O_UNCONNECTED;
  wire [3:0]NLW_cnt_clk_10_w0_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_cnt_clk_10_w1_carry_O_UNCONNECTED;
  wire [3:2]NLW_cnt_clk_10_w1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_cnt_clk_10_w1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_cnt_clk_10_w_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:0]NLW_m00_axis_tlast_w0_carry_O_UNCONNECTED;
  wire [3:0]NLW_m00_axis_tlast_w0_carry__0_O_UNCONNECTED;
  wire [3:3]NLW_m00_axis_tlast_w0_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_m00_axis_tlast_w0_carry__1_O_UNCONNECTED;
  wire [3:0]NLW_m00_axis_tvalid_w1_carry_O_UNCONNECTED;
  wire [3:0]NLW_m00_axis_tvalid_w1_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_m00_axis_tvalid_w1_carry__1_O_UNCONNECTED;
  wire [3:0]NLW_m00_axis_tvalid_w1_carry__2_O_UNCONNECTED;
  wire [2:2]NLW_m00_axis_tvalid_w2_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_m00_axis_tvalid_w2_carry__2_O_UNCONNECTED;
  wire NLW_memory_array_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_array_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_array_reg_0_DBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_0_SBITERR_UNCONNECTED;
  wire [31:0]NLW_memory_array_reg_0_DOADO_UNCONNECTED;
  wire [31:4]NLW_memory_array_reg_0_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_array_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_array_reg_0_RDADDRECC_UNCONNECTED;
  wire NLW_memory_array_reg_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_array_reg_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_array_reg_1_DBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_1_SBITERR_UNCONNECTED;
  wire [31:0]NLW_memory_array_reg_1_DOADO_UNCONNECTED;
  wire [31:4]NLW_memory_array_reg_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_array_reg_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_array_reg_1_RDADDRECC_UNCONNECTED;
  wire NLW_memory_array_reg_2_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_array_reg_2_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_array_reg_2_DBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_2_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_2_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_2_SBITERR_UNCONNECTED;
  wire [31:0]NLW_memory_array_reg_2_DOADO_UNCONNECTED;
  wire [31:4]NLW_memory_array_reg_2_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_2_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_2_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_array_reg_2_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_array_reg_2_RDADDRECC_UNCONNECTED;
  wire NLW_memory_array_reg_3_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_array_reg_3_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_array_reg_3_DBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_3_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_3_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_3_SBITERR_UNCONNECTED;
  wire [31:0]NLW_memory_array_reg_3_DOADO_UNCONNECTED;
  wire [31:4]NLW_memory_array_reg_3_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_3_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_3_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_array_reg_3_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_array_reg_3_RDADDRECC_UNCONNECTED;
  wire NLW_memory_array_reg_4_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_array_reg_4_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_array_reg_4_DBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_4_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_4_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_4_SBITERR_UNCONNECTED;
  wire [31:0]NLW_memory_array_reg_4_DOADO_UNCONNECTED;
  wire [31:4]NLW_memory_array_reg_4_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_4_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_4_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_array_reg_4_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_array_reg_4_RDADDRECC_UNCONNECTED;
  wire NLW_memory_array_reg_5_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_array_reg_5_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_array_reg_5_DBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_5_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_5_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_5_SBITERR_UNCONNECTED;
  wire [31:0]NLW_memory_array_reg_5_DOADO_UNCONNECTED;
  wire [31:4]NLW_memory_array_reg_5_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_5_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_5_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_array_reg_5_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_array_reg_5_RDADDRECC_UNCONNECTED;
  wire NLW_memory_array_reg_6_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_array_reg_6_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_array_reg_6_DBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_6_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_6_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_6_SBITERR_UNCONNECTED;
  wire [31:0]NLW_memory_array_reg_6_DOADO_UNCONNECTED;
  wire [31:4]NLW_memory_array_reg_6_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_6_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_6_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_array_reg_6_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_array_reg_6_RDADDRECC_UNCONNECTED;
  wire NLW_memory_array_reg_7_CASCADEOUTA_UNCONNECTED;
  wire NLW_memory_array_reg_7_CASCADEOUTB_UNCONNECTED;
  wire NLW_memory_array_reg_7_DBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_7_INJECTDBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_7_INJECTSBITERR_UNCONNECTED;
  wire NLW_memory_array_reg_7_SBITERR_UNCONNECTED;
  wire [31:0]NLW_memory_array_reg_7_DOADO_UNCONNECTED;
  wire [31:4]NLW_memory_array_reg_7_DOBDO_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_7_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_memory_array_reg_7_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_memory_array_reg_7_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_memory_array_reg_7_RDADDRECC_UNCONNECTED;

  CARRY4 cnt_clk_10_w0_carry
       (.CI(1'b0),
        .CO({cnt_clk_10_w0_carry_n_0,cnt_clk_10_w0_carry_n_1,cnt_clk_10_w0_carry_n_2,cnt_clk_10_w0_carry_n_3}),
        .CYINIT(1'b1),
        .DI({cnt_clk_10_w0_carry_i_1_n_0,cnt_clk_10_w0_carry_i_2_n_0,cnt_clk_10_w0_carry_i_3_n_0,cnt_clk_10_w0_carry_i_4_n_0}),
        .O(NLW_cnt_clk_10_w0_carry_O_UNCONNECTED[3:0]),
        .S({cnt_clk_10_w0_carry_i_5_n_0,cnt_clk_10_w0_carry_i_6_n_0,cnt_clk_10_w0_carry_i_7_n_0,cnt_clk_10_w0_carry_i_8_n_0}));
  CARRY4 cnt_clk_10_w0_carry__0
       (.CI(cnt_clk_10_w0_carry_n_0),
        .CO({cnt_clk_10_w0_carry__0_n_0,cnt_clk_10_w0_carry__0_n_1,cnt_clk_10_w0_carry__0_n_2,cnt_clk_10_w0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({cnt_clk_10_w0_carry__0_i_1_n_0,cnt_clk_10_w0_carry__0_i_2_n_0,cnt_clk_10_w0_carry__0_i_3_n_0,cnt_clk_10_w0_carry__0_i_4_n_0}),
        .O(NLW_cnt_clk_10_w0_carry__0_O_UNCONNECTED[3:0]),
        .S({cnt_clk_10_w0_carry__0_i_5_n_0,cnt_clk_10_w0_carry__0_i_6_n_0,cnt_clk_10_w0_carry__0_i_7_n_0,cnt_clk_10_w0_carry__0_i_8_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    cnt_clk_10_w0_carry__0_i_1
       (.I0(FrameSize[14]),
        .I1(FrameSize[15]),
        .O(cnt_clk_10_w0_carry__0_i_1_n_0));
  LUT3 #(
    .INIT(8'hF2)) 
    cnt_clk_10_w0_carry__0_i_2
       (.I0(FrameSize[12]),
        .I1(cnt_clk_10[12]),
        .I2(FrameSize[13]),
        .O(cnt_clk_10_w0_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    cnt_clk_10_w0_carry__0_i_3
       (.I0(FrameSize[10]),
        .I1(cnt_clk_10[10]),
        .I2(cnt_clk_10[11]),
        .I3(FrameSize[11]),
        .O(cnt_clk_10_w0_carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    cnt_clk_10_w0_carry__0_i_4
       (.I0(FrameSize[8]),
        .I1(cnt_clk_10[8]),
        .I2(cnt_clk_10[9]),
        .I3(FrameSize[9]),
        .O(cnt_clk_10_w0_carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    cnt_clk_10_w0_carry__0_i_5
       (.I0(FrameSize[14]),
        .I1(FrameSize[15]),
        .O(cnt_clk_10_w0_carry__0_i_5_n_0));
  LUT3 #(
    .INIT(8'h09)) 
    cnt_clk_10_w0_carry__0_i_6
       (.I0(FrameSize[12]),
        .I1(cnt_clk_10[12]),
        .I2(FrameSize[13]),
        .O(cnt_clk_10_w0_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    cnt_clk_10_w0_carry__0_i_7
       (.I0(FrameSize[10]),
        .I1(cnt_clk_10[10]),
        .I2(FrameSize[11]),
        .I3(cnt_clk_10[11]),
        .O(cnt_clk_10_w0_carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    cnt_clk_10_w0_carry__0_i_8
       (.I0(FrameSize[8]),
        .I1(cnt_clk_10[8]),
        .I2(FrameSize[9]),
        .I3(cnt_clk_10[9]),
        .O(cnt_clk_10_w0_carry__0_i_8_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    cnt_clk_10_w0_carry_i_1
       (.I0(FrameSize[6]),
        .I1(cnt_clk_10[6]),
        .I2(cnt_clk_10[7]),
        .I3(FrameSize[7]),
        .O(cnt_clk_10_w0_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    cnt_clk_10_w0_carry_i_2
       (.I0(FrameSize[4]),
        .I1(cnt_clk_10[4]),
        .I2(cnt_clk_10[5]),
        .I3(FrameSize[5]),
        .O(cnt_clk_10_w0_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    cnt_clk_10_w0_carry_i_3
       (.I0(FrameSize[2]),
        .I1(cnt_clk_10[2]),
        .I2(cnt_clk_10[3]),
        .I3(FrameSize[3]),
        .O(cnt_clk_10_w0_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    cnt_clk_10_w0_carry_i_4
       (.I0(FrameSize[0]),
        .I1(cnt_clk_10[0]),
        .I2(cnt_clk_10[1]),
        .I3(FrameSize[1]),
        .O(cnt_clk_10_w0_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    cnt_clk_10_w0_carry_i_5
       (.I0(FrameSize[6]),
        .I1(cnt_clk_10[6]),
        .I2(FrameSize[7]),
        .I3(cnt_clk_10[7]),
        .O(cnt_clk_10_w0_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    cnt_clk_10_w0_carry_i_6
       (.I0(FrameSize[4]),
        .I1(cnt_clk_10[4]),
        .I2(FrameSize[5]),
        .I3(cnt_clk_10[5]),
        .O(cnt_clk_10_w0_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    cnt_clk_10_w0_carry_i_7
       (.I0(FrameSize[2]),
        .I1(cnt_clk_10[2]),
        .I2(FrameSize[3]),
        .I3(cnt_clk_10[3]),
        .O(cnt_clk_10_w0_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    cnt_clk_10_w0_carry_i_8
       (.I0(FrameSize[0]),
        .I1(cnt_clk_10[0]),
        .I2(FrameSize[1]),
        .I3(cnt_clk_10[1]),
        .O(cnt_clk_10_w0_carry_i_8_n_0));
  CARRY4 cnt_clk_10_w1_carry
       (.CI(1'b0),
        .CO({cnt_clk_10_w1_carry_n_0,cnt_clk_10_w1_carry_n_1,cnt_clk_10_w1_carry_n_2,cnt_clk_10_w1_carry_n_3}),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b1,1'b1}),
        .O(NLW_cnt_clk_10_w1_carry_O_UNCONNECTED[3:0]),
        .S({cnt_clk_10_w1_carry_i_1_n_0,cnt_clk_10_w1_carry_i_2_n_0,cnt_clk_10_w1_carry_i_3_n_0,cnt_clk_10_w1_carry_i_4_n_0}));
  CARRY4 cnt_clk_10_w1_carry__0
       (.CI(cnt_clk_10_w1_carry_n_0),
        .CO({NLW_cnt_clk_10_w1_carry__0_CO_UNCONNECTED[3:2],cnt_clk_10_w1_carry__0_n_2,cnt_clk_10_w1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b1,1'b1}),
        .O(NLW_cnt_clk_10_w1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,cnt_clk_10_w1_carry__0_i_1_n_0,cnt_clk_10_w1_carry__0_i_2_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    cnt_clk_10_w1_carry__0_i_1
       (.I0(FrameSize[15]),
        .O(cnt_clk_10_w1_carry__0_i_1_n_0));
  LUT4 #(
    .INIT(16'h0009)) 
    cnt_clk_10_w1_carry__0_i_2
       (.I0(cnt_clk_10[12]),
        .I1(FrameSize[12]),
        .I2(FrameSize[14]),
        .I3(FrameSize[13]),
        .O(cnt_clk_10_w1_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt_clk_10_w1_carry_i_1
       (.I0(cnt_clk_10[9]),
        .I1(FrameSize[9]),
        .I2(FrameSize[11]),
        .I3(cnt_clk_10[11]),
        .I4(FrameSize[10]),
        .I5(cnt_clk_10[10]),
        .O(cnt_clk_10_w1_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt_clk_10_w1_carry_i_2
       (.I0(cnt_clk_10[6]),
        .I1(FrameSize[6]),
        .I2(FrameSize[8]),
        .I3(cnt_clk_10[8]),
        .I4(FrameSize[7]),
        .I5(cnt_clk_10[7]),
        .O(cnt_clk_10_w1_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt_clk_10_w1_carry_i_3
       (.I0(cnt_clk_10[3]),
        .I1(FrameSize[3]),
        .I2(FrameSize[5]),
        .I3(cnt_clk_10[5]),
        .I4(FrameSize[4]),
        .I5(cnt_clk_10[4]),
        .O(cnt_clk_10_w1_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt_clk_10_w1_carry_i_4
       (.I0(cnt_clk_10[0]),
        .I1(FrameSize[0]),
        .I2(FrameSize[2]),
        .I3(cnt_clk_10[2]),
        .I4(FrameSize[1]),
        .I5(cnt_clk_10[1]),
        .O(cnt_clk_10_w1_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h4FFF)) 
    \cnt_clk_10_w[0]_i_1 
       (.I0(cnt_clk_10_w1_carry__0_n_2),
        .I1(i_write),
        .I2(m00_axis_tready),
        .I3(m00_axis_aresetn),
        .O(\cnt_clk_10_w[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \cnt_clk_10_w[0]_i_2 
       (.I0(i_write),
        .I1(cnt_clk_10_w0_carry__0_n_0),
        .O(\cnt_clk_10_w[0]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_clk_10_w[0]_i_4 
       (.I0(cnt_clk_10[0]),
        .O(\cnt_clk_10_w[0]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_clk_10_w_reg[0] 
       (.C(clk_W10_R100),
        .CE(\cnt_clk_10_w[0]_i_2_n_0 ),
        .D(\cnt_clk_10_w_reg[0]_i_3_n_7 ),
        .Q(cnt_clk_10[0]),
        .R(\cnt_clk_10_w[0]_i_1_n_0 ));
  CARRY4 \cnt_clk_10_w_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\cnt_clk_10_w_reg[0]_i_3_n_0 ,\cnt_clk_10_w_reg[0]_i_3_n_1 ,\cnt_clk_10_w_reg[0]_i_3_n_2 ,\cnt_clk_10_w_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_clk_10_w_reg[0]_i_3_n_4 ,\cnt_clk_10_w_reg[0]_i_3_n_5 ,\cnt_clk_10_w_reg[0]_i_3_n_6 ,\cnt_clk_10_w_reg[0]_i_3_n_7 }),
        .S({cnt_clk_10[3:1],\cnt_clk_10_w[0]_i_4_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_clk_10_w_reg[10] 
       (.C(clk_W10_R100),
        .CE(\cnt_clk_10_w[0]_i_2_n_0 ),
        .D(\cnt_clk_10_w_reg[8]_i_1_n_5 ),
        .Q(cnt_clk_10[10]),
        .R(\cnt_clk_10_w[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_clk_10_w_reg[11] 
       (.C(clk_W10_R100),
        .CE(\cnt_clk_10_w[0]_i_2_n_0 ),
        .D(\cnt_clk_10_w_reg[8]_i_1_n_4 ),
        .Q(cnt_clk_10[11]),
        .R(\cnt_clk_10_w[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_clk_10_w_reg[12] 
       (.C(clk_W10_R100),
        .CE(\cnt_clk_10_w[0]_i_2_n_0 ),
        .D(\cnt_clk_10_w_reg[12]_i_1_n_7 ),
        .Q(cnt_clk_10[12]),
        .R(\cnt_clk_10_w[0]_i_1_n_0 ));
  CARRY4 \cnt_clk_10_w_reg[12]_i_1 
       (.CI(\cnt_clk_10_w_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt_clk_10_w_reg[12]_i_1_CO_UNCONNECTED [3],\cnt_clk_10_w_reg[12]_i_1_n_1 ,\cnt_clk_10_w_reg[12]_i_1_n_2 ,\cnt_clk_10_w_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_clk_10_w_reg[12]_i_1_n_4 ,\cnt_clk_10_w_reg[12]_i_1_n_5 ,\cnt_clk_10_w_reg[12]_i_1_n_6 ,\cnt_clk_10_w_reg[12]_i_1_n_7 }),
        .S({\cnt_clk_10_w_reg_n_0_[15] ,\cnt_clk_10_w_reg_n_0_[14] ,\cnt_clk_10_w_reg_n_0_[13] ,cnt_clk_10[12]}));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_clk_10_w_reg[13] 
       (.C(clk_W10_R100),
        .CE(\cnt_clk_10_w[0]_i_2_n_0 ),
        .D(\cnt_clk_10_w_reg[12]_i_1_n_6 ),
        .Q(\cnt_clk_10_w_reg_n_0_[13] ),
        .R(\cnt_clk_10_w[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_clk_10_w_reg[14] 
       (.C(clk_W10_R100),
        .CE(\cnt_clk_10_w[0]_i_2_n_0 ),
        .D(\cnt_clk_10_w_reg[12]_i_1_n_5 ),
        .Q(\cnt_clk_10_w_reg_n_0_[14] ),
        .R(\cnt_clk_10_w[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_clk_10_w_reg[15] 
       (.C(clk_W10_R100),
        .CE(\cnt_clk_10_w[0]_i_2_n_0 ),
        .D(\cnt_clk_10_w_reg[12]_i_1_n_4 ),
        .Q(\cnt_clk_10_w_reg_n_0_[15] ),
        .R(\cnt_clk_10_w[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_clk_10_w_reg[1] 
       (.C(clk_W10_R100),
        .CE(\cnt_clk_10_w[0]_i_2_n_0 ),
        .D(\cnt_clk_10_w_reg[0]_i_3_n_6 ),
        .Q(cnt_clk_10[1]),
        .R(\cnt_clk_10_w[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_clk_10_w_reg[2] 
       (.C(clk_W10_R100),
        .CE(\cnt_clk_10_w[0]_i_2_n_0 ),
        .D(\cnt_clk_10_w_reg[0]_i_3_n_5 ),
        .Q(cnt_clk_10[2]),
        .R(\cnt_clk_10_w[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_clk_10_w_reg[3] 
       (.C(clk_W10_R100),
        .CE(\cnt_clk_10_w[0]_i_2_n_0 ),
        .D(\cnt_clk_10_w_reg[0]_i_3_n_4 ),
        .Q(cnt_clk_10[3]),
        .R(\cnt_clk_10_w[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_clk_10_w_reg[4] 
       (.C(clk_W10_R100),
        .CE(\cnt_clk_10_w[0]_i_2_n_0 ),
        .D(\cnt_clk_10_w_reg[4]_i_1_n_7 ),
        .Q(cnt_clk_10[4]),
        .R(\cnt_clk_10_w[0]_i_1_n_0 ));
  CARRY4 \cnt_clk_10_w_reg[4]_i_1 
       (.CI(\cnt_clk_10_w_reg[0]_i_3_n_0 ),
        .CO({\cnt_clk_10_w_reg[4]_i_1_n_0 ,\cnt_clk_10_w_reg[4]_i_1_n_1 ,\cnt_clk_10_w_reg[4]_i_1_n_2 ,\cnt_clk_10_w_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_clk_10_w_reg[4]_i_1_n_4 ,\cnt_clk_10_w_reg[4]_i_1_n_5 ,\cnt_clk_10_w_reg[4]_i_1_n_6 ,\cnt_clk_10_w_reg[4]_i_1_n_7 }),
        .S(cnt_clk_10[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_clk_10_w_reg[5] 
       (.C(clk_W10_R100),
        .CE(\cnt_clk_10_w[0]_i_2_n_0 ),
        .D(\cnt_clk_10_w_reg[4]_i_1_n_6 ),
        .Q(cnt_clk_10[5]),
        .R(\cnt_clk_10_w[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_clk_10_w_reg[6] 
       (.C(clk_W10_R100),
        .CE(\cnt_clk_10_w[0]_i_2_n_0 ),
        .D(\cnt_clk_10_w_reg[4]_i_1_n_5 ),
        .Q(cnt_clk_10[6]),
        .R(\cnt_clk_10_w[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_clk_10_w_reg[7] 
       (.C(clk_W10_R100),
        .CE(\cnt_clk_10_w[0]_i_2_n_0 ),
        .D(\cnt_clk_10_w_reg[4]_i_1_n_4 ),
        .Q(cnt_clk_10[7]),
        .R(\cnt_clk_10_w[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_clk_10_w_reg[8] 
       (.C(clk_W10_R100),
        .CE(\cnt_clk_10_w[0]_i_2_n_0 ),
        .D(\cnt_clk_10_w_reg[8]_i_1_n_7 ),
        .Q(cnt_clk_10[8]),
        .R(\cnt_clk_10_w[0]_i_1_n_0 ));
  CARRY4 \cnt_clk_10_w_reg[8]_i_1 
       (.CI(\cnt_clk_10_w_reg[4]_i_1_n_0 ),
        .CO({\cnt_clk_10_w_reg[8]_i_1_n_0 ,\cnt_clk_10_w_reg[8]_i_1_n_1 ,\cnt_clk_10_w_reg[8]_i_1_n_2 ,\cnt_clk_10_w_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_clk_10_w_reg[8]_i_1_n_4 ,\cnt_clk_10_w_reg[8]_i_1_n_5 ,\cnt_clk_10_w_reg[8]_i_1_n_6 ,\cnt_clk_10_w_reg[8]_i_1_n_7 }),
        .S(cnt_clk_10[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_clk_10_w_reg[9] 
       (.C(clk_W10_R100),
        .CE(\cnt_clk_10_w[0]_i_2_n_0 ),
        .D(\cnt_clk_10_w_reg[8]_i_1_n_6 ),
        .Q(cnt_clk_10[9]),
        .R(\cnt_clk_10_w[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \i_addr[12]_i_1 
       (.I0(m00_axis_tready),
        .I1(m00_axis_aresetn),
        .O(\i_addr[12]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \i_addr_reg[0] 
       (.C(clk_W10_R100),
        .CE(1'b1),
        .D(cnt_clk_10[0]),
        .Q(i_addr[0]),
        .R(\i_addr[12]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \i_addr_reg[10] 
       (.C(clk_W10_R100),
        .CE(1'b1),
        .D(cnt_clk_10[10]),
        .Q(i_addr[10]),
        .R(\i_addr[12]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \i_addr_reg[11] 
       (.C(clk_W10_R100),
        .CE(1'b1),
        .D(cnt_clk_10[11]),
        .Q(i_addr[11]),
        .R(\i_addr[12]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \i_addr_reg[12] 
       (.C(clk_W10_R100),
        .CE(1'b1),
        .D(cnt_clk_10[12]),
        .Q(i_addr[12]),
        .R(\i_addr[12]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \i_addr_reg[1] 
       (.C(clk_W10_R100),
        .CE(1'b1),
        .D(cnt_clk_10[1]),
        .Q(i_addr[1]),
        .R(\i_addr[12]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \i_addr_reg[2] 
       (.C(clk_W10_R100),
        .CE(1'b1),
        .D(cnt_clk_10[2]),
        .Q(i_addr[2]),
        .R(\i_addr[12]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \i_addr_reg[3] 
       (.C(clk_W10_R100),
        .CE(1'b1),
        .D(cnt_clk_10[3]),
        .Q(i_addr[3]),
        .R(\i_addr[12]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \i_addr_reg[4] 
       (.C(clk_W10_R100),
        .CE(1'b1),
        .D(cnt_clk_10[4]),
        .Q(i_addr[4]),
        .R(\i_addr[12]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \i_addr_reg[5] 
       (.C(clk_W10_R100),
        .CE(1'b1),
        .D(cnt_clk_10[5]),
        .Q(i_addr[5]),
        .R(\i_addr[12]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \i_addr_reg[6] 
       (.C(clk_W10_R100),
        .CE(1'b1),
        .D(cnt_clk_10[6]),
        .Q(i_addr[6]),
        .R(\i_addr[12]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \i_addr_reg[7] 
       (.C(clk_W10_R100),
        .CE(1'b1),
        .D(cnt_clk_10[7]),
        .Q(i_addr[7]),
        .R(\i_addr[12]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \i_addr_reg[8] 
       (.C(clk_W10_R100),
        .CE(1'b1),
        .D(cnt_clk_10[8]),
        .Q(i_addr[8]),
        .R(\i_addr[12]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \i_addr_reg[9] 
       (.C(clk_W10_R100),
        .CE(1'b1),
        .D(cnt_clk_10[9]),
        .Q(i_addr[9]),
        .R(\i_addr[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h8FFF)) 
    i_write_i_1
       (.I0(i_write),
        .I1(cnt_clk_10_w1_carry__0_n_2),
        .I2(m00_axis_aresetn),
        .I3(m00_axis_tready),
        .O(i_write_i_1_n_0));
  FDRE i_write_reg
       (.C(clk_W10_R100),
        .CE(1'b1),
        .D(i_write_i_1_n_0),
        .Q(i_write),
        .R(1'b0));
  CARRY4 m00_axis_tlast_w0_carry
       (.CI(1'b0),
        .CO({m00_axis_tlast_w0_carry_n_0,m00_axis_tlast_w0_carry_n_1,m00_axis_tlast_w0_carry_n_2,m00_axis_tlast_w0_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_m00_axis_tlast_w0_carry_O_UNCONNECTED[3:0]),
        .S({m00_axis_tlast_w0_carry_i_1_n_0,m00_axis_tlast_w0_carry_i_2_n_0,m00_axis_tlast_w0_carry_i_3_n_0,m00_axis_tlast_w0_carry_i_4_n_0}));
  CARRY4 m00_axis_tlast_w0_carry__0
       (.CI(m00_axis_tlast_w0_carry_n_0),
        .CO({m00_axis_tlast_w0_carry__0_n_0,m00_axis_tlast_w0_carry__0_n_1,m00_axis_tlast_w0_carry__0_n_2,m00_axis_tlast_w0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_m00_axis_tlast_w0_carry__0_O_UNCONNECTED[3:0]),
        .S({m00_axis_tvalid_w2_carry__2_n_0,m00_axis_tvalid_w2_carry__2_n_0,m00_axis_tlast_w0_carry__0_i_1_n_0,m00_axis_tlast_w0_carry__0_i_2_n_0}));
  LUT3 #(
    .INIT(8'h90)) 
    m00_axis_tlast_w0_carry__0_i_1
       (.I0(\cnt_clk_10_w_reg_n_0_[15] ),
        .I1(m00_axis_tvalid_w2[15]),
        .I2(m00_axis_tvalid_w2_carry__2_n_0),
        .O(m00_axis_tlast_w0_carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    m00_axis_tlast_w0_carry__0_i_2
       (.I0(cnt_clk_10[12]),
        .I1(m00_axis_tvalid_w2[12]),
        .I2(m00_axis_tvalid_w2[14]),
        .I3(\cnt_clk_10_w_reg_n_0_[14] ),
        .I4(m00_axis_tvalid_w2[13]),
        .I5(\cnt_clk_10_w_reg_n_0_[13] ),
        .O(m00_axis_tlast_w0_carry__0_i_2_n_0));
  CARRY4 m00_axis_tlast_w0_carry__1
       (.CI(m00_axis_tlast_w0_carry__0_n_0),
        .CO({NLW_m00_axis_tlast_w0_carry__1_CO_UNCONNECTED[3],p_0_in,m00_axis_tlast_w0_carry__1_n_2,m00_axis_tlast_w0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_m00_axis_tlast_w0_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,m00_axis_tvalid_w2_carry__2_n_0,m00_axis_tvalid_w2_carry__2_n_0,m00_axis_tvalid_w2_carry__2_n_0}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    m00_axis_tlast_w0_carry_i_1
       (.I0(cnt_clk_10[9]),
        .I1(m00_axis_tvalid_w2[9]),
        .I2(m00_axis_tvalid_w2[11]),
        .I3(cnt_clk_10[11]),
        .I4(m00_axis_tvalid_w2[10]),
        .I5(cnt_clk_10[10]),
        .O(m00_axis_tlast_w0_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    m00_axis_tlast_w0_carry_i_2
       (.I0(cnt_clk_10[6]),
        .I1(m00_axis_tvalid_w2[6]),
        .I2(m00_axis_tvalid_w2[8]),
        .I3(cnt_clk_10[8]),
        .I4(m00_axis_tvalid_w2[7]),
        .I5(cnt_clk_10[7]),
        .O(m00_axis_tlast_w0_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    m00_axis_tlast_w0_carry_i_3
       (.I0(cnt_clk_10[3]),
        .I1(m00_axis_tvalid_w2[3]),
        .I2(m00_axis_tvalid_w2[5]),
        .I3(cnt_clk_10[5]),
        .I4(m00_axis_tvalid_w2[4]),
        .I5(cnt_clk_10[4]),
        .O(m00_axis_tlast_w0_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000900990090000)) 
    m00_axis_tlast_w0_carry_i_4
       (.I0(m00_axis_tvalid_w2[2]),
        .I1(cnt_clk_10[2]),
        .I2(m00_axis_tvalid_w2[1]),
        .I3(cnt_clk_10[1]),
        .I4(FrameSize[0]),
        .I5(cnt_clk_10[0]),
        .O(m00_axis_tlast_w0_carry_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    m00_axis_tlast_w_i_1
       (.I0(p_0_in),
        .I1(m00_axis_tready),
        .I2(m00_axis_aresetn),
        .I3(i_write),
        .O(m00_axis_tlast_w_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    m00_axis_tlast_w_reg
       (.C(clk_W10_R100),
        .CE(1'b1),
        .D(m00_axis_tlast_w_i_1_n_0),
        .Q(m00_axis_tlast),
        .R(1'b0));
  CARRY4 m00_axis_tvalid_w1_carry
       (.CI(1'b0),
        .CO({m00_axis_tvalid_w1_carry_n_0,m00_axis_tvalid_w1_carry_n_1,m00_axis_tvalid_w1_carry_n_2,m00_axis_tvalid_w1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({m00_axis_tvalid_w1_carry_i_1_n_0,m00_axis_tvalid_w1_carry_i_2_n_0,m00_axis_tvalid_w1_carry_i_3_n_0,m00_axis_tvalid_w1_carry_i_4_n_0}),
        .O(NLW_m00_axis_tvalid_w1_carry_O_UNCONNECTED[3:0]),
        .S({m00_axis_tvalid_w1_carry_i_5_n_0,m00_axis_tvalid_w1_carry_i_6_n_0,m00_axis_tvalid_w1_carry_i_7_n_0,m00_axis_tvalid_w1_carry_i_8_n_0}));
  CARRY4 m00_axis_tvalid_w1_carry__0
       (.CI(m00_axis_tvalid_w1_carry_n_0),
        .CO({m00_axis_tvalid_w1_carry__0_n_0,m00_axis_tvalid_w1_carry__0_n_1,m00_axis_tvalid_w1_carry__0_n_2,m00_axis_tvalid_w1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({m00_axis_tvalid_w1_carry__0_i_1_n_0,m00_axis_tvalid_w1_carry__0_i_2_n_0,m00_axis_tvalid_w1_carry__0_i_3_n_0,m00_axis_tvalid_w1_carry__0_i_4_n_0}),
        .O(NLW_m00_axis_tvalid_w1_carry__0_O_UNCONNECTED[3:0]),
        .S({m00_axis_tvalid_w1_carry__0_i_5_n_0,m00_axis_tvalid_w1_carry__0_i_6_n_0,m00_axis_tvalid_w1_carry__0_i_7_n_0,m00_axis_tvalid_w1_carry__0_i_8_n_0}));
  LUT4 #(
    .INIT(16'h2F02)) 
    m00_axis_tvalid_w1_carry__0_i_1
       (.I0(m00_axis_tvalid_w2[14]),
        .I1(\cnt_clk_10_w_reg_n_0_[14] ),
        .I2(\cnt_clk_10_w_reg_n_0_[15] ),
        .I3(m00_axis_tvalid_w2[15]),
        .O(m00_axis_tvalid_w1_carry__0_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    m00_axis_tvalid_w1_carry__0_i_2
       (.I0(m00_axis_tvalid_w2[12]),
        .I1(cnt_clk_10[12]),
        .I2(\cnt_clk_10_w_reg_n_0_[13] ),
        .I3(m00_axis_tvalid_w2[13]),
        .O(m00_axis_tvalid_w1_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    m00_axis_tvalid_w1_carry__0_i_3
       (.I0(m00_axis_tvalid_w2[10]),
        .I1(cnt_clk_10[10]),
        .I2(cnt_clk_10[11]),
        .I3(m00_axis_tvalid_w2[11]),
        .O(m00_axis_tvalid_w1_carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    m00_axis_tvalid_w1_carry__0_i_4
       (.I0(m00_axis_tvalid_w2[8]),
        .I1(cnt_clk_10[8]),
        .I2(cnt_clk_10[9]),
        .I3(m00_axis_tvalid_w2[9]),
        .O(m00_axis_tvalid_w1_carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    m00_axis_tvalid_w1_carry__0_i_5
       (.I0(m00_axis_tvalid_w2[14]),
        .I1(\cnt_clk_10_w_reg_n_0_[14] ),
        .I2(m00_axis_tvalid_w2[15]),
        .I3(\cnt_clk_10_w_reg_n_0_[15] ),
        .O(m00_axis_tvalid_w1_carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    m00_axis_tvalid_w1_carry__0_i_6
       (.I0(m00_axis_tvalid_w2[12]),
        .I1(cnt_clk_10[12]),
        .I2(m00_axis_tvalid_w2[13]),
        .I3(\cnt_clk_10_w_reg_n_0_[13] ),
        .O(m00_axis_tvalid_w1_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    m00_axis_tvalid_w1_carry__0_i_7
       (.I0(m00_axis_tvalid_w2[10]),
        .I1(cnt_clk_10[10]),
        .I2(m00_axis_tvalid_w2[11]),
        .I3(cnt_clk_10[11]),
        .O(m00_axis_tvalid_w1_carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    m00_axis_tvalid_w1_carry__0_i_8
       (.I0(m00_axis_tvalid_w2[8]),
        .I1(cnt_clk_10[8]),
        .I2(m00_axis_tvalid_w2[9]),
        .I3(cnt_clk_10[9]),
        .O(m00_axis_tvalid_w1_carry__0_i_8_n_0));
  CARRY4 m00_axis_tvalid_w1_carry__1
       (.CI(m00_axis_tvalid_w1_carry__0_n_0),
        .CO({m00_axis_tvalid_w1_carry__1_n_0,m00_axis_tvalid_w1_carry__1_n_1,m00_axis_tvalid_w1_carry__1_n_2,m00_axis_tvalid_w1_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({m00_axis_tvalid_w1_carry__1_i_1_n_0,m00_axis_tvalid_w1_carry__1_i_2_n_0,m00_axis_tvalid_w1_carry__1_i_3_n_0,m00_axis_tvalid_w1_carry__1_i_4_n_0}),
        .O(NLW_m00_axis_tvalid_w1_carry__1_O_UNCONNECTED[3:0]),
        .S({m00_axis_tvalid_w2_carry__2_n_0,m00_axis_tvalid_w2_carry__2_n_0,m00_axis_tvalid_w2_carry__2_n_0,m00_axis_tvalid_w2_carry__2_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w1_carry__1_i_1
       (.I0(m00_axis_tvalid_w2_carry__2_n_0),
        .O(m00_axis_tvalid_w1_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w1_carry__1_i_2
       (.I0(m00_axis_tvalid_w2_carry__2_n_0),
        .O(m00_axis_tvalid_w1_carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w1_carry__1_i_3
       (.I0(m00_axis_tvalid_w2_carry__2_n_0),
        .O(m00_axis_tvalid_w1_carry__1_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w1_carry__1_i_4
       (.I0(m00_axis_tvalid_w2_carry__2_n_0),
        .O(m00_axis_tvalid_w1_carry__1_i_4_n_0));
  CARRY4 m00_axis_tvalid_w1_carry__2
       (.CI(m00_axis_tvalid_w1_carry__1_n_0),
        .CO({m00_axis_tvalid_w1,m00_axis_tvalid_w1_carry__2_n_1,m00_axis_tvalid_w1_carry__2_n_2,m00_axis_tvalid_w1_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({m00_axis_tvalid_w2[31],m00_axis_tvalid_w1_carry__2_i_2_n_0,m00_axis_tvalid_w1_carry__2_i_3_n_0,m00_axis_tvalid_w1_carry__2_i_4_n_0}),
        .O(NLW_m00_axis_tvalid_w1_carry__2_O_UNCONNECTED[3:0]),
        .S({m00_axis_tvalid_w2_carry__2_n_0,m00_axis_tvalid_w2_carry__2_n_0,m00_axis_tvalid_w2_carry__2_n_0,m00_axis_tvalid_w2_carry__2_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w1_carry__2_i_1
       (.I0(m00_axis_tvalid_w2_carry__2_n_0),
        .O(m00_axis_tvalid_w2[31]));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w1_carry__2_i_2
       (.I0(m00_axis_tvalid_w2_carry__2_n_0),
        .O(m00_axis_tvalid_w1_carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w1_carry__2_i_3
       (.I0(m00_axis_tvalid_w2_carry__2_n_0),
        .O(m00_axis_tvalid_w1_carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w1_carry__2_i_4
       (.I0(m00_axis_tvalid_w2_carry__2_n_0),
        .O(m00_axis_tvalid_w1_carry__2_i_4_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    m00_axis_tvalid_w1_carry_i_1
       (.I0(m00_axis_tvalid_w2[6]),
        .I1(cnt_clk_10[6]),
        .I2(cnt_clk_10[7]),
        .I3(m00_axis_tvalid_w2[7]),
        .O(m00_axis_tvalid_w1_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    m00_axis_tvalid_w1_carry_i_2
       (.I0(m00_axis_tvalid_w2[4]),
        .I1(cnt_clk_10[4]),
        .I2(cnt_clk_10[5]),
        .I3(m00_axis_tvalid_w2[5]),
        .O(m00_axis_tvalid_w1_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    m00_axis_tvalid_w1_carry_i_3
       (.I0(m00_axis_tvalid_w2[2]),
        .I1(cnt_clk_10[2]),
        .I2(cnt_clk_10[3]),
        .I3(m00_axis_tvalid_w2[3]),
        .O(m00_axis_tvalid_w1_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h1F01)) 
    m00_axis_tvalid_w1_carry_i_4
       (.I0(cnt_clk_10[0]),
        .I1(FrameSize[0]),
        .I2(cnt_clk_10[1]),
        .I3(m00_axis_tvalid_w2[1]),
        .O(m00_axis_tvalid_w1_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    m00_axis_tvalid_w1_carry_i_5
       (.I0(m00_axis_tvalid_w2[6]),
        .I1(cnt_clk_10[6]),
        .I2(m00_axis_tvalid_w2[7]),
        .I3(cnt_clk_10[7]),
        .O(m00_axis_tvalid_w1_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    m00_axis_tvalid_w1_carry_i_6
       (.I0(m00_axis_tvalid_w2[4]),
        .I1(cnt_clk_10[4]),
        .I2(m00_axis_tvalid_w2[5]),
        .I3(cnt_clk_10[5]),
        .O(m00_axis_tvalid_w1_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    m00_axis_tvalid_w1_carry_i_7
       (.I0(m00_axis_tvalid_w2[2]),
        .I1(cnt_clk_10[2]),
        .I2(m00_axis_tvalid_w2[3]),
        .I3(cnt_clk_10[3]),
        .O(m00_axis_tvalid_w1_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h0990)) 
    m00_axis_tvalid_w1_carry_i_8
       (.I0(m00_axis_tvalid_w2[1]),
        .I1(cnt_clk_10[1]),
        .I2(cnt_clk_10[0]),
        .I3(FrameSize[0]),
        .O(m00_axis_tvalid_w1_carry_i_8_n_0));
  CARRY4 m00_axis_tvalid_w2_carry
       (.CI(1'b0),
        .CO({m00_axis_tvalid_w2_carry_n_0,m00_axis_tvalid_w2_carry_n_1,m00_axis_tvalid_w2_carry_n_2,m00_axis_tvalid_w2_carry_n_3}),
        .CYINIT(FrameSize[0]),
        .DI(FrameSize[4:1]),
        .O(m00_axis_tvalid_w2[4:1]),
        .S({m00_axis_tvalid_w2_carry_i_1_n_0,m00_axis_tvalid_w2_carry_i_2_n_0,m00_axis_tvalid_w2_carry_i_3_n_0,m00_axis_tvalid_w2_carry_i_4_n_0}));
  CARRY4 m00_axis_tvalid_w2_carry__0
       (.CI(m00_axis_tvalid_w2_carry_n_0),
        .CO({m00_axis_tvalid_w2_carry__0_n_0,m00_axis_tvalid_w2_carry__0_n_1,m00_axis_tvalid_w2_carry__0_n_2,m00_axis_tvalid_w2_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(FrameSize[8:5]),
        .O(m00_axis_tvalid_w2[8:5]),
        .S({m00_axis_tvalid_w2_carry__0_i_1_n_0,m00_axis_tvalid_w2_carry__0_i_2_n_0,m00_axis_tvalid_w2_carry__0_i_3_n_0,m00_axis_tvalid_w2_carry__0_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w2_carry__0_i_1
       (.I0(FrameSize[8]),
        .O(m00_axis_tvalid_w2_carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w2_carry__0_i_2
       (.I0(FrameSize[7]),
        .O(m00_axis_tvalid_w2_carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w2_carry__0_i_3
       (.I0(FrameSize[6]),
        .O(m00_axis_tvalid_w2_carry__0_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w2_carry__0_i_4
       (.I0(FrameSize[5]),
        .O(m00_axis_tvalid_w2_carry__0_i_4_n_0));
  CARRY4 m00_axis_tvalid_w2_carry__1
       (.CI(m00_axis_tvalid_w2_carry__0_n_0),
        .CO({m00_axis_tvalid_w2_carry__1_n_0,m00_axis_tvalid_w2_carry__1_n_1,m00_axis_tvalid_w2_carry__1_n_2,m00_axis_tvalid_w2_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(FrameSize[12:9]),
        .O(m00_axis_tvalid_w2[12:9]),
        .S({m00_axis_tvalid_w2_carry__1_i_1_n_0,m00_axis_tvalid_w2_carry__1_i_2_n_0,m00_axis_tvalid_w2_carry__1_i_3_n_0,m00_axis_tvalid_w2_carry__1_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w2_carry__1_i_1
       (.I0(FrameSize[12]),
        .O(m00_axis_tvalid_w2_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w2_carry__1_i_2
       (.I0(FrameSize[11]),
        .O(m00_axis_tvalid_w2_carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w2_carry__1_i_3
       (.I0(FrameSize[10]),
        .O(m00_axis_tvalid_w2_carry__1_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w2_carry__1_i_4
       (.I0(FrameSize[9]),
        .O(m00_axis_tvalid_w2_carry__1_i_4_n_0));
  CARRY4 m00_axis_tvalid_w2_carry__2
       (.CI(m00_axis_tvalid_w2_carry__1_n_0),
        .CO({m00_axis_tvalid_w2_carry__2_n_0,NLW_m00_axis_tvalid_w2_carry__2_CO_UNCONNECTED[2],m00_axis_tvalid_w2_carry__2_n_2,m00_axis_tvalid_w2_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,FrameSize[15:13]}),
        .O({NLW_m00_axis_tvalid_w2_carry__2_O_UNCONNECTED[3],m00_axis_tvalid_w2[15:13]}),
        .S({1'b1,m00_axis_tvalid_w2_carry__2_i_1_n_0,m00_axis_tvalid_w2_carry__2_i_2_n_0,m00_axis_tvalid_w2_carry__2_i_3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w2_carry__2_i_1
       (.I0(FrameSize[15]),
        .O(m00_axis_tvalid_w2_carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w2_carry__2_i_2
       (.I0(FrameSize[14]),
        .O(m00_axis_tvalid_w2_carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w2_carry__2_i_3
       (.I0(FrameSize[13]),
        .O(m00_axis_tvalid_w2_carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w2_carry_i_1
       (.I0(FrameSize[4]),
        .O(m00_axis_tvalid_w2_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w2_carry_i_2
       (.I0(FrameSize[3]),
        .O(m00_axis_tvalid_w2_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w2_carry_i_3
       (.I0(FrameSize[2]),
        .O(m00_axis_tvalid_w2_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tvalid_w2_carry_i_4
       (.I0(FrameSize[1]),
        .O(m00_axis_tvalid_w2_carry_i_4_n_0));
  LUT6 #(
    .INIT(64'h00000000A8000000)) 
    m00_axis_tvalid_w_i_1
       (.I0(m00_axis_tvalid_w1),
        .I1(m00_axis_tvalid_w_i_2_n_0),
        .I2(m00_axis_tvalid_w_i_3_n_0),
        .I3(m00_axis_tready),
        .I4(m00_axis_aresetn),
        .I5(i_write),
        .O(m00_axis_tvalid_w_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    m00_axis_tvalid_w_i_2
       (.I0(cnt_clk_10[3]),
        .I1(cnt_clk_10[4]),
        .I2(cnt_clk_10[1]),
        .I3(cnt_clk_10[2]),
        .I4(m00_axis_tvalid_w_i_4_n_0),
        .O(m00_axis_tvalid_w_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    m00_axis_tvalid_w_i_3
       (.I0(cnt_clk_10[11]),
        .I1(cnt_clk_10[12]),
        .I2(cnt_clk_10[9]),
        .I3(cnt_clk_10[10]),
        .I4(m00_axis_tvalid_w_i_5_n_0),
        .O(m00_axis_tvalid_w_i_3_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_axis_tvalid_w_i_4
       (.I0(cnt_clk_10[6]),
        .I1(cnt_clk_10[5]),
        .I2(cnt_clk_10[8]),
        .I3(cnt_clk_10[7]),
        .O(m00_axis_tvalid_w_i_4_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_axis_tvalid_w_i_5
       (.I0(\cnt_clk_10_w_reg_n_0_[14] ),
        .I1(\cnt_clk_10_w_reg_n_0_[13] ),
        .I2(cnt_clk_10[0]),
        .I3(\cnt_clk_10_w_reg_n_0_[15] ),
        .O(m00_axis_tvalid_w_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    m00_axis_tvalid_w_reg
       (.C(clk_W10_R100),
        .CE(1'b1),
        .D(m00_axis_tvalid_w_i_1_n_0),
        .Q(m00_axis_tvalid),
        .R(1'b0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "262144" *) 
  (* RTL_RAM_NAME = "memory_array" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "3" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "3" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(4),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(4)) 
    memory_array_reg_0
       (.ADDRARDADDR({1'b1,i_addr,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,i_addr,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_memory_array_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_array_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(clk_W10_R100),
        .CLKBWRCLK(clk_W10_R100),
        .DBITERR(NLW_memory_array_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,data_in_ram[3:0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_memory_array_reg_0_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_memory_array_reg_0_DOBDO_UNCONNECTED[31:4],m00_axis_tdata[3:0]}),
        .DOPADOP(NLW_memory_array_reg_0_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_array_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_array_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(memory_array_reg_7_i_2_n_0),
        .ENBWREN(memory_array_reg_7_i_3_n_0),
        .INJECTDBITERR(NLW_memory_array_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_array_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_array_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(memory_array_reg_7_i_4_n_0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_array_reg_0_SBITERR_UNCONNECTED),
        .WEA({m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_0_i_1
       (.I0(i_write),
        .I1(data_in_IF1[3]),
        .O(data_in_ram[3]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_0_i_2
       (.I0(i_write),
        .I1(data_in_IF1[2]),
        .O(data_in_ram[2]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_0_i_3
       (.I0(i_write),
        .I1(data_in_IF1[1]),
        .O(data_in_ram[1]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_0_i_4
       (.I0(i_write),
        .I1(data_in_IF1[0]),
        .O(data_in_ram[0]));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "262144" *) 
  (* RTL_RAM_NAME = "memory_array" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "4" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "4" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(4),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(4)) 
    memory_array_reg_1
       (.ADDRARDADDR({1'b1,i_addr,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,i_addr,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_memory_array_reg_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_array_reg_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(clk_W10_R100),
        .CLKBWRCLK(clk_W10_R100),
        .DBITERR(NLW_memory_array_reg_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,data_in_ram[7:4]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_memory_array_reg_1_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_memory_array_reg_1_DOBDO_UNCONNECTED[31:4],m00_axis_tdata[7:4]}),
        .DOPADOP(NLW_memory_array_reg_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_array_reg_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_array_reg_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(memory_array_reg_7_i_2_n_0),
        .ENBWREN(memory_array_reg_7_i_3_n_0),
        .INJECTDBITERR(NLW_memory_array_reg_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_array_reg_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_array_reg_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(memory_array_reg_7_i_4_n_0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_array_reg_1_SBITERR_UNCONNECTED),
        .WEA({m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_1_i_1
       (.I0(i_write),
        .I1(data_in_IF1[7]),
        .O(data_in_ram[7]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_1_i_2
       (.I0(i_write),
        .I1(data_in_IF1[6]),
        .O(data_in_ram[6]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_1_i_3
       (.I0(i_write),
        .I1(data_in_IF1[5]),
        .O(data_in_ram[5]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_1_i_4
       (.I0(i_write),
        .I1(data_in_IF1[4]),
        .O(data_in_ram[4]));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "262144" *) 
  (* RTL_RAM_NAME = "memory_array" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "8" *) 
  (* bram_slice_end = "11" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "8" *) 
  (* ram_slice_end = "11" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(4),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(4)) 
    memory_array_reg_2
       (.ADDRARDADDR({1'b1,i_addr,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,i_addr,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_memory_array_reg_2_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_array_reg_2_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(clk_W10_R100),
        .CLKBWRCLK(clk_W10_R100),
        .DBITERR(NLW_memory_array_reg_2_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,data_in_ram[11:8]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_memory_array_reg_2_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_memory_array_reg_2_DOBDO_UNCONNECTED[31:4],m00_axis_tdata[11:8]}),
        .DOPADOP(NLW_memory_array_reg_2_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_array_reg_2_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_array_reg_2_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(memory_array_reg_7_i_2_n_0),
        .ENBWREN(memory_array_reg_7_i_3_n_0),
        .INJECTDBITERR(NLW_memory_array_reg_2_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_array_reg_2_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_array_reg_2_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(memory_array_reg_7_i_4_n_0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_array_reg_2_SBITERR_UNCONNECTED),
        .WEA({m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_2_i_1
       (.I0(i_write),
        .I1(data_in_IF1[11]),
        .O(data_in_ram[11]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_2_i_2
       (.I0(i_write),
        .I1(data_in_IF1[10]),
        .O(data_in_ram[10]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_2_i_3
       (.I0(i_write),
        .I1(data_in_IF1[9]),
        .O(data_in_ram[9]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_2_i_4
       (.I0(i_write),
        .I1(data_in_IF1[8]),
        .O(data_in_ram[8]));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "262144" *) 
  (* RTL_RAM_NAME = "memory_array" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "12" *) 
  (* bram_slice_end = "15" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "15" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(4),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(4)) 
    memory_array_reg_3
       (.ADDRARDADDR({1'b1,i_addr,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,i_addr,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_memory_array_reg_3_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_array_reg_3_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(clk_W10_R100),
        .CLKBWRCLK(clk_W10_R100),
        .DBITERR(NLW_memory_array_reg_3_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,data_in_ram[15:12]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_memory_array_reg_3_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_memory_array_reg_3_DOBDO_UNCONNECTED[31:4],m00_axis_tdata[15:12]}),
        .DOPADOP(NLW_memory_array_reg_3_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_array_reg_3_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_array_reg_3_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(memory_array_reg_7_i_2_n_0),
        .ENBWREN(memory_array_reg_7_i_3_n_0),
        .INJECTDBITERR(NLW_memory_array_reg_3_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_array_reg_3_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_array_reg_3_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(memory_array_reg_7_i_4_n_0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_array_reg_3_SBITERR_UNCONNECTED),
        .WEA({m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_3_i_1
       (.I0(i_write),
        .I1(data_in_IF1[15]),
        .O(data_in_ram[15]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_3_i_2
       (.I0(i_write),
        .I1(data_in_IF1[14]),
        .O(data_in_ram[14]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_3_i_3
       (.I0(i_write),
        .I1(data_in_IF1[13]),
        .O(data_in_ram[13]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_3_i_4
       (.I0(i_write),
        .I1(data_in_IF1[12]),
        .O(data_in_ram[12]));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "262144" *) 
  (* RTL_RAM_NAME = "memory_array" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "16" *) 
  (* bram_slice_end = "19" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "16" *) 
  (* ram_slice_end = "19" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(4),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(4)) 
    memory_array_reg_4
       (.ADDRARDADDR({1'b1,i_addr,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,i_addr,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_memory_array_reg_4_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_array_reg_4_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(clk_W10_R100),
        .CLKBWRCLK(clk_W10_R100),
        .DBITERR(NLW_memory_array_reg_4_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,data_in_ram[19:16]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_memory_array_reg_4_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_memory_array_reg_4_DOBDO_UNCONNECTED[31:4],m00_axis_tdata[19:16]}),
        .DOPADOP(NLW_memory_array_reg_4_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_array_reg_4_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_array_reg_4_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(memory_array_reg_7_i_2_n_0),
        .ENBWREN(memory_array_reg_7_i_3_n_0),
        .INJECTDBITERR(NLW_memory_array_reg_4_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_array_reg_4_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_array_reg_4_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(memory_array_reg_7_i_4_n_0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_array_reg_4_SBITERR_UNCONNECTED),
        .WEA({m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_4_i_1
       (.I0(i_write),
        .I1(data_in_IF2[3]),
        .O(data_in_ram[19]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_4_i_2
       (.I0(i_write),
        .I1(data_in_IF2[2]),
        .O(data_in_ram[18]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_4_i_3
       (.I0(i_write),
        .I1(data_in_IF2[1]),
        .O(data_in_ram[17]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_4_i_4
       (.I0(i_write),
        .I1(data_in_IF2[0]),
        .O(data_in_ram[16]));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "262144" *) 
  (* RTL_RAM_NAME = "memory_array" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "20" *) 
  (* bram_slice_end = "23" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "20" *) 
  (* ram_slice_end = "23" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(4),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(4)) 
    memory_array_reg_5
       (.ADDRARDADDR({1'b1,i_addr,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,i_addr,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_memory_array_reg_5_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_array_reg_5_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(clk_W10_R100),
        .CLKBWRCLK(clk_W10_R100),
        .DBITERR(NLW_memory_array_reg_5_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,data_in_ram[23:20]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_memory_array_reg_5_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_memory_array_reg_5_DOBDO_UNCONNECTED[31:4],m00_axis_tdata[23:20]}),
        .DOPADOP(NLW_memory_array_reg_5_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_array_reg_5_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_array_reg_5_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(memory_array_reg_7_i_2_n_0),
        .ENBWREN(memory_array_reg_7_i_3_n_0),
        .INJECTDBITERR(NLW_memory_array_reg_5_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_array_reg_5_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_array_reg_5_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(memory_array_reg_7_i_4_n_0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_array_reg_5_SBITERR_UNCONNECTED),
        .WEA({m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_5_i_1
       (.I0(i_write),
        .I1(data_in_IF2[7]),
        .O(data_in_ram[23]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_5_i_2
       (.I0(i_write),
        .I1(data_in_IF2[6]),
        .O(data_in_ram[22]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_5_i_3
       (.I0(i_write),
        .I1(data_in_IF2[5]),
        .O(data_in_ram[21]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_5_i_4
       (.I0(i_write),
        .I1(data_in_IF2[4]),
        .O(data_in_ram[20]));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "262144" *) 
  (* RTL_RAM_NAME = "memory_array" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "24" *) 
  (* bram_slice_end = "27" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "27" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(4),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(4)) 
    memory_array_reg_6
       (.ADDRARDADDR({1'b1,i_addr,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,i_addr,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_memory_array_reg_6_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_array_reg_6_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(clk_W10_R100),
        .CLKBWRCLK(clk_W10_R100),
        .DBITERR(NLW_memory_array_reg_6_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,data_in_ram[27:24]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_memory_array_reg_6_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_memory_array_reg_6_DOBDO_UNCONNECTED[31:4],m00_axis_tdata[27:24]}),
        .DOPADOP(NLW_memory_array_reg_6_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_array_reg_6_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_array_reg_6_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(memory_array_reg_7_i_2_n_0),
        .ENBWREN(memory_array_reg_7_i_3_n_0),
        .INJECTDBITERR(NLW_memory_array_reg_6_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_array_reg_6_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_array_reg_6_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(memory_array_reg_7_i_4_n_0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_array_reg_6_SBITERR_UNCONNECTED),
        .WEA({m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_6_i_1
       (.I0(i_write),
        .I1(data_in_IF2[11]),
        .O(data_in_ram[27]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_6_i_2
       (.I0(i_write),
        .I1(data_in_IF2[10]),
        .O(data_in_ram[26]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_6_i_3
       (.I0(i_write),
        .I1(data_in_IF2[9]),
        .O(data_in_ram[25]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_6_i_4
       (.I0(i_write),
        .I1(data_in_IF2[8]),
        .O(data_in_ram[24]));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "262144" *) 
  (* RTL_RAM_NAME = "memory_array" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "28" *) 
  (* bram_slice_end = "31" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "28" *) 
  (* ram_slice_end = "31" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(4),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(4)) 
    memory_array_reg_7
       (.ADDRARDADDR({1'b1,i_addr,1'b1,1'b1}),
        .ADDRBWRADDR({1'b1,i_addr,1'b1,1'b1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_memory_array_reg_7_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_memory_array_reg_7_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(clk_W10_R100),
        .CLKBWRCLK(clk_W10_R100),
        .DBITERR(NLW_memory_array_reg_7_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,data_in_ram[31:28]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_memory_array_reg_7_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_memory_array_reg_7_DOBDO_UNCONNECTED[31:4],m00_axis_tdata[31:28]}),
        .DOPADOP(NLW_memory_array_reg_7_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_memory_array_reg_7_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_memory_array_reg_7_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(memory_array_reg_7_i_2_n_0),
        .ENBWREN(memory_array_reg_7_i_3_n_0),
        .INJECTDBITERR(NLW_memory_array_reg_7_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_memory_array_reg_7_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_memory_array_reg_7_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(memory_array_reg_7_i_4_n_0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_memory_array_reg_7_SBITERR_UNCONNECTED),
        .WEA({m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out,m00_axis_tvalid_w12_out}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT3 #(
    .INIT(8'hB8)) 
    memory_array_reg_7_i_1
       (.I0(data_clk),
        .I1(i_write),
        .I2(m00_axis_aclk),
        .O(clk_W10_R100));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_7_i_2
       (.I0(i_write),
        .I1(cnt_clk_10_w1_carry__0_n_2),
        .O(memory_array_reg_7_i_2_n_0));
  LUT3 #(
    .INIT(8'h40)) 
    memory_array_reg_7_i_3
       (.I0(i_write),
        .I1(m00_axis_aresetn),
        .I2(m00_axis_tready),
        .O(memory_array_reg_7_i_3_n_0));
  LUT4 #(
    .INIT(16'h0040)) 
    memory_array_reg_7_i_4
       (.I0(i_write),
        .I1(m00_axis_aresetn),
        .I2(m00_axis_tready),
        .I3(cnt_clk_10_w0_carry__0_n_0),
        .O(memory_array_reg_7_i_4_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_7_i_5
       (.I0(i_write),
        .I1(data_in_IF2[15]),
        .O(data_in_ram[31]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_7_i_6
       (.I0(i_write),
        .I1(data_in_IF2[14]),
        .O(data_in_ram[30]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_7_i_7
       (.I0(i_write),
        .I1(data_in_IF2[13]),
        .O(data_in_ram[29]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_7_i_8
       (.I0(i_write),
        .I1(data_in_IF2[12]),
        .O(data_in_ram[28]));
  LUT2 #(
    .INIT(4'h8)) 
    memory_array_reg_7_i_9
       (.I0(m00_axis_aresetn),
        .I1(m00_axis_tready),
        .O(m00_axis_tvalid_w12_out));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_Sample_Generator_0_0,Sample_Generator_v3_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "Sample_Generator_v3_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (FrameSize,
    data_clk,
    data_in_IF1,
    data_in_IF2,
    m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tvalid,
    m00_axis_tready,
    m00_axis_aclk,
    m00_axis_aresetn);
  input [15:0]FrameSize;
  input data_clk;
  input [15:0]data_in_IF1;
  input [15:0]data_in_IF2;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TDATA" *) output [31:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB" *) output [3:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0" *) input m00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input m00_axis_aresetn;

  wire \<const1> ;
  wire [15:0]FrameSize;
  wire data_clk;
  wire [15:0]data_in_IF1;
  wire [15:0]data_in_IF2;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire m00_axis_tready;
  wire m00_axis_tvalid;

  assign m00_axis_tstrb[3] = \<const1> ;
  assign m00_axis_tstrb[2] = \<const1> ;
  assign m00_axis_tstrb[1] = \<const1> ;
  assign m00_axis_tstrb[0] = \<const1> ;
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0 inst
       (.FrameSize(FrameSize),
        .data_clk(data_clk),
        .data_in_IF1(data_in_IF1),
        .data_in_IF2(data_in_IF2),
        .m00_axis_aclk(m00_axis_aclk),
        .m00_axis_aresetn(m00_axis_aresetn),
        .m00_axis_tdata(m00_axis_tdata),
        .m00_axis_tlast(m00_axis_tlast),
        .m00_axis_tready(m00_axis_tready),
        .m00_axis_tvalid(m00_axis_tvalid));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
