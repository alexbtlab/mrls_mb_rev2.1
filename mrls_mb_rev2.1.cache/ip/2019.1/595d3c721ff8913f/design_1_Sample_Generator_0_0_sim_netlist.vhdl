-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Mon Sep 14 17:57:13 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_Sample_Generator_0_0_sim_netlist.vhdl
-- Design      : design_1_Sample_Generator_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0 is
  port (
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_clk : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    data_in_IF1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_in_IF2 : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0 is
  signal clk_W10_R100 : STD_LOGIC;
  signal cnt_clk_10 : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \cnt_clk_10_w0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_10_w0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_10_w0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \cnt_clk_10_w0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \cnt_clk_10_w0_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \cnt_clk_10_w0_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \cnt_clk_10_w0_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \cnt_clk_10_w0_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \cnt_clk_10_w0_carry__0_n_0\ : STD_LOGIC;
  signal \cnt_clk_10_w0_carry__0_n_1\ : STD_LOGIC;
  signal \cnt_clk_10_w0_carry__0_n_2\ : STD_LOGIC;
  signal \cnt_clk_10_w0_carry__0_n_3\ : STD_LOGIC;
  signal cnt_clk_10_w0_carry_i_1_n_0 : STD_LOGIC;
  signal cnt_clk_10_w0_carry_i_2_n_0 : STD_LOGIC;
  signal cnt_clk_10_w0_carry_i_3_n_0 : STD_LOGIC;
  signal cnt_clk_10_w0_carry_i_4_n_0 : STD_LOGIC;
  signal cnt_clk_10_w0_carry_i_5_n_0 : STD_LOGIC;
  signal cnt_clk_10_w0_carry_i_6_n_0 : STD_LOGIC;
  signal cnt_clk_10_w0_carry_i_7_n_0 : STD_LOGIC;
  signal cnt_clk_10_w0_carry_i_8_n_0 : STD_LOGIC;
  signal cnt_clk_10_w0_carry_n_0 : STD_LOGIC;
  signal cnt_clk_10_w0_carry_n_1 : STD_LOGIC;
  signal cnt_clk_10_w0_carry_n_2 : STD_LOGIC;
  signal cnt_clk_10_w0_carry_n_3 : STD_LOGIC;
  signal \cnt_clk_10_w1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_10_w1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_10_w1_carry__0_n_2\ : STD_LOGIC;
  signal \cnt_clk_10_w1_carry__0_n_3\ : STD_LOGIC;
  signal cnt_clk_10_w1_carry_i_1_n_0 : STD_LOGIC;
  signal cnt_clk_10_w1_carry_i_2_n_0 : STD_LOGIC;
  signal cnt_clk_10_w1_carry_i_3_n_0 : STD_LOGIC;
  signal cnt_clk_10_w1_carry_i_4_n_0 : STD_LOGIC;
  signal cnt_clk_10_w1_carry_n_0 : STD_LOGIC;
  signal cnt_clk_10_w1_carry_n_1 : STD_LOGIC;
  signal cnt_clk_10_w1_carry_n_2 : STD_LOGIC;
  signal cnt_clk_10_w1_carry_n_3 : STD_LOGIC;
  signal \cnt_clk_10_w[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_10_w[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_10_w[0]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg_n_0_[13]\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg_n_0_[14]\ : STD_LOGIC;
  signal \cnt_clk_10_w_reg_n_0_[15]\ : STD_LOGIC;
  signal data_in_ram : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal i_addr : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal \i_addr[12]_i_1_n_0\ : STD_LOGIC;
  signal i_write : STD_LOGIC;
  signal i_write_i_1_n_0 : STD_LOGIC;
  signal \m00_axis_tlast_w0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tlast_w0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tlast_w0_carry__0_n_0\ : STD_LOGIC;
  signal \m00_axis_tlast_w0_carry__0_n_1\ : STD_LOGIC;
  signal \m00_axis_tlast_w0_carry__0_n_2\ : STD_LOGIC;
  signal \m00_axis_tlast_w0_carry__0_n_3\ : STD_LOGIC;
  signal \m00_axis_tlast_w0_carry__1_n_2\ : STD_LOGIC;
  signal \m00_axis_tlast_w0_carry__1_n_3\ : STD_LOGIC;
  signal m00_axis_tlast_w0_carry_i_1_n_0 : STD_LOGIC;
  signal m00_axis_tlast_w0_carry_i_2_n_0 : STD_LOGIC;
  signal m00_axis_tlast_w0_carry_i_3_n_0 : STD_LOGIC;
  signal m00_axis_tlast_w0_carry_i_4_n_0 : STD_LOGIC;
  signal m00_axis_tlast_w0_carry_n_0 : STD_LOGIC;
  signal m00_axis_tlast_w0_carry_n_1 : STD_LOGIC;
  signal m00_axis_tlast_w0_carry_n_2 : STD_LOGIC;
  signal m00_axis_tlast_w0_carry_n_3 : STD_LOGIC;
  signal m00_axis_tlast_w_i_1_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w1 : STD_LOGIC;
  signal m00_axis_tvalid_w12_out : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__0_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__0_n_1\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__0_n_2\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__0_n_3\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__1_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__1_n_1\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__1_n_2\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__1_n_3\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__2_n_1\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__2_n_2\ : STD_LOGIC;
  signal \m00_axis_tvalid_w1_carry__2_n_3\ : STD_LOGIC;
  signal m00_axis_tvalid_w1_carry_i_1_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w1_carry_i_2_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w1_carry_i_3_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w1_carry_i_4_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w1_carry_i_5_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w1_carry_i_6_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w1_carry_i_7_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w1_carry_i_8_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w1_carry_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w1_carry_n_1 : STD_LOGIC;
  signal m00_axis_tvalid_w1_carry_n_2 : STD_LOGIC;
  signal m00_axis_tvalid_w1_carry_n_3 : STD_LOGIC;
  signal m00_axis_tvalid_w2 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \m00_axis_tvalid_w2_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__0_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__0_n_1\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__0_n_2\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__0_n_3\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__1_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__1_n_1\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__1_n_2\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__1_n_3\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__2_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__2_n_2\ : STD_LOGIC;
  signal \m00_axis_tvalid_w2_carry__2_n_3\ : STD_LOGIC;
  signal m00_axis_tvalid_w2_carry_i_1_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w2_carry_i_2_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w2_carry_i_3_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w2_carry_i_4_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w2_carry_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w2_carry_n_1 : STD_LOGIC;
  signal m00_axis_tvalid_w2_carry_n_2 : STD_LOGIC;
  signal m00_axis_tvalid_w2_carry_n_3 : STD_LOGIC;
  signal m00_axis_tvalid_w_i_1_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w_i_2_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w_i_3_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w_i_4_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_w_i_5_n_0 : STD_LOGIC;
  signal memory_array_reg_7_i_2_n_0 : STD_LOGIC;
  signal memory_array_reg_7_i_3_n_0 : STD_LOGIC;
  signal memory_array_reg_7_i_4_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal NLW_cnt_clk_10_w0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cnt_clk_10_w0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_cnt_clk_10_w1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cnt_clk_10_w1_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_cnt_clk_10_w1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cnt_clk_10_w_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_m00_axis_tlast_w0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_m00_axis_tlast_w0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_m00_axis_tlast_w0_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_m00_axis_tlast_w0_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_m00_axis_tvalid_w1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_m00_axis_tvalid_w1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_m00_axis_tvalid_w1_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_m00_axis_tvalid_w1_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_m00_axis_tvalid_w2_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_m00_axis_tvalid_w2_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_memory_array_reg_0_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_0_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_0_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_0_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_0_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_0_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_0_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_memory_array_reg_0_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal NLW_memory_array_reg_0_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_0_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_0_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_memory_array_reg_0_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_memory_array_reg_1_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_1_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_1_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_1_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_1_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_1_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_1_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_memory_array_reg_1_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal NLW_memory_array_reg_1_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_1_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_1_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_memory_array_reg_1_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_memory_array_reg_2_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_2_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_2_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_2_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_2_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_2_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_2_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_memory_array_reg_2_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal NLW_memory_array_reg_2_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_2_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_2_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_memory_array_reg_2_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_memory_array_reg_3_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_3_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_3_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_3_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_3_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_3_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_3_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_memory_array_reg_3_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal NLW_memory_array_reg_3_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_3_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_3_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_memory_array_reg_3_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_memory_array_reg_4_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_4_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_4_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_4_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_4_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_4_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_4_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_memory_array_reg_4_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal NLW_memory_array_reg_4_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_4_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_4_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_memory_array_reg_4_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_memory_array_reg_5_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_5_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_5_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_5_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_5_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_5_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_5_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_memory_array_reg_5_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal NLW_memory_array_reg_5_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_5_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_5_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_memory_array_reg_5_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_memory_array_reg_6_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_6_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_6_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_6_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_6_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_6_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_6_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_memory_array_reg_6_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal NLW_memory_array_reg_6_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_6_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_6_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_memory_array_reg_6_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal NLW_memory_array_reg_7_CASCADEOUTA_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_7_CASCADEOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_7_DBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_7_INJECTDBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_7_INJECTSBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_7_SBITERR_UNCONNECTED : STD_LOGIC;
  signal NLW_memory_array_reg_7_DOADO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_memory_array_reg_7_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal NLW_memory_array_reg_7_DOPADOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_7_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_memory_array_reg_7_ECCPARITY_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_memory_array_reg_7_RDADDRECC_UNCONNECTED : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of i_write_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of m00_axis_tlast_w_i_1 : label is "soft_lutpair0";
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of memory_array_reg_0 : label is "p0_d4";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of memory_array_reg_0 : label is "p0_d4";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of memory_array_reg_0 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of memory_array_reg_0 : label is 262144;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of memory_array_reg_0 : label is "memory_array";
  attribute bram_addr_begin : integer;
  attribute bram_addr_begin of memory_array_reg_0 : label is 0;
  attribute bram_addr_end : integer;
  attribute bram_addr_end of memory_array_reg_0 : label is 8191;
  attribute bram_slice_begin : integer;
  attribute bram_slice_begin of memory_array_reg_0 : label is 0;
  attribute bram_slice_end : integer;
  attribute bram_slice_end of memory_array_reg_0 : label is 3;
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of memory_array_reg_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of memory_array_reg_0 : label is 8191;
  attribute ram_offset : integer;
  attribute ram_offset of memory_array_reg_0 : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of memory_array_reg_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of memory_array_reg_0 : label is 3;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of memory_array_reg_1 : label is "p0_d4";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of memory_array_reg_1 : label is "p0_d4";
  attribute METHODOLOGY_DRC_VIOS of memory_array_reg_1 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of memory_array_reg_1 : label is 262144;
  attribute RTL_RAM_NAME of memory_array_reg_1 : label is "memory_array";
  attribute bram_addr_begin of memory_array_reg_1 : label is 0;
  attribute bram_addr_end of memory_array_reg_1 : label is 8191;
  attribute bram_slice_begin of memory_array_reg_1 : label is 4;
  attribute bram_slice_end of memory_array_reg_1 : label is 7;
  attribute ram_addr_begin of memory_array_reg_1 : label is 0;
  attribute ram_addr_end of memory_array_reg_1 : label is 8191;
  attribute ram_offset of memory_array_reg_1 : label is 0;
  attribute ram_slice_begin of memory_array_reg_1 : label is 4;
  attribute ram_slice_end of memory_array_reg_1 : label is 7;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of memory_array_reg_2 : label is "p0_d4";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of memory_array_reg_2 : label is "p0_d4";
  attribute METHODOLOGY_DRC_VIOS of memory_array_reg_2 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of memory_array_reg_2 : label is 262144;
  attribute RTL_RAM_NAME of memory_array_reg_2 : label is "memory_array";
  attribute bram_addr_begin of memory_array_reg_2 : label is 0;
  attribute bram_addr_end of memory_array_reg_2 : label is 8191;
  attribute bram_slice_begin of memory_array_reg_2 : label is 8;
  attribute bram_slice_end of memory_array_reg_2 : label is 11;
  attribute ram_addr_begin of memory_array_reg_2 : label is 0;
  attribute ram_addr_end of memory_array_reg_2 : label is 8191;
  attribute ram_offset of memory_array_reg_2 : label is 0;
  attribute ram_slice_begin of memory_array_reg_2 : label is 8;
  attribute ram_slice_end of memory_array_reg_2 : label is 11;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of memory_array_reg_3 : label is "p0_d4";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of memory_array_reg_3 : label is "p0_d4";
  attribute METHODOLOGY_DRC_VIOS of memory_array_reg_3 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of memory_array_reg_3 : label is 262144;
  attribute RTL_RAM_NAME of memory_array_reg_3 : label is "memory_array";
  attribute bram_addr_begin of memory_array_reg_3 : label is 0;
  attribute bram_addr_end of memory_array_reg_3 : label is 8191;
  attribute bram_slice_begin of memory_array_reg_3 : label is 12;
  attribute bram_slice_end of memory_array_reg_3 : label is 15;
  attribute ram_addr_begin of memory_array_reg_3 : label is 0;
  attribute ram_addr_end of memory_array_reg_3 : label is 8191;
  attribute ram_offset of memory_array_reg_3 : label is 0;
  attribute ram_slice_begin of memory_array_reg_3 : label is 12;
  attribute ram_slice_end of memory_array_reg_3 : label is 15;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of memory_array_reg_4 : label is "p0_d4";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of memory_array_reg_4 : label is "p0_d4";
  attribute METHODOLOGY_DRC_VIOS of memory_array_reg_4 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of memory_array_reg_4 : label is 262144;
  attribute RTL_RAM_NAME of memory_array_reg_4 : label is "memory_array";
  attribute bram_addr_begin of memory_array_reg_4 : label is 0;
  attribute bram_addr_end of memory_array_reg_4 : label is 8191;
  attribute bram_slice_begin of memory_array_reg_4 : label is 16;
  attribute bram_slice_end of memory_array_reg_4 : label is 19;
  attribute ram_addr_begin of memory_array_reg_4 : label is 0;
  attribute ram_addr_end of memory_array_reg_4 : label is 8191;
  attribute ram_offset of memory_array_reg_4 : label is 0;
  attribute ram_slice_begin of memory_array_reg_4 : label is 16;
  attribute ram_slice_end of memory_array_reg_4 : label is 19;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of memory_array_reg_5 : label is "p0_d4";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of memory_array_reg_5 : label is "p0_d4";
  attribute METHODOLOGY_DRC_VIOS of memory_array_reg_5 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of memory_array_reg_5 : label is 262144;
  attribute RTL_RAM_NAME of memory_array_reg_5 : label is "memory_array";
  attribute bram_addr_begin of memory_array_reg_5 : label is 0;
  attribute bram_addr_end of memory_array_reg_5 : label is 8191;
  attribute bram_slice_begin of memory_array_reg_5 : label is 20;
  attribute bram_slice_end of memory_array_reg_5 : label is 23;
  attribute ram_addr_begin of memory_array_reg_5 : label is 0;
  attribute ram_addr_end of memory_array_reg_5 : label is 8191;
  attribute ram_offset of memory_array_reg_5 : label is 0;
  attribute ram_slice_begin of memory_array_reg_5 : label is 20;
  attribute ram_slice_end of memory_array_reg_5 : label is 23;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of memory_array_reg_6 : label is "p0_d4";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of memory_array_reg_6 : label is "p0_d4";
  attribute METHODOLOGY_DRC_VIOS of memory_array_reg_6 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of memory_array_reg_6 : label is 262144;
  attribute RTL_RAM_NAME of memory_array_reg_6 : label is "memory_array";
  attribute bram_addr_begin of memory_array_reg_6 : label is 0;
  attribute bram_addr_end of memory_array_reg_6 : label is 8191;
  attribute bram_slice_begin of memory_array_reg_6 : label is 24;
  attribute bram_slice_end of memory_array_reg_6 : label is 27;
  attribute ram_addr_begin of memory_array_reg_6 : label is 0;
  attribute ram_addr_end of memory_array_reg_6 : label is 8191;
  attribute ram_offset of memory_array_reg_6 : label is 0;
  attribute ram_slice_begin of memory_array_reg_6 : label is 24;
  attribute ram_slice_end of memory_array_reg_6 : label is 27;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of memory_array_reg_7 : label is "p0_d4";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of memory_array_reg_7 : label is "p0_d4";
  attribute METHODOLOGY_DRC_VIOS of memory_array_reg_7 : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS of memory_array_reg_7 : label is 262144;
  attribute RTL_RAM_NAME of memory_array_reg_7 : label is "memory_array";
  attribute bram_addr_begin of memory_array_reg_7 : label is 0;
  attribute bram_addr_end of memory_array_reg_7 : label is 8191;
  attribute bram_slice_begin of memory_array_reg_7 : label is 28;
  attribute bram_slice_end of memory_array_reg_7 : label is 31;
  attribute ram_addr_begin of memory_array_reg_7 : label is 0;
  attribute ram_addr_end of memory_array_reg_7 : label is 8191;
  attribute ram_offset of memory_array_reg_7 : label is 0;
  attribute ram_slice_begin of memory_array_reg_7 : label is 28;
  attribute ram_slice_end of memory_array_reg_7 : label is 31;
begin
cnt_clk_10_w0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => cnt_clk_10_w0_carry_n_0,
      CO(2) => cnt_clk_10_w0_carry_n_1,
      CO(1) => cnt_clk_10_w0_carry_n_2,
      CO(0) => cnt_clk_10_w0_carry_n_3,
      CYINIT => '1',
      DI(3) => cnt_clk_10_w0_carry_i_1_n_0,
      DI(2) => cnt_clk_10_w0_carry_i_2_n_0,
      DI(1) => cnt_clk_10_w0_carry_i_3_n_0,
      DI(0) => cnt_clk_10_w0_carry_i_4_n_0,
      O(3 downto 0) => NLW_cnt_clk_10_w0_carry_O_UNCONNECTED(3 downto 0),
      S(3) => cnt_clk_10_w0_carry_i_5_n_0,
      S(2) => cnt_clk_10_w0_carry_i_6_n_0,
      S(1) => cnt_clk_10_w0_carry_i_7_n_0,
      S(0) => cnt_clk_10_w0_carry_i_8_n_0
    );
\cnt_clk_10_w0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => cnt_clk_10_w0_carry_n_0,
      CO(3) => \cnt_clk_10_w0_carry__0_n_0\,
      CO(2) => \cnt_clk_10_w0_carry__0_n_1\,
      CO(1) => \cnt_clk_10_w0_carry__0_n_2\,
      CO(0) => \cnt_clk_10_w0_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \cnt_clk_10_w0_carry__0_i_1_n_0\,
      DI(2) => \cnt_clk_10_w0_carry__0_i_2_n_0\,
      DI(1) => \cnt_clk_10_w0_carry__0_i_3_n_0\,
      DI(0) => \cnt_clk_10_w0_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_cnt_clk_10_w0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \cnt_clk_10_w0_carry__0_i_5_n_0\,
      S(2) => \cnt_clk_10_w0_carry__0_i_6_n_0\,
      S(1) => \cnt_clk_10_w0_carry__0_i_7_n_0\,
      S(0) => \cnt_clk_10_w0_carry__0_i_8_n_0\
    );
\cnt_clk_10_w0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => FrameSize(14),
      I1 => FrameSize(15),
      O => \cnt_clk_10_w0_carry__0_i_1_n_0\
    );
\cnt_clk_10_w0_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F2"
    )
        port map (
      I0 => FrameSize(12),
      I1 => cnt_clk_10(12),
      I2 => FrameSize(13),
      O => \cnt_clk_10_w0_carry__0_i_2_n_0\
    );
\cnt_clk_10_w0_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => FrameSize(10),
      I1 => cnt_clk_10(10),
      I2 => cnt_clk_10(11),
      I3 => FrameSize(11),
      O => \cnt_clk_10_w0_carry__0_i_3_n_0\
    );
\cnt_clk_10_w0_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => FrameSize(8),
      I1 => cnt_clk_10(8),
      I2 => cnt_clk_10(9),
      I3 => FrameSize(9),
      O => \cnt_clk_10_w0_carry__0_i_4_n_0\
    );
\cnt_clk_10_w0_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(14),
      I1 => FrameSize(15),
      O => \cnt_clk_10_w0_carry__0_i_5_n_0\
    );
\cnt_clk_10_w0_carry__0_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"09"
    )
        port map (
      I0 => FrameSize(12),
      I1 => cnt_clk_10(12),
      I2 => FrameSize(13),
      O => \cnt_clk_10_w0_carry__0_i_6_n_0\
    );
\cnt_clk_10_w0_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => FrameSize(10),
      I1 => cnt_clk_10(10),
      I2 => FrameSize(11),
      I3 => cnt_clk_10(11),
      O => \cnt_clk_10_w0_carry__0_i_7_n_0\
    );
\cnt_clk_10_w0_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => FrameSize(8),
      I1 => cnt_clk_10(8),
      I2 => FrameSize(9),
      I3 => cnt_clk_10(9),
      O => \cnt_clk_10_w0_carry__0_i_8_n_0\
    );
cnt_clk_10_w0_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => FrameSize(6),
      I1 => cnt_clk_10(6),
      I2 => cnt_clk_10(7),
      I3 => FrameSize(7),
      O => cnt_clk_10_w0_carry_i_1_n_0
    );
cnt_clk_10_w0_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => FrameSize(4),
      I1 => cnt_clk_10(4),
      I2 => cnt_clk_10(5),
      I3 => FrameSize(5),
      O => cnt_clk_10_w0_carry_i_2_n_0
    );
cnt_clk_10_w0_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => FrameSize(2),
      I1 => cnt_clk_10(2),
      I2 => cnt_clk_10(3),
      I3 => FrameSize(3),
      O => cnt_clk_10_w0_carry_i_3_n_0
    );
cnt_clk_10_w0_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => FrameSize(0),
      I1 => cnt_clk_10(0),
      I2 => cnt_clk_10(1),
      I3 => FrameSize(1),
      O => cnt_clk_10_w0_carry_i_4_n_0
    );
cnt_clk_10_w0_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => FrameSize(6),
      I1 => cnt_clk_10(6),
      I2 => FrameSize(7),
      I3 => cnt_clk_10(7),
      O => cnt_clk_10_w0_carry_i_5_n_0
    );
cnt_clk_10_w0_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => FrameSize(4),
      I1 => cnt_clk_10(4),
      I2 => FrameSize(5),
      I3 => cnt_clk_10(5),
      O => cnt_clk_10_w0_carry_i_6_n_0
    );
cnt_clk_10_w0_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => FrameSize(2),
      I1 => cnt_clk_10(2),
      I2 => FrameSize(3),
      I3 => cnt_clk_10(3),
      O => cnt_clk_10_w0_carry_i_7_n_0
    );
cnt_clk_10_w0_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => FrameSize(0),
      I1 => cnt_clk_10(0),
      I2 => FrameSize(1),
      I3 => cnt_clk_10(1),
      O => cnt_clk_10_w0_carry_i_8_n_0
    );
cnt_clk_10_w1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => cnt_clk_10_w1_carry_n_0,
      CO(2) => cnt_clk_10_w1_carry_n_1,
      CO(1) => cnt_clk_10_w1_carry_n_2,
      CO(0) => cnt_clk_10_w1_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => B"1111",
      O(3 downto 0) => NLW_cnt_clk_10_w1_carry_O_UNCONNECTED(3 downto 0),
      S(3) => cnt_clk_10_w1_carry_i_1_n_0,
      S(2) => cnt_clk_10_w1_carry_i_2_n_0,
      S(1) => cnt_clk_10_w1_carry_i_3_n_0,
      S(0) => cnt_clk_10_w1_carry_i_4_n_0
    );
\cnt_clk_10_w1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => cnt_clk_10_w1_carry_n_0,
      CO(3 downto 2) => \NLW_cnt_clk_10_w1_carry__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \cnt_clk_10_w1_carry__0_n_2\,
      CO(0) => \cnt_clk_10_w1_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0011",
      O(3 downto 0) => \NLW_cnt_clk_10_w1_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \cnt_clk_10_w1_carry__0_i_1_n_0\,
      S(0) => \cnt_clk_10_w1_carry__0_i_2_n_0\
    );
\cnt_clk_10_w1_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(15),
      O => \cnt_clk_10_w1_carry__0_i_1_n_0\
    );
\cnt_clk_10_w1_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0009"
    )
        port map (
      I0 => cnt_clk_10(12),
      I1 => FrameSize(12),
      I2 => FrameSize(14),
      I3 => FrameSize(13),
      O => \cnt_clk_10_w1_carry__0_i_2_n_0\
    );
cnt_clk_10_w1_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => cnt_clk_10(9),
      I1 => FrameSize(9),
      I2 => FrameSize(11),
      I3 => cnt_clk_10(11),
      I4 => FrameSize(10),
      I5 => cnt_clk_10(10),
      O => cnt_clk_10_w1_carry_i_1_n_0
    );
cnt_clk_10_w1_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => cnt_clk_10(6),
      I1 => FrameSize(6),
      I2 => FrameSize(8),
      I3 => cnt_clk_10(8),
      I4 => FrameSize(7),
      I5 => cnt_clk_10(7),
      O => cnt_clk_10_w1_carry_i_2_n_0
    );
cnt_clk_10_w1_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => cnt_clk_10(3),
      I1 => FrameSize(3),
      I2 => FrameSize(5),
      I3 => cnt_clk_10(5),
      I4 => FrameSize(4),
      I5 => cnt_clk_10(4),
      O => cnt_clk_10_w1_carry_i_3_n_0
    );
cnt_clk_10_w1_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => cnt_clk_10(0),
      I1 => FrameSize(0),
      I2 => FrameSize(2),
      I3 => cnt_clk_10(2),
      I4 => FrameSize(1),
      I5 => cnt_clk_10(1),
      O => cnt_clk_10_w1_carry_i_4_n_0
    );
\cnt_clk_10_w[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4FFF"
    )
        port map (
      I0 => \cnt_clk_10_w1_carry__0_n_2\,
      I1 => i_write,
      I2 => m00_axis_tready,
      I3 => m00_axis_aresetn,
      O => \cnt_clk_10_w[0]_i_1_n_0\
    );
\cnt_clk_10_w[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => i_write,
      I1 => \cnt_clk_10_w0_carry__0_n_0\,
      O => \cnt_clk_10_w[0]_i_2_n_0\
    );
\cnt_clk_10_w[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_clk_10(0),
      O => \cnt_clk_10_w[0]_i_4_n_0\
    );
\cnt_clk_10_w_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => \cnt_clk_10_w[0]_i_2_n_0\,
      D => \cnt_clk_10_w_reg[0]_i_3_n_7\,
      Q => cnt_clk_10(0),
      R => \cnt_clk_10_w[0]_i_1_n_0\
    );
\cnt_clk_10_w_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_clk_10_w_reg[0]_i_3_n_0\,
      CO(2) => \cnt_clk_10_w_reg[0]_i_3_n_1\,
      CO(1) => \cnt_clk_10_w_reg[0]_i_3_n_2\,
      CO(0) => \cnt_clk_10_w_reg[0]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt_clk_10_w_reg[0]_i_3_n_4\,
      O(2) => \cnt_clk_10_w_reg[0]_i_3_n_5\,
      O(1) => \cnt_clk_10_w_reg[0]_i_3_n_6\,
      O(0) => \cnt_clk_10_w_reg[0]_i_3_n_7\,
      S(3 downto 1) => cnt_clk_10(3 downto 1),
      S(0) => \cnt_clk_10_w[0]_i_4_n_0\
    );
\cnt_clk_10_w_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => \cnt_clk_10_w[0]_i_2_n_0\,
      D => \cnt_clk_10_w_reg[8]_i_1_n_5\,
      Q => cnt_clk_10(10),
      R => \cnt_clk_10_w[0]_i_1_n_0\
    );
\cnt_clk_10_w_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => \cnt_clk_10_w[0]_i_2_n_0\,
      D => \cnt_clk_10_w_reg[8]_i_1_n_4\,
      Q => cnt_clk_10(11),
      R => \cnt_clk_10_w[0]_i_1_n_0\
    );
\cnt_clk_10_w_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => \cnt_clk_10_w[0]_i_2_n_0\,
      D => \cnt_clk_10_w_reg[12]_i_1_n_7\,
      Q => cnt_clk_10(12),
      R => \cnt_clk_10_w[0]_i_1_n_0\
    );
\cnt_clk_10_w_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_clk_10_w_reg[8]_i_1_n_0\,
      CO(3) => \NLW_cnt_clk_10_w_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt_clk_10_w_reg[12]_i_1_n_1\,
      CO(1) => \cnt_clk_10_w_reg[12]_i_1_n_2\,
      CO(0) => \cnt_clk_10_w_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_clk_10_w_reg[12]_i_1_n_4\,
      O(2) => \cnt_clk_10_w_reg[12]_i_1_n_5\,
      O(1) => \cnt_clk_10_w_reg[12]_i_1_n_6\,
      O(0) => \cnt_clk_10_w_reg[12]_i_1_n_7\,
      S(3) => \cnt_clk_10_w_reg_n_0_[15]\,
      S(2) => \cnt_clk_10_w_reg_n_0_[14]\,
      S(1) => \cnt_clk_10_w_reg_n_0_[13]\,
      S(0) => cnt_clk_10(12)
    );
\cnt_clk_10_w_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => \cnt_clk_10_w[0]_i_2_n_0\,
      D => \cnt_clk_10_w_reg[12]_i_1_n_6\,
      Q => \cnt_clk_10_w_reg_n_0_[13]\,
      R => \cnt_clk_10_w[0]_i_1_n_0\
    );
\cnt_clk_10_w_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => \cnt_clk_10_w[0]_i_2_n_0\,
      D => \cnt_clk_10_w_reg[12]_i_1_n_5\,
      Q => \cnt_clk_10_w_reg_n_0_[14]\,
      R => \cnt_clk_10_w[0]_i_1_n_0\
    );
\cnt_clk_10_w_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => \cnt_clk_10_w[0]_i_2_n_0\,
      D => \cnt_clk_10_w_reg[12]_i_1_n_4\,
      Q => \cnt_clk_10_w_reg_n_0_[15]\,
      R => \cnt_clk_10_w[0]_i_1_n_0\
    );
\cnt_clk_10_w_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => \cnt_clk_10_w[0]_i_2_n_0\,
      D => \cnt_clk_10_w_reg[0]_i_3_n_6\,
      Q => cnt_clk_10(1),
      R => \cnt_clk_10_w[0]_i_1_n_0\
    );
\cnt_clk_10_w_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => \cnt_clk_10_w[0]_i_2_n_0\,
      D => \cnt_clk_10_w_reg[0]_i_3_n_5\,
      Q => cnt_clk_10(2),
      R => \cnt_clk_10_w[0]_i_1_n_0\
    );
\cnt_clk_10_w_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => \cnt_clk_10_w[0]_i_2_n_0\,
      D => \cnt_clk_10_w_reg[0]_i_3_n_4\,
      Q => cnt_clk_10(3),
      R => \cnt_clk_10_w[0]_i_1_n_0\
    );
\cnt_clk_10_w_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => \cnt_clk_10_w[0]_i_2_n_0\,
      D => \cnt_clk_10_w_reg[4]_i_1_n_7\,
      Q => cnt_clk_10(4),
      R => \cnt_clk_10_w[0]_i_1_n_0\
    );
\cnt_clk_10_w_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_clk_10_w_reg[0]_i_3_n_0\,
      CO(3) => \cnt_clk_10_w_reg[4]_i_1_n_0\,
      CO(2) => \cnt_clk_10_w_reg[4]_i_1_n_1\,
      CO(1) => \cnt_clk_10_w_reg[4]_i_1_n_2\,
      CO(0) => \cnt_clk_10_w_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_clk_10_w_reg[4]_i_1_n_4\,
      O(2) => \cnt_clk_10_w_reg[4]_i_1_n_5\,
      O(1) => \cnt_clk_10_w_reg[4]_i_1_n_6\,
      O(0) => \cnt_clk_10_w_reg[4]_i_1_n_7\,
      S(3 downto 0) => cnt_clk_10(7 downto 4)
    );
\cnt_clk_10_w_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => \cnt_clk_10_w[0]_i_2_n_0\,
      D => \cnt_clk_10_w_reg[4]_i_1_n_6\,
      Q => cnt_clk_10(5),
      R => \cnt_clk_10_w[0]_i_1_n_0\
    );
\cnt_clk_10_w_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => \cnt_clk_10_w[0]_i_2_n_0\,
      D => \cnt_clk_10_w_reg[4]_i_1_n_5\,
      Q => cnt_clk_10(6),
      R => \cnt_clk_10_w[0]_i_1_n_0\
    );
\cnt_clk_10_w_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => \cnt_clk_10_w[0]_i_2_n_0\,
      D => \cnt_clk_10_w_reg[4]_i_1_n_4\,
      Q => cnt_clk_10(7),
      R => \cnt_clk_10_w[0]_i_1_n_0\
    );
\cnt_clk_10_w_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => \cnt_clk_10_w[0]_i_2_n_0\,
      D => \cnt_clk_10_w_reg[8]_i_1_n_7\,
      Q => cnt_clk_10(8),
      R => \cnt_clk_10_w[0]_i_1_n_0\
    );
\cnt_clk_10_w_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_clk_10_w_reg[4]_i_1_n_0\,
      CO(3) => \cnt_clk_10_w_reg[8]_i_1_n_0\,
      CO(2) => \cnt_clk_10_w_reg[8]_i_1_n_1\,
      CO(1) => \cnt_clk_10_w_reg[8]_i_1_n_2\,
      CO(0) => \cnt_clk_10_w_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_clk_10_w_reg[8]_i_1_n_4\,
      O(2) => \cnt_clk_10_w_reg[8]_i_1_n_5\,
      O(1) => \cnt_clk_10_w_reg[8]_i_1_n_6\,
      O(0) => \cnt_clk_10_w_reg[8]_i_1_n_7\,
      S(3 downto 0) => cnt_clk_10(11 downto 8)
    );
\cnt_clk_10_w_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => \cnt_clk_10_w[0]_i_2_n_0\,
      D => \cnt_clk_10_w_reg[8]_i_1_n_6\,
      Q => cnt_clk_10(9),
      R => \cnt_clk_10_w[0]_i_1_n_0\
    );
\i_addr[12]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => m00_axis_tready,
      I1 => m00_axis_aresetn,
      O => \i_addr[12]_i_1_n_0\
    );
\i_addr_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_W10_R100,
      CE => '1',
      D => cnt_clk_10(0),
      Q => i_addr(0),
      R => \i_addr[12]_i_1_n_0\
    );
\i_addr_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_W10_R100,
      CE => '1',
      D => cnt_clk_10(10),
      Q => i_addr(10),
      R => \i_addr[12]_i_1_n_0\
    );
\i_addr_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_W10_R100,
      CE => '1',
      D => cnt_clk_10(11),
      Q => i_addr(11),
      R => \i_addr[12]_i_1_n_0\
    );
\i_addr_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_W10_R100,
      CE => '1',
      D => cnt_clk_10(12),
      Q => i_addr(12),
      R => \i_addr[12]_i_1_n_0\
    );
\i_addr_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_W10_R100,
      CE => '1',
      D => cnt_clk_10(1),
      Q => i_addr(1),
      R => \i_addr[12]_i_1_n_0\
    );
\i_addr_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_W10_R100,
      CE => '1',
      D => cnt_clk_10(2),
      Q => i_addr(2),
      R => \i_addr[12]_i_1_n_0\
    );
\i_addr_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_W10_R100,
      CE => '1',
      D => cnt_clk_10(3),
      Q => i_addr(3),
      R => \i_addr[12]_i_1_n_0\
    );
\i_addr_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_W10_R100,
      CE => '1',
      D => cnt_clk_10(4),
      Q => i_addr(4),
      R => \i_addr[12]_i_1_n_0\
    );
\i_addr_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_W10_R100,
      CE => '1',
      D => cnt_clk_10(5),
      Q => i_addr(5),
      R => \i_addr[12]_i_1_n_0\
    );
\i_addr_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_W10_R100,
      CE => '1',
      D => cnt_clk_10(6),
      Q => i_addr(6),
      R => \i_addr[12]_i_1_n_0\
    );
\i_addr_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_W10_R100,
      CE => '1',
      D => cnt_clk_10(7),
      Q => i_addr(7),
      R => \i_addr[12]_i_1_n_0\
    );
\i_addr_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_W10_R100,
      CE => '1',
      D => cnt_clk_10(8),
      Q => i_addr(8),
      R => \i_addr[12]_i_1_n_0\
    );
\i_addr_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_W10_R100,
      CE => '1',
      D => cnt_clk_10(9),
      Q => i_addr(9),
      R => \i_addr[12]_i_1_n_0\
    );
i_write_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8FFF"
    )
        port map (
      I0 => i_write,
      I1 => \cnt_clk_10_w1_carry__0_n_2\,
      I2 => m00_axis_aresetn,
      I3 => m00_axis_tready,
      O => i_write_i_1_n_0
    );
i_write_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_W10_R100,
      CE => '1',
      D => i_write_i_1_n_0,
      Q => i_write,
      R => '0'
    );
m00_axis_tlast_w0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => m00_axis_tlast_w0_carry_n_0,
      CO(2) => m00_axis_tlast_w0_carry_n_1,
      CO(1) => m00_axis_tlast_w0_carry_n_2,
      CO(0) => m00_axis_tlast_w0_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_m00_axis_tlast_w0_carry_O_UNCONNECTED(3 downto 0),
      S(3) => m00_axis_tlast_w0_carry_i_1_n_0,
      S(2) => m00_axis_tlast_w0_carry_i_2_n_0,
      S(1) => m00_axis_tlast_w0_carry_i_3_n_0,
      S(0) => m00_axis_tlast_w0_carry_i_4_n_0
    );
\m00_axis_tlast_w0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => m00_axis_tlast_w0_carry_n_0,
      CO(3) => \m00_axis_tlast_w0_carry__0_n_0\,
      CO(2) => \m00_axis_tlast_w0_carry__0_n_1\,
      CO(1) => \m00_axis_tlast_w0_carry__0_n_2\,
      CO(0) => \m00_axis_tlast_w0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_m00_axis_tlast_w0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \m00_axis_tvalid_w2_carry__2_n_0\,
      S(2) => \m00_axis_tvalid_w2_carry__2_n_0\,
      S(1) => \m00_axis_tlast_w0_carry__0_i_1_n_0\,
      S(0) => \m00_axis_tlast_w0_carry__0_i_2_n_0\
    );
\m00_axis_tlast_w0_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_clk_10_w_reg_n_0_[15]\,
      I1 => m00_axis_tvalid_w2(15),
      I2 => \m00_axis_tvalid_w2_carry__2_n_0\,
      O => \m00_axis_tlast_w0_carry__0_i_1_n_0\
    );
\m00_axis_tlast_w0_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => cnt_clk_10(12),
      I1 => m00_axis_tvalid_w2(12),
      I2 => m00_axis_tvalid_w2(14),
      I3 => \cnt_clk_10_w_reg_n_0_[14]\,
      I4 => m00_axis_tvalid_w2(13),
      I5 => \cnt_clk_10_w_reg_n_0_[13]\,
      O => \m00_axis_tlast_w0_carry__0_i_2_n_0\
    );
\m00_axis_tlast_w0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \m00_axis_tlast_w0_carry__0_n_0\,
      CO(3) => \NLW_m00_axis_tlast_w0_carry__1_CO_UNCONNECTED\(3),
      CO(2) => p_0_in,
      CO(1) => \m00_axis_tlast_w0_carry__1_n_2\,
      CO(0) => \m00_axis_tlast_w0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_m00_axis_tlast_w0_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => \m00_axis_tvalid_w2_carry__2_n_0\,
      S(1) => \m00_axis_tvalid_w2_carry__2_n_0\,
      S(0) => \m00_axis_tvalid_w2_carry__2_n_0\
    );
m00_axis_tlast_w0_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => cnt_clk_10(9),
      I1 => m00_axis_tvalid_w2(9),
      I2 => m00_axis_tvalid_w2(11),
      I3 => cnt_clk_10(11),
      I4 => m00_axis_tvalid_w2(10),
      I5 => cnt_clk_10(10),
      O => m00_axis_tlast_w0_carry_i_1_n_0
    );
m00_axis_tlast_w0_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => cnt_clk_10(6),
      I1 => m00_axis_tvalid_w2(6),
      I2 => m00_axis_tvalid_w2(8),
      I3 => cnt_clk_10(8),
      I4 => m00_axis_tvalid_w2(7),
      I5 => cnt_clk_10(7),
      O => m00_axis_tlast_w0_carry_i_2_n_0
    );
m00_axis_tlast_w0_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => cnt_clk_10(3),
      I1 => m00_axis_tvalid_w2(3),
      I2 => m00_axis_tvalid_w2(5),
      I3 => cnt_clk_10(5),
      I4 => m00_axis_tvalid_w2(4),
      I5 => cnt_clk_10(4),
      O => m00_axis_tlast_w0_carry_i_3_n_0
    );
m00_axis_tlast_w0_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000900990090000"
    )
        port map (
      I0 => m00_axis_tvalid_w2(2),
      I1 => cnt_clk_10(2),
      I2 => m00_axis_tvalid_w2(1),
      I3 => cnt_clk_10(1),
      I4 => FrameSize(0),
      I5 => cnt_clk_10(0),
      O => m00_axis_tlast_w0_carry_i_4_n_0
    );
m00_axis_tlast_w_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => p_0_in,
      I1 => m00_axis_tready,
      I2 => m00_axis_aresetn,
      I3 => i_write,
      O => m00_axis_tlast_w_i_1_n_0
    );
m00_axis_tlast_w_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => '1',
      D => m00_axis_tlast_w_i_1_n_0,
      Q => m00_axis_tlast,
      R => '0'
    );
m00_axis_tvalid_w1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => m00_axis_tvalid_w1_carry_n_0,
      CO(2) => m00_axis_tvalid_w1_carry_n_1,
      CO(1) => m00_axis_tvalid_w1_carry_n_2,
      CO(0) => m00_axis_tvalid_w1_carry_n_3,
      CYINIT => '1',
      DI(3) => m00_axis_tvalid_w1_carry_i_1_n_0,
      DI(2) => m00_axis_tvalid_w1_carry_i_2_n_0,
      DI(1) => m00_axis_tvalid_w1_carry_i_3_n_0,
      DI(0) => m00_axis_tvalid_w1_carry_i_4_n_0,
      O(3 downto 0) => NLW_m00_axis_tvalid_w1_carry_O_UNCONNECTED(3 downto 0),
      S(3) => m00_axis_tvalid_w1_carry_i_5_n_0,
      S(2) => m00_axis_tvalid_w1_carry_i_6_n_0,
      S(1) => m00_axis_tvalid_w1_carry_i_7_n_0,
      S(0) => m00_axis_tvalid_w1_carry_i_8_n_0
    );
\m00_axis_tvalid_w1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => m00_axis_tvalid_w1_carry_n_0,
      CO(3) => \m00_axis_tvalid_w1_carry__0_n_0\,
      CO(2) => \m00_axis_tvalid_w1_carry__0_n_1\,
      CO(1) => \m00_axis_tvalid_w1_carry__0_n_2\,
      CO(0) => \m00_axis_tvalid_w1_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \m00_axis_tvalid_w1_carry__0_i_1_n_0\,
      DI(2) => \m00_axis_tvalid_w1_carry__0_i_2_n_0\,
      DI(1) => \m00_axis_tvalid_w1_carry__0_i_3_n_0\,
      DI(0) => \m00_axis_tvalid_w1_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_m00_axis_tvalid_w1_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \m00_axis_tvalid_w1_carry__0_i_5_n_0\,
      S(2) => \m00_axis_tvalid_w1_carry__0_i_6_n_0\,
      S(1) => \m00_axis_tvalid_w1_carry__0_i_7_n_0\,
      S(0) => \m00_axis_tvalid_w1_carry__0_i_8_n_0\
    );
\m00_axis_tvalid_w1_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => m00_axis_tvalid_w2(14),
      I1 => \cnt_clk_10_w_reg_n_0_[14]\,
      I2 => \cnt_clk_10_w_reg_n_0_[15]\,
      I3 => m00_axis_tvalid_w2(15),
      O => \m00_axis_tvalid_w1_carry__0_i_1_n_0\
    );
\m00_axis_tvalid_w1_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => m00_axis_tvalid_w2(12),
      I1 => cnt_clk_10(12),
      I2 => \cnt_clk_10_w_reg_n_0_[13]\,
      I3 => m00_axis_tvalid_w2(13),
      O => \m00_axis_tvalid_w1_carry__0_i_2_n_0\
    );
\m00_axis_tvalid_w1_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => m00_axis_tvalid_w2(10),
      I1 => cnt_clk_10(10),
      I2 => cnt_clk_10(11),
      I3 => m00_axis_tvalid_w2(11),
      O => \m00_axis_tvalid_w1_carry__0_i_3_n_0\
    );
\m00_axis_tvalid_w1_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => m00_axis_tvalid_w2(8),
      I1 => cnt_clk_10(8),
      I2 => cnt_clk_10(9),
      I3 => m00_axis_tvalid_w2(9),
      O => \m00_axis_tvalid_w1_carry__0_i_4_n_0\
    );
\m00_axis_tvalid_w1_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => m00_axis_tvalid_w2(14),
      I1 => \cnt_clk_10_w_reg_n_0_[14]\,
      I2 => m00_axis_tvalid_w2(15),
      I3 => \cnt_clk_10_w_reg_n_0_[15]\,
      O => \m00_axis_tvalid_w1_carry__0_i_5_n_0\
    );
\m00_axis_tvalid_w1_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => m00_axis_tvalid_w2(12),
      I1 => cnt_clk_10(12),
      I2 => m00_axis_tvalid_w2(13),
      I3 => \cnt_clk_10_w_reg_n_0_[13]\,
      O => \m00_axis_tvalid_w1_carry__0_i_6_n_0\
    );
\m00_axis_tvalid_w1_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => m00_axis_tvalid_w2(10),
      I1 => cnt_clk_10(10),
      I2 => m00_axis_tvalid_w2(11),
      I3 => cnt_clk_10(11),
      O => \m00_axis_tvalid_w1_carry__0_i_7_n_0\
    );
\m00_axis_tvalid_w1_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => m00_axis_tvalid_w2(8),
      I1 => cnt_clk_10(8),
      I2 => m00_axis_tvalid_w2(9),
      I3 => cnt_clk_10(9),
      O => \m00_axis_tvalid_w1_carry__0_i_8_n_0\
    );
\m00_axis_tvalid_w1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \m00_axis_tvalid_w1_carry__0_n_0\,
      CO(3) => \m00_axis_tvalid_w1_carry__1_n_0\,
      CO(2) => \m00_axis_tvalid_w1_carry__1_n_1\,
      CO(1) => \m00_axis_tvalid_w1_carry__1_n_2\,
      CO(0) => \m00_axis_tvalid_w1_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => \m00_axis_tvalid_w1_carry__1_i_1_n_0\,
      DI(2) => \m00_axis_tvalid_w1_carry__1_i_2_n_0\,
      DI(1) => \m00_axis_tvalid_w1_carry__1_i_3_n_0\,
      DI(0) => \m00_axis_tvalid_w1_carry__1_i_4_n_0\,
      O(3 downto 0) => \NLW_m00_axis_tvalid_w1_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => \m00_axis_tvalid_w2_carry__2_n_0\,
      S(2) => \m00_axis_tvalid_w2_carry__2_n_0\,
      S(1) => \m00_axis_tvalid_w2_carry__2_n_0\,
      S(0) => \m00_axis_tvalid_w2_carry__2_n_0\
    );
\m00_axis_tvalid_w1_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \m00_axis_tvalid_w2_carry__2_n_0\,
      O => \m00_axis_tvalid_w1_carry__1_i_1_n_0\
    );
\m00_axis_tvalid_w1_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \m00_axis_tvalid_w2_carry__2_n_0\,
      O => \m00_axis_tvalid_w1_carry__1_i_2_n_0\
    );
\m00_axis_tvalid_w1_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \m00_axis_tvalid_w2_carry__2_n_0\,
      O => \m00_axis_tvalid_w1_carry__1_i_3_n_0\
    );
\m00_axis_tvalid_w1_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \m00_axis_tvalid_w2_carry__2_n_0\,
      O => \m00_axis_tvalid_w1_carry__1_i_4_n_0\
    );
\m00_axis_tvalid_w1_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \m00_axis_tvalid_w1_carry__1_n_0\,
      CO(3) => m00_axis_tvalid_w1,
      CO(2) => \m00_axis_tvalid_w1_carry__2_n_1\,
      CO(1) => \m00_axis_tvalid_w1_carry__2_n_2\,
      CO(0) => \m00_axis_tvalid_w1_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => m00_axis_tvalid_w2(31),
      DI(2) => \m00_axis_tvalid_w1_carry__2_i_2_n_0\,
      DI(1) => \m00_axis_tvalid_w1_carry__2_i_3_n_0\,
      DI(0) => \m00_axis_tvalid_w1_carry__2_i_4_n_0\,
      O(3 downto 0) => \NLW_m00_axis_tvalid_w1_carry__2_O_UNCONNECTED\(3 downto 0),
      S(3) => \m00_axis_tvalid_w2_carry__2_n_0\,
      S(2) => \m00_axis_tvalid_w2_carry__2_n_0\,
      S(1) => \m00_axis_tvalid_w2_carry__2_n_0\,
      S(0) => \m00_axis_tvalid_w2_carry__2_n_0\
    );
\m00_axis_tvalid_w1_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \m00_axis_tvalid_w2_carry__2_n_0\,
      O => m00_axis_tvalid_w2(31)
    );
\m00_axis_tvalid_w1_carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \m00_axis_tvalid_w2_carry__2_n_0\,
      O => \m00_axis_tvalid_w1_carry__2_i_2_n_0\
    );
\m00_axis_tvalid_w1_carry__2_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \m00_axis_tvalid_w2_carry__2_n_0\,
      O => \m00_axis_tvalid_w1_carry__2_i_3_n_0\
    );
\m00_axis_tvalid_w1_carry__2_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \m00_axis_tvalid_w2_carry__2_n_0\,
      O => \m00_axis_tvalid_w1_carry__2_i_4_n_0\
    );
m00_axis_tvalid_w1_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => m00_axis_tvalid_w2(6),
      I1 => cnt_clk_10(6),
      I2 => cnt_clk_10(7),
      I3 => m00_axis_tvalid_w2(7),
      O => m00_axis_tvalid_w1_carry_i_1_n_0
    );
m00_axis_tvalid_w1_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => m00_axis_tvalid_w2(4),
      I1 => cnt_clk_10(4),
      I2 => cnt_clk_10(5),
      I3 => m00_axis_tvalid_w2(5),
      O => m00_axis_tvalid_w1_carry_i_2_n_0
    );
m00_axis_tvalid_w1_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => m00_axis_tvalid_w2(2),
      I1 => cnt_clk_10(2),
      I2 => cnt_clk_10(3),
      I3 => m00_axis_tvalid_w2(3),
      O => m00_axis_tvalid_w1_carry_i_3_n_0
    );
m00_axis_tvalid_w1_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1F01"
    )
        port map (
      I0 => cnt_clk_10(0),
      I1 => FrameSize(0),
      I2 => cnt_clk_10(1),
      I3 => m00_axis_tvalid_w2(1),
      O => m00_axis_tvalid_w1_carry_i_4_n_0
    );
m00_axis_tvalid_w1_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => m00_axis_tvalid_w2(6),
      I1 => cnt_clk_10(6),
      I2 => m00_axis_tvalid_w2(7),
      I3 => cnt_clk_10(7),
      O => m00_axis_tvalid_w1_carry_i_5_n_0
    );
m00_axis_tvalid_w1_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => m00_axis_tvalid_w2(4),
      I1 => cnt_clk_10(4),
      I2 => m00_axis_tvalid_w2(5),
      I3 => cnt_clk_10(5),
      O => m00_axis_tvalid_w1_carry_i_6_n_0
    );
m00_axis_tvalid_w1_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => m00_axis_tvalid_w2(2),
      I1 => cnt_clk_10(2),
      I2 => m00_axis_tvalid_w2(3),
      I3 => cnt_clk_10(3),
      O => m00_axis_tvalid_w1_carry_i_7_n_0
    );
m00_axis_tvalid_w1_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0990"
    )
        port map (
      I0 => m00_axis_tvalid_w2(1),
      I1 => cnt_clk_10(1),
      I2 => cnt_clk_10(0),
      I3 => FrameSize(0),
      O => m00_axis_tvalid_w1_carry_i_8_n_0
    );
m00_axis_tvalid_w2_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => m00_axis_tvalid_w2_carry_n_0,
      CO(2) => m00_axis_tvalid_w2_carry_n_1,
      CO(1) => m00_axis_tvalid_w2_carry_n_2,
      CO(0) => m00_axis_tvalid_w2_carry_n_3,
      CYINIT => FrameSize(0),
      DI(3 downto 0) => FrameSize(4 downto 1),
      O(3 downto 0) => m00_axis_tvalid_w2(4 downto 1),
      S(3) => m00_axis_tvalid_w2_carry_i_1_n_0,
      S(2) => m00_axis_tvalid_w2_carry_i_2_n_0,
      S(1) => m00_axis_tvalid_w2_carry_i_3_n_0,
      S(0) => m00_axis_tvalid_w2_carry_i_4_n_0
    );
\m00_axis_tvalid_w2_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => m00_axis_tvalid_w2_carry_n_0,
      CO(3) => \m00_axis_tvalid_w2_carry__0_n_0\,
      CO(2) => \m00_axis_tvalid_w2_carry__0_n_1\,
      CO(1) => \m00_axis_tvalid_w2_carry__0_n_2\,
      CO(0) => \m00_axis_tvalid_w2_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => FrameSize(8 downto 5),
      O(3 downto 0) => m00_axis_tvalid_w2(8 downto 5),
      S(3) => \m00_axis_tvalid_w2_carry__0_i_1_n_0\,
      S(2) => \m00_axis_tvalid_w2_carry__0_i_2_n_0\,
      S(1) => \m00_axis_tvalid_w2_carry__0_i_3_n_0\,
      S(0) => \m00_axis_tvalid_w2_carry__0_i_4_n_0\
    );
\m00_axis_tvalid_w2_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(8),
      O => \m00_axis_tvalid_w2_carry__0_i_1_n_0\
    );
\m00_axis_tvalid_w2_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(7),
      O => \m00_axis_tvalid_w2_carry__0_i_2_n_0\
    );
\m00_axis_tvalid_w2_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(6),
      O => \m00_axis_tvalid_w2_carry__0_i_3_n_0\
    );
\m00_axis_tvalid_w2_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(5),
      O => \m00_axis_tvalid_w2_carry__0_i_4_n_0\
    );
\m00_axis_tvalid_w2_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \m00_axis_tvalid_w2_carry__0_n_0\,
      CO(3) => \m00_axis_tvalid_w2_carry__1_n_0\,
      CO(2) => \m00_axis_tvalid_w2_carry__1_n_1\,
      CO(1) => \m00_axis_tvalid_w2_carry__1_n_2\,
      CO(0) => \m00_axis_tvalid_w2_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => FrameSize(12 downto 9),
      O(3 downto 0) => m00_axis_tvalid_w2(12 downto 9),
      S(3) => \m00_axis_tvalid_w2_carry__1_i_1_n_0\,
      S(2) => \m00_axis_tvalid_w2_carry__1_i_2_n_0\,
      S(1) => \m00_axis_tvalid_w2_carry__1_i_3_n_0\,
      S(0) => \m00_axis_tvalid_w2_carry__1_i_4_n_0\
    );
\m00_axis_tvalid_w2_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(12),
      O => \m00_axis_tvalid_w2_carry__1_i_1_n_0\
    );
\m00_axis_tvalid_w2_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(11),
      O => \m00_axis_tvalid_w2_carry__1_i_2_n_0\
    );
\m00_axis_tvalid_w2_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(10),
      O => \m00_axis_tvalid_w2_carry__1_i_3_n_0\
    );
\m00_axis_tvalid_w2_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(9),
      O => \m00_axis_tvalid_w2_carry__1_i_4_n_0\
    );
\m00_axis_tvalid_w2_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \m00_axis_tvalid_w2_carry__1_n_0\,
      CO(3) => \m00_axis_tvalid_w2_carry__2_n_0\,
      CO(2) => \NLW_m00_axis_tvalid_w2_carry__2_CO_UNCONNECTED\(2),
      CO(1) => \m00_axis_tvalid_w2_carry__2_n_2\,
      CO(0) => \m00_axis_tvalid_w2_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => FrameSize(15 downto 13),
      O(3) => \NLW_m00_axis_tvalid_w2_carry__2_O_UNCONNECTED\(3),
      O(2 downto 0) => m00_axis_tvalid_w2(15 downto 13),
      S(3) => '1',
      S(2) => \m00_axis_tvalid_w2_carry__2_i_1_n_0\,
      S(1) => \m00_axis_tvalid_w2_carry__2_i_2_n_0\,
      S(0) => \m00_axis_tvalid_w2_carry__2_i_3_n_0\
    );
\m00_axis_tvalid_w2_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(15),
      O => \m00_axis_tvalid_w2_carry__2_i_1_n_0\
    );
\m00_axis_tvalid_w2_carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(14),
      O => \m00_axis_tvalid_w2_carry__2_i_2_n_0\
    );
\m00_axis_tvalid_w2_carry__2_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(13),
      O => \m00_axis_tvalid_w2_carry__2_i_3_n_0\
    );
m00_axis_tvalid_w2_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(4),
      O => m00_axis_tvalid_w2_carry_i_1_n_0
    );
m00_axis_tvalid_w2_carry_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(3),
      O => m00_axis_tvalid_w2_carry_i_2_n_0
    );
m00_axis_tvalid_w2_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(2),
      O => m00_axis_tvalid_w2_carry_i_3_n_0
    );
m00_axis_tvalid_w2_carry_i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(1),
      O => m00_axis_tvalid_w2_carry_i_4_n_0
    );
m00_axis_tvalid_w_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000A8000000"
    )
        port map (
      I0 => m00_axis_tvalid_w1,
      I1 => m00_axis_tvalid_w_i_2_n_0,
      I2 => m00_axis_tvalid_w_i_3_n_0,
      I3 => m00_axis_tready,
      I4 => m00_axis_aresetn,
      I5 => i_write,
      O => m00_axis_tvalid_w_i_1_n_0
    );
m00_axis_tvalid_w_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => cnt_clk_10(3),
      I1 => cnt_clk_10(4),
      I2 => cnt_clk_10(1),
      I3 => cnt_clk_10(2),
      I4 => m00_axis_tvalid_w_i_4_n_0,
      O => m00_axis_tvalid_w_i_2_n_0
    );
m00_axis_tvalid_w_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => cnt_clk_10(11),
      I1 => cnt_clk_10(12),
      I2 => cnt_clk_10(9),
      I3 => cnt_clk_10(10),
      I4 => m00_axis_tvalid_w_i_5_n_0,
      O => m00_axis_tvalid_w_i_3_n_0
    );
m00_axis_tvalid_w_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt_clk_10(6),
      I1 => cnt_clk_10(5),
      I2 => cnt_clk_10(8),
      I3 => cnt_clk_10(7),
      O => m00_axis_tvalid_w_i_4_n_0
    );
m00_axis_tvalid_w_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \cnt_clk_10_w_reg_n_0_[14]\,
      I1 => \cnt_clk_10_w_reg_n_0_[13]\,
      I2 => cnt_clk_10(0),
      I3 => \cnt_clk_10_w_reg_n_0_[15]\,
      O => m00_axis_tvalid_w_i_5_n_0
    );
m00_axis_tvalid_w_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_W10_R100,
      CE => '1',
      D => m00_axis_tvalid_w_i_1_n_0,
      Q => m00_axis_tvalid,
      R => '0'
    );
memory_array_reg_0: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 4,
      READ_WIDTH_B => 4,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 4,
      WRITE_WIDTH_B => 4
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRARDADDR(1 downto 0) => B"11",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRBWRADDR(1 downto 0) => B"11",
      CASCADEINA => '1',
      CASCADEINB => '1',
      CASCADEOUTA => NLW_memory_array_reg_0_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_memory_array_reg_0_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => clk_W10_R100,
      CLKBWRCLK => clk_W10_R100,
      DBITERR => NLW_memory_array_reg_0_DBITERR_UNCONNECTED,
      DIADI(31 downto 4) => B"0000000000000000000000000000",
      DIADI(3 downto 0) => data_in_ram(3 downto 0),
      DIBDI(31 downto 0) => B"00000000000000000000000000001111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 0) => NLW_memory_array_reg_0_DOADO_UNCONNECTED(31 downto 0),
      DOBDO(31 downto 4) => NLW_memory_array_reg_0_DOBDO_UNCONNECTED(31 downto 4),
      DOBDO(3 downto 0) => m00_axis_tdata(3 downto 0),
      DOPADOP(3 downto 0) => NLW_memory_array_reg_0_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_memory_array_reg_0_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_memory_array_reg_0_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => memory_array_reg_7_i_2_n_0,
      ENBWREN => memory_array_reg_7_i_3_n_0,
      INJECTDBITERR => NLW_memory_array_reg_0_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_memory_array_reg_0_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_memory_array_reg_0_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => memory_array_reg_7_i_4_n_0,
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_memory_array_reg_0_SBITERR_UNCONNECTED,
      WEA(3) => m00_axis_tvalid_w12_out,
      WEA(2) => m00_axis_tvalid_w12_out,
      WEA(1) => m00_axis_tvalid_w12_out,
      WEA(0) => m00_axis_tvalid_w12_out,
      WEBWE(7 downto 0) => B"00000000"
    );
memory_array_reg_0_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF1(3),
      O => data_in_ram(3)
    );
memory_array_reg_0_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF1(2),
      O => data_in_ram(2)
    );
memory_array_reg_0_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF1(1),
      O => data_in_ram(1)
    );
memory_array_reg_0_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF1(0),
      O => data_in_ram(0)
    );
memory_array_reg_1: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 4,
      READ_WIDTH_B => 4,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 4,
      WRITE_WIDTH_B => 4
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRARDADDR(1 downto 0) => B"11",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRBWRADDR(1 downto 0) => B"11",
      CASCADEINA => '1',
      CASCADEINB => '1',
      CASCADEOUTA => NLW_memory_array_reg_1_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_memory_array_reg_1_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => clk_W10_R100,
      CLKBWRCLK => clk_W10_R100,
      DBITERR => NLW_memory_array_reg_1_DBITERR_UNCONNECTED,
      DIADI(31 downto 4) => B"0000000000000000000000000000",
      DIADI(3 downto 0) => data_in_ram(7 downto 4),
      DIBDI(31 downto 0) => B"00000000000000000000000000001111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 0) => NLW_memory_array_reg_1_DOADO_UNCONNECTED(31 downto 0),
      DOBDO(31 downto 4) => NLW_memory_array_reg_1_DOBDO_UNCONNECTED(31 downto 4),
      DOBDO(3 downto 0) => m00_axis_tdata(7 downto 4),
      DOPADOP(3 downto 0) => NLW_memory_array_reg_1_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_memory_array_reg_1_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_memory_array_reg_1_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => memory_array_reg_7_i_2_n_0,
      ENBWREN => memory_array_reg_7_i_3_n_0,
      INJECTDBITERR => NLW_memory_array_reg_1_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_memory_array_reg_1_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_memory_array_reg_1_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => memory_array_reg_7_i_4_n_0,
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_memory_array_reg_1_SBITERR_UNCONNECTED,
      WEA(3) => m00_axis_tvalid_w12_out,
      WEA(2) => m00_axis_tvalid_w12_out,
      WEA(1) => m00_axis_tvalid_w12_out,
      WEA(0) => m00_axis_tvalid_w12_out,
      WEBWE(7 downto 0) => B"00000000"
    );
memory_array_reg_1_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF1(7),
      O => data_in_ram(7)
    );
memory_array_reg_1_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF1(6),
      O => data_in_ram(6)
    );
memory_array_reg_1_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF1(5),
      O => data_in_ram(5)
    );
memory_array_reg_1_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF1(4),
      O => data_in_ram(4)
    );
memory_array_reg_2: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 4,
      READ_WIDTH_B => 4,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 4,
      WRITE_WIDTH_B => 4
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRARDADDR(1 downto 0) => B"11",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRBWRADDR(1 downto 0) => B"11",
      CASCADEINA => '1',
      CASCADEINB => '1',
      CASCADEOUTA => NLW_memory_array_reg_2_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_memory_array_reg_2_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => clk_W10_R100,
      CLKBWRCLK => clk_W10_R100,
      DBITERR => NLW_memory_array_reg_2_DBITERR_UNCONNECTED,
      DIADI(31 downto 4) => B"0000000000000000000000000000",
      DIADI(3 downto 0) => data_in_ram(11 downto 8),
      DIBDI(31 downto 0) => B"00000000000000000000000000001111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 0) => NLW_memory_array_reg_2_DOADO_UNCONNECTED(31 downto 0),
      DOBDO(31 downto 4) => NLW_memory_array_reg_2_DOBDO_UNCONNECTED(31 downto 4),
      DOBDO(3 downto 0) => m00_axis_tdata(11 downto 8),
      DOPADOP(3 downto 0) => NLW_memory_array_reg_2_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_memory_array_reg_2_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_memory_array_reg_2_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => memory_array_reg_7_i_2_n_0,
      ENBWREN => memory_array_reg_7_i_3_n_0,
      INJECTDBITERR => NLW_memory_array_reg_2_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_memory_array_reg_2_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_memory_array_reg_2_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => memory_array_reg_7_i_4_n_0,
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_memory_array_reg_2_SBITERR_UNCONNECTED,
      WEA(3) => m00_axis_tvalid_w12_out,
      WEA(2) => m00_axis_tvalid_w12_out,
      WEA(1) => m00_axis_tvalid_w12_out,
      WEA(0) => m00_axis_tvalid_w12_out,
      WEBWE(7 downto 0) => B"00000000"
    );
memory_array_reg_2_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF1(11),
      O => data_in_ram(11)
    );
memory_array_reg_2_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF1(10),
      O => data_in_ram(10)
    );
memory_array_reg_2_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF1(9),
      O => data_in_ram(9)
    );
memory_array_reg_2_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF1(8),
      O => data_in_ram(8)
    );
memory_array_reg_3: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 4,
      READ_WIDTH_B => 4,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 4,
      WRITE_WIDTH_B => 4
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRARDADDR(1 downto 0) => B"11",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRBWRADDR(1 downto 0) => B"11",
      CASCADEINA => '1',
      CASCADEINB => '1',
      CASCADEOUTA => NLW_memory_array_reg_3_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_memory_array_reg_3_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => clk_W10_R100,
      CLKBWRCLK => clk_W10_R100,
      DBITERR => NLW_memory_array_reg_3_DBITERR_UNCONNECTED,
      DIADI(31 downto 4) => B"0000000000000000000000000000",
      DIADI(3 downto 0) => data_in_ram(15 downto 12),
      DIBDI(31 downto 0) => B"00000000000000000000000000001111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 0) => NLW_memory_array_reg_3_DOADO_UNCONNECTED(31 downto 0),
      DOBDO(31 downto 4) => NLW_memory_array_reg_3_DOBDO_UNCONNECTED(31 downto 4),
      DOBDO(3 downto 0) => m00_axis_tdata(15 downto 12),
      DOPADOP(3 downto 0) => NLW_memory_array_reg_3_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_memory_array_reg_3_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_memory_array_reg_3_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => memory_array_reg_7_i_2_n_0,
      ENBWREN => memory_array_reg_7_i_3_n_0,
      INJECTDBITERR => NLW_memory_array_reg_3_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_memory_array_reg_3_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_memory_array_reg_3_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => memory_array_reg_7_i_4_n_0,
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_memory_array_reg_3_SBITERR_UNCONNECTED,
      WEA(3) => m00_axis_tvalid_w12_out,
      WEA(2) => m00_axis_tvalid_w12_out,
      WEA(1) => m00_axis_tvalid_w12_out,
      WEA(0) => m00_axis_tvalid_w12_out,
      WEBWE(7 downto 0) => B"00000000"
    );
memory_array_reg_3_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF1(15),
      O => data_in_ram(15)
    );
memory_array_reg_3_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF1(14),
      O => data_in_ram(14)
    );
memory_array_reg_3_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF1(13),
      O => data_in_ram(13)
    );
memory_array_reg_3_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF1(12),
      O => data_in_ram(12)
    );
memory_array_reg_4: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 4,
      READ_WIDTH_B => 4,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 4,
      WRITE_WIDTH_B => 4
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRARDADDR(1 downto 0) => B"11",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRBWRADDR(1 downto 0) => B"11",
      CASCADEINA => '1',
      CASCADEINB => '1',
      CASCADEOUTA => NLW_memory_array_reg_4_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_memory_array_reg_4_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => clk_W10_R100,
      CLKBWRCLK => clk_W10_R100,
      DBITERR => NLW_memory_array_reg_4_DBITERR_UNCONNECTED,
      DIADI(31 downto 4) => B"0000000000000000000000000000",
      DIADI(3 downto 0) => data_in_ram(19 downto 16),
      DIBDI(31 downto 0) => B"00000000000000000000000000001111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 0) => NLW_memory_array_reg_4_DOADO_UNCONNECTED(31 downto 0),
      DOBDO(31 downto 4) => NLW_memory_array_reg_4_DOBDO_UNCONNECTED(31 downto 4),
      DOBDO(3 downto 0) => m00_axis_tdata(19 downto 16),
      DOPADOP(3 downto 0) => NLW_memory_array_reg_4_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_memory_array_reg_4_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_memory_array_reg_4_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => memory_array_reg_7_i_2_n_0,
      ENBWREN => memory_array_reg_7_i_3_n_0,
      INJECTDBITERR => NLW_memory_array_reg_4_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_memory_array_reg_4_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_memory_array_reg_4_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => memory_array_reg_7_i_4_n_0,
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_memory_array_reg_4_SBITERR_UNCONNECTED,
      WEA(3) => m00_axis_tvalid_w12_out,
      WEA(2) => m00_axis_tvalid_w12_out,
      WEA(1) => m00_axis_tvalid_w12_out,
      WEA(0) => m00_axis_tvalid_w12_out,
      WEBWE(7 downto 0) => B"00000000"
    );
memory_array_reg_4_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF2(3),
      O => data_in_ram(19)
    );
memory_array_reg_4_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF2(2),
      O => data_in_ram(18)
    );
memory_array_reg_4_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF2(1),
      O => data_in_ram(17)
    );
memory_array_reg_4_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF2(0),
      O => data_in_ram(16)
    );
memory_array_reg_5: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 4,
      READ_WIDTH_B => 4,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 4,
      WRITE_WIDTH_B => 4
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRARDADDR(1 downto 0) => B"11",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRBWRADDR(1 downto 0) => B"11",
      CASCADEINA => '1',
      CASCADEINB => '1',
      CASCADEOUTA => NLW_memory_array_reg_5_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_memory_array_reg_5_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => clk_W10_R100,
      CLKBWRCLK => clk_W10_R100,
      DBITERR => NLW_memory_array_reg_5_DBITERR_UNCONNECTED,
      DIADI(31 downto 4) => B"0000000000000000000000000000",
      DIADI(3 downto 0) => data_in_ram(23 downto 20),
      DIBDI(31 downto 0) => B"00000000000000000000000000001111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 0) => NLW_memory_array_reg_5_DOADO_UNCONNECTED(31 downto 0),
      DOBDO(31 downto 4) => NLW_memory_array_reg_5_DOBDO_UNCONNECTED(31 downto 4),
      DOBDO(3 downto 0) => m00_axis_tdata(23 downto 20),
      DOPADOP(3 downto 0) => NLW_memory_array_reg_5_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_memory_array_reg_5_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_memory_array_reg_5_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => memory_array_reg_7_i_2_n_0,
      ENBWREN => memory_array_reg_7_i_3_n_0,
      INJECTDBITERR => NLW_memory_array_reg_5_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_memory_array_reg_5_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_memory_array_reg_5_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => memory_array_reg_7_i_4_n_0,
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_memory_array_reg_5_SBITERR_UNCONNECTED,
      WEA(3) => m00_axis_tvalid_w12_out,
      WEA(2) => m00_axis_tvalid_w12_out,
      WEA(1) => m00_axis_tvalid_w12_out,
      WEA(0) => m00_axis_tvalid_w12_out,
      WEBWE(7 downto 0) => B"00000000"
    );
memory_array_reg_5_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF2(7),
      O => data_in_ram(23)
    );
memory_array_reg_5_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF2(6),
      O => data_in_ram(22)
    );
memory_array_reg_5_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF2(5),
      O => data_in_ram(21)
    );
memory_array_reg_5_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF2(4),
      O => data_in_ram(20)
    );
memory_array_reg_6: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 4,
      READ_WIDTH_B => 4,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 4,
      WRITE_WIDTH_B => 4
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRARDADDR(1 downto 0) => B"11",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRBWRADDR(1 downto 0) => B"11",
      CASCADEINA => '1',
      CASCADEINB => '1',
      CASCADEOUTA => NLW_memory_array_reg_6_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_memory_array_reg_6_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => clk_W10_R100,
      CLKBWRCLK => clk_W10_R100,
      DBITERR => NLW_memory_array_reg_6_DBITERR_UNCONNECTED,
      DIADI(31 downto 4) => B"0000000000000000000000000000",
      DIADI(3 downto 0) => data_in_ram(27 downto 24),
      DIBDI(31 downto 0) => B"00000000000000000000000000001111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 0) => NLW_memory_array_reg_6_DOADO_UNCONNECTED(31 downto 0),
      DOBDO(31 downto 4) => NLW_memory_array_reg_6_DOBDO_UNCONNECTED(31 downto 4),
      DOBDO(3 downto 0) => m00_axis_tdata(27 downto 24),
      DOPADOP(3 downto 0) => NLW_memory_array_reg_6_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_memory_array_reg_6_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_memory_array_reg_6_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => memory_array_reg_7_i_2_n_0,
      ENBWREN => memory_array_reg_7_i_3_n_0,
      INJECTDBITERR => NLW_memory_array_reg_6_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_memory_array_reg_6_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_memory_array_reg_6_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => memory_array_reg_7_i_4_n_0,
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_memory_array_reg_6_SBITERR_UNCONNECTED,
      WEA(3) => m00_axis_tvalid_w12_out,
      WEA(2) => m00_axis_tvalid_w12_out,
      WEA(1) => m00_axis_tvalid_w12_out,
      WEA(0) => m00_axis_tvalid_w12_out,
      WEBWE(7 downto 0) => B"00000000"
    );
memory_array_reg_6_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF2(11),
      O => data_in_ram(27)
    );
memory_array_reg_6_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF2(10),
      O => data_in_ram(26)
    );
memory_array_reg_6_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF2(9),
      O => data_in_ram(25)
    );
memory_array_reg_6_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF2(8),
      O => data_in_ram(24)
    );
memory_array_reg_7: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 4,
      READ_WIDTH_B => 4,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "READ_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 4,
      WRITE_WIDTH_B => 4
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRARDADDR(1 downto 0) => B"11",
      ADDRBWRADDR(15) => '1',
      ADDRBWRADDR(14 downto 2) => i_addr(12 downto 0),
      ADDRBWRADDR(1 downto 0) => B"11",
      CASCADEINA => '1',
      CASCADEINB => '1',
      CASCADEOUTA => NLW_memory_array_reg_7_CASCADEOUTA_UNCONNECTED,
      CASCADEOUTB => NLW_memory_array_reg_7_CASCADEOUTB_UNCONNECTED,
      CLKARDCLK => clk_W10_R100,
      CLKBWRCLK => clk_W10_R100,
      DBITERR => NLW_memory_array_reg_7_DBITERR_UNCONNECTED,
      DIADI(31 downto 4) => B"0000000000000000000000000000",
      DIADI(3 downto 0) => data_in_ram(31 downto 28),
      DIBDI(31 downto 0) => B"00000000000000000000000000001111",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 0) => NLW_memory_array_reg_7_DOADO_UNCONNECTED(31 downto 0),
      DOBDO(31 downto 4) => NLW_memory_array_reg_7_DOBDO_UNCONNECTED(31 downto 4),
      DOBDO(3 downto 0) => m00_axis_tdata(31 downto 28),
      DOPADOP(3 downto 0) => NLW_memory_array_reg_7_DOPADOP_UNCONNECTED(3 downto 0),
      DOPBDOP(3 downto 0) => NLW_memory_array_reg_7_DOPBDOP_UNCONNECTED(3 downto 0),
      ECCPARITY(7 downto 0) => NLW_memory_array_reg_7_ECCPARITY_UNCONNECTED(7 downto 0),
      ENARDEN => memory_array_reg_7_i_2_n_0,
      ENBWREN => memory_array_reg_7_i_3_n_0,
      INJECTDBITERR => NLW_memory_array_reg_7_INJECTDBITERR_UNCONNECTED,
      INJECTSBITERR => NLW_memory_array_reg_7_INJECTSBITERR_UNCONNECTED,
      RDADDRECC(8 downto 0) => NLW_memory_array_reg_7_RDADDRECC_UNCONNECTED(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => memory_array_reg_7_i_4_n_0,
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => NLW_memory_array_reg_7_SBITERR_UNCONNECTED,
      WEA(3) => m00_axis_tvalid_w12_out,
      WEA(2) => m00_axis_tvalid_w12_out,
      WEA(1) => m00_axis_tvalid_w12_out,
      WEA(0) => m00_axis_tvalid_w12_out,
      WEBWE(7 downto 0) => B"00000000"
    );
memory_array_reg_7_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data_clk,
      I1 => i_write,
      I2 => m00_axis_aclk,
      O => clk_W10_R100
    );
memory_array_reg_7_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => \cnt_clk_10_w1_carry__0_n_2\,
      O => memory_array_reg_7_i_2_n_0
    );
memory_array_reg_7_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => i_write,
      I1 => m00_axis_aresetn,
      I2 => m00_axis_tready,
      O => memory_array_reg_7_i_3_n_0
    );
memory_array_reg_7_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => i_write,
      I1 => m00_axis_aresetn,
      I2 => m00_axis_tready,
      I3 => \cnt_clk_10_w0_carry__0_n_0\,
      O => memory_array_reg_7_i_4_n_0
    );
memory_array_reg_7_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF2(15),
      O => data_in_ram(31)
    );
memory_array_reg_7_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF2(14),
      O => data_in_ram(30)
    );
memory_array_reg_7_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF2(13),
      O => data_in_ram(29)
    );
memory_array_reg_7_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i_write,
      I1 => data_in_IF2(12),
      O => data_in_ram(28)
    );
memory_array_reg_7_i_9: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => m00_axis_aresetn,
      I1 => m00_axis_tready,
      O => m00_axis_tvalid_w12_out
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_clk : in STD_LOGIC;
    data_in_IF1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_in_IF2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_Sample_Generator_0_0,Sample_Generator_v3_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "Sample_Generator_v3_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const1>\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TVALID";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB";
begin
  m00_axis_tstrb(3) <= \<const1>\;
  m00_axis_tstrb(2) <= \<const1>\;
  m00_axis_tstrb(1) <= \<const1>\;
  m00_axis_tstrb(0) <= \<const1>\;
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0
     port map (
      FrameSize(15 downto 0) => FrameSize(15 downto 0),
      data_clk => data_clk,
      data_in_IF1(15 downto 0) => data_in_IF1(15 downto 0),
      data_in_IF2(15 downto 0) => data_in_IF2(15 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_aresetn => m00_axis_aresetn,
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      m00_axis_tlast => m00_axis_tlast,
      m00_axis_tready => m00_axis_tready,
      m00_axis_tvalid => m00_axis_tvalid
    );
end STRUCTURE;
