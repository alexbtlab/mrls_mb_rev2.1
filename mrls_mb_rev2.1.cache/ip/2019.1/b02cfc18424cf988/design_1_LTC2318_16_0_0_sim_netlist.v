// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Wed Oct  6 15:58:54 2021
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_LTC2318_16_0_0_sim_netlist.v
// Design      : design_1_LTC2318_16_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_LTC2387_16
   (adc_clkx_p,
    adc_clkx_n,
    fpga_clk_sync,
    ADC_DATA,
    adc_dax_p,
    adc_dax_n,
    adc_dbx_p,
    adc_dbx_n,
    adc_dcox_p,
    adc_dcox_n,
    clk_200,
    fpga_clk_async,
    clk_100);
  output adc_clkx_p;
  output adc_clkx_n;
  output fpga_clk_sync;
  output [15:0]ADC_DATA;
  input adc_dax_p;
  input adc_dax_n;
  input adc_dbx_p;
  input adc_dbx_n;
  input adc_dcox_p;
  input adc_dcox_n;
  input clk_200;
  input fpga_clk_async;
  input clk_100;

  wire [15:0]ADC_DATA;
  wire [15:0]ADC_DATA_w;
  wire \FSM_onehot_cnt_pos_dco[0]_i_1_n_0 ;
  wire \FSM_onehot_cnt_pos_dco[1]_i_1_n_0 ;
  wire \FSM_onehot_cnt_pos_dco[2]_i_1_n_0 ;
  wire \FSM_onehot_cnt_pos_dco_reg_n_0_[1] ;
  wire \FSM_onehot_cnt_pos_dco_reg_n_0_[2] ;
  wire adc_clk;
  wire adc_clkx_n;
  wire adc_clkx_p;
  wire adc_da;
  wire adc_dax_n;
  wire adc_dax_p;
  wire adc_db;
  wire adc_dbx_n;
  wire adc_dbx_p;
  wire adc_dco;
  wire adc_dco_delayed;
  wire adc_dcox_n;
  wire adc_dcox_p;
  wire allowed;
  wire allowed0__0;
  wire bit_0_i_1_n_0;
  wire bit_10_i_1_n_0;
  wire bit_11_i_1_n_0;
  wire bit_12;
  wire bit_12_i_1_n_0;
  wire bit_13_i_1_n_0;
  wire bit_14_i_1_n_0;
  wire bit_15_i_1_n_0;
  wire bit_1_i_1_n_0;
  wire bit_2_i_1_n_0;
  wire bit_3_i_1_n_0;
  wire bit_4_i_1_n_0;
  wire bit_5_i_1_n_0;
  wire bit_6_i_1_n_0;
  wire bit_7_i_1_n_0;
  wire bit_8_i_1_n_0;
  wire bit_9;
  wire bit_9_i_1_n_0;
  wire clk_100;
  wire clk_200;
  wire [2:0]cnt100;
  wire \cnt100[0]_i_1_n_0 ;
  wire \cnt100[1]_i_1_n_0 ;
  wire \cnt100[2]_i_1_n_0 ;
  wire fpga_clk_async;
  wire fpga_clk_sync;
  wire NLW_IDELAYCTRL_inst_RDY_UNCONNECTED;
  wire NLW_IDELAYE2_adc_if2_ch1_dco_IDATAIN_UNCONNECTED;
  wire [4:0]NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEIN_UNCONNECTED;
  wire [4:0]NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEOUT_UNCONNECTED;

  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADC_DATA_r_reg[0] 
       (.C(fpga_clk_sync),
        .CE(1'b1),
        .D(ADC_DATA_w[0]),
        .Q(ADC_DATA[0]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADC_DATA_r_reg[10] 
       (.C(fpga_clk_sync),
        .CE(1'b1),
        .D(ADC_DATA_w[10]),
        .Q(ADC_DATA[10]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADC_DATA_r_reg[11] 
       (.C(fpga_clk_sync),
        .CE(1'b1),
        .D(ADC_DATA_w[11]),
        .Q(ADC_DATA[11]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADC_DATA_r_reg[12] 
       (.C(fpga_clk_sync),
        .CE(1'b1),
        .D(ADC_DATA_w[12]),
        .Q(ADC_DATA[12]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADC_DATA_r_reg[13] 
       (.C(fpga_clk_sync),
        .CE(1'b1),
        .D(ADC_DATA_w[13]),
        .Q(ADC_DATA[13]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADC_DATA_r_reg[14] 
       (.C(fpga_clk_sync),
        .CE(1'b1),
        .D(ADC_DATA_w[14]),
        .Q(ADC_DATA[14]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADC_DATA_r_reg[15] 
       (.C(fpga_clk_sync),
        .CE(1'b1),
        .D(ADC_DATA_w[15]),
        .Q(ADC_DATA[15]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADC_DATA_r_reg[1] 
       (.C(fpga_clk_sync),
        .CE(1'b1),
        .D(ADC_DATA_w[1]),
        .Q(ADC_DATA[1]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADC_DATA_r_reg[2] 
       (.C(fpga_clk_sync),
        .CE(1'b1),
        .D(ADC_DATA_w[2]),
        .Q(ADC_DATA[2]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADC_DATA_r_reg[3] 
       (.C(fpga_clk_sync),
        .CE(1'b1),
        .D(ADC_DATA_w[3]),
        .Q(ADC_DATA[3]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADC_DATA_r_reg[4] 
       (.C(fpga_clk_sync),
        .CE(1'b1),
        .D(ADC_DATA_w[4]),
        .Q(ADC_DATA[4]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADC_DATA_r_reg[5] 
       (.C(fpga_clk_sync),
        .CE(1'b1),
        .D(ADC_DATA_w[5]),
        .Q(ADC_DATA[5]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADC_DATA_r_reg[6] 
       (.C(fpga_clk_sync),
        .CE(1'b1),
        .D(ADC_DATA_w[6]),
        .Q(ADC_DATA[6]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADC_DATA_r_reg[7] 
       (.C(fpga_clk_sync),
        .CE(1'b1),
        .D(ADC_DATA_w[7]),
        .Q(ADC_DATA[7]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADC_DATA_r_reg[8] 
       (.C(fpga_clk_sync),
        .CE(1'b1),
        .D(ADC_DATA_w[8]),
        .Q(ADC_DATA[8]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \ADC_DATA_r_reg[9] 
       (.C(fpga_clk_sync),
        .CE(1'b1),
        .D(ADC_DATA_w[9]),
        .Q(ADC_DATA[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \FSM_onehot_cnt_pos_dco[0]_i_1 
       (.I0(cnt100[2]),
        .I1(cnt100[0]),
        .I2(cnt100[1]),
        .O(\FSM_onehot_cnt_pos_dco[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h8AAA)) 
    \FSM_onehot_cnt_pos_dco[1]_i_1 
       (.I0(bit_9),
        .I1(cnt100[2]),
        .I2(cnt100[0]),
        .I3(cnt100[1]),
        .O(\FSM_onehot_cnt_pos_dco[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h8AAA)) 
    \FSM_onehot_cnt_pos_dco[2]_i_1 
       (.I0(\FSM_onehot_cnt_pos_dco_reg_n_0_[1] ),
        .I1(cnt100[2]),
        .I2(cnt100[0]),
        .I3(cnt100[1]),
        .O(\FSM_onehot_cnt_pos_dco[2]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "iSTATE:1000,iSTATE0:0100,iSTATE1:0001,iSTATE2:0010," *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_cnt_pos_dco_reg[0] 
       (.C(adc_dco_delayed),
        .CE(1'b1),
        .D(\FSM_onehot_cnt_pos_dco[0]_i_1_n_0 ),
        .Q(bit_9),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "iSTATE:1000,iSTATE0:0100,iSTATE1:0001,iSTATE2:0010," *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_cnt_pos_dco_reg[1] 
       (.C(adc_dco_delayed),
        .CE(1'b1),
        .D(\FSM_onehot_cnt_pos_dco[1]_i_1_n_0 ),
        .Q(\FSM_onehot_cnt_pos_dco_reg_n_0_[1] ),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "iSTATE:1000,iSTATE0:0100,iSTATE1:0001,iSTATE2:0010," *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_cnt_pos_dco_reg[2] 
       (.C(adc_dco_delayed),
        .CE(1'b1),
        .D(\FSM_onehot_cnt_pos_dco[2]_i_1_n_0 ),
        .Q(\FSM_onehot_cnt_pos_dco_reg_n_0_[2] ),
        .R(1'b0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_da
       (.I(adc_dax_p),
        .IB(adc_dax_n),
        .O(adc_da));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_db
       (.I(adc_dbx_p),
        .IB(adc_dbx_n),
        .O(adc_db));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_dco
       (.I(adc_dcox_p),
        .IB(adc_dcox_n),
        .O(adc_dco));
  (* BOX_TYPE = "PRIMITIVE" *) 
  IDELAYCTRL #(
    .SIM_DEVICE("7SERIES")) 
    IDELAYCTRL_inst
       (.RDY(NLW_IDELAYCTRL_inst_RDY_UNCONNECTED),
        .REFCLK(clk_200),
        .RST(1'b0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SIM_DELAY_D = "0" *) 
  IDELAYE2 #(
    .CINVCTRL_SEL("FALSE"),
    .DELAY_SRC("DATAIN"),
    .HIGH_PERFORMANCE_MODE("TRUE"),
    .IDELAY_TYPE("FIXED"),
    .IDELAY_VALUE(15),
    .IS_C_INVERTED(1'b0),
    .IS_DATAIN_INVERTED(1'b0),
    .IS_IDATAIN_INVERTED(1'b0),
    .PIPE_SEL("FALSE"),
    .REFCLK_FREQUENCY(200.000000),
    .SIGNAL_PATTERN("CLOCK")) 
    IDELAYE2_adc_if2_ch1_dco
       (.C(clk_200),
        .CE(1'b0),
        .CINVCTRL(1'b0),
        .CNTVALUEIN(NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEIN_UNCONNECTED[4:0]),
        .CNTVALUEOUT(NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEOUT_UNCONNECTED[4:0]),
        .DATAIN(adc_dco),
        .DATAOUT(adc_dco_delayed),
        .IDATAIN(NLW_IDELAYE2_adc_if2_ch1_dco_IDATAIN_UNCONNECTED),
        .INC(1'b0),
        .LD(1'b0),
        .LDPIPEEN(1'b0),
        .REGRST(1'b0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    OBUFDS_adc_clk
       (.I(adc_clk),
        .O(adc_clkx_p),
        .OB(adc_clkx_n));
  LUT2 #(
    .INIT(4'h8)) 
    OBUFDS_adc_clk_i_1
       (.I0(allowed),
        .I1(clk_100),
        .O(adc_clk));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h6)) 
    allowed0
       (.I0(cnt100[1]),
        .I1(cnt100[2]),
        .O(allowed0__0));
  FDRE #(
    .INIT(1'b0)) 
    allowed_reg
       (.C(clk_100),
        .CE(1'b1),
        .D(allowed0__0),
        .Q(allowed),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFEFF00000200)) 
    bit_0_i_1
       (.I0(adc_db),
        .I1(\FSM_onehot_cnt_pos_dco_reg_n_0_[1] ),
        .I2(bit_9),
        .I3(\FSM_onehot_cnt_pos_dco_reg_n_0_[2] ),
        .I4(bit_12),
        .I5(ADC_DATA_w[0]),
        .O(bit_0_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h08)) 
    bit_0_i_2
       (.I0(cnt100[1]),
        .I1(cnt100[0]),
        .I2(cnt100[2]),
        .O(bit_12));
  FDRE #(
    .INIT(1'b0)) 
    bit_0_reg
       (.C(adc_dco_delayed),
        .CE(1'b1),
        .D(bit_0_i_1_n_0),
        .Q(ADC_DATA_w[0]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    bit_10_i_1
       (.I0(adc_db),
        .I1(bit_9),
        .I2(ADC_DATA_w[10]),
        .O(bit_10_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    bit_10_reg
       (.C(adc_dco_delayed),
        .CE(1'b1),
        .D(bit_10_i_1_n_0),
        .Q(ADC_DATA_w[10]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    bit_11_i_1
       (.I0(adc_da),
        .I1(bit_9),
        .I2(ADC_DATA_w[11]),
        .O(bit_11_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    bit_11_reg
       (.C(adc_dco_delayed),
        .CE(1'b1),
        .D(bit_11_i_1_n_0),
        .Q(ADC_DATA_w[11]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    bit_12_i_1
       (.I0(adc_db),
        .I1(cnt100[1]),
        .I2(cnt100[0]),
        .I3(cnt100[2]),
        .I4(ADC_DATA_w[12]),
        .O(bit_12_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    bit_12_reg
       (.C(adc_dco_delayed),
        .CE(1'b1),
        .D(bit_12_i_1_n_0),
        .Q(ADC_DATA_w[12]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    bit_13_i_1
       (.I0(adc_da),
        .I1(cnt100[1]),
        .I2(cnt100[0]),
        .I3(cnt100[2]),
        .I4(ADC_DATA_w[13]),
        .O(bit_13_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    bit_13_reg
       (.C(adc_dco_delayed),
        .CE(1'b1),
        .D(bit_13_i_1_n_0),
        .Q(ADC_DATA_w[13]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    bit_14_i_1
       (.I0(adc_db),
        .I1(cnt100[0]),
        .I2(cnt100[1]),
        .I3(cnt100[2]),
        .I4(ADC_DATA_w[14]),
        .O(bit_14_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    bit_14_reg
       (.C(clk_100),
        .CE(1'b1),
        .D(bit_14_i_1_n_0),
        .Q(ADC_DATA_w[14]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    bit_15_i_1
       (.I0(adc_da),
        .I1(cnt100[0]),
        .I2(cnt100[1]),
        .I3(cnt100[2]),
        .I4(ADC_DATA_w[15]),
        .O(bit_15_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    bit_15_reg
       (.C(clk_100),
        .CE(1'b1),
        .D(bit_15_i_1_n_0),
        .Q(ADC_DATA_w[15]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFEFF00000200)) 
    bit_1_i_1
       (.I0(adc_da),
        .I1(\FSM_onehot_cnt_pos_dco_reg_n_0_[1] ),
        .I2(bit_9),
        .I3(\FSM_onehot_cnt_pos_dco_reg_n_0_[2] ),
        .I4(bit_12),
        .I5(ADC_DATA_w[1]),
        .O(bit_1_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    bit_1_reg
       (.C(adc_dco_delayed),
        .CE(1'b1),
        .D(bit_1_i_1_n_0),
        .Q(ADC_DATA_w[1]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    bit_2_i_1
       (.I0(adc_db),
        .I1(\FSM_onehot_cnt_pos_dco_reg_n_0_[2] ),
        .I2(bit_9),
        .I3(\FSM_onehot_cnt_pos_dco_reg_n_0_[1] ),
        .I4(ADC_DATA_w[2]),
        .O(bit_2_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    bit_2_reg
       (.C(adc_dco_delayed),
        .CE(1'b1),
        .D(bit_2_i_1_n_0),
        .Q(ADC_DATA_w[2]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFB0008)) 
    bit_3_i_1
       (.I0(adc_da),
        .I1(\FSM_onehot_cnt_pos_dco_reg_n_0_[2] ),
        .I2(bit_9),
        .I3(\FSM_onehot_cnt_pos_dco_reg_n_0_[1] ),
        .I4(ADC_DATA_w[3]),
        .O(bit_3_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    bit_3_reg
       (.C(adc_dco_delayed),
        .CE(1'b1),
        .D(bit_3_i_1_n_0),
        .Q(ADC_DATA_w[3]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hBFBBBBBB80888888)) 
    bit_4_i_1
       (.I0(adc_db),
        .I1(\FSM_onehot_cnt_pos_dco_reg_n_0_[1] ),
        .I2(cnt100[2]),
        .I3(cnt100[0]),
        .I4(cnt100[1]),
        .I5(ADC_DATA_w[4]),
        .O(bit_4_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    bit_4_reg
       (.C(adc_dco_delayed),
        .CE(1'b1),
        .D(bit_4_i_1_n_0),
        .Q(ADC_DATA_w[4]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hBFBBBBBB80888888)) 
    bit_5_i_1
       (.I0(adc_da),
        .I1(\FSM_onehot_cnt_pos_dco_reg_n_0_[1] ),
        .I2(cnt100[2]),
        .I3(cnt100[0]),
        .I4(cnt100[1]),
        .I5(ADC_DATA_w[5]),
        .O(bit_5_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    bit_5_reg
       (.C(adc_dco_delayed),
        .CE(1'b1),
        .D(bit_5_i_1_n_0),
        .Q(ADC_DATA_w[5]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    bit_6_i_1
       (.I0(adc_db),
        .I1(\FSM_onehot_cnt_pos_dco_reg_n_0_[1] ),
        .I2(ADC_DATA_w[6]),
        .O(bit_6_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    bit_6_reg
       (.C(adc_dco_delayed),
        .CE(1'b1),
        .D(bit_6_i_1_n_0),
        .Q(ADC_DATA_w[6]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    bit_7_i_1
       (.I0(adc_da),
        .I1(\FSM_onehot_cnt_pos_dco_reg_n_0_[1] ),
        .I2(ADC_DATA_w[7]),
        .O(bit_7_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    bit_7_reg
       (.C(adc_dco_delayed),
        .CE(1'b1),
        .D(bit_7_i_1_n_0),
        .Q(ADC_DATA_w[7]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hBFBBBBBB80888888)) 
    bit_8_i_1
       (.I0(adc_db),
        .I1(bit_9),
        .I2(cnt100[2]),
        .I3(cnt100[0]),
        .I4(cnt100[1]),
        .I5(ADC_DATA_w[8]),
        .O(bit_8_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    bit_8_reg
       (.C(adc_dco_delayed),
        .CE(1'b1),
        .D(bit_8_i_1_n_0),
        .Q(ADC_DATA_w[8]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hBFBBBBBB80888888)) 
    bit_9_i_1
       (.I0(adc_da),
        .I1(bit_9),
        .I2(cnt100[2]),
        .I3(cnt100[0]),
        .I4(cnt100[1]),
        .I5(ADC_DATA_w[9]),
        .O(bit_9_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    bit_9_reg
       (.C(adc_dco_delayed),
        .CE(1'b1),
        .D(bit_9_i_1_n_0),
        .Q(ADC_DATA_w[9]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h1)) 
    \cnt100[0]_i_1 
       (.I0(cnt100[0]),
        .I1(fpga_clk_sync),
        .O(\cnt100[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h06)) 
    \cnt100[1]_i_1 
       (.I0(cnt100[1]),
        .I1(cnt100[0]),
        .I2(fpga_clk_sync),
        .O(\cnt100[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h006A)) 
    \cnt100[2]_i_1 
       (.I0(cnt100[2]),
        .I1(cnt100[0]),
        .I2(cnt100[1]),
        .I3(fpga_clk_sync),
        .O(\cnt100[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[0] 
       (.C(clk_100),
        .CE(1'b1),
        .D(\cnt100[0]_i_1_n_0 ),
        .Q(cnt100[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[1] 
       (.C(clk_100),
        .CE(1'b1),
        .D(\cnt100[1]_i_1_n_0 ),
        .Q(cnt100[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[2] 
       (.C(clk_100),
        .CE(1'b1),
        .D(\cnt100[2]_i_1_n_0 ),
        .Q(cnt100[2]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    fpga_clk_sync_r_reg
       (.C(clk_100),
        .CE(1'b1),
        .D(fpga_clk_async),
        .Q(fpga_clk_sync),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_LTC2318_16_0_0,LTC2387_16,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "LTC2387_16,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (ADC_DATA,
    fpga_clk_async,
    fpga_clk_sync,
    adc_dax_p,
    adc_dax_n,
    adc_dbx_p,
    adc_dbx_n,
    adc_dcox_p,
    adc_dcox_n,
    clk_100,
    adc_clkx_p,
    adc_clkx_n,
    clk_200);
  output [15:0]ADC_DATA;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 fpga_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME fpga_clk, FREQ_HZ 10000000, PHASE 0.000, CLK_DOMAIN design_1_FPGA_CLK, INSERT_VIP 0" *) input fpga_clk_async;
  output fpga_clk_sync;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dax p" *) input adc_dax_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dax n" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_dax, SV_INTERFACE true" *) input adc_dax_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dbx p" *) input adc_dbx_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dbx n" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_dbx, SV_INTERFACE true" *) input adc_dbx_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dcox p" *) input adc_dcox_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dcox n" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_dcox, SV_INTERFACE true" *) input adc_dcox_n;
  input clk_100;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_clkx p" *) output adc_clkx_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_clkx n" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_clkx, SV_INTERFACE true" *) output adc_clkx_n;
  input clk_200;

  wire [15:0]ADC_DATA;
  (* SLEW = "SLOW" *) wire adc_clkx_n;
  (* SLEW = "SLOW" *) wire adc_clkx_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dax_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dax_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dbx_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dbx_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dcox_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dcox_p;
  wire clk_100;
  wire clk_200;
  wire fpga_clk_async;
  wire fpga_clk_sync;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_LTC2387_16 inst
       (.ADC_DATA(ADC_DATA),
        .adc_clkx_n(adc_clkx_n),
        .adc_clkx_p(adc_clkx_p),
        .adc_dax_n(adc_dax_n),
        .adc_dax_p(adc_dax_p),
        .adc_dbx_n(adc_dbx_n),
        .adc_dbx_p(adc_dbx_p),
        .adc_dcox_n(adc_dcox_n),
        .adc_dcox_p(adc_dcox_p),
        .clk_100(clk_100),
        .clk_200(clk_200),
        .fpga_clk_async(fpga_clk_async),
        .fpga_clk_sync(fpga_clk_sync));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
