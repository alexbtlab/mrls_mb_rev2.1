-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Fri Sep 25 17:56:19 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_VETAL_0_0_sim_netlist.vhdl
-- Design      : design_1_ADC_VETAL_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_VETAL is
  port (
    adc_clkx_p : out STD_LOGIC;
    adc_clkx_n : out STD_LOGIC;
    adc_data : out STD_LOGIC_VECTOR ( 15 downto 0 );
    adc_dax_p : in STD_LOGIC;
    adc_dax_n : in STD_LOGIC;
    adc_dbx_p : in STD_LOGIC;
    adc_dbx_n : in STD_LOGIC;
    adc_dcox_p : in STD_LOGIC;
    adc_dcox_n : in STD_LOGIC;
    fpga_clk : in STD_LOGIC;
    clk_100MHz : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_VETAL;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_VETAL is
  signal adc_clk : STD_LOGIC;
  signal adc_da : STD_LOGIC;
  signal \adc_data_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal adc_data_temp : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal adc_db : STD_LOGIC;
  signal adc_dco : STD_LOGIC;
  signal adc_dco_bg : STD_LOGIC;
  signal adc_reading_allowed_reg_i_1_n_0 : STD_LOGIC;
  signal adc_reading_allowed_reg_reg_n_0 : STD_LOGIC;
  signal data_0_i_1_n_0 : STD_LOGIC;
  signal data_10_i_1_n_0 : STD_LOGIC;
  signal data_11_i_1_n_0 : STD_LOGIC;
  signal data_12_i_1_n_0 : STD_LOGIC;
  signal data_13_i_1_n_0 : STD_LOGIC;
  signal data_14_i_1_n_0 : STD_LOGIC;
  signal data_14_i_2_n_0 : STD_LOGIC;
  signal data_15_i_1_n_0 : STD_LOGIC;
  signal data_1_i_1_n_0 : STD_LOGIC;
  signal data_2_i_1_n_0 : STD_LOGIC;
  signal data_3_i_1_n_0 : STD_LOGIC;
  signal data_4_i_1_n_0 : STD_LOGIC;
  signal data_5_i_1_n_0 : STD_LOGIC;
  signal data_6_i_1_n_0 : STD_LOGIC;
  signal data_7_i_1_n_0 : STD_LOGIC;
  signal data_8_i_1_n_0 : STD_LOGIC;
  signal data_9_i_1_n_0 : STD_LOGIC;
  signal \dco_neg_num[0]_i_1_n_0\ : STD_LOGIC;
  signal \dco_neg_num[1]_i_1_n_0\ : STD_LOGIC;
  signal \dco_neg_num_reg_n_0_[0]\ : STD_LOGIC;
  signal \dco_neg_num_reg_n_0_[1]\ : STD_LOGIC;
  signal \dco_pos_num[0]_i_1_n_0\ : STD_LOGIC;
  signal \dco_pos_num[1]_i_1_n_0\ : STD_LOGIC;
  signal \dco_pos_num_reg_n_0_[0]\ : STD_LOGIC;
  signal \dco_pos_num_reg_n_0_[1]\ : STD_LOGIC;
  signal even_odd_frame : STD_LOGIC;
  signal even_odd_frame_i_1_n_0 : STD_LOGIC;
  signal even_odd_frame_i_2_n_0 : STD_LOGIC;
  signal even_odd_frame_prev_neg : STD_LOGIC;
  signal even_odd_frame_prev_pos : STD_LOGIC;
  signal fpga_clk_prev : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 7 downto 1 );
  signal tconv_cnt_reg : STD_LOGIC;
  signal \tconv_cnt_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \tconv_cnt_reg[7]_i_3_n_0\ : STD_LOGIC;
  signal tconv_cnt_reg_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of BUFG_adc_clk : label is "PRIMITIVE";
  attribute BOX_TYPE of IBUFDS_adc_da : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of IBUFDS_adc_da : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_da : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of IBUFDS_adc_da : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_db : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_db : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_db : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_db : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_dco : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_dco : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_dco : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_dco : label is "AUTO";
  attribute BOX_TYPE of OBUFDS_adc_clk : label is "PRIMITIVE";
  attribute CAPACITANCE of OBUFDS_adc_clk : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of OBUFDS_adc_clk : label is "OBUFDS";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of data_0_i_1 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of data_14_i_2 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of data_1_i_1 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of data_2_i_1 : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of data_3_i_1 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of data_4_i_1 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of data_5_i_1 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of data_6_i_1 : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of data_7_i_1 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \dco_neg_num[0]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \dco_neg_num[1]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \dco_pos_num[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \dco_pos_num[1]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of even_odd_frame_i_2 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[0]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[1]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[2]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[4]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \tconv_cnt_reg[7]_i_2\ : label is "soft_lutpair1";
begin
BUFG_adc_clk: unisim.vcomponents.BUFG
     port map (
      I => adc_dco_bg,
      O => adc_dco
    );
IBUFDS_adc_da: unisim.vcomponents.IBUFDS
     port map (
      I => adc_dax_p,
      IB => adc_dax_n,
      O => adc_da
    );
IBUFDS_adc_db: unisim.vcomponents.IBUFDS
     port map (
      I => adc_dbx_p,
      IB => adc_dbx_n,
      O => adc_db
    );
IBUFDS_adc_dco: unisim.vcomponents.IBUFDS
     port map (
      I => adc_dcox_p,
      IB => adc_dcox_n,
      O => adc_dco_bg
    );
OBUFDS_adc_clk: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => adc_clk,
      O => adc_clkx_p,
      OB => adc_clkx_n
    );
OBUFDS_adc_clk_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_100MHz,
      I1 => adc_reading_allowed_reg_reg_n_0,
      O => adc_clk
    );
\adc_data_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \dco_neg_num_reg_n_0_[1]\,
      I1 => \dco_neg_num_reg_n_0_[0]\,
      O => \adc_data_reg[15]_i_1_n_0\
    );
\adc_data_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(0),
      Q => adc_data(0),
      R => '0'
    );
\adc_data_reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(10),
      Q => adc_data(10),
      R => '0'
    );
\adc_data_reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(11),
      Q => adc_data(11),
      R => '0'
    );
\adc_data_reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(12),
      Q => adc_data(12),
      R => '0'
    );
\adc_data_reg_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(13),
      Q => adc_data(13),
      R => '0'
    );
\adc_data_reg_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(14),
      Q => adc_data(14),
      R => '0'
    );
\adc_data_reg_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(15),
      Q => adc_data(15),
      R => '0'
    );
\adc_data_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(1),
      Q => adc_data(1),
      R => '0'
    );
\adc_data_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(2),
      Q => adc_data(2),
      R => '0'
    );
\adc_data_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(3),
      Q => adc_data(3),
      R => '0'
    );
\adc_data_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(4),
      Q => adc_data(4),
      R => '0'
    );
\adc_data_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(5),
      Q => adc_data(5),
      R => '0'
    );
\adc_data_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(6),
      Q => adc_data(6),
      R => '0'
    );
\adc_data_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(7),
      Q => adc_data(7),
      R => '0'
    );
\adc_data_reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(8),
      Q => adc_data(8),
      R => '0'
    );
\adc_data_reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => \adc_data_reg[15]_i_1_n_0\,
      D => adc_data_temp(9),
      Q => adc_data(9),
      R => '0'
    );
adc_reading_allowed_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAEAAA8AA"
    )
        port map (
      I0 => adc_reading_allowed_reg_reg_n_0,
      I1 => tconv_cnt_reg_reg(2),
      I2 => tconv_cnt_reg_reg(3),
      I3 => tconv_cnt_reg_reg(0),
      I4 => tconv_cnt_reg_reg(1),
      I5 => even_odd_frame_i_2_n_0,
      O => adc_reading_allowed_reg_i_1_n_0
    );
adc_reading_allowed_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100MHz,
      CE => '1',
      D => adc_reading_allowed_reg_i_1_n_0,
      Q => adc_reading_allowed_reg_reg_n_0,
      R => '0'
    );
data_0_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => adc_db,
      I1 => \dco_pos_num_reg_n_0_[0]\,
      I2 => \dco_pos_num_reg_n_0_[1]\,
      I3 => adc_data_temp(0),
      O => data_0_i_1_n_0
    );
data_0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => data_0_i_1_n_0,
      Q => adc_data_temp(0),
      R => '0'
    );
data_10_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEFF00020200"
    )
        port map (
      I0 => adc_db,
      I1 => \dco_neg_num_reg_n_0_[0]\,
      I2 => \dco_neg_num_reg_n_0_[1]\,
      I3 => even_odd_frame_prev_neg,
      I4 => even_odd_frame,
      I5 => adc_data_temp(10),
      O => data_10_i_1_n_0
    );
data_10_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => data_10_i_1_n_0,
      Q => adc_data_temp(10),
      R => '0'
    );
data_11_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEFF00020200"
    )
        port map (
      I0 => adc_da,
      I1 => \dco_neg_num_reg_n_0_[0]\,
      I2 => \dco_neg_num_reg_n_0_[1]\,
      I3 => even_odd_frame_prev_neg,
      I4 => even_odd_frame,
      I5 => adc_data_temp(11),
      O => data_11_i_1_n_0
    );
data_11_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => data_11_i_1_n_0,
      Q => adc_data_temp(11),
      R => '0'
    );
data_12_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEFF00020200"
    )
        port map (
      I0 => adc_db,
      I1 => \dco_pos_num_reg_n_0_[0]\,
      I2 => \dco_pos_num_reg_n_0_[1]\,
      I3 => even_odd_frame_prev_pos,
      I4 => even_odd_frame,
      I5 => adc_data_temp(12),
      O => data_12_i_1_n_0
    );
data_12_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => data_12_i_1_n_0,
      Q => adc_data_temp(12),
      R => '0'
    );
data_13_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFEFF00020200"
    )
        port map (
      I0 => adc_da,
      I1 => \dco_pos_num_reg_n_0_[0]\,
      I2 => \dco_pos_num_reg_n_0_[1]\,
      I3 => even_odd_frame_prev_pos,
      I4 => even_odd_frame,
      I5 => adc_data_temp(13),
      O => data_13_i_1_n_0
    );
data_13_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => data_13_i_1_n_0,
      Q => adc_data_temp(13),
      R => '0'
    );
data_14_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => adc_db,
      I1 => even_odd_frame_i_2_n_0,
      I2 => data_14_i_2_n_0,
      I3 => tconv_cnt_reg_reg(3),
      I4 => tconv_cnt_reg_reg(2),
      I5 => adc_data_temp(14),
      O => data_14_i_1_n_0
    );
data_14_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => tconv_cnt_reg_reg(1),
      I1 => tconv_cnt_reg_reg(0),
      O => data_14_i_2_n_0
    );
data_14_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100MHz,
      CE => '1',
      D => data_14_i_1_n_0,
      Q => adc_data_temp(14),
      R => '0'
    );
data_15_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => adc_da,
      I1 => even_odd_frame_i_2_n_0,
      I2 => data_14_i_2_n_0,
      I3 => tconv_cnt_reg_reg(3),
      I4 => tconv_cnt_reg_reg(2),
      I5 => adc_data_temp(15),
      O => data_15_i_1_n_0
    );
data_15_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100MHz,
      CE => '1',
      D => data_15_i_1_n_0,
      Q => adc_data_temp(15),
      R => '0'
    );
data_1_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => adc_da,
      I1 => \dco_pos_num_reg_n_0_[0]\,
      I2 => \dco_pos_num_reg_n_0_[1]\,
      I3 => adc_data_temp(1),
      O => data_1_i_1_n_0
    );
data_1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => data_1_i_1_n_0,
      Q => adc_data_temp(1),
      R => '0'
    );
data_2_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => adc_db,
      I1 => \dco_neg_num_reg_n_0_[1]\,
      I2 => \dco_neg_num_reg_n_0_[0]\,
      I3 => adc_data_temp(2),
      O => data_2_i_1_n_0
    );
data_2_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => data_2_i_1_n_0,
      Q => adc_data_temp(2),
      R => '0'
    );
data_3_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => adc_da,
      I1 => \dco_neg_num_reg_n_0_[1]\,
      I2 => \dco_neg_num_reg_n_0_[0]\,
      I3 => adc_data_temp(3),
      O => data_3_i_1_n_0
    );
data_3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => data_3_i_1_n_0,
      Q => adc_data_temp(3),
      R => '0'
    );
data_4_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => adc_db,
      I1 => \dco_pos_num_reg_n_0_[1]\,
      I2 => \dco_pos_num_reg_n_0_[0]\,
      I3 => adc_data_temp(4),
      O => data_4_i_1_n_0
    );
data_4_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => data_4_i_1_n_0,
      Q => adc_data_temp(4),
      R => '0'
    );
data_5_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => adc_da,
      I1 => \dco_pos_num_reg_n_0_[1]\,
      I2 => \dco_pos_num_reg_n_0_[0]\,
      I3 => adc_data_temp(5),
      O => data_5_i_1_n_0
    );
data_5_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => data_5_i_1_n_0,
      Q => adc_data_temp(5),
      R => '0'
    );
data_6_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => adc_db,
      I1 => \dco_neg_num_reg_n_0_[1]\,
      I2 => \dco_neg_num_reg_n_0_[0]\,
      I3 => adc_data_temp(6),
      O => data_6_i_1_n_0
    );
data_6_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => data_6_i_1_n_0,
      Q => adc_data_temp(6),
      R => '0'
    );
data_7_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => adc_da,
      I1 => \dco_neg_num_reg_n_0_[1]\,
      I2 => \dco_neg_num_reg_n_0_[0]\,
      I3 => adc_data_temp(7),
      O => data_7_i_1_n_0
    );
data_7_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => data_7_i_1_n_0,
      Q => adc_data_temp(7),
      R => '0'
    );
data_8_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => adc_db,
      I1 => \dco_pos_num_reg_n_0_[0]\,
      I2 => \dco_pos_num_reg_n_0_[1]\,
      I3 => adc_data_temp(8),
      O => data_8_i_1_n_0
    );
data_8_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => data_8_i_1_n_0,
      Q => adc_data_temp(8),
      R => '0'
    );
data_9_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => adc_da,
      I1 => \dco_pos_num_reg_n_0_[0]\,
      I2 => \dco_pos_num_reg_n_0_[1]\,
      I3 => adc_data_temp(9),
      O => data_9_i_1_n_0
    );
data_9_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => data_9_i_1_n_0,
      Q => adc_data_temp(9),
      R => '0'
    );
\dco_neg_num[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4FF4"
    )
        port map (
      I0 => \dco_neg_num_reg_n_0_[0]\,
      I1 => \dco_neg_num_reg_n_0_[1]\,
      I2 => even_odd_frame_prev_neg,
      I3 => even_odd_frame,
      O => \dco_neg_num[0]_i_1_n_0\
    );
\dco_neg_num[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0990"
    )
        port map (
      I0 => even_odd_frame,
      I1 => even_odd_frame_prev_neg,
      I2 => \dco_neg_num_reg_n_0_[0]\,
      I3 => \dco_neg_num_reg_n_0_[1]\,
      O => \dco_neg_num[1]_i_1_n_0\
    );
\dco_neg_num_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => \dco_neg_num[0]_i_1_n_0\,
      Q => \dco_neg_num_reg_n_0_[0]\,
      R => '0'
    );
\dco_neg_num_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => \dco_neg_num[1]_i_1_n_0\,
      Q => \dco_neg_num_reg_n_0_[1]\,
      R => '0'
    );
\dco_pos_num[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4FF4"
    )
        port map (
      I0 => \dco_pos_num_reg_n_0_[0]\,
      I1 => \dco_pos_num_reg_n_0_[1]\,
      I2 => even_odd_frame_prev_pos,
      I3 => even_odd_frame,
      O => \dco_pos_num[0]_i_1_n_0\
    );
\dco_pos_num[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0990"
    )
        port map (
      I0 => even_odd_frame,
      I1 => even_odd_frame_prev_pos,
      I2 => \dco_pos_num_reg_n_0_[0]\,
      I3 => \dco_pos_num_reg_n_0_[1]\,
      O => \dco_pos_num[1]_i_1_n_0\
    );
\dco_pos_num_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => \dco_pos_num[0]_i_1_n_0\,
      Q => \dco_pos_num_reg_n_0_[0]\,
      R => '0'
    );
\dco_pos_num_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => \dco_pos_num[1]_i_1_n_0\,
      Q => \dco_pos_num_reg_n_0_[1]\,
      R => '0'
    );
even_odd_frame_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000010"
    )
        port map (
      I0 => tconv_cnt_reg_reg(0),
      I1 => tconv_cnt_reg_reg(3),
      I2 => tconv_cnt_reg_reg(2),
      I3 => tconv_cnt_reg_reg(1),
      I4 => even_odd_frame_i_2_n_0,
      I5 => even_odd_frame,
      O => even_odd_frame_i_1_n_0
    );
even_odd_frame_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => tconv_cnt_reg_reg(7),
      I1 => tconv_cnt_reg_reg(6),
      I2 => tconv_cnt_reg_reg(4),
      I3 => tconv_cnt_reg_reg(5),
      O => even_odd_frame_i_2_n_0
    );
even_odd_frame_prev_neg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => even_odd_frame,
      Q => even_odd_frame_prev_neg,
      R => '0'
    );
even_odd_frame_prev_pos_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco,
      CE => '1',
      D => even_odd_frame,
      Q => even_odd_frame_prev_pos,
      R => '0'
    );
even_odd_frame_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100MHz,
      CE => '1',
      D => even_odd_frame_i_1_n_0,
      Q => even_odd_frame,
      R => '0'
    );
fpga_clk_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100MHz,
      CE => '1',
      D => fpga_clk,
      Q => fpga_clk_prev,
      R => '0'
    );
\tconv_cnt_reg[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => tconv_cnt_reg_reg(0),
      O => \tconv_cnt_reg[0]_i_1_n_0\
    );
\tconv_cnt_reg[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => tconv_cnt_reg_reg(0),
      I1 => tconv_cnt_reg_reg(1),
      O => p_0_in(1)
    );
\tconv_cnt_reg[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => tconv_cnt_reg_reg(1),
      I1 => tconv_cnt_reg_reg(0),
      I2 => tconv_cnt_reg_reg(2),
      O => p_0_in(2)
    );
\tconv_cnt_reg[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => tconv_cnt_reg_reg(0),
      I1 => tconv_cnt_reg_reg(1),
      I2 => tconv_cnt_reg_reg(2),
      I3 => tconv_cnt_reg_reg(3),
      O => p_0_in(3)
    );
\tconv_cnt_reg[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => tconv_cnt_reg_reg(2),
      I1 => tconv_cnt_reg_reg(1),
      I2 => tconv_cnt_reg_reg(0),
      I3 => tconv_cnt_reg_reg(3),
      I4 => tconv_cnt_reg_reg(4),
      O => p_0_in(4)
    );
\tconv_cnt_reg[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => tconv_cnt_reg_reg(3),
      I1 => tconv_cnt_reg_reg(0),
      I2 => tconv_cnt_reg_reg(1),
      I3 => tconv_cnt_reg_reg(2),
      I4 => tconv_cnt_reg_reg(4),
      I5 => tconv_cnt_reg_reg(5),
      O => p_0_in(5)
    );
\tconv_cnt_reg[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \tconv_cnt_reg[7]_i_3_n_0\,
      I1 => tconv_cnt_reg_reg(6),
      O => p_0_in(6)
    );
\tconv_cnt_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => fpga_clk,
      I1 => fpga_clk_prev,
      O => tconv_cnt_reg
    );
\tconv_cnt_reg[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \tconv_cnt_reg[7]_i_3_n_0\,
      I1 => tconv_cnt_reg_reg(6),
      I2 => tconv_cnt_reg_reg(7),
      O => p_0_in(7)
    );
\tconv_cnt_reg[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => tconv_cnt_reg_reg(5),
      I1 => tconv_cnt_reg_reg(3),
      I2 => tconv_cnt_reg_reg(0),
      I3 => tconv_cnt_reg_reg(1),
      I4 => tconv_cnt_reg_reg(2),
      I5 => tconv_cnt_reg_reg(4),
      O => \tconv_cnt_reg[7]_i_3_n_0\
    );
\tconv_cnt_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100MHz,
      CE => '1',
      D => \tconv_cnt_reg[0]_i_1_n_0\,
      Q => tconv_cnt_reg_reg(0),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100MHz,
      CE => '1',
      D => p_0_in(1),
      Q => tconv_cnt_reg_reg(1),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100MHz,
      CE => '1',
      D => p_0_in(2),
      Q => tconv_cnt_reg_reg(2),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100MHz,
      CE => '1',
      D => p_0_in(3),
      Q => tconv_cnt_reg_reg(3),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100MHz,
      CE => '1',
      D => p_0_in(4),
      Q => tconv_cnt_reg_reg(4),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100MHz,
      CE => '1',
      D => p_0_in(5),
      Q => tconv_cnt_reg_reg(5),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100MHz,
      CE => '1',
      D => p_0_in(6),
      Q => tconv_cnt_reg_reg(6),
      R => tconv_cnt_reg
    );
\tconv_cnt_reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100MHz,
      CE => '1',
      D => p_0_in(7),
      Q => tconv_cnt_reg_reg(7),
      R => tconv_cnt_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    fpga_clk : in STD_LOGIC;
    clk_100MHz : in STD_LOGIC;
    fpga_clk_inv : out STD_LOGIC;
    adc_data : out STD_LOGIC_VECTOR ( 15 downto 0 );
    adc_dax_p : in STD_LOGIC;
    adc_dax_n : in STD_LOGIC;
    adc_dbx_p : in STD_LOGIC;
    adc_dbx_n : in STD_LOGIC;
    adc_dcox_p : in STD_LOGIC;
    adc_dcox_n : in STD_LOGIC;
    adc_clkx_p : out STD_LOGIC;
    adc_clkx_n : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ADC_VETAL_0_0,ADC_VETAL,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ADC_VETAL,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of adc_clkx_n : signal is "bt.local:interface:diff:1.0 adc_clkx n";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of adc_clkx_n : signal is "XIL_INTERFACENAME adc_clkx, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of adc_clkx_p : signal is "bt.local:interface:diff:1.0 adc_clkx p";
  attribute X_INTERFACE_INFO of adc_dax_n : signal is "bt.local:interface:diff:1.0 adc_dax n";
  attribute X_INTERFACE_PARAMETER of adc_dax_n : signal is "XIL_INTERFACENAME adc_dax, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of adc_dax_p : signal is "bt.local:interface:diff:1.0 adc_dax p";
  attribute X_INTERFACE_INFO of adc_dbx_n : signal is "bt.local:interface:diff:1.0 adc_dbx n";
  attribute X_INTERFACE_PARAMETER of adc_dbx_n : signal is "XIL_INTERFACENAME adc_dbx, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of adc_dbx_p : signal is "bt.local:interface:diff:1.0 adc_dbx p";
  attribute X_INTERFACE_INFO of adc_dcox_n : signal is "bt.local:interface:diff:1.0 adc_dcox n";
  attribute X_INTERFACE_PARAMETER of adc_dcox_n : signal is "XIL_INTERFACENAME adc_dcox, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of adc_dcox_p : signal is "bt.local:interface:diff:1.0 adc_dcox p";
begin
fpga_clk_inv_INST_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => fpga_clk,
      O => fpga_clk_inv
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_VETAL
     port map (
      adc_clkx_n => adc_clkx_n,
      adc_clkx_p => adc_clkx_p,
      adc_data(15 downto 0) => adc_data(15 downto 0),
      adc_dax_n => adc_dax_n,
      adc_dax_p => adc_dax_p,
      adc_dbx_n => adc_dbx_n,
      adc_dbx_p => adc_dbx_p,
      adc_dcox_n => adc_dcox_n,
      adc_dcox_p => adc_dcox_p,
      clk_100MHz => clk_100MHz,
      fpga_clk => fpga_clk
    );
end STRUCTURE;
