// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Wed Dec  2 17:42:56 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_Sample_Generator_0_0_sim_netlist.v
// Design      : design_1_Sample_Generator_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0
   (m00_axis_tdata,
    m00_axis_tlast,
    m00_axis_tvalid,
    data_in_IF1,
    data_in_IF2,
    FrameSize,
    data_clk,
    m00_axis_aclk,
    clk_5MHz,
    m00_axis_aresetn);
  output [33:0]m00_axis_tdata;
  output m00_axis_tlast;
  output m00_axis_tvalid;
  input [15:0]data_in_IF1;
  input [15:0]data_in_IF2;
  input [15:0]FrameSize;
  input data_clk;
  input m00_axis_aclk;
  input clk_5MHz;
  input m00_axis_aresetn;

  wire [15:0]FrameSize;
  wire clear;
  wire clk_5MHz;
  wire cnt10;
  wire \cnt100[0]_i_2_n_0 ;
  wire [15:0]cnt100_reg;
  wire \cnt100_reg[0]_i_1_n_0 ;
  wire \cnt100_reg[0]_i_1_n_1 ;
  wire \cnt100_reg[0]_i_1_n_2 ;
  wire \cnt100_reg[0]_i_1_n_3 ;
  wire \cnt100_reg[0]_i_1_n_4 ;
  wire \cnt100_reg[0]_i_1_n_5 ;
  wire \cnt100_reg[0]_i_1_n_6 ;
  wire \cnt100_reg[0]_i_1_n_7 ;
  wire \cnt100_reg[12]_i_1_n_1 ;
  wire \cnt100_reg[12]_i_1_n_2 ;
  wire \cnt100_reg[12]_i_1_n_3 ;
  wire \cnt100_reg[12]_i_1_n_4 ;
  wire \cnt100_reg[12]_i_1_n_5 ;
  wire \cnt100_reg[12]_i_1_n_6 ;
  wire \cnt100_reg[12]_i_1_n_7 ;
  wire \cnt100_reg[4]_i_1_n_0 ;
  wire \cnt100_reg[4]_i_1_n_1 ;
  wire \cnt100_reg[4]_i_1_n_2 ;
  wire \cnt100_reg[4]_i_1_n_3 ;
  wire \cnt100_reg[4]_i_1_n_4 ;
  wire \cnt100_reg[4]_i_1_n_5 ;
  wire \cnt100_reg[4]_i_1_n_6 ;
  wire \cnt100_reg[4]_i_1_n_7 ;
  wire \cnt100_reg[8]_i_1_n_0 ;
  wire \cnt100_reg[8]_i_1_n_1 ;
  wire \cnt100_reg[8]_i_1_n_2 ;
  wire \cnt100_reg[8]_i_1_n_3 ;
  wire \cnt100_reg[8]_i_1_n_4 ;
  wire \cnt100_reg[8]_i_1_n_5 ;
  wire \cnt100_reg[8]_i_1_n_6 ;
  wire \cnt100_reg[8]_i_1_n_7 ;
  wire cnt50_carry__0_i_1_n_0;
  wire cnt50_carry__0_i_2_n_0;
  wire cnt50_carry__0_n_3;
  wire cnt50_carry_i_1_n_0;
  wire cnt50_carry_i_2_n_0;
  wire cnt50_carry_i_3_n_0;
  wire cnt50_carry_i_4_n_0;
  wire cnt50_carry_n_0;
  wire cnt50_carry_n_1;
  wire cnt50_carry_n_2;
  wire cnt50_carry_n_3;
  wire \cnt5[0]_i_2_n_0 ;
  wire [15:0]cnt5_reg;
  wire \cnt5_reg[0]_i_1_n_0 ;
  wire \cnt5_reg[0]_i_1_n_1 ;
  wire \cnt5_reg[0]_i_1_n_2 ;
  wire \cnt5_reg[0]_i_1_n_3 ;
  wire \cnt5_reg[0]_i_1_n_4 ;
  wire \cnt5_reg[0]_i_1_n_5 ;
  wire \cnt5_reg[0]_i_1_n_6 ;
  wire \cnt5_reg[0]_i_1_n_7 ;
  wire \cnt5_reg[12]_i_1_n_1 ;
  wire \cnt5_reg[12]_i_1_n_2 ;
  wire \cnt5_reg[12]_i_1_n_3 ;
  wire \cnt5_reg[12]_i_1_n_4 ;
  wire \cnt5_reg[12]_i_1_n_5 ;
  wire \cnt5_reg[12]_i_1_n_6 ;
  wire \cnt5_reg[12]_i_1_n_7 ;
  wire \cnt5_reg[4]_i_1_n_0 ;
  wire \cnt5_reg[4]_i_1_n_1 ;
  wire \cnt5_reg[4]_i_1_n_2 ;
  wire \cnt5_reg[4]_i_1_n_3 ;
  wire \cnt5_reg[4]_i_1_n_4 ;
  wire \cnt5_reg[4]_i_1_n_5 ;
  wire \cnt5_reg[4]_i_1_n_6 ;
  wire \cnt5_reg[4]_i_1_n_7 ;
  wire \cnt5_reg[8]_i_1_n_0 ;
  wire \cnt5_reg[8]_i_1_n_1 ;
  wire \cnt5_reg[8]_i_1_n_2 ;
  wire \cnt5_reg[8]_i_1_n_3 ;
  wire \cnt5_reg[8]_i_1_n_4 ;
  wire \cnt5_reg[8]_i_1_n_5 ;
  wire \cnt5_reg[8]_i_1_n_6 ;
  wire \cnt5_reg[8]_i_1_n_7 ;
  wire data_clk;
  wire [16:0]data_even_1;
  wire [16:0]data_even_2;
  wire [15:0]data_in_IF1;
  wire [15:0]data_in_IF2;
  wire [16:0]data_sum_10;
  wire \data_sum_1[11]_i_2_n_0 ;
  wire \data_sum_1[11]_i_3_n_0 ;
  wire \data_sum_1[11]_i_4_n_0 ;
  wire \data_sum_1[11]_i_5_n_0 ;
  wire \data_sum_1[15]_i_2_n_0 ;
  wire \data_sum_1[15]_i_3_n_0 ;
  wire \data_sum_1[15]_i_4_n_0 ;
  wire \data_sum_1[15]_i_5_n_0 ;
  wire \data_sum_1[16]_i_2_n_0 ;
  wire \data_sum_1[3]_i_2_n_0 ;
  wire \data_sum_1[3]_i_3_n_0 ;
  wire \data_sum_1[3]_i_4_n_0 ;
  wire \data_sum_1[3]_i_5_n_0 ;
  wire \data_sum_1[7]_i_2_n_0 ;
  wire \data_sum_1[7]_i_3_n_0 ;
  wire \data_sum_1[7]_i_4_n_0 ;
  wire \data_sum_1[7]_i_5_n_0 ;
  wire \data_sum_1_reg[11]_i_1_n_0 ;
  wire \data_sum_1_reg[11]_i_1_n_1 ;
  wire \data_sum_1_reg[11]_i_1_n_2 ;
  wire \data_sum_1_reg[11]_i_1_n_3 ;
  wire \data_sum_1_reg[15]_i_1_n_0 ;
  wire \data_sum_1_reg[15]_i_1_n_1 ;
  wire \data_sum_1_reg[15]_i_1_n_2 ;
  wire \data_sum_1_reg[15]_i_1_n_3 ;
  wire \data_sum_1_reg[3]_i_1_n_0 ;
  wire \data_sum_1_reg[3]_i_1_n_1 ;
  wire \data_sum_1_reg[3]_i_1_n_2 ;
  wire \data_sum_1_reg[3]_i_1_n_3 ;
  wire \data_sum_1_reg[7]_i_1_n_0 ;
  wire \data_sum_1_reg[7]_i_1_n_1 ;
  wire \data_sum_1_reg[7]_i_1_n_2 ;
  wire \data_sum_1_reg[7]_i_1_n_3 ;
  wire [16:0]data_sum_20;
  wire \data_sum_2[11]_i_2_n_0 ;
  wire \data_sum_2[11]_i_3_n_0 ;
  wire \data_sum_2[11]_i_4_n_0 ;
  wire \data_sum_2[11]_i_5_n_0 ;
  wire \data_sum_2[15]_i_2_n_0 ;
  wire \data_sum_2[15]_i_3_n_0 ;
  wire \data_sum_2[15]_i_4_n_0 ;
  wire \data_sum_2[15]_i_5_n_0 ;
  wire \data_sum_2[16]_i_3_n_0 ;
  wire \data_sum_2[3]_i_2_n_0 ;
  wire \data_sum_2[3]_i_3_n_0 ;
  wire \data_sum_2[3]_i_4_n_0 ;
  wire \data_sum_2[3]_i_5_n_0 ;
  wire \data_sum_2[7]_i_2_n_0 ;
  wire \data_sum_2[7]_i_3_n_0 ;
  wire \data_sum_2[7]_i_4_n_0 ;
  wire \data_sum_2[7]_i_5_n_0 ;
  wire \data_sum_2_reg[11]_i_1_n_0 ;
  wire \data_sum_2_reg[11]_i_1_n_1 ;
  wire \data_sum_2_reg[11]_i_1_n_2 ;
  wire \data_sum_2_reg[11]_i_1_n_3 ;
  wire \data_sum_2_reg[15]_i_1_n_0 ;
  wire \data_sum_2_reg[15]_i_1_n_1 ;
  wire \data_sum_2_reg[15]_i_1_n_2 ;
  wire \data_sum_2_reg[15]_i_1_n_3 ;
  wire \data_sum_2_reg[3]_i_1_n_0 ;
  wire \data_sum_2_reg[3]_i_1_n_1 ;
  wire \data_sum_2_reg[3]_i_1_n_2 ;
  wire \data_sum_2_reg[3]_i_1_n_3 ;
  wire \data_sum_2_reg[7]_i_1_n_0 ;
  wire \data_sum_2_reg[7]_i_1_n_1 ;
  wire \data_sum_2_reg[7]_i_1_n_2 ;
  wire \data_sum_2_reg[7]_i_1_n_3 ;
  wire i__carry__0_i_1_n_0;
  wire i__carry__0_i_2_n_0;
  wire i__carry__0_i_3_n_0;
  wire i__carry__0_i_4_n_0;
  wire i__carry__0_i_5_n_0;
  wire i__carry__0_i_6_n_0;
  wire i__carry__0_i_7_n_0;
  wire i__carry__0_i_8_n_0;
  wire i__carry__0_i_9_n_0;
  wire i__carry__0_i_9_n_1;
  wire i__carry__0_i_9_n_2;
  wire i__carry__0_i_9_n_3;
  wire i__carry__1_i_1_n_2;
  wire i__carry__1_i_1_n_3;
  wire i__carry__1_i_2_n_0;
  wire i__carry_i_10_n_0;
  wire i__carry_i_10_n_1;
  wire i__carry_i_10_n_2;
  wire i__carry_i_10_n_3;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2_n_0;
  wire i__carry_i_3_n_0;
  wire i__carry_i_4_n_0;
  wire i__carry_i_5_n_0;
  wire i__carry_i_6_n_0;
  wire i__carry_i_7_n_0;
  wire i__carry_i_8_n_0;
  wire i__carry_i_9_n_0;
  wire i__carry_i_9_n_1;
  wire i__carry_i_9_n_2;
  wire i__carry_i_9_n_3;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [33:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire m00_axis_tlast_r0;
  wire m00_axis_tlast_r0_carry__0_i_1_n_0;
  wire m00_axis_tlast_r0_carry__0_i_1_n_2;
  wire m00_axis_tlast_r0_carry__0_i_1_n_3;
  wire m00_axis_tlast_r0_carry__0_i_2_n_0;
  wire m00_axis_tlast_r0_carry__0_i_3_n_0;
  wire m00_axis_tlast_r0_carry__0_i_4_n_0;
  wire m00_axis_tlast_r0_carry__0_i_5_n_0;
  wire m00_axis_tlast_r0_carry__0_i_6_n_0;
  wire m00_axis_tlast_r0_carry__0_n_0;
  wire m00_axis_tlast_r0_carry__0_n_1;
  wire m00_axis_tlast_r0_carry__0_n_2;
  wire m00_axis_tlast_r0_carry__0_n_3;
  wire m00_axis_tlast_r0_carry__1_n_2;
  wire m00_axis_tlast_r0_carry__1_n_3;
  wire m00_axis_tlast_r0_carry_i_10_n_0;
  wire m00_axis_tlast_r0_carry_i_11_n_0;
  wire m00_axis_tlast_r0_carry_i_12_n_0;
  wire m00_axis_tlast_r0_carry_i_13_n_0;
  wire m00_axis_tlast_r0_carry_i_14_n_0;
  wire m00_axis_tlast_r0_carry_i_15_n_0;
  wire m00_axis_tlast_r0_carry_i_16_n_0;
  wire m00_axis_tlast_r0_carry_i_17_n_0;
  wire m00_axis_tlast_r0_carry_i_18_n_0;
  wire m00_axis_tlast_r0_carry_i_19_n_0;
  wire m00_axis_tlast_r0_carry_i_1_n_0;
  wire m00_axis_tlast_r0_carry_i_2_n_0;
  wire m00_axis_tlast_r0_carry_i_3_n_0;
  wire m00_axis_tlast_r0_carry_i_4_n_0;
  wire m00_axis_tlast_r0_carry_i_5_n_0;
  wire m00_axis_tlast_r0_carry_i_5_n_1;
  wire m00_axis_tlast_r0_carry_i_5_n_2;
  wire m00_axis_tlast_r0_carry_i_5_n_3;
  wire m00_axis_tlast_r0_carry_i_6_n_0;
  wire m00_axis_tlast_r0_carry_i_6_n_1;
  wire m00_axis_tlast_r0_carry_i_6_n_2;
  wire m00_axis_tlast_r0_carry_i_6_n_3;
  wire m00_axis_tlast_r0_carry_i_7_n_0;
  wire m00_axis_tlast_r0_carry_i_7_n_1;
  wire m00_axis_tlast_r0_carry_i_7_n_2;
  wire m00_axis_tlast_r0_carry_i_7_n_3;
  wire m00_axis_tlast_r0_carry_i_8_n_0;
  wire m00_axis_tlast_r0_carry_i_9_n_0;
  wire m00_axis_tlast_r0_carry_n_0;
  wire m00_axis_tlast_r0_carry_n_1;
  wire m00_axis_tlast_r0_carry_n_2;
  wire m00_axis_tlast_r0_carry_n_3;
  wire [15:1]m00_axis_tlast_r1;
  wire m00_axis_tlast_r_i_1_n_0;
  wire m00_axis_tlast_r_i_3_n_0;
  wire m00_axis_tlast_r_i_4_n_0;
  wire m00_axis_tlast_r_i_5_n_0;
  wire m00_axis_tvalid;
  wire m00_axis_tvalid_r1;
  wire m00_axis_tvalid_r13_out;
  wire \m00_axis_tvalid_r1_inferred__0/i__carry__0_n_0 ;
  wire \m00_axis_tvalid_r1_inferred__0/i__carry__0_n_1 ;
  wire \m00_axis_tvalid_r1_inferred__0/i__carry__0_n_2 ;
  wire \m00_axis_tvalid_r1_inferred__0/i__carry__0_n_3 ;
  wire \m00_axis_tvalid_r1_inferred__0/i__carry_n_0 ;
  wire \m00_axis_tvalid_r1_inferred__0/i__carry_n_1 ;
  wire \m00_axis_tvalid_r1_inferred__0/i__carry_n_2 ;
  wire \m00_axis_tvalid_r1_inferred__0/i__carry_n_3 ;
  wire [16:1]m00_axis_tvalid_r2;
  wire m00_axis_tvalid_r_i_1_n_0;
  wire m00_axis_tvalid_r_i_2_n_0;
  wire m00_axis_tvalid_r_i_3_n_0;
  wire m00_axis_tvalid_r_i_4_n_0;
  wire m00_axis_tvalid_r_i_5_n_0;
  wire p_1_in;
  wire sel;
  wire [3:3]\NLW_cnt100_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:0]NLW_cnt50_carry_O_UNCONNECTED;
  wire [3:2]NLW_cnt50_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_cnt50_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_cnt5_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:0]\NLW_data_sum_1_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_data_sum_1_reg[16]_i_1_O_UNCONNECTED ;
  wire [3:0]\NLW_data_sum_2_reg[16]_i_2_CO_UNCONNECTED ;
  wire [3:1]\NLW_data_sum_2_reg[16]_i_2_O_UNCONNECTED ;
  wire [2:2]NLW_i__carry__1_i_1_CO_UNCONNECTED;
  wire [3:3]NLW_i__carry__1_i_1_O_UNCONNECTED;
  wire [3:0]NLW_m00_axis_tlast_r0_carry_O_UNCONNECTED;
  wire [3:0]NLW_m00_axis_tlast_r0_carry__0_O_UNCONNECTED;
  wire [2:2]NLW_m00_axis_tlast_r0_carry__0_i_1_CO_UNCONNECTED;
  wire [3:3]NLW_m00_axis_tlast_r0_carry__0_i_1_O_UNCONNECTED;
  wire [3:3]NLW_m00_axis_tlast_r0_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_m00_axis_tlast_r0_carry__1_O_UNCONNECTED;
  wire [3:0]\NLW_m00_axis_tvalid_r1_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW_m00_axis_tvalid_r1_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [3:1]\NLW_m00_axis_tvalid_r1_inferred__0/i__carry__1_CO_UNCONNECTED ;
  wire [3:0]\NLW_m00_axis_tvalid_r1_inferred__0/i__carry__1_O_UNCONNECTED ;

  LUT1 #(
    .INIT(2'h1)) 
    \cnt100[0]_i_2 
       (.I0(cnt100_reg[0]),
        .O(\cnt100[0]_i_2_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[0]_i_1_n_7 ),
        .Q(cnt100_reg[0]),
        .R(cnt10));
  CARRY4 \cnt100_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\cnt100_reg[0]_i_1_n_0 ,\cnt100_reg[0]_i_1_n_1 ,\cnt100_reg[0]_i_1_n_2 ,\cnt100_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt100_reg[0]_i_1_n_4 ,\cnt100_reg[0]_i_1_n_5 ,\cnt100_reg[0]_i_1_n_6 ,\cnt100_reg[0]_i_1_n_7 }),
        .S({cnt100_reg[3:1],\cnt100[0]_i_2_n_0 }));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[10] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[8]_i_1_n_5 ),
        .Q(cnt100_reg[10]),
        .R(cnt10));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[11] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[8]_i_1_n_4 ),
        .Q(cnt100_reg[11]),
        .R(cnt10));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[12] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[12]_i_1_n_7 ),
        .Q(cnt100_reg[12]),
        .R(cnt10));
  CARRY4 \cnt100_reg[12]_i_1 
       (.CI(\cnt100_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt100_reg[12]_i_1_CO_UNCONNECTED [3],\cnt100_reg[12]_i_1_n_1 ,\cnt100_reg[12]_i_1_n_2 ,\cnt100_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt100_reg[12]_i_1_n_4 ,\cnt100_reg[12]_i_1_n_5 ,\cnt100_reg[12]_i_1_n_6 ,\cnt100_reg[12]_i_1_n_7 }),
        .S(cnt100_reg[15:12]));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[13] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[12]_i_1_n_6 ),
        .Q(cnt100_reg[13]),
        .R(cnt10));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[14] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[12]_i_1_n_5 ),
        .Q(cnt100_reg[14]),
        .R(cnt10));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[15] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[12]_i_1_n_4 ),
        .Q(cnt100_reg[15]),
        .R(cnt10));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[0]_i_1_n_6 ),
        .Q(cnt100_reg[1]),
        .R(cnt10));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[0]_i_1_n_5 ),
        .Q(cnt100_reg[2]),
        .R(cnt10));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[3] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[0]_i_1_n_4 ),
        .Q(cnt100_reg[3]),
        .R(cnt10));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[4]_i_1_n_7 ),
        .Q(cnt100_reg[4]),
        .R(cnt10));
  CARRY4 \cnt100_reg[4]_i_1 
       (.CI(\cnt100_reg[0]_i_1_n_0 ),
        .CO({\cnt100_reg[4]_i_1_n_0 ,\cnt100_reg[4]_i_1_n_1 ,\cnt100_reg[4]_i_1_n_2 ,\cnt100_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt100_reg[4]_i_1_n_4 ,\cnt100_reg[4]_i_1_n_5 ,\cnt100_reg[4]_i_1_n_6 ,\cnt100_reg[4]_i_1_n_7 }),
        .S(cnt100_reg[7:4]));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[5] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[4]_i_1_n_6 ),
        .Q(cnt100_reg[5]),
        .R(cnt10));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[6] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[4]_i_1_n_5 ),
        .Q(cnt100_reg[6]),
        .R(cnt10));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[7] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[4]_i_1_n_4 ),
        .Q(cnt100_reg[7]),
        .R(cnt10));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[8] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[8]_i_1_n_7 ),
        .Q(cnt100_reg[8]),
        .R(cnt10));
  CARRY4 \cnt100_reg[8]_i_1 
       (.CI(\cnt100_reg[4]_i_1_n_0 ),
        .CO({\cnt100_reg[8]_i_1_n_0 ,\cnt100_reg[8]_i_1_n_1 ,\cnt100_reg[8]_i_1_n_2 ,\cnt100_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt100_reg[8]_i_1_n_4 ,\cnt100_reg[8]_i_1_n_5 ,\cnt100_reg[8]_i_1_n_6 ,\cnt100_reg[8]_i_1_n_7 }),
        .S(cnt100_reg[11:8]));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt100_reg[9] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[8]_i_1_n_6 ),
        .Q(cnt100_reg[9]),
        .R(cnt10));
  LUT1 #(
    .INIT(2'h1)) 
    cnt10_i_1
       (.I0(cnt10),
        .O(p_1_in));
  FDRE cnt10_reg
       (.C(data_clk),
        .CE(1'b1),
        .D(p_1_in),
        .Q(cnt10),
        .R(1'b0));
  CARRY4 cnt50_carry
       (.CI(1'b0),
        .CO({cnt50_carry_n_0,cnt50_carry_n_1,cnt50_carry_n_2,cnt50_carry_n_3}),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b1,1'b1}),
        .O(NLW_cnt50_carry_O_UNCONNECTED[3:0]),
        .S({cnt50_carry_i_1_n_0,cnt50_carry_i_2_n_0,cnt50_carry_i_3_n_0,cnt50_carry_i_4_n_0}));
  CARRY4 cnt50_carry__0
       (.CI(cnt50_carry_n_0),
        .CO({NLW_cnt50_carry__0_CO_UNCONNECTED[3:2],sel,cnt50_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b1,1'b1}),
        .O(NLW_cnt50_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,cnt50_carry__0_i_1_n_0,cnt50_carry__0_i_2_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    cnt50_carry__0_i_1
       (.I0(FrameSize[15]),
        .I1(cnt5_reg[15]),
        .O(cnt50_carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt50_carry__0_i_2
       (.I0(cnt5_reg[13]),
        .I1(FrameSize[13]),
        .I2(cnt5_reg[12]),
        .I3(FrameSize[12]),
        .I4(FrameSize[14]),
        .I5(cnt5_reg[14]),
        .O(cnt50_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt50_carry_i_1
       (.I0(cnt5_reg[10]),
        .I1(FrameSize[10]),
        .I2(cnt5_reg[9]),
        .I3(FrameSize[9]),
        .I4(FrameSize[11]),
        .I5(cnt5_reg[11]),
        .O(cnt50_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt50_carry_i_2
       (.I0(cnt5_reg[7]),
        .I1(FrameSize[7]),
        .I2(cnt5_reg[6]),
        .I3(FrameSize[6]),
        .I4(FrameSize[8]),
        .I5(cnt5_reg[8]),
        .O(cnt50_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt50_carry_i_3
       (.I0(cnt5_reg[4]),
        .I1(FrameSize[4]),
        .I2(cnt5_reg[3]),
        .I3(FrameSize[3]),
        .I4(FrameSize[5]),
        .I5(cnt5_reg[5]),
        .O(cnt50_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt50_carry_i_4
       (.I0(cnt5_reg[1]),
        .I1(FrameSize[1]),
        .I2(cnt5_reg[2]),
        .I3(FrameSize[2]),
        .I4(FrameSize[0]),
        .I5(cnt5_reg[0]),
        .O(cnt50_carry_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt5[0]_i_2 
       (.I0(cnt5_reg[0]),
        .O(\cnt5[0]_i_2_n_0 ));
  FDRE \cnt5_reg[0] 
       (.C(clk_5MHz),
        .CE(sel),
        .D(\cnt5_reg[0]_i_1_n_7 ),
        .Q(cnt5_reg[0]),
        .R(clear));
  CARRY4 \cnt5_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\cnt5_reg[0]_i_1_n_0 ,\cnt5_reg[0]_i_1_n_1 ,\cnt5_reg[0]_i_1_n_2 ,\cnt5_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt5_reg[0]_i_1_n_4 ,\cnt5_reg[0]_i_1_n_5 ,\cnt5_reg[0]_i_1_n_6 ,\cnt5_reg[0]_i_1_n_7 }),
        .S({cnt5_reg[3:1],\cnt5[0]_i_2_n_0 }));
  FDRE \cnt5_reg[10] 
       (.C(clk_5MHz),
        .CE(sel),
        .D(\cnt5_reg[8]_i_1_n_5 ),
        .Q(cnt5_reg[10]),
        .R(clear));
  FDRE \cnt5_reg[11] 
       (.C(clk_5MHz),
        .CE(sel),
        .D(\cnt5_reg[8]_i_1_n_4 ),
        .Q(cnt5_reg[11]),
        .R(clear));
  FDRE \cnt5_reg[12] 
       (.C(clk_5MHz),
        .CE(sel),
        .D(\cnt5_reg[12]_i_1_n_7 ),
        .Q(cnt5_reg[12]),
        .R(clear));
  CARRY4 \cnt5_reg[12]_i_1 
       (.CI(\cnt5_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt5_reg[12]_i_1_CO_UNCONNECTED [3],\cnt5_reg[12]_i_1_n_1 ,\cnt5_reg[12]_i_1_n_2 ,\cnt5_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt5_reg[12]_i_1_n_4 ,\cnt5_reg[12]_i_1_n_5 ,\cnt5_reg[12]_i_1_n_6 ,\cnt5_reg[12]_i_1_n_7 }),
        .S(cnt5_reg[15:12]));
  FDRE \cnt5_reg[13] 
       (.C(clk_5MHz),
        .CE(sel),
        .D(\cnt5_reg[12]_i_1_n_6 ),
        .Q(cnt5_reg[13]),
        .R(clear));
  FDRE \cnt5_reg[14] 
       (.C(clk_5MHz),
        .CE(sel),
        .D(\cnt5_reg[12]_i_1_n_5 ),
        .Q(cnt5_reg[14]),
        .R(clear));
  FDRE \cnt5_reg[15] 
       (.C(clk_5MHz),
        .CE(sel),
        .D(\cnt5_reg[12]_i_1_n_4 ),
        .Q(cnt5_reg[15]),
        .R(clear));
  FDRE \cnt5_reg[1] 
       (.C(clk_5MHz),
        .CE(sel),
        .D(\cnt5_reg[0]_i_1_n_6 ),
        .Q(cnt5_reg[1]),
        .R(clear));
  FDRE \cnt5_reg[2] 
       (.C(clk_5MHz),
        .CE(sel),
        .D(\cnt5_reg[0]_i_1_n_5 ),
        .Q(cnt5_reg[2]),
        .R(clear));
  FDRE \cnt5_reg[3] 
       (.C(clk_5MHz),
        .CE(sel),
        .D(\cnt5_reg[0]_i_1_n_4 ),
        .Q(cnt5_reg[3]),
        .R(clear));
  FDRE \cnt5_reg[4] 
       (.C(clk_5MHz),
        .CE(sel),
        .D(\cnt5_reg[4]_i_1_n_7 ),
        .Q(cnt5_reg[4]),
        .R(clear));
  CARRY4 \cnt5_reg[4]_i_1 
       (.CI(\cnt5_reg[0]_i_1_n_0 ),
        .CO({\cnt5_reg[4]_i_1_n_0 ,\cnt5_reg[4]_i_1_n_1 ,\cnt5_reg[4]_i_1_n_2 ,\cnt5_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt5_reg[4]_i_1_n_4 ,\cnt5_reg[4]_i_1_n_5 ,\cnt5_reg[4]_i_1_n_6 ,\cnt5_reg[4]_i_1_n_7 }),
        .S(cnt5_reg[7:4]));
  FDRE \cnt5_reg[5] 
       (.C(clk_5MHz),
        .CE(sel),
        .D(\cnt5_reg[4]_i_1_n_6 ),
        .Q(cnt5_reg[5]),
        .R(clear));
  FDRE \cnt5_reg[6] 
       (.C(clk_5MHz),
        .CE(sel),
        .D(\cnt5_reg[4]_i_1_n_5 ),
        .Q(cnt5_reg[6]),
        .R(clear));
  FDRE \cnt5_reg[7] 
       (.C(clk_5MHz),
        .CE(sel),
        .D(\cnt5_reg[4]_i_1_n_4 ),
        .Q(cnt5_reg[7]),
        .R(clear));
  FDRE \cnt5_reg[8] 
       (.C(clk_5MHz),
        .CE(sel),
        .D(\cnt5_reg[8]_i_1_n_7 ),
        .Q(cnt5_reg[8]),
        .R(clear));
  CARRY4 \cnt5_reg[8]_i_1 
       (.CI(\cnt5_reg[4]_i_1_n_0 ),
        .CO({\cnt5_reg[8]_i_1_n_0 ,\cnt5_reg[8]_i_1_n_1 ,\cnt5_reg[8]_i_1_n_2 ,\cnt5_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt5_reg[8]_i_1_n_4 ,\cnt5_reg[8]_i_1_n_5 ,\cnt5_reg[8]_i_1_n_6 ,\cnt5_reg[8]_i_1_n_7 }),
        .S(cnt5_reg[11:8]));
  FDRE \cnt5_reg[9] 
       (.C(clk_5MHz),
        .CE(sel),
        .D(\cnt5_reg[8]_i_1_n_6 ),
        .Q(cnt5_reg[9]),
        .R(clear));
  FDRE \data_even_1_reg[0] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF1[0]),
        .Q(data_even_1[0]),
        .R(clear));
  FDRE \data_even_1_reg[10] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF1[10]),
        .Q(data_even_1[10]),
        .R(clear));
  FDRE \data_even_1_reg[11] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF1[11]),
        .Q(data_even_1[11]),
        .R(clear));
  FDRE \data_even_1_reg[12] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF1[12]),
        .Q(data_even_1[12]),
        .R(clear));
  FDRE \data_even_1_reg[13] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF1[13]),
        .Q(data_even_1[13]),
        .R(clear));
  FDRE \data_even_1_reg[14] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF1[14]),
        .Q(data_even_1[14]),
        .R(clear));
  FDRE \data_even_1_reg[16] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF1[15]),
        .Q(data_even_1[16]),
        .R(clear));
  FDRE \data_even_1_reg[1] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF1[1]),
        .Q(data_even_1[1]),
        .R(clear));
  FDRE \data_even_1_reg[2] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF1[2]),
        .Q(data_even_1[2]),
        .R(clear));
  FDRE \data_even_1_reg[3] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF1[3]),
        .Q(data_even_1[3]),
        .R(clear));
  FDRE \data_even_1_reg[4] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF1[4]),
        .Q(data_even_1[4]),
        .R(clear));
  FDRE \data_even_1_reg[5] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF1[5]),
        .Q(data_even_1[5]),
        .R(clear));
  FDRE \data_even_1_reg[6] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF1[6]),
        .Q(data_even_1[6]),
        .R(clear));
  FDRE \data_even_1_reg[7] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF1[7]),
        .Q(data_even_1[7]),
        .R(clear));
  FDRE \data_even_1_reg[8] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF1[8]),
        .Q(data_even_1[8]),
        .R(clear));
  FDRE \data_even_1_reg[9] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF1[9]),
        .Q(data_even_1[9]),
        .R(clear));
  FDRE \data_even_2_reg[0] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF2[0]),
        .Q(data_even_2[0]),
        .R(clear));
  FDRE \data_even_2_reg[10] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF2[10]),
        .Q(data_even_2[10]),
        .R(clear));
  FDRE \data_even_2_reg[11] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF2[11]),
        .Q(data_even_2[11]),
        .R(clear));
  FDRE \data_even_2_reg[12] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF2[12]),
        .Q(data_even_2[12]),
        .R(clear));
  FDRE \data_even_2_reg[13] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF2[13]),
        .Q(data_even_2[13]),
        .R(clear));
  FDRE \data_even_2_reg[14] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF2[14]),
        .Q(data_even_2[14]),
        .R(clear));
  FDRE \data_even_2_reg[16] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF2[15]),
        .Q(data_even_2[16]),
        .R(clear));
  FDRE \data_even_2_reg[1] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF2[1]),
        .Q(data_even_2[1]),
        .R(clear));
  FDRE \data_even_2_reg[2] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF2[2]),
        .Q(data_even_2[2]),
        .R(clear));
  FDRE \data_even_2_reg[3] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF2[3]),
        .Q(data_even_2[3]),
        .R(clear));
  FDRE \data_even_2_reg[4] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF2[4]),
        .Q(data_even_2[4]),
        .R(clear));
  FDRE \data_even_2_reg[5] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF2[5]),
        .Q(data_even_2[5]),
        .R(clear));
  FDRE \data_even_2_reg[6] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF2[6]),
        .Q(data_even_2[6]),
        .R(clear));
  FDRE \data_even_2_reg[7] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF2[7]),
        .Q(data_even_2[7]),
        .R(clear));
  FDRE \data_even_2_reg[8] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF2[8]),
        .Q(data_even_2[8]),
        .R(clear));
  FDRE \data_even_2_reg[9] 
       (.C(data_clk),
        .CE(p_1_in),
        .D(data_in_IF2[9]),
        .Q(data_even_2[9]),
        .R(clear));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_1[11]_i_2 
       (.I0(data_even_1[11]),
        .I1(data_in_IF1[11]),
        .O(\data_sum_1[11]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_1[11]_i_3 
       (.I0(data_even_1[10]),
        .I1(data_in_IF1[10]),
        .O(\data_sum_1[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_1[11]_i_4 
       (.I0(data_even_1[9]),
        .I1(data_in_IF1[9]),
        .O(\data_sum_1[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_1[11]_i_5 
       (.I0(data_even_1[8]),
        .I1(data_in_IF1[8]),
        .O(\data_sum_1[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_1[15]_i_2 
       (.I0(data_in_IF1[15]),
        .I1(data_even_1[16]),
        .O(\data_sum_1[15]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_1[15]_i_3 
       (.I0(data_even_1[14]),
        .I1(data_in_IF1[14]),
        .O(\data_sum_1[15]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_1[15]_i_4 
       (.I0(data_even_1[13]),
        .I1(data_in_IF1[13]),
        .O(\data_sum_1[15]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_1[15]_i_5 
       (.I0(data_even_1[12]),
        .I1(data_in_IF1[12]),
        .O(\data_sum_1[15]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_1[16]_i_2 
       (.I0(data_in_IF1[15]),
        .I1(data_even_1[16]),
        .O(\data_sum_1[16]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_1[3]_i_2 
       (.I0(data_even_1[3]),
        .I1(data_in_IF1[3]),
        .O(\data_sum_1[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_1[3]_i_3 
       (.I0(data_even_1[2]),
        .I1(data_in_IF1[2]),
        .O(\data_sum_1[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_1[3]_i_4 
       (.I0(data_even_1[1]),
        .I1(data_in_IF1[1]),
        .O(\data_sum_1[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_1[3]_i_5 
       (.I0(data_even_1[0]),
        .I1(data_in_IF1[0]),
        .O(\data_sum_1[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_1[7]_i_2 
       (.I0(data_even_1[7]),
        .I1(data_in_IF1[7]),
        .O(\data_sum_1[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_1[7]_i_3 
       (.I0(data_even_1[6]),
        .I1(data_in_IF1[6]),
        .O(\data_sum_1[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_1[7]_i_4 
       (.I0(data_even_1[5]),
        .I1(data_in_IF1[5]),
        .O(\data_sum_1[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_1[7]_i_5 
       (.I0(data_even_1[4]),
        .I1(data_in_IF1[4]),
        .O(\data_sum_1[7]_i_5_n_0 ));
  FDRE \data_sum_1_reg[0] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_10[0]),
        .Q(m00_axis_tdata[0]),
        .R(clear));
  FDRE \data_sum_1_reg[10] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_10[10]),
        .Q(m00_axis_tdata[10]),
        .R(clear));
  FDRE \data_sum_1_reg[11] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_10[11]),
        .Q(m00_axis_tdata[11]),
        .R(clear));
  CARRY4 \data_sum_1_reg[11]_i_1 
       (.CI(\data_sum_1_reg[7]_i_1_n_0 ),
        .CO({\data_sum_1_reg[11]_i_1_n_0 ,\data_sum_1_reg[11]_i_1_n_1 ,\data_sum_1_reg[11]_i_1_n_2 ,\data_sum_1_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(data_even_1[11:8]),
        .O(data_sum_10[11:8]),
        .S({\data_sum_1[11]_i_2_n_0 ,\data_sum_1[11]_i_3_n_0 ,\data_sum_1[11]_i_4_n_0 ,\data_sum_1[11]_i_5_n_0 }));
  FDRE \data_sum_1_reg[12] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_10[12]),
        .Q(m00_axis_tdata[12]),
        .R(clear));
  FDRE \data_sum_1_reg[13] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_10[13]),
        .Q(m00_axis_tdata[13]),
        .R(clear));
  FDRE \data_sum_1_reg[14] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_10[14]),
        .Q(m00_axis_tdata[14]),
        .R(clear));
  FDRE \data_sum_1_reg[15] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_10[15]),
        .Q(m00_axis_tdata[15]),
        .R(clear));
  CARRY4 \data_sum_1_reg[15]_i_1 
       (.CI(\data_sum_1_reg[11]_i_1_n_0 ),
        .CO({\data_sum_1_reg[15]_i_1_n_0 ,\data_sum_1_reg[15]_i_1_n_1 ,\data_sum_1_reg[15]_i_1_n_2 ,\data_sum_1_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({data_in_IF1[15],data_even_1[14:12]}),
        .O(data_sum_10[15:12]),
        .S({\data_sum_1[15]_i_2_n_0 ,\data_sum_1[15]_i_3_n_0 ,\data_sum_1[15]_i_4_n_0 ,\data_sum_1[15]_i_5_n_0 }));
  FDRE \data_sum_1_reg[16] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_10[16]),
        .Q(m00_axis_tdata[16]),
        .R(clear));
  CARRY4 \data_sum_1_reg[16]_i_1 
       (.CI(\data_sum_1_reg[15]_i_1_n_0 ),
        .CO(\NLW_data_sum_1_reg[16]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_data_sum_1_reg[16]_i_1_O_UNCONNECTED [3:1],data_sum_10[16]}),
        .S({1'b0,1'b0,1'b0,\data_sum_1[16]_i_2_n_0 }));
  FDRE \data_sum_1_reg[1] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_10[1]),
        .Q(m00_axis_tdata[1]),
        .R(clear));
  FDRE \data_sum_1_reg[2] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_10[2]),
        .Q(m00_axis_tdata[2]),
        .R(clear));
  FDRE \data_sum_1_reg[3] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_10[3]),
        .Q(m00_axis_tdata[3]),
        .R(clear));
  CARRY4 \data_sum_1_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\data_sum_1_reg[3]_i_1_n_0 ,\data_sum_1_reg[3]_i_1_n_1 ,\data_sum_1_reg[3]_i_1_n_2 ,\data_sum_1_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(data_even_1[3:0]),
        .O(data_sum_10[3:0]),
        .S({\data_sum_1[3]_i_2_n_0 ,\data_sum_1[3]_i_3_n_0 ,\data_sum_1[3]_i_4_n_0 ,\data_sum_1[3]_i_5_n_0 }));
  FDRE \data_sum_1_reg[4] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_10[4]),
        .Q(m00_axis_tdata[4]),
        .R(clear));
  FDRE \data_sum_1_reg[5] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_10[5]),
        .Q(m00_axis_tdata[5]),
        .R(clear));
  FDRE \data_sum_1_reg[6] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_10[6]),
        .Q(m00_axis_tdata[6]),
        .R(clear));
  FDRE \data_sum_1_reg[7] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_10[7]),
        .Q(m00_axis_tdata[7]),
        .R(clear));
  CARRY4 \data_sum_1_reg[7]_i_1 
       (.CI(\data_sum_1_reg[3]_i_1_n_0 ),
        .CO({\data_sum_1_reg[7]_i_1_n_0 ,\data_sum_1_reg[7]_i_1_n_1 ,\data_sum_1_reg[7]_i_1_n_2 ,\data_sum_1_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(data_even_1[7:4]),
        .O(data_sum_10[7:4]),
        .S({\data_sum_1[7]_i_2_n_0 ,\data_sum_1[7]_i_3_n_0 ,\data_sum_1[7]_i_4_n_0 ,\data_sum_1[7]_i_5_n_0 }));
  FDRE \data_sum_1_reg[8] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_10[8]),
        .Q(m00_axis_tdata[8]),
        .R(clear));
  FDRE \data_sum_1_reg[9] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_10[9]),
        .Q(m00_axis_tdata[9]),
        .R(clear));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_2[11]_i_2 
       (.I0(data_even_2[11]),
        .I1(data_in_IF2[11]),
        .O(\data_sum_2[11]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_2[11]_i_3 
       (.I0(data_even_2[10]),
        .I1(data_in_IF2[10]),
        .O(\data_sum_2[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_2[11]_i_4 
       (.I0(data_even_2[9]),
        .I1(data_in_IF2[9]),
        .O(\data_sum_2[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_2[11]_i_5 
       (.I0(data_even_2[8]),
        .I1(data_in_IF2[8]),
        .O(\data_sum_2[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_2[15]_i_2 
       (.I0(data_in_IF2[15]),
        .I1(data_even_2[16]),
        .O(\data_sum_2[15]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_2[15]_i_3 
       (.I0(data_even_2[14]),
        .I1(data_in_IF2[14]),
        .O(\data_sum_2[15]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_2[15]_i_4 
       (.I0(data_even_2[13]),
        .I1(data_in_IF2[13]),
        .O(\data_sum_2[15]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_2[15]_i_5 
       (.I0(data_even_2[12]),
        .I1(data_in_IF2[12]),
        .O(\data_sum_2[15]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \data_sum_2[16]_i_1 
       (.I0(m00_axis_aresetn),
        .O(clear));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_2[16]_i_3 
       (.I0(data_in_IF2[15]),
        .I1(data_even_2[16]),
        .O(\data_sum_2[16]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_2[3]_i_2 
       (.I0(data_even_2[3]),
        .I1(data_in_IF2[3]),
        .O(\data_sum_2[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_2[3]_i_3 
       (.I0(data_even_2[2]),
        .I1(data_in_IF2[2]),
        .O(\data_sum_2[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_2[3]_i_4 
       (.I0(data_even_2[1]),
        .I1(data_in_IF2[1]),
        .O(\data_sum_2[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_2[3]_i_5 
       (.I0(data_even_2[0]),
        .I1(data_in_IF2[0]),
        .O(\data_sum_2[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_2[7]_i_2 
       (.I0(data_even_2[7]),
        .I1(data_in_IF2[7]),
        .O(\data_sum_2[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_2[7]_i_3 
       (.I0(data_even_2[6]),
        .I1(data_in_IF2[6]),
        .O(\data_sum_2[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_2[7]_i_4 
       (.I0(data_even_2[5]),
        .I1(data_in_IF2[5]),
        .O(\data_sum_2[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \data_sum_2[7]_i_5 
       (.I0(data_even_2[4]),
        .I1(data_in_IF2[4]),
        .O(\data_sum_2[7]_i_5_n_0 ));
  FDRE \data_sum_2_reg[0] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_20[0]),
        .Q(m00_axis_tdata[17]),
        .R(clear));
  FDRE \data_sum_2_reg[10] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_20[10]),
        .Q(m00_axis_tdata[27]),
        .R(clear));
  FDRE \data_sum_2_reg[11] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_20[11]),
        .Q(m00_axis_tdata[28]),
        .R(clear));
  CARRY4 \data_sum_2_reg[11]_i_1 
       (.CI(\data_sum_2_reg[7]_i_1_n_0 ),
        .CO({\data_sum_2_reg[11]_i_1_n_0 ,\data_sum_2_reg[11]_i_1_n_1 ,\data_sum_2_reg[11]_i_1_n_2 ,\data_sum_2_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(data_even_2[11:8]),
        .O(data_sum_20[11:8]),
        .S({\data_sum_2[11]_i_2_n_0 ,\data_sum_2[11]_i_3_n_0 ,\data_sum_2[11]_i_4_n_0 ,\data_sum_2[11]_i_5_n_0 }));
  FDRE \data_sum_2_reg[12] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_20[12]),
        .Q(m00_axis_tdata[29]),
        .R(clear));
  FDRE \data_sum_2_reg[13] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_20[13]),
        .Q(m00_axis_tdata[30]),
        .R(clear));
  FDRE \data_sum_2_reg[14] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_20[14]),
        .Q(m00_axis_tdata[31]),
        .R(clear));
  FDRE \data_sum_2_reg[15] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_20[15]),
        .Q(m00_axis_tdata[32]),
        .R(clear));
  CARRY4 \data_sum_2_reg[15]_i_1 
       (.CI(\data_sum_2_reg[11]_i_1_n_0 ),
        .CO({\data_sum_2_reg[15]_i_1_n_0 ,\data_sum_2_reg[15]_i_1_n_1 ,\data_sum_2_reg[15]_i_1_n_2 ,\data_sum_2_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({data_in_IF2[15],data_even_2[14:12]}),
        .O(data_sum_20[15:12]),
        .S({\data_sum_2[15]_i_2_n_0 ,\data_sum_2[15]_i_3_n_0 ,\data_sum_2[15]_i_4_n_0 ,\data_sum_2[15]_i_5_n_0 }));
  FDRE \data_sum_2_reg[16] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_20[16]),
        .Q(m00_axis_tdata[33]),
        .R(clear));
  CARRY4 \data_sum_2_reg[16]_i_2 
       (.CI(\data_sum_2_reg[15]_i_1_n_0 ),
        .CO(\NLW_data_sum_2_reg[16]_i_2_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_data_sum_2_reg[16]_i_2_O_UNCONNECTED [3:1],data_sum_20[16]}),
        .S({1'b0,1'b0,1'b0,\data_sum_2[16]_i_3_n_0 }));
  FDRE \data_sum_2_reg[1] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_20[1]),
        .Q(m00_axis_tdata[18]),
        .R(clear));
  FDRE \data_sum_2_reg[2] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_20[2]),
        .Q(m00_axis_tdata[19]),
        .R(clear));
  FDRE \data_sum_2_reg[3] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_20[3]),
        .Q(m00_axis_tdata[20]),
        .R(clear));
  CARRY4 \data_sum_2_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\data_sum_2_reg[3]_i_1_n_0 ,\data_sum_2_reg[3]_i_1_n_1 ,\data_sum_2_reg[3]_i_1_n_2 ,\data_sum_2_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(data_even_2[3:0]),
        .O(data_sum_20[3:0]),
        .S({\data_sum_2[3]_i_2_n_0 ,\data_sum_2[3]_i_3_n_0 ,\data_sum_2[3]_i_4_n_0 ,\data_sum_2[3]_i_5_n_0 }));
  FDRE \data_sum_2_reg[4] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_20[4]),
        .Q(m00_axis_tdata[21]),
        .R(clear));
  FDRE \data_sum_2_reg[5] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_20[5]),
        .Q(m00_axis_tdata[22]),
        .R(clear));
  FDRE \data_sum_2_reg[6] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_20[6]),
        .Q(m00_axis_tdata[23]),
        .R(clear));
  FDRE \data_sum_2_reg[7] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_20[7]),
        .Q(m00_axis_tdata[24]),
        .R(clear));
  CARRY4 \data_sum_2_reg[7]_i_1 
       (.CI(\data_sum_2_reg[3]_i_1_n_0 ),
        .CO({\data_sum_2_reg[7]_i_1_n_0 ,\data_sum_2_reg[7]_i_1_n_1 ,\data_sum_2_reg[7]_i_1_n_2 ,\data_sum_2_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(data_even_2[7:4]),
        .O(data_sum_20[7:4]),
        .S({\data_sum_2[7]_i_2_n_0 ,\data_sum_2[7]_i_3_n_0 ,\data_sum_2[7]_i_4_n_0 ,\data_sum_2[7]_i_5_n_0 }));
  FDRE \data_sum_2_reg[8] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_20[8]),
        .Q(m00_axis_tdata[25]),
        .R(clear));
  FDRE \data_sum_2_reg[9] 
       (.C(data_clk),
        .CE(cnt10),
        .D(data_sum_20[9]),
        .Q(m00_axis_tdata[26]),
        .R(clear));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_1
       (.I0(m00_axis_tvalid_r2[14]),
        .I1(cnt5_reg[14]),
        .I2(cnt5_reg[15]),
        .I3(m00_axis_tvalid_r2[15]),
        .O(i__carry__0_i_1_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_2
       (.I0(m00_axis_tvalid_r2[12]),
        .I1(cnt5_reg[12]),
        .I2(cnt5_reg[13]),
        .I3(m00_axis_tvalid_r2[13]),
        .O(i__carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_3
       (.I0(m00_axis_tvalid_r2[10]),
        .I1(cnt5_reg[10]),
        .I2(cnt5_reg[11]),
        .I3(m00_axis_tvalid_r2[11]),
        .O(i__carry__0_i_3_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry__0_i_4
       (.I0(m00_axis_tvalid_r2[8]),
        .I1(cnt5_reg[8]),
        .I2(cnt5_reg[9]),
        .I3(m00_axis_tvalid_r2[9]),
        .O(i__carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5
       (.I0(cnt5_reg[15]),
        .I1(m00_axis_tvalid_r2[15]),
        .I2(m00_axis_tvalid_r2[14]),
        .I3(cnt5_reg[14]),
        .O(i__carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6
       (.I0(cnt5_reg[13]),
        .I1(m00_axis_tvalid_r2[13]),
        .I2(m00_axis_tvalid_r2[12]),
        .I3(cnt5_reg[12]),
        .O(i__carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_7
       (.I0(cnt5_reg[11]),
        .I1(m00_axis_tvalid_r2[11]),
        .I2(m00_axis_tvalid_r2[10]),
        .I3(cnt5_reg[10]),
        .O(i__carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_8
       (.I0(cnt5_reg[9]),
        .I1(m00_axis_tvalid_r2[9]),
        .I2(m00_axis_tvalid_r2[8]),
        .I3(cnt5_reg[8]),
        .O(i__carry__0_i_8_n_0));
  CARRY4 i__carry__0_i_9
       (.CI(i__carry_i_9_n_0),
        .CO({i__carry__0_i_9_n_0,i__carry__0_i_9_n_1,i__carry__0_i_9_n_2,i__carry__0_i_9_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(m00_axis_tvalid_r2[12:9]),
        .S(FrameSize[12:9]));
  CARRY4 i__carry__1_i_1
       (.CI(i__carry__0_i_9_n_0),
        .CO({m00_axis_tvalid_r2[16],NLW_i__carry__1_i_1_CO_UNCONNECTED[2],i__carry__1_i_1_n_2,i__carry__1_i_1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_i__carry__1_i_1_O_UNCONNECTED[3],m00_axis_tvalid_r2[15:13]}),
        .S({1'b1,FrameSize[15:13]}));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_2
       (.I0(m00_axis_tvalid_r2[16]),
        .O(i__carry__1_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_1
       (.I0(m00_axis_tvalid_r2[6]),
        .I1(cnt5_reg[6]),
        .I2(cnt5_reg[7]),
        .I3(m00_axis_tvalid_r2[7]),
        .O(i__carry_i_1_n_0));
  CARRY4 i__carry_i_10
       (.CI(1'b0),
        .CO({i__carry_i_10_n_0,i__carry_i_10_n_1,i__carry_i_10_n_2,i__carry_i_10_n_3}),
        .CYINIT(FrameSize[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(m00_axis_tvalid_r2[4:1]),
        .S(FrameSize[4:1]));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_2
       (.I0(m00_axis_tvalid_r2[4]),
        .I1(cnt5_reg[4]),
        .I2(cnt5_reg[5]),
        .I3(m00_axis_tvalid_r2[5]),
        .O(i__carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2F02)) 
    i__carry_i_3
       (.I0(m00_axis_tvalid_r2[2]),
        .I1(cnt5_reg[2]),
        .I2(cnt5_reg[3]),
        .I3(m00_axis_tvalid_r2[3]),
        .O(i__carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h1F01)) 
    i__carry_i_4
       (.I0(FrameSize[0]),
        .I1(cnt5_reg[0]),
        .I2(cnt5_reg[1]),
        .I3(m00_axis_tvalid_r2[1]),
        .O(i__carry_i_4_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5
       (.I0(cnt5_reg[7]),
        .I1(m00_axis_tvalid_r2[7]),
        .I2(m00_axis_tvalid_r2[6]),
        .I3(cnt5_reg[6]),
        .O(i__carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6
       (.I0(cnt5_reg[5]),
        .I1(m00_axis_tvalid_r2[5]),
        .I2(m00_axis_tvalid_r2[4]),
        .I3(cnt5_reg[4]),
        .O(i__carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7
       (.I0(cnt5_reg[3]),
        .I1(m00_axis_tvalid_r2[3]),
        .I2(m00_axis_tvalid_r2[2]),
        .I3(cnt5_reg[2]),
        .O(i__carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h6006)) 
    i__carry_i_8
       (.I0(FrameSize[0]),
        .I1(cnt5_reg[0]),
        .I2(m00_axis_tvalid_r2[1]),
        .I3(cnt5_reg[1]),
        .O(i__carry_i_8_n_0));
  CARRY4 i__carry_i_9
       (.CI(i__carry_i_10_n_0),
        .CO({i__carry_i_9_n_0,i__carry_i_9_n_1,i__carry_i_9_n_2,i__carry_i_9_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(m00_axis_tvalid_r2[8:5]),
        .S(FrameSize[8:5]));
  CARRY4 m00_axis_tlast_r0_carry
       (.CI(1'b0),
        .CO({m00_axis_tlast_r0_carry_n_0,m00_axis_tlast_r0_carry_n_1,m00_axis_tlast_r0_carry_n_2,m00_axis_tlast_r0_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_m00_axis_tlast_r0_carry_O_UNCONNECTED[3:0]),
        .S({m00_axis_tlast_r0_carry_i_1_n_0,m00_axis_tlast_r0_carry_i_2_n_0,m00_axis_tlast_r0_carry_i_3_n_0,m00_axis_tlast_r0_carry_i_4_n_0}));
  CARRY4 m00_axis_tlast_r0_carry__0
       (.CI(m00_axis_tlast_r0_carry_n_0),
        .CO({m00_axis_tlast_r0_carry__0_n_0,m00_axis_tlast_r0_carry__0_n_1,m00_axis_tlast_r0_carry__0_n_2,m00_axis_tlast_r0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_m00_axis_tlast_r0_carry__0_O_UNCONNECTED[3:0]),
        .S({m00_axis_tlast_r0_carry__0_i_1_n_0,m00_axis_tlast_r0_carry__0_i_1_n_0,m00_axis_tlast_r0_carry__0_i_2_n_0,m00_axis_tlast_r0_carry__0_i_3_n_0}));
  CARRY4 m00_axis_tlast_r0_carry__0_i_1
       (.CI(m00_axis_tlast_r0_carry_i_5_n_0),
        .CO({m00_axis_tlast_r0_carry__0_i_1_n_0,NLW_m00_axis_tlast_r0_carry__0_i_1_CO_UNCONNECTED[2],m00_axis_tlast_r0_carry__0_i_1_n_2,m00_axis_tlast_r0_carry__0_i_1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,FrameSize[15:13]}),
        .O({NLW_m00_axis_tlast_r0_carry__0_i_1_O_UNCONNECTED[3],m00_axis_tlast_r1[15:13]}),
        .S({1'b1,m00_axis_tlast_r0_carry__0_i_4_n_0,m00_axis_tlast_r0_carry__0_i_5_n_0,m00_axis_tlast_r0_carry__0_i_6_n_0}));
  LUT3 #(
    .INIT(8'h82)) 
    m00_axis_tlast_r0_carry__0_i_2
       (.I0(m00_axis_tlast_r0_carry__0_i_1_n_0),
        .I1(m00_axis_tlast_r1[15]),
        .I2(cnt5_reg[15]),
        .O(m00_axis_tlast_r0_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    m00_axis_tlast_r0_carry__0_i_3
       (.I0(cnt5_reg[13]),
        .I1(m00_axis_tlast_r1[13]),
        .I2(cnt5_reg[12]),
        .I3(m00_axis_tlast_r1[12]),
        .I4(cnt5_reg[14]),
        .I5(m00_axis_tlast_r1[14]),
        .O(m00_axis_tlast_r0_carry__0_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tlast_r0_carry__0_i_4
       (.I0(FrameSize[15]),
        .O(m00_axis_tlast_r0_carry__0_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tlast_r0_carry__0_i_5
       (.I0(FrameSize[14]),
        .O(m00_axis_tlast_r0_carry__0_i_5_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tlast_r0_carry__0_i_6
       (.I0(FrameSize[13]),
        .O(m00_axis_tlast_r0_carry__0_i_6_n_0));
  CARRY4 m00_axis_tlast_r0_carry__1
       (.CI(m00_axis_tlast_r0_carry__0_n_0),
        .CO({NLW_m00_axis_tlast_r0_carry__1_CO_UNCONNECTED[3],m00_axis_tlast_r0,m00_axis_tlast_r0_carry__1_n_2,m00_axis_tlast_r0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_m00_axis_tlast_r0_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,m00_axis_tlast_r0_carry__0_i_1_n_0,m00_axis_tlast_r0_carry__0_i_1_n_0,m00_axis_tlast_r0_carry__0_i_1_n_0}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    m00_axis_tlast_r0_carry_i_1
       (.I0(cnt5_reg[10]),
        .I1(m00_axis_tlast_r1[10]),
        .I2(cnt5_reg[9]),
        .I3(m00_axis_tlast_r1[9]),
        .I4(cnt5_reg[11]),
        .I5(m00_axis_tlast_r1[11]),
        .O(m00_axis_tlast_r0_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tlast_r0_carry_i_10
       (.I0(FrameSize[10]),
        .O(m00_axis_tlast_r0_carry_i_10_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tlast_r0_carry_i_11
       (.I0(FrameSize[9]),
        .O(m00_axis_tlast_r0_carry_i_11_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tlast_r0_carry_i_12
       (.I0(FrameSize[8]),
        .O(m00_axis_tlast_r0_carry_i_12_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tlast_r0_carry_i_13
       (.I0(FrameSize[7]),
        .O(m00_axis_tlast_r0_carry_i_13_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tlast_r0_carry_i_14
       (.I0(FrameSize[6]),
        .O(m00_axis_tlast_r0_carry_i_14_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tlast_r0_carry_i_15
       (.I0(FrameSize[5]),
        .O(m00_axis_tlast_r0_carry_i_15_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tlast_r0_carry_i_16
       (.I0(FrameSize[4]),
        .O(m00_axis_tlast_r0_carry_i_16_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tlast_r0_carry_i_17
       (.I0(FrameSize[3]),
        .O(m00_axis_tlast_r0_carry_i_17_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tlast_r0_carry_i_18
       (.I0(FrameSize[2]),
        .O(m00_axis_tlast_r0_carry_i_18_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tlast_r0_carry_i_19
       (.I0(FrameSize[1]),
        .O(m00_axis_tlast_r0_carry_i_19_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    m00_axis_tlast_r0_carry_i_2
       (.I0(cnt5_reg[7]),
        .I1(m00_axis_tlast_r1[7]),
        .I2(cnt5_reg[6]),
        .I3(m00_axis_tlast_r1[6]),
        .I4(cnt5_reg[8]),
        .I5(m00_axis_tlast_r1[8]),
        .O(m00_axis_tlast_r0_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    m00_axis_tlast_r0_carry_i_3
       (.I0(cnt5_reg[4]),
        .I1(m00_axis_tlast_r1[4]),
        .I2(cnt5_reg[3]),
        .I3(m00_axis_tlast_r1[3]),
        .I4(cnt5_reg[5]),
        .I5(m00_axis_tlast_r1[5]),
        .O(m00_axis_tlast_r0_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000900990090000)) 
    m00_axis_tlast_r0_carry_i_4
       (.I0(cnt5_reg[1]),
        .I1(m00_axis_tlast_r1[1]),
        .I2(m00_axis_tlast_r1[2]),
        .I3(cnt5_reg[2]),
        .I4(FrameSize[0]),
        .I5(cnt5_reg[0]),
        .O(m00_axis_tlast_r0_carry_i_4_n_0));
  CARRY4 m00_axis_tlast_r0_carry_i_5
       (.CI(m00_axis_tlast_r0_carry_i_6_n_0),
        .CO({m00_axis_tlast_r0_carry_i_5_n_0,m00_axis_tlast_r0_carry_i_5_n_1,m00_axis_tlast_r0_carry_i_5_n_2,m00_axis_tlast_r0_carry_i_5_n_3}),
        .CYINIT(1'b0),
        .DI(FrameSize[12:9]),
        .O(m00_axis_tlast_r1[12:9]),
        .S({m00_axis_tlast_r0_carry_i_8_n_0,m00_axis_tlast_r0_carry_i_9_n_0,m00_axis_tlast_r0_carry_i_10_n_0,m00_axis_tlast_r0_carry_i_11_n_0}));
  CARRY4 m00_axis_tlast_r0_carry_i_6
       (.CI(m00_axis_tlast_r0_carry_i_7_n_0),
        .CO({m00_axis_tlast_r0_carry_i_6_n_0,m00_axis_tlast_r0_carry_i_6_n_1,m00_axis_tlast_r0_carry_i_6_n_2,m00_axis_tlast_r0_carry_i_6_n_3}),
        .CYINIT(1'b0),
        .DI(FrameSize[8:5]),
        .O(m00_axis_tlast_r1[8:5]),
        .S({m00_axis_tlast_r0_carry_i_12_n_0,m00_axis_tlast_r0_carry_i_13_n_0,m00_axis_tlast_r0_carry_i_14_n_0,m00_axis_tlast_r0_carry_i_15_n_0}));
  CARRY4 m00_axis_tlast_r0_carry_i_7
       (.CI(1'b0),
        .CO({m00_axis_tlast_r0_carry_i_7_n_0,m00_axis_tlast_r0_carry_i_7_n_1,m00_axis_tlast_r0_carry_i_7_n_2,m00_axis_tlast_r0_carry_i_7_n_3}),
        .CYINIT(FrameSize[0]),
        .DI(FrameSize[4:1]),
        .O(m00_axis_tlast_r1[4:1]),
        .S({m00_axis_tlast_r0_carry_i_16_n_0,m00_axis_tlast_r0_carry_i_17_n_0,m00_axis_tlast_r0_carry_i_18_n_0,m00_axis_tlast_r0_carry_i_19_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tlast_r0_carry_i_8
       (.I0(FrameSize[12]),
        .O(m00_axis_tlast_r0_carry_i_8_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    m00_axis_tlast_r0_carry_i_9
       (.I0(FrameSize[11]),
        .O(m00_axis_tlast_r0_carry_i_9_n_0));
  LUT3 #(
    .INIT(8'hE0)) 
    m00_axis_tlast_r_i_1
       (.I0(m00_axis_tlast_r0),
        .I1(m00_axis_tlast),
        .I2(m00_axis_tvalid_r13_out),
        .O(m00_axis_tlast_r_i_1_n_0));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    m00_axis_tlast_r_i_2
       (.I0(cnt100_reg[1]),
        .I1(cnt100_reg[0]),
        .I2(m00_axis_aresetn),
        .I3(m00_axis_tlast_r_i_3_n_0),
        .I4(m00_axis_tlast_r_i_4_n_0),
        .I5(m00_axis_tlast_r_i_5_n_0),
        .O(m00_axis_tvalid_r13_out));
  LUT4 #(
    .INIT(16'h0001)) 
    m00_axis_tlast_r_i_3
       (.I0(cnt100_reg[5]),
        .I1(cnt100_reg[4]),
        .I2(cnt100_reg[3]),
        .I3(cnt100_reg[2]),
        .O(m00_axis_tlast_r_i_3_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    m00_axis_tlast_r_i_4
       (.I0(cnt100_reg[9]),
        .I1(cnt100_reg[8]),
        .I2(cnt100_reg[7]),
        .I3(cnt100_reg[6]),
        .O(m00_axis_tlast_r_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    m00_axis_tlast_r_i_5
       (.I0(cnt100_reg[10]),
        .I1(cnt100_reg[11]),
        .I2(cnt100_reg[12]),
        .I3(cnt100_reg[13]),
        .I4(cnt100_reg[15]),
        .I5(cnt100_reg[14]),
        .O(m00_axis_tlast_r_i_5_n_0));
  FDRE m00_axis_tlast_r_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(m00_axis_tlast_r_i_1_n_0),
        .Q(m00_axis_tlast),
        .R(1'b0));
  CARRY4 \m00_axis_tvalid_r1_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\m00_axis_tvalid_r1_inferred__0/i__carry_n_0 ,\m00_axis_tvalid_r1_inferred__0/i__carry_n_1 ,\m00_axis_tvalid_r1_inferred__0/i__carry_n_2 ,\m00_axis_tvalid_r1_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({i__carry_i_1_n_0,i__carry_i_2_n_0,i__carry_i_3_n_0,i__carry_i_4_n_0}),
        .O(\NLW_m00_axis_tvalid_r1_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({i__carry_i_5_n_0,i__carry_i_6_n_0,i__carry_i_7_n_0,i__carry_i_8_n_0}));
  CARRY4 \m00_axis_tvalid_r1_inferred__0/i__carry__0 
       (.CI(\m00_axis_tvalid_r1_inferred__0/i__carry_n_0 ),
        .CO({\m00_axis_tvalid_r1_inferred__0/i__carry__0_n_0 ,\m00_axis_tvalid_r1_inferred__0/i__carry__0_n_1 ,\m00_axis_tvalid_r1_inferred__0/i__carry__0_n_2 ,\m00_axis_tvalid_r1_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({i__carry__0_i_1_n_0,i__carry__0_i_2_n_0,i__carry__0_i_3_n_0,i__carry__0_i_4_n_0}),
        .O(\NLW_m00_axis_tvalid_r1_inferred__0/i__carry__0_O_UNCONNECTED [3:0]),
        .S({i__carry__0_i_5_n_0,i__carry__0_i_6_n_0,i__carry__0_i_7_n_0,i__carry__0_i_8_n_0}));
  CARRY4 \m00_axis_tvalid_r1_inferred__0/i__carry__1 
       (.CI(\m00_axis_tvalid_r1_inferred__0/i__carry__0_n_0 ),
        .CO({\NLW_m00_axis_tvalid_r1_inferred__0/i__carry__1_CO_UNCONNECTED [3:1],m00_axis_tvalid_r1}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,m00_axis_tvalid_r2[16]}),
        .O(\NLW_m00_axis_tvalid_r1_inferred__0/i__carry__1_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,i__carry__1_i_2_n_0}));
  LUT5 #(
    .INIT(32'hFFE00000)) 
    m00_axis_tvalid_r_i_1
       (.I0(m00_axis_tvalid_r_i_2_n_0),
        .I1(m00_axis_tvalid_r_i_3_n_0),
        .I2(m00_axis_tvalid_r1),
        .I3(m00_axis_tvalid),
        .I4(m00_axis_tvalid_r13_out),
        .O(m00_axis_tvalid_r_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    m00_axis_tvalid_r_i_2
       (.I0(cnt5_reg[5]),
        .I1(cnt5_reg[6]),
        .I2(cnt5_reg[7]),
        .I3(cnt5_reg[8]),
        .I4(m00_axis_tvalid_r_i_4_n_0),
        .O(m00_axis_tvalid_r_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    m00_axis_tvalid_r_i_3
       (.I0(cnt5_reg[13]),
        .I1(cnt5_reg[14]),
        .I2(cnt5_reg[15]),
        .I3(cnt5_reg[0]),
        .I4(m00_axis_tvalid_r_i_5_n_0),
        .O(m00_axis_tvalid_r_i_3_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_axis_tvalid_r_i_4
       (.I0(cnt5_reg[4]),
        .I1(cnt5_reg[3]),
        .I2(cnt5_reg[2]),
        .I3(cnt5_reg[1]),
        .O(m00_axis_tvalid_r_i_4_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_axis_tvalid_r_i_5
       (.I0(cnt5_reg[12]),
        .I1(cnt5_reg[11]),
        .I2(cnt5_reg[10]),
        .I3(cnt5_reg[9]),
        .O(m00_axis_tvalid_r_i_5_n_0));
  FDRE m00_axis_tvalid_r_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(m00_axis_tvalid_r_i_1_n_0),
        .Q(m00_axis_tvalid),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_Sample_Generator_0_0,Sample_Generator_v3_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "Sample_Generator_v3_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (FrameSize,
    data_clk,
    clk_5MHz,
    data_in_IF1,
    data_in_IF2,
    m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tvalid,
    m00_axis_tready,
    m00_axis_aclk,
    m00_axis_config_tvalid,
    m00_axis_config_tdata,
    m00_axis_config_tready,
    m00_axis_aresetn);
  input [15:0]FrameSize;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 data_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME data_clk, FREQ_HZ 10000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_2_clk_out1, INSERT_VIP 0" *) input data_clk;
  input clk_5MHz;
  input [15:0]data_in_IF1;
  input [15:0]data_in_IF2;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TDATA" *) output [47:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB" *) output [5:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 6, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0" *) input m00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS_CONFIG TVALID" *) output m00_axis_config_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS_CONFIG TDATA" *) output [47:0]m00_axis_config_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS_CONFIG TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M_AXIS_CONFIG, TDATA_NUM_BYTES 6, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_config_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input m00_axis_aresetn;

  wire \<const0> ;
  wire \<const1> ;
  wire [15:0]FrameSize;
  wire clk_5MHz;
  wire data_clk;
  wire [15:0]data_in_IF1;
  wire [15:0]data_in_IF2;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [40:0]\^m00_axis_tdata ;
  wire m00_axis_tlast;
  wire m00_axis_tvalid;

  assign m00_axis_config_tdata[47] = \<const0> ;
  assign m00_axis_config_tdata[46] = \<const0> ;
  assign m00_axis_config_tdata[45] = \<const0> ;
  assign m00_axis_config_tdata[44] = \<const0> ;
  assign m00_axis_config_tdata[43] = \<const0> ;
  assign m00_axis_config_tdata[42] = \<const0> ;
  assign m00_axis_config_tdata[41] = \<const0> ;
  assign m00_axis_config_tdata[40] = \<const0> ;
  assign m00_axis_config_tdata[39] = \<const0> ;
  assign m00_axis_config_tdata[38] = \<const0> ;
  assign m00_axis_config_tdata[37] = \<const0> ;
  assign m00_axis_config_tdata[36] = \<const0> ;
  assign m00_axis_config_tdata[35] = \<const0> ;
  assign m00_axis_config_tdata[34] = \<const0> ;
  assign m00_axis_config_tdata[33] = \<const0> ;
  assign m00_axis_config_tdata[32] = \<const0> ;
  assign m00_axis_config_tdata[31] = \<const0> ;
  assign m00_axis_config_tdata[30] = \<const0> ;
  assign m00_axis_config_tdata[29] = \<const0> ;
  assign m00_axis_config_tdata[28] = \<const0> ;
  assign m00_axis_config_tdata[27] = \<const0> ;
  assign m00_axis_config_tdata[26] = \<const0> ;
  assign m00_axis_config_tdata[25] = \<const0> ;
  assign m00_axis_config_tdata[24] = \<const0> ;
  assign m00_axis_config_tdata[23] = \<const0> ;
  assign m00_axis_config_tdata[22] = \<const0> ;
  assign m00_axis_config_tdata[21] = \<const0> ;
  assign m00_axis_config_tdata[20] = \<const0> ;
  assign m00_axis_config_tdata[19] = \<const0> ;
  assign m00_axis_config_tdata[18] = \<const0> ;
  assign m00_axis_config_tdata[17] = \<const0> ;
  assign m00_axis_config_tdata[16] = \<const0> ;
  assign m00_axis_config_tdata[15] = \<const0> ;
  assign m00_axis_config_tdata[14] = \<const0> ;
  assign m00_axis_config_tdata[13] = \<const0> ;
  assign m00_axis_config_tdata[12] = \<const0> ;
  assign m00_axis_config_tdata[11] = \<const0> ;
  assign m00_axis_config_tdata[10] = \<const0> ;
  assign m00_axis_config_tdata[9] = \<const0> ;
  assign m00_axis_config_tdata[8] = \<const0> ;
  assign m00_axis_config_tdata[7] = \<const0> ;
  assign m00_axis_config_tdata[6] = \<const0> ;
  assign m00_axis_config_tdata[5] = \<const0> ;
  assign m00_axis_config_tdata[4] = \<const0> ;
  assign m00_axis_config_tdata[3] = \<const0> ;
  assign m00_axis_config_tdata[2] = \<const0> ;
  assign m00_axis_config_tdata[1] = \<const0> ;
  assign m00_axis_config_tdata[0] = \<const0> ;
  assign m00_axis_config_tvalid = \<const0> ;
  assign m00_axis_tdata[47] = \<const0> ;
  assign m00_axis_tdata[46] = \<const0> ;
  assign m00_axis_tdata[45] = \<const0> ;
  assign m00_axis_tdata[44] = \<const0> ;
  assign m00_axis_tdata[43] = \<const0> ;
  assign m00_axis_tdata[42] = \<const0> ;
  assign m00_axis_tdata[41] = \<const0> ;
  assign m00_axis_tdata[40:24] = \^m00_axis_tdata [40:24];
  assign m00_axis_tdata[23] = \<const0> ;
  assign m00_axis_tdata[22] = \<const0> ;
  assign m00_axis_tdata[21] = \<const0> ;
  assign m00_axis_tdata[20] = \<const0> ;
  assign m00_axis_tdata[19] = \<const0> ;
  assign m00_axis_tdata[18] = \<const0> ;
  assign m00_axis_tdata[17] = \<const0> ;
  assign m00_axis_tdata[16:0] = \^m00_axis_tdata [16:0];
  assign m00_axis_tstrb[5] = \<const0> ;
  assign m00_axis_tstrb[4] = \<const0> ;
  assign m00_axis_tstrb[3] = \<const1> ;
  assign m00_axis_tstrb[2] = \<const1> ;
  assign m00_axis_tstrb[1] = \<const1> ;
  assign m00_axis_tstrb[0] = \<const1> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0 inst
       (.FrameSize(FrameSize),
        .clk_5MHz(clk_5MHz),
        .data_clk(data_clk),
        .data_in_IF1(data_in_IF1),
        .data_in_IF2(data_in_IF2),
        .m00_axis_aclk(m00_axis_aclk),
        .m00_axis_aresetn(m00_axis_aresetn),
        .m00_axis_tdata({\^m00_axis_tdata [40:24],\^m00_axis_tdata [16:0]}),
        .m00_axis_tlast(m00_axis_tlast),
        .m00_axis_tvalid(m00_axis_tvalid));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
