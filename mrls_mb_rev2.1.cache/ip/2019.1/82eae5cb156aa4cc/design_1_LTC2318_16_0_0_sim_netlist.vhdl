-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Wed Oct  6 14:07:18 2021
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_LTC2318_16_0_0_sim_netlist.vhdl
-- Design      : design_1_LTC2318_16_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_LTC2387_16 is
  port (
    adc_clkx_p : out STD_LOGIC;
    adc_clkx_n : out STD_LOGIC;
    ADC_DATA : out STD_LOGIC_VECTOR ( 15 downto 0 );
    adc_dax_p : in STD_LOGIC;
    adc_dax_n : in STD_LOGIC;
    adc_dbx_p : in STD_LOGIC;
    adc_dbx_n : in STD_LOGIC;
    adc_dcox_p : in STD_LOGIC;
    adc_dcox_n : in STD_LOGIC;
    clk_200 : in STD_LOGIC;
    fpga_clk : in STD_LOGIC;
    clk_100 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_LTC2387_16;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_LTC2387_16 is
  signal ADC_DATA_w : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \FSM_onehot_cnt_pos_dco[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_cnt_pos_dco[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_cnt_pos_dco[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_cnt_pos_dco_reg_n_0_[1]\ : STD_LOGIC;
  signal \FSM_onehot_cnt_pos_dco_reg_n_0_[2]\ : STD_LOGIC;
  signal adc_clk : STD_LOGIC;
  signal adc_da : STD_LOGIC;
  signal adc_db : STD_LOGIC;
  signal adc_dco : STD_LOGIC;
  signal adc_dco_delayed : STD_LOGIC;
  signal allowed : STD_LOGIC;
  signal \allowed0__0\ : STD_LOGIC;
  signal bit_0_i_1_n_0 : STD_LOGIC;
  signal bit_10_i_1_n_0 : STD_LOGIC;
  signal bit_11_i_1_n_0 : STD_LOGIC;
  signal bit_12 : STD_LOGIC;
  signal bit_12_i_1_n_0 : STD_LOGIC;
  signal bit_13_i_1_n_0 : STD_LOGIC;
  signal bit_14_i_1_n_0 : STD_LOGIC;
  signal bit_15_i_1_n_0 : STD_LOGIC;
  signal bit_1_i_1_n_0 : STD_LOGIC;
  signal bit_2_i_1_n_0 : STD_LOGIC;
  signal bit_3_i_1_n_0 : STD_LOGIC;
  signal bit_4_i_1_n_0 : STD_LOGIC;
  signal bit_5_i_1_n_0 : STD_LOGIC;
  signal bit_6_i_1_n_0 : STD_LOGIC;
  signal bit_7_i_1_n_0 : STD_LOGIC;
  signal bit_8_i_1_n_0 : STD_LOGIC;
  signal bit_9 : STD_LOGIC;
  signal bit_9_i_1_n_0 : STD_LOGIC;
  signal cnt100 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \cnt100[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt100[1]_i_1_n_0\ : STD_LOGIC;
  signal \cnt100[2]_i_1_n_0\ : STD_LOGIC;
  signal NLW_IDELAYCTRL_inst_RDY_UNCONNECTED : STD_LOGIC;
  signal NLW_IDELAYE2_adc_if2_ch1_dco_IDATAIN_UNCONNECTED : STD_LOGIC;
  signal NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEIN_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_cnt_pos_dco[0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \FSM_onehot_cnt_pos_dco[1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \FSM_onehot_cnt_pos_dco[2]_i_1\ : label is "soft_lutpair3";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_cnt_pos_dco_reg[0]\ : label is "iSTATE:1000,iSTATE0:0100,iSTATE1:0001,iSTATE2:0010,";
  attribute FSM_ENCODED_STATES of \FSM_onehot_cnt_pos_dco_reg[1]\ : label is "iSTATE:1000,iSTATE0:0100,iSTATE1:0001,iSTATE2:0010,";
  attribute FSM_ENCODED_STATES of \FSM_onehot_cnt_pos_dco_reg[2]\ : label is "iSTATE:1000,iSTATE0:0100,iSTATE1:0001,iSTATE2:0010,";
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of IBUFDS_adc_da : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of IBUFDS_adc_da : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_da : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of IBUFDS_adc_da : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_db : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_db : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_db : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_db : label is "AUTO";
  attribute BOX_TYPE of IBUFDS_adc_dco : label is "PRIMITIVE";
  attribute CAPACITANCE of IBUFDS_adc_dco : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of IBUFDS_adc_dco : label is "0";
  attribute IFD_DELAY_VALUE of IBUFDS_adc_dco : label is "AUTO";
  attribute BOX_TYPE of IDELAYCTRL_inst : label is "PRIMITIVE";
  attribute BOX_TYPE of IDELAYE2_adc_if2_ch1_dco : label is "PRIMITIVE";
  attribute SIM_DELAY_D : integer;
  attribute SIM_DELAY_D of IDELAYE2_adc_if2_ch1_dco : label is 0;
  attribute BOX_TYPE of OBUFDS_adc_clk : label is "PRIMITIVE";
  attribute CAPACITANCE of OBUFDS_adc_clk : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of OBUFDS_adc_clk : label is "OBUFDS";
  attribute SOFT_HLUTNM of allowed0 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of bit_0_i_2 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of bit_10_i_1 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of bit_11_i_1 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of bit_13_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of bit_14_i_1 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of bit_15_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of bit_6_i_1 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of bit_7_i_1 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \cnt100[1]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \cnt100[2]_i_1\ : label is "soft_lutpair4";
begin
\ADC_DATA_r_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => ADC_DATA_w(0),
      Q => ADC_DATA(0),
      R => '0'
    );
\ADC_DATA_r_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => ADC_DATA_w(10),
      Q => ADC_DATA(10),
      R => '0'
    );
\ADC_DATA_r_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => ADC_DATA_w(11),
      Q => ADC_DATA(11),
      R => '0'
    );
\ADC_DATA_r_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => ADC_DATA_w(12),
      Q => ADC_DATA(12),
      R => '0'
    );
\ADC_DATA_r_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => ADC_DATA_w(13),
      Q => ADC_DATA(13),
      R => '0'
    );
\ADC_DATA_r_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => ADC_DATA_w(14),
      Q => ADC_DATA(14),
      R => '0'
    );
\ADC_DATA_r_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => ADC_DATA_w(15),
      Q => ADC_DATA(15),
      R => '0'
    );
\ADC_DATA_r_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => ADC_DATA_w(1),
      Q => ADC_DATA(1),
      R => '0'
    );
\ADC_DATA_r_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => ADC_DATA_w(2),
      Q => ADC_DATA(2),
      R => '0'
    );
\ADC_DATA_r_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => ADC_DATA_w(3),
      Q => ADC_DATA(3),
      R => '0'
    );
\ADC_DATA_r_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => ADC_DATA_w(4),
      Q => ADC_DATA(4),
      R => '0'
    );
\ADC_DATA_r_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => ADC_DATA_w(5),
      Q => ADC_DATA(5),
      R => '0'
    );
\ADC_DATA_r_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => ADC_DATA_w(6),
      Q => ADC_DATA(6),
      R => '0'
    );
\ADC_DATA_r_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => ADC_DATA_w(7),
      Q => ADC_DATA(7),
      R => '0'
    );
\ADC_DATA_r_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => ADC_DATA_w(8),
      Q => ADC_DATA(8),
      R => '0'
    );
\ADC_DATA_r_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => fpga_clk,
      CE => '1',
      D => ADC_DATA_w(9),
      Q => ADC_DATA(9),
      R => '0'
    );
\FSM_onehot_cnt_pos_dco[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => cnt100(2),
      I1 => cnt100(0),
      I2 => cnt100(1),
      O => \FSM_onehot_cnt_pos_dco[0]_i_1_n_0\
    );
\FSM_onehot_cnt_pos_dco[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8AAA"
    )
        port map (
      I0 => bit_9,
      I1 => cnt100(2),
      I2 => cnt100(0),
      I3 => cnt100(1),
      O => \FSM_onehot_cnt_pos_dco[1]_i_1_n_0\
    );
\FSM_onehot_cnt_pos_dco[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8AAA"
    )
        port map (
      I0 => \FSM_onehot_cnt_pos_dco_reg_n_0_[1]\,
      I1 => cnt100(2),
      I2 => cnt100(0),
      I3 => cnt100(1),
      O => \FSM_onehot_cnt_pos_dco[2]_i_1_n_0\
    );
\FSM_onehot_cnt_pos_dco_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco_delayed,
      CE => '1',
      D => \FSM_onehot_cnt_pos_dco[0]_i_1_n_0\,
      Q => bit_9,
      R => '0'
    );
\FSM_onehot_cnt_pos_dco_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco_delayed,
      CE => '1',
      D => \FSM_onehot_cnt_pos_dco[1]_i_1_n_0\,
      Q => \FSM_onehot_cnt_pos_dco_reg_n_0_[1]\,
      R => '0'
    );
\FSM_onehot_cnt_pos_dco_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco_delayed,
      CE => '1',
      D => \FSM_onehot_cnt_pos_dco[2]_i_1_n_0\,
      Q => \FSM_onehot_cnt_pos_dco_reg_n_0_[2]\,
      R => '0'
    );
IBUFDS_adc_da: unisim.vcomponents.IBUFDS
     port map (
      I => adc_dax_p,
      IB => adc_dax_n,
      O => adc_da
    );
IBUFDS_adc_db: unisim.vcomponents.IBUFDS
     port map (
      I => adc_dbx_p,
      IB => adc_dbx_n,
      O => adc_db
    );
IBUFDS_adc_dco: unisim.vcomponents.IBUFDS
     port map (
      I => adc_dcox_p,
      IB => adc_dcox_n,
      O => adc_dco
    );
IDELAYCTRL_inst: unisim.vcomponents.IDELAYCTRL
    generic map(
      SIM_DEVICE => "7SERIES"
    )
        port map (
      RDY => NLW_IDELAYCTRL_inst_RDY_UNCONNECTED,
      REFCLK => clk_200,
      RST => '0'
    );
IDELAYE2_adc_if2_ch1_dco: unisim.vcomponents.IDELAYE2
    generic map(
      CINVCTRL_SEL => "FALSE",
      DELAY_SRC => "DATAIN",
      HIGH_PERFORMANCE_MODE => "TRUE",
      IDELAY_TYPE => "FIXED",
      IDELAY_VALUE => 31,
      IS_C_INVERTED => '0',
      IS_DATAIN_INVERTED => '0',
      IS_IDATAIN_INVERTED => '0',
      PIPE_SEL => "FALSE",
      REFCLK_FREQUENCY => 200.000000,
      SIGNAL_PATTERN => "CLOCK"
    )
        port map (
      C => clk_200,
      CE => '0',
      CINVCTRL => '0',
      CNTVALUEIN(4 downto 0) => NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEIN_UNCONNECTED(4 downto 0),
      CNTVALUEOUT(4 downto 0) => NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEOUT_UNCONNECTED(4 downto 0),
      DATAIN => adc_dco,
      DATAOUT => adc_dco_delayed,
      IDATAIN => NLW_IDELAYE2_adc_if2_ch1_dco_IDATAIN_UNCONNECTED,
      INC => '0',
      LD => '0',
      LDPIPEEN => '0',
      REGRST => '0'
    );
OBUFDS_adc_clk: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => adc_clk,
      O => adc_clkx_p,
      OB => adc_clkx_n
    );
OBUFDS_adc_clk_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => allowed,
      I1 => clk_100,
      O => adc_clk
    );
allowed0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cnt100(1),
      I1 => cnt100(2),
      O => \allowed0__0\
    );
allowed_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100,
      CE => '1',
      D => \allowed0__0\,
      Q => allowed,
      R => '0'
    );
bit_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFF00000200"
    )
        port map (
      I0 => adc_db,
      I1 => \FSM_onehot_cnt_pos_dco_reg_n_0_[1]\,
      I2 => bit_9,
      I3 => \FSM_onehot_cnt_pos_dco_reg_n_0_[2]\,
      I4 => bit_12,
      I5 => ADC_DATA_w(0),
      O => bit_0_i_1_n_0
    );
bit_0_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => cnt100(1),
      I1 => cnt100(0),
      I2 => cnt100(2),
      O => bit_12
    );
bit_0_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco_delayed,
      CE => '1',
      D => bit_0_i_1_n_0,
      Q => ADC_DATA_w(0),
      R => '0'
    );
bit_10_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => adc_db,
      I1 => bit_9,
      I2 => ADC_DATA_w(10),
      O => bit_10_i_1_n_0
    );
bit_10_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco_delayed,
      CE => '1',
      D => bit_10_i_1_n_0,
      Q => ADC_DATA_w(10),
      R => '0'
    );
bit_11_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => adc_da,
      I1 => bit_9,
      I2 => ADC_DATA_w(11),
      O => bit_11_i_1_n_0
    );
bit_11_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco_delayed,
      CE => '1',
      D => bit_11_i_1_n_0,
      Q => ADC_DATA_w(11),
      R => '0'
    );
bit_12_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBF0080"
    )
        port map (
      I0 => adc_db,
      I1 => cnt100(1),
      I2 => cnt100(0),
      I3 => cnt100(2),
      I4 => ADC_DATA_w(12),
      O => bit_12_i_1_n_0
    );
bit_12_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco_delayed,
      CE => '1',
      D => bit_12_i_1_n_0,
      Q => ADC_DATA_w(12),
      R => '0'
    );
bit_13_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBF0080"
    )
        port map (
      I0 => adc_da,
      I1 => cnt100(1),
      I2 => cnt100(0),
      I3 => cnt100(2),
      I4 => ADC_DATA_w(13),
      O => bit_13_i_1_n_0
    );
bit_13_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco_delayed,
      CE => '1',
      D => bit_13_i_1_n_0,
      Q => ADC_DATA_w(13),
      R => '0'
    );
bit_14_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0020"
    )
        port map (
      I0 => adc_db,
      I1 => cnt100(0),
      I2 => cnt100(1),
      I3 => cnt100(2),
      I4 => ADC_DATA_w(14),
      O => bit_14_i_1_n_0
    );
bit_14_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100,
      CE => '1',
      D => bit_14_i_1_n_0,
      Q => ADC_DATA_w(14),
      R => '0'
    );
bit_15_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0020"
    )
        port map (
      I0 => adc_da,
      I1 => cnt100(0),
      I2 => cnt100(1),
      I3 => cnt100(2),
      I4 => ADC_DATA_w(15),
      O => bit_15_i_1_n_0
    );
bit_15_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100,
      CE => '1',
      D => bit_15_i_1_n_0,
      Q => ADC_DATA_w(15),
      R => '0'
    );
bit_1_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFF00000200"
    )
        port map (
      I0 => adc_da,
      I1 => \FSM_onehot_cnt_pos_dco_reg_n_0_[1]\,
      I2 => bit_9,
      I3 => \FSM_onehot_cnt_pos_dco_reg_n_0_[2]\,
      I4 => bit_12,
      I5 => ADC_DATA_w(1),
      O => bit_1_i_1_n_0
    );
bit_1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco_delayed,
      CE => '1',
      D => bit_1_i_1_n_0,
      Q => ADC_DATA_w(1),
      R => '0'
    );
bit_2_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0008"
    )
        port map (
      I0 => adc_db,
      I1 => \FSM_onehot_cnt_pos_dco_reg_n_0_[2]\,
      I2 => bit_9,
      I3 => \FSM_onehot_cnt_pos_dco_reg_n_0_[1]\,
      I4 => ADC_DATA_w(2),
      O => bit_2_i_1_n_0
    );
bit_2_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco_delayed,
      CE => '1',
      D => bit_2_i_1_n_0,
      Q => ADC_DATA_w(2),
      R => '0'
    );
bit_3_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFB0008"
    )
        port map (
      I0 => adc_da,
      I1 => \FSM_onehot_cnt_pos_dco_reg_n_0_[2]\,
      I2 => bit_9,
      I3 => \FSM_onehot_cnt_pos_dco_reg_n_0_[1]\,
      I4 => ADC_DATA_w(3),
      O => bit_3_i_1_n_0
    );
bit_3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco_delayed,
      CE => '1',
      D => bit_3_i_1_n_0,
      Q => ADC_DATA_w(3),
      R => '0'
    );
bit_4_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFBBBBBB80888888"
    )
        port map (
      I0 => adc_db,
      I1 => \FSM_onehot_cnt_pos_dco_reg_n_0_[1]\,
      I2 => cnt100(2),
      I3 => cnt100(0),
      I4 => cnt100(1),
      I5 => ADC_DATA_w(4),
      O => bit_4_i_1_n_0
    );
bit_4_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco_delayed,
      CE => '1',
      D => bit_4_i_1_n_0,
      Q => ADC_DATA_w(4),
      R => '0'
    );
bit_5_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFBBBBBB80888888"
    )
        port map (
      I0 => adc_da,
      I1 => \FSM_onehot_cnt_pos_dco_reg_n_0_[1]\,
      I2 => cnt100(2),
      I3 => cnt100(0),
      I4 => cnt100(1),
      I5 => ADC_DATA_w(5),
      O => bit_5_i_1_n_0
    );
bit_5_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco_delayed,
      CE => '1',
      D => bit_5_i_1_n_0,
      Q => ADC_DATA_w(5),
      R => '0'
    );
bit_6_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => adc_db,
      I1 => \FSM_onehot_cnt_pos_dco_reg_n_0_[1]\,
      I2 => ADC_DATA_w(6),
      O => bit_6_i_1_n_0
    );
bit_6_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco_delayed,
      CE => '1',
      D => bit_6_i_1_n_0,
      Q => ADC_DATA_w(6),
      R => '0'
    );
bit_7_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => adc_da,
      I1 => \FSM_onehot_cnt_pos_dco_reg_n_0_[1]\,
      I2 => ADC_DATA_w(7),
      O => bit_7_i_1_n_0
    );
bit_7_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => adc_dco_delayed,
      CE => '1',
      D => bit_7_i_1_n_0,
      Q => ADC_DATA_w(7),
      R => '0'
    );
bit_8_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFBBBBBB80888888"
    )
        port map (
      I0 => adc_db,
      I1 => bit_9,
      I2 => cnt100(2),
      I3 => cnt100(0),
      I4 => cnt100(1),
      I5 => ADC_DATA_w(8),
      O => bit_8_i_1_n_0
    );
bit_8_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco_delayed,
      CE => '1',
      D => bit_8_i_1_n_0,
      Q => ADC_DATA_w(8),
      R => '0'
    );
bit_9_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFBBBBBB80888888"
    )
        port map (
      I0 => adc_da,
      I1 => bit_9,
      I2 => cnt100(2),
      I3 => cnt100(0),
      I4 => cnt100(1),
      I5 => ADC_DATA_w(9),
      O => bit_9_i_1_n_0
    );
bit_9_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => adc_dco_delayed,
      CE => '1',
      D => bit_9_i_1_n_0,
      Q => ADC_DATA_w(9),
      R => '0'
    );
\cnt100[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt100(0),
      I1 => fpga_clk,
      O => \cnt100[0]_i_1_n_0\
    );
\cnt100[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"06"
    )
        port map (
      I0 => cnt100(1),
      I1 => cnt100(0),
      I2 => fpga_clk,
      O => \cnt100[1]_i_1_n_0\
    );
\cnt100[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"006A"
    )
        port map (
      I0 => cnt100(2),
      I1 => cnt100(0),
      I2 => cnt100(1),
      I3 => fpga_clk,
      O => \cnt100[2]_i_1_n_0\
    );
\cnt100_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100,
      CE => '1',
      D => \cnt100[0]_i_1_n_0\,
      Q => cnt100(0),
      R => '0'
    );
\cnt100_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100,
      CE => '1',
      D => \cnt100[1]_i_1_n_0\,
      Q => cnt100(1),
      R => '0'
    );
\cnt100_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_100,
      CE => '1',
      D => \cnt100[2]_i_1_n_0\,
      Q => cnt100(2),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    ADC_DATA : out STD_LOGIC_VECTOR ( 15 downto 0 );
    adc_dax_p : in STD_LOGIC;
    adc_dax_n : in STD_LOGIC;
    adc_dbx_p : in STD_LOGIC;
    adc_dbx_n : in STD_LOGIC;
    adc_dcox_p : in STD_LOGIC;
    adc_dcox_n : in STD_LOGIC;
    fpga_clk : in STD_LOGIC;
    clk_100 : in STD_LOGIC;
    adc_clkx_p : out STD_LOGIC;
    adc_clkx_n : out STD_LOGIC;
    clk_200 : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_LTC2318_16_0_0,LTC2387_16,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "LTC2387_16,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of adc_clkx_n : signal is "bt.local:interface:diff:1.0 adc_clkx n";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of adc_clkx_n : signal is "XIL_INTERFACENAME adc_clkx, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of adc_clkx_p : signal is "bt.local:interface:diff:1.0 adc_clkx p";
  attribute X_INTERFACE_INFO of adc_dax_n : signal is "bt.local:interface:diff:1.0 adc_dax n";
  attribute X_INTERFACE_PARAMETER of adc_dax_n : signal is "XIL_INTERFACENAME adc_dax, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of adc_dax_p : signal is "bt.local:interface:diff:1.0 adc_dax p";
  attribute X_INTERFACE_INFO of adc_dbx_n : signal is "bt.local:interface:diff:1.0 adc_dbx n";
  attribute X_INTERFACE_PARAMETER of adc_dbx_n : signal is "XIL_INTERFACENAME adc_dbx, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of adc_dbx_p : signal is "bt.local:interface:diff:1.0 adc_dbx p";
  attribute X_INTERFACE_INFO of adc_dcox_n : signal is "bt.local:interface:diff:1.0 adc_dcox n";
  attribute X_INTERFACE_PARAMETER of adc_dcox_n : signal is "XIL_INTERFACENAME adc_dcox, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of adc_dcox_p : signal is "bt.local:interface:diff:1.0 adc_dcox p";
  attribute X_INTERFACE_INFO of fpga_clk : signal is "xilinx.com:signal:clock:1.0 fpga_clk CLK";
  attribute X_INTERFACE_PARAMETER of fpga_clk : signal is "XIL_INTERFACENAME fpga_clk, FREQ_HZ 10000000, PHASE 0.000, CLK_DOMAIN design_1_FPGA_CLK, INSERT_VIP 0";
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_LTC2387_16
     port map (
      ADC_DATA(15 downto 0) => ADC_DATA(15 downto 0),
      adc_clkx_n => adc_clkx_n,
      adc_clkx_p => adc_clkx_p,
      adc_dax_n => adc_dax_n,
      adc_dax_p => adc_dax_p,
      adc_dbx_n => adc_dbx_n,
      adc_dbx_p => adc_dbx_p,
      adc_dcox_n => adc_dcox_n,
      adc_dcox_p => adc_dcox_p,
      clk_100 => clk_100,
      clk_200 => clk_200,
      fpga_clk => fpga_clk
    );
end STRUCTURE;
