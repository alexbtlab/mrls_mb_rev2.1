// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Oct 13 11:44:45 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_counter_0_0_sim_netlist.v
// Design      : design_1_counter_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_counter
   (count,
    FrameSize,
    reset,
    clk);
  output [15:0]count;
  input [15:0]FrameSize;
  input reset;
  input clk;

  wire [15:0]FrameSize;
  wire clk;
  wire cnt0_carry__0_i_1_n_0;
  wire cnt0_carry__0_i_2_n_0;
  wire cnt0_carry__0_n_2;
  wire cnt0_carry__0_n_3;
  wire cnt0_carry_i_1_n_0;
  wire cnt0_carry_i_2_n_0;
  wire cnt0_carry_i_3_n_0;
  wire cnt0_carry_i_4_n_0;
  wire cnt0_carry_n_0;
  wire cnt0_carry_n_1;
  wire cnt0_carry_n_2;
  wire cnt0_carry_n_3;
  wire \cnt[15]_i_1_n_0 ;
  wire \cnt[3]_i_2_n_0 ;
  wire \cnt_reg[11]_i_1_n_0 ;
  wire \cnt_reg[11]_i_1_n_1 ;
  wire \cnt_reg[11]_i_1_n_2 ;
  wire \cnt_reg[11]_i_1_n_3 ;
  wire \cnt_reg[11]_i_1_n_4 ;
  wire \cnt_reg[11]_i_1_n_5 ;
  wire \cnt_reg[11]_i_1_n_6 ;
  wire \cnt_reg[11]_i_1_n_7 ;
  wire \cnt_reg[15]_i_2_n_1 ;
  wire \cnt_reg[15]_i_2_n_2 ;
  wire \cnt_reg[15]_i_2_n_3 ;
  wire \cnt_reg[15]_i_2_n_4 ;
  wire \cnt_reg[15]_i_2_n_5 ;
  wire \cnt_reg[15]_i_2_n_6 ;
  wire \cnt_reg[15]_i_2_n_7 ;
  wire \cnt_reg[3]_i_1_n_0 ;
  wire \cnt_reg[3]_i_1_n_1 ;
  wire \cnt_reg[3]_i_1_n_2 ;
  wire \cnt_reg[3]_i_1_n_3 ;
  wire \cnt_reg[3]_i_1_n_4 ;
  wire \cnt_reg[3]_i_1_n_5 ;
  wire \cnt_reg[3]_i_1_n_6 ;
  wire \cnt_reg[3]_i_1_n_7 ;
  wire \cnt_reg[7]_i_1_n_0 ;
  wire \cnt_reg[7]_i_1_n_1 ;
  wire \cnt_reg[7]_i_1_n_2 ;
  wire \cnt_reg[7]_i_1_n_3 ;
  wire \cnt_reg[7]_i_1_n_4 ;
  wire \cnt_reg[7]_i_1_n_5 ;
  wire \cnt_reg[7]_i_1_n_6 ;
  wire \cnt_reg[7]_i_1_n_7 ;
  wire [15:0]count;
  wire reset;
  wire [3:0]NLW_cnt0_carry_O_UNCONNECTED;
  wire [3:2]NLW_cnt0_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_cnt0_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_cnt_reg[15]_i_2_CO_UNCONNECTED ;

  CARRY4 cnt0_carry
       (.CI(1'b0),
        .CO({cnt0_carry_n_0,cnt0_carry_n_1,cnt0_carry_n_2,cnt0_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_cnt0_carry_O_UNCONNECTED[3:0]),
        .S({cnt0_carry_i_1_n_0,cnt0_carry_i_2_n_0,cnt0_carry_i_3_n_0,cnt0_carry_i_4_n_0}));
  CARRY4 cnt0_carry__0
       (.CI(cnt0_carry_n_0),
        .CO({NLW_cnt0_carry__0_CO_UNCONNECTED[3:2],cnt0_carry__0_n_2,cnt0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_cnt0_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,cnt0_carry__0_i_1_n_0,cnt0_carry__0_i_2_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    cnt0_carry__0_i_1
       (.I0(FrameSize[15]),
        .I1(count[15]),
        .O(cnt0_carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt0_carry__0_i_2
       (.I0(count[12]),
        .I1(FrameSize[12]),
        .I2(FrameSize[14]),
        .I3(count[14]),
        .I4(FrameSize[13]),
        .I5(count[13]),
        .O(cnt0_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt0_carry_i_1
       (.I0(count[9]),
        .I1(FrameSize[9]),
        .I2(FrameSize[11]),
        .I3(count[11]),
        .I4(FrameSize[10]),
        .I5(count[10]),
        .O(cnt0_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt0_carry_i_2
       (.I0(count[6]),
        .I1(FrameSize[6]),
        .I2(FrameSize[8]),
        .I3(count[8]),
        .I4(FrameSize[7]),
        .I5(count[7]),
        .O(cnt0_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt0_carry_i_3
       (.I0(count[3]),
        .I1(FrameSize[3]),
        .I2(FrameSize[5]),
        .I3(count[5]),
        .I4(FrameSize[4]),
        .I5(count[4]),
        .O(cnt0_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt0_carry_i_4
       (.I0(count[0]),
        .I1(FrameSize[0]),
        .I2(FrameSize[2]),
        .I3(count[2]),
        .I4(FrameSize[1]),
        .I5(count[1]),
        .O(cnt0_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    \cnt[15]_i_1 
       (.I0(cnt0_carry__0_n_2),
        .I1(reset),
        .O(\cnt[15]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt[3]_i_2 
       (.I0(count[0]),
        .O(\cnt[3]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[3]_i_1_n_7 ),
        .Q(count[0]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[11]_i_1_n_5 ),
        .Q(count[10]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[11]_i_1_n_4 ),
        .Q(count[11]),
        .R(\cnt[15]_i_1_n_0 ));
  CARRY4 \cnt_reg[11]_i_1 
       (.CI(\cnt_reg[7]_i_1_n_0 ),
        .CO({\cnt_reg[11]_i_1_n_0 ,\cnt_reg[11]_i_1_n_1 ,\cnt_reg[11]_i_1_n_2 ,\cnt_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[11]_i_1_n_4 ,\cnt_reg[11]_i_1_n_5 ,\cnt_reg[11]_i_1_n_6 ,\cnt_reg[11]_i_1_n_7 }),
        .S(count[11:8]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[15]_i_2_n_7 ),
        .Q(count[12]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[15]_i_2_n_6 ),
        .Q(count[13]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[15]_i_2_n_5 ),
        .Q(count[14]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[15]_i_2_n_4 ),
        .Q(count[15]),
        .R(\cnt[15]_i_1_n_0 ));
  CARRY4 \cnt_reg[15]_i_2 
       (.CI(\cnt_reg[11]_i_1_n_0 ),
        .CO({\NLW_cnt_reg[15]_i_2_CO_UNCONNECTED [3],\cnt_reg[15]_i_2_n_1 ,\cnt_reg[15]_i_2_n_2 ,\cnt_reg[15]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[15]_i_2_n_4 ,\cnt_reg[15]_i_2_n_5 ,\cnt_reg[15]_i_2_n_6 ,\cnt_reg[15]_i_2_n_7 }),
        .S(count[15:12]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[3]_i_1_n_6 ),
        .Q(count[1]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[3]_i_1_n_5 ),
        .Q(count[2]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[3]_i_1_n_4 ),
        .Q(count[3]),
        .R(\cnt[15]_i_1_n_0 ));
  CARRY4 \cnt_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\cnt_reg[3]_i_1_n_0 ,\cnt_reg[3]_i_1_n_1 ,\cnt_reg[3]_i_1_n_2 ,\cnt_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_reg[3]_i_1_n_4 ,\cnt_reg[3]_i_1_n_5 ,\cnt_reg[3]_i_1_n_6 ,\cnt_reg[3]_i_1_n_7 }),
        .S({count[3:1],\cnt[3]_i_2_n_0 }));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[7]_i_1_n_7 ),
        .Q(count[4]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[7]_i_1_n_6 ),
        .Q(count[5]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[7]_i_1_n_5 ),
        .Q(count[6]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[7]_i_1_n_4 ),
        .Q(count[7]),
        .R(\cnt[15]_i_1_n_0 ));
  CARRY4 \cnt_reg[7]_i_1 
       (.CI(\cnt_reg[3]_i_1_n_0 ),
        .CO({\cnt_reg[7]_i_1_n_0 ,\cnt_reg[7]_i_1_n_1 ,\cnt_reg[7]_i_1_n_2 ,\cnt_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[7]_i_1_n_4 ,\cnt_reg[7]_i_1_n_5 ,\cnt_reg[7]_i_1_n_6 ,\cnt_reg[7]_i_1_n_7 }),
        .S(count[7:4]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[11]_i_1_n_7 ),
        .Q(count[8]),
        .R(\cnt[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[11]_i_1_n_6 ),
        .Q(count[9]),
        .R(\cnt[15]_i_1_n_0 ));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_counter_0_0,counter,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "counter,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk,
    reset,
    FrameSize,
    ready,
    count,
    div_clk);
  input clk;
  input reset;
  input [15:0]FrameSize;
  output ready;
  output [15:0]count;
  output div_clk;

  wire [15:0]FrameSize;
  wire clk;
  wire [15:0]\^count ;
  wire div_clk;
  wire reset;

  assign count[15:14] = \^count [15:14];
  assign count[13] = div_clk;
  assign count[12:0] = \^count [12:0];
  assign ready = clk;
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_counter inst
       (.FrameSize(FrameSize),
        .clk(clk),
        .count({\^count [15:14],div_clk,\^count [12:0]}),
        .reset(reset));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
