// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Mon Sep 28 14:07:38 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_VETAL_1_0_sim_netlist.v
// Design      : design_1_ADC_VETAL_1_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_VETAL
   (adc_clkx_p,
    adc_clkx_n,
    adc_clk_ILA,
    adc_dax_p_0,
    adc_dbx_p_0,
    adc_dco_ILA,
    clk_200_0,
    adc_data,
    adc_dax_p,
    adc_dax_n,
    adc_dbx_p,
    adc_dbx_n,
    adc_dcox_p,
    adc_dcox_n,
    clk_200,
    fpga_clk,
    clk_100MHz);
  output adc_clkx_p;
  output adc_clkx_n;
  output adc_clk_ILA;
  output adc_dax_p_0;
  output adc_dbx_p_0;
  output adc_dco_ILA;
  output clk_200_0;
  output [15:0]adc_data;
  input adc_dax_p;
  input adc_dax_n;
  input adc_dbx_p;
  input adc_dbx_n;
  input adc_dcox_p;
  input adc_dcox_n;
  input clk_200;
  input fpga_clk;
  input clk_100MHz;

  wire adc_clk_ILA;
  wire adc_clkx_n;
  wire adc_clkx_p;
  wire [15:0]adc_data;
  wire \adc_data_reg[15]_i_1_n_0 ;
  wire [15:0]adc_data_temp;
  wire adc_dax_n;
  wire adc_dax_p;
  wire adc_dax_p_0;
  wire adc_dbx_n;
  wire adc_dbx_p;
  wire adc_dbx_p_0;
  wire adc_dco_ILA;
  wire adc_dcox_n;
  wire adc_dcox_p;
  wire adc_reading_allowed_reg;
  wire adc_reading_allowed_reg_i_1_n_0;
  wire clk_100MHz;
  wire clk_200;
  wire clk_200_0;
  wire data_0_i_1_n_0;
  wire data_10_i_1_n_0;
  wire data_11_i_1_n_0;
  wire data_12_i_1_n_0;
  wire data_13_i_1_n_0;
  wire data_14_i_1_n_0;
  wire data_15_i_1_n_0;
  wire data_1_i_1_n_0;
  wire data_2_i_1_n_0;
  wire data_3_i_1_n_0;
  wire data_4_i_1_n_0;
  wire data_5_i_1_n_0;
  wire data_6_i_1_n_0;
  wire data_7_i_1_n_0;
  wire data_8_i_1_n_0;
  wire data_9_i_1_n_0;
  wire \dco_neg_num[0]_i_1_n_0 ;
  wire \dco_neg_num[1]_i_1_n_0 ;
  wire \dco_neg_num_reg_n_0_[0] ;
  wire \dco_neg_num_reg_n_0_[1] ;
  wire \dco_pos_num[0]_i_1_n_0 ;
  wire \dco_pos_num[1]_i_1_n_0 ;
  wire \dco_pos_num_reg_n_0_[0] ;
  wire \dco_pos_num_reg_n_0_[1] ;
  wire even_odd_frame;
  wire even_odd_frame_i_1_n_0;
  wire even_odd_frame_prev_neg;
  wire even_odd_frame_prev_pos;
  wire fpga_clk;
  wire fpga_clk_prev;
  wire [3:1]p_0_in;
  wire tconv_cnt_reg;
  wire \tconv_cnt_reg[0]_i_1_n_0 ;
  wire \tconv_cnt_reg[2]_i_1_n_0 ;
  wire [3:0]tconv_cnt_reg_reg;
  wire NLW_IDELAYCTRL_inst_RDY_UNCONNECTED;
  wire NLW_IDELAYE2_adc_if2_ch1_dco_IDATAIN_UNCONNECTED;
  wire [4:0]NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEIN_UNCONNECTED;
  wire [4:0]NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEOUT_UNCONNECTED;

  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_da
       (.I(adc_dax_p),
        .IB(adc_dax_n),
        .O(adc_dax_p_0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_db
       (.I(adc_dbx_p),
        .IB(adc_dbx_n),
        .O(adc_dbx_p_0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* IBUF_DELAY_VALUE = "0" *) 
  (* IFD_DELAY_VALUE = "AUTO" *) 
  IBUFDS IBUFDS_adc_dco
       (.I(adc_dcox_p),
        .IB(adc_dcox_n),
        .O(adc_dco_ILA));
  (* BOX_TYPE = "PRIMITIVE" *) 
  IDELAYCTRL #(
    .SIM_DEVICE("7SERIES")) 
    IDELAYCTRL_inst
       (.RDY(NLW_IDELAYCTRL_inst_RDY_UNCONNECTED),
        .REFCLK(clk_200),
        .RST(1'b0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* SIM_DELAY_D = "0" *) 
  IDELAYE2 #(
    .CINVCTRL_SEL("FALSE"),
    .DELAY_SRC("DATAIN"),
    .HIGH_PERFORMANCE_MODE("TRUE"),
    .IDELAY_TYPE("FIXED"),
    .IDELAY_VALUE(8),
    .IS_C_INVERTED(1'b0),
    .IS_DATAIN_INVERTED(1'b0),
    .IS_IDATAIN_INVERTED(1'b0),
    .PIPE_SEL("FALSE"),
    .REFCLK_FREQUENCY(200.000000),
    .SIGNAL_PATTERN("CLOCK")) 
    IDELAYE2_adc_if2_ch1_dco
       (.C(clk_200),
        .CE(1'b0),
        .CINVCTRL(1'b0),
        .CNTVALUEIN(NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEIN_UNCONNECTED[4:0]),
        .CNTVALUEOUT(NLW_IDELAYE2_adc_if2_ch1_dco_CNTVALUEOUT_UNCONNECTED[4:0]),
        .DATAIN(adc_dco_ILA),
        .DATAOUT(clk_200_0),
        .IDATAIN(NLW_IDELAYE2_adc_if2_ch1_dco_IDATAIN_UNCONNECTED),
        .INC(1'b0),
        .LD(1'b0),
        .LDPIPEEN(1'b0),
        .REGRST(1'b0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  (* XILINX_LEGACY_PRIM = "OBUFDS" *) 
  OBUFDS #(
    .IOSTANDARD("DEFAULT")) 
    OBUFDS_adc_clk
       (.I(adc_clk_ILA),
        .O(adc_clkx_p),
        .OB(adc_clkx_n));
  LUT2 #(
    .INIT(4'h8)) 
    adc_clk_ILA_INST_0
       (.I0(clk_100MHz),
        .I1(adc_reading_allowed_reg),
        .O(adc_clk_ILA));
  LUT2 #(
    .INIT(4'h8)) 
    \adc_data_reg[15]_i_1 
       (.I0(\dco_neg_num_reg_n_0_[1] ),
        .I1(\dco_neg_num_reg_n_0_[0] ),
        .O(\adc_data_reg[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[0] 
       (.C(clk_200_0),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[0]),
        .Q(adc_data[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[10] 
       (.C(clk_200_0),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[10]),
        .Q(adc_data[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[11] 
       (.C(clk_200_0),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[11]),
        .Q(adc_data[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[12] 
       (.C(clk_200_0),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[12]),
        .Q(adc_data[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[13] 
       (.C(clk_200_0),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[13]),
        .Q(adc_data[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[14] 
       (.C(clk_200_0),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[14]),
        .Q(adc_data[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[15] 
       (.C(clk_200_0),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[15]),
        .Q(adc_data[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[1] 
       (.C(clk_200_0),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[1]),
        .Q(adc_data[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[2] 
       (.C(clk_200_0),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[2]),
        .Q(adc_data[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[3] 
       (.C(clk_200_0),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[3]),
        .Q(adc_data[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[4] 
       (.C(clk_200_0),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[4]),
        .Q(adc_data[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[5] 
       (.C(clk_200_0),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[5]),
        .Q(adc_data[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[6] 
       (.C(clk_200_0),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[6]),
        .Q(adc_data[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[7] 
       (.C(clk_200_0),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[7]),
        .Q(adc_data[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[8] 
       (.C(clk_200_0),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[8]),
        .Q(adc_data[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \adc_data_reg_reg[9] 
       (.C(clk_200_0),
        .CE(\adc_data_reg[15]_i_1_n_0 ),
        .D(adc_data_temp[9]),
        .Q(adc_data[9]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hAAEAAA8A)) 
    adc_reading_allowed_reg_i_1
       (.I0(adc_reading_allowed_reg),
        .I1(tconv_cnt_reg_reg[1]),
        .I2(tconv_cnt_reg_reg[0]),
        .I3(tconv_cnt_reg_reg[3]),
        .I4(tconv_cnt_reg_reg[2]),
        .O(adc_reading_allowed_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    adc_reading_allowed_reg_reg
       (.C(clk_100MHz),
        .CE(1'b1),
        .D(adc_reading_allowed_reg_i_1_n_0),
        .Q(adc_reading_allowed_reg),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hBF80)) 
    data_0_i_1
       (.I0(adc_dbx_p_0),
        .I1(\dco_pos_num_reg_n_0_[0] ),
        .I2(\dco_pos_num_reg_n_0_[1] ),
        .I3(adc_data_temp[0]),
        .O(data_0_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_0_reg
       (.C(clk_200_0),
        .CE(1'b1),
        .D(data_0_i_1_n_0),
        .Q(adc_data_temp[0]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFEFEFF00020200)) 
    data_10_i_1
       (.I0(adc_dbx_p_0),
        .I1(\dco_neg_num_reg_n_0_[0] ),
        .I2(\dco_neg_num_reg_n_0_[1] ),
        .I3(even_odd_frame_prev_neg),
        .I4(even_odd_frame),
        .I5(adc_data_temp[10]),
        .O(data_10_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_10_reg
       (.C(clk_200_0),
        .CE(1'b1),
        .D(data_10_i_1_n_0),
        .Q(adc_data_temp[10]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFEFEFF00020200)) 
    data_11_i_1
       (.I0(adc_dax_p_0),
        .I1(\dco_neg_num_reg_n_0_[0] ),
        .I2(\dco_neg_num_reg_n_0_[1] ),
        .I3(even_odd_frame_prev_neg),
        .I4(even_odd_frame),
        .I5(adc_data_temp[11]),
        .O(data_11_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_11_reg
       (.C(clk_200_0),
        .CE(1'b1),
        .D(data_11_i_1_n_0),
        .Q(adc_data_temp[11]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFEFEFF00020200)) 
    data_12_i_1
       (.I0(adc_dbx_p_0),
        .I1(\dco_pos_num_reg_n_0_[0] ),
        .I2(\dco_pos_num_reg_n_0_[1] ),
        .I3(even_odd_frame_prev_pos),
        .I4(even_odd_frame),
        .I5(adc_data_temp[12]),
        .O(data_12_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_12_reg
       (.C(clk_200_0),
        .CE(1'b1),
        .D(data_12_i_1_n_0),
        .Q(adc_data_temp[12]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFEFEFF00020200)) 
    data_13_i_1
       (.I0(adc_dax_p_0),
        .I1(\dco_pos_num_reg_n_0_[0] ),
        .I2(\dco_pos_num_reg_n_0_[1] ),
        .I3(even_odd_frame_prev_pos),
        .I4(even_odd_frame),
        .I5(adc_data_temp[13]),
        .O(data_13_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_13_reg
       (.C(clk_200_0),
        .CE(1'b1),
        .D(data_13_i_1_n_0),
        .Q(adc_data_temp[13]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFBFFFFFF08000000)) 
    data_14_i_1
       (.I0(adc_dbx_p_0),
        .I1(tconv_cnt_reg_reg[2]),
        .I2(tconv_cnt_reg_reg[3]),
        .I3(tconv_cnt_reg_reg[0]),
        .I4(tconv_cnt_reg_reg[1]),
        .I5(adc_data_temp[14]),
        .O(data_14_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_14_reg
       (.C(clk_100MHz),
        .CE(1'b1),
        .D(data_14_i_1_n_0),
        .Q(adc_data_temp[14]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFBFFFFFF08000000)) 
    data_15_i_1
       (.I0(adc_dax_p_0),
        .I1(tconv_cnt_reg_reg[2]),
        .I2(tconv_cnt_reg_reg[3]),
        .I3(tconv_cnt_reg_reg[0]),
        .I4(tconv_cnt_reg_reg[1]),
        .I5(adc_data_temp[15]),
        .O(data_15_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_15_reg
       (.C(clk_100MHz),
        .CE(1'b1),
        .D(data_15_i_1_n_0),
        .Q(adc_data_temp[15]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hBF80)) 
    data_1_i_1
       (.I0(adc_dax_p_0),
        .I1(\dco_pos_num_reg_n_0_[0] ),
        .I2(\dco_pos_num_reg_n_0_[1] ),
        .I3(adc_data_temp[1]),
        .O(data_1_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_1_reg
       (.C(clk_200_0),
        .CE(1'b1),
        .D(data_1_i_1_n_0),
        .Q(adc_data_temp[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    data_2_i_1
       (.I0(adc_dbx_p_0),
        .I1(\dco_neg_num_reg_n_0_[1] ),
        .I2(\dco_neg_num_reg_n_0_[0] ),
        .I3(adc_data_temp[2]),
        .O(data_2_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_2_reg
       (.C(clk_200_0),
        .CE(1'b1),
        .D(data_2_i_1_n_0),
        .Q(adc_data_temp[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    data_3_i_1
       (.I0(adc_dax_p_0),
        .I1(\dco_neg_num_reg_n_0_[1] ),
        .I2(\dco_neg_num_reg_n_0_[0] ),
        .I3(adc_data_temp[3]),
        .O(data_3_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_3_reg
       (.C(clk_200_0),
        .CE(1'b1),
        .D(data_3_i_1_n_0),
        .Q(adc_data_temp[3]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    data_4_i_1
       (.I0(adc_dbx_p_0),
        .I1(\dco_pos_num_reg_n_0_[1] ),
        .I2(\dco_pos_num_reg_n_0_[0] ),
        .I3(adc_data_temp[4]),
        .O(data_4_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_4_reg
       (.C(clk_200_0),
        .CE(1'b1),
        .D(data_4_i_1_n_0),
        .Q(adc_data_temp[4]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    data_5_i_1
       (.I0(adc_dax_p_0),
        .I1(\dco_pos_num_reg_n_0_[1] ),
        .I2(\dco_pos_num_reg_n_0_[0] ),
        .I3(adc_data_temp[5]),
        .O(data_5_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_5_reg
       (.C(clk_200_0),
        .CE(1'b1),
        .D(data_5_i_1_n_0),
        .Q(adc_data_temp[5]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hEF20)) 
    data_6_i_1
       (.I0(adc_dbx_p_0),
        .I1(\dco_neg_num_reg_n_0_[1] ),
        .I2(\dco_neg_num_reg_n_0_[0] ),
        .I3(adc_data_temp[6]),
        .O(data_6_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_6_reg
       (.C(clk_200_0),
        .CE(1'b1),
        .D(data_6_i_1_n_0),
        .Q(adc_data_temp[6]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hEF20)) 
    data_7_i_1
       (.I0(adc_dax_p_0),
        .I1(\dco_neg_num_reg_n_0_[1] ),
        .I2(\dco_neg_num_reg_n_0_[0] ),
        .I3(adc_data_temp[7]),
        .O(data_7_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    data_7_reg
       (.C(clk_200_0),
        .CE(1'b1),
        .D(data_7_i_1_n_0),
        .Q(adc_data_temp[7]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    data_8_i_1
       (.I0(adc_dbx_p_0),
        .I1(\dco_pos_num_reg_n_0_[0] ),
        .I2(\dco_pos_num_reg_n_0_[1] ),
        .I3(adc_data_temp[8]),
        .O(data_8_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_8_reg
       (.C(clk_200_0),
        .CE(1'b1),
        .D(data_8_i_1_n_0),
        .Q(adc_data_temp[8]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    data_9_i_1
       (.I0(adc_dax_p_0),
        .I1(\dco_pos_num_reg_n_0_[0] ),
        .I2(\dco_pos_num_reg_n_0_[1] ),
        .I3(adc_data_temp[9]),
        .O(data_9_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_9_reg
       (.C(clk_200_0),
        .CE(1'b1),
        .D(data_9_i_1_n_0),
        .Q(adc_data_temp[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h4FF4)) 
    \dco_neg_num[0]_i_1 
       (.I0(\dco_neg_num_reg_n_0_[0] ),
        .I1(\dco_neg_num_reg_n_0_[1] ),
        .I2(even_odd_frame_prev_neg),
        .I3(even_odd_frame),
        .O(\dco_neg_num[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h0990)) 
    \dco_neg_num[1]_i_1 
       (.I0(even_odd_frame),
        .I1(even_odd_frame_prev_neg),
        .I2(\dco_neg_num_reg_n_0_[0] ),
        .I3(\dco_neg_num_reg_n_0_[1] ),
        .O(\dco_neg_num[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \dco_neg_num_reg[0] 
       (.C(clk_200_0),
        .CE(1'b1),
        .D(\dco_neg_num[0]_i_1_n_0 ),
        .Q(\dco_neg_num_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \dco_neg_num_reg[1] 
       (.C(clk_200_0),
        .CE(1'b1),
        .D(\dco_neg_num[1]_i_1_n_0 ),
        .Q(\dco_neg_num_reg_n_0_[1] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h4FF4)) 
    \dco_pos_num[0]_i_1 
       (.I0(\dco_pos_num_reg_n_0_[0] ),
        .I1(\dco_pos_num_reg_n_0_[1] ),
        .I2(even_odd_frame_prev_pos),
        .I3(even_odd_frame),
        .O(\dco_pos_num[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h0990)) 
    \dco_pos_num[1]_i_1 
       (.I0(even_odd_frame),
        .I1(even_odd_frame_prev_pos),
        .I2(\dco_pos_num_reg_n_0_[0] ),
        .I3(\dco_pos_num_reg_n_0_[1] ),
        .O(\dco_pos_num[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \dco_pos_num_reg[0] 
       (.C(clk_200_0),
        .CE(1'b1),
        .D(\dco_pos_num[0]_i_1_n_0 ),
        .Q(\dco_pos_num_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \dco_pos_num_reg[1] 
       (.C(clk_200_0),
        .CE(1'b1),
        .D(\dco_pos_num[1]_i_1_n_0 ),
        .Q(\dco_pos_num_reg_n_0_[1] ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFB0004)) 
    even_odd_frame_i_1
       (.I0(tconv_cnt_reg_reg[1]),
        .I1(tconv_cnt_reg_reg[2]),
        .I2(tconv_cnt_reg_reg[0]),
        .I3(tconv_cnt_reg_reg[3]),
        .I4(even_odd_frame),
        .O(even_odd_frame_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    even_odd_frame_prev_neg_reg
       (.C(clk_200_0),
        .CE(1'b1),
        .D(even_odd_frame),
        .Q(even_odd_frame_prev_neg),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    even_odd_frame_prev_pos_reg
       (.C(clk_200_0),
        .CE(1'b1),
        .D(even_odd_frame),
        .Q(even_odd_frame_prev_pos),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    even_odd_frame_reg
       (.C(clk_100MHz),
        .CE(1'b1),
        .D(even_odd_frame_i_1_n_0),
        .Q(even_odd_frame),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    fpga_clk_prev_reg
       (.C(clk_100MHz),
        .CE(1'b1),
        .D(fpga_clk),
        .Q(fpga_clk_prev),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \tconv_cnt_reg[0]_i_1 
       (.I0(tconv_cnt_reg_reg[0]),
        .O(\tconv_cnt_reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \tconv_cnt_reg[1]_i_1 
       (.I0(tconv_cnt_reg_reg[0]),
        .I1(tconv_cnt_reg_reg[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \tconv_cnt_reg[2]_i_1 
       (.I0(tconv_cnt_reg_reg[0]),
        .I1(tconv_cnt_reg_reg[1]),
        .I2(tconv_cnt_reg_reg[2]),
        .O(\tconv_cnt_reg[2]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \tconv_cnt_reg[3]_i_1 
       (.I0(fpga_clk),
        .I1(fpga_clk_prev),
        .O(tconv_cnt_reg));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \tconv_cnt_reg[3]_i_2 
       (.I0(tconv_cnt_reg_reg[1]),
        .I1(tconv_cnt_reg_reg[0]),
        .I2(tconv_cnt_reg_reg[2]),
        .I3(tconv_cnt_reg_reg[3]),
        .O(p_0_in[3]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[0] 
       (.C(clk_100MHz),
        .CE(1'b1),
        .D(\tconv_cnt_reg[0]_i_1_n_0 ),
        .Q(tconv_cnt_reg_reg[0]),
        .R(tconv_cnt_reg));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[1] 
       (.C(clk_100MHz),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(tconv_cnt_reg_reg[1]),
        .R(tconv_cnt_reg));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[2] 
       (.C(clk_100MHz),
        .CE(1'b1),
        .D(\tconv_cnt_reg[2]_i_1_n_0 ),
        .Q(tconv_cnt_reg_reg[2]),
        .R(tconv_cnt_reg));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \tconv_cnt_reg_reg[3] 
       (.C(clk_100MHz),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(tconv_cnt_reg_reg[3]),
        .R(tconv_cnt_reg));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_ADC_VETAL_1_0,ADC_VETAL,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "ADC_VETAL,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (fpga_clk,
    clk_200,
    clk_100MHz,
    fpga_clk_inv,
    adc_data,
    adc_dax_p,
    adc_dax_n,
    adc_dbx_p,
    adc_dbx_n,
    adc_dcox_p,
    adc_dcox_n,
    adc_clkx_p,
    adc_clkx_n,
    adc_dco_ILA,
    adc_clk_ILA,
    adc_da_ILA,
    adc_db_ILA,
    adc_dco_delayed_ILA);
  input fpga_clk;
  input clk_200;
  input clk_100MHz;
  output fpga_clk_inv;
  output [15:0]adc_data;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dax p" *) input adc_dax_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dax n" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_dax, SV_INTERFACE true" *) input adc_dax_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dbx p" *) input adc_dbx_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dbx n" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_dbx, SV_INTERFACE true" *) input adc_dbx_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dcox p" *) input adc_dcox_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_dcox n" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_dcox, SV_INTERFACE true" *) input adc_dcox_n;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_clkx p" *) output adc_clkx_p;
  (* X_INTERFACE_INFO = "bt.local:interface:diff:1.0 adc_clkx n" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME adc_clkx, SV_INTERFACE true" *) output adc_clkx_n;
  output adc_dco_ILA;
  output adc_clk_ILA;
  output adc_da_ILA;
  output adc_db_ILA;
  output adc_dco_delayed_ILA;

  (* SLEW = "SLOW" *) wire adc_clk_ILA;
  (* SLEW = "SLOW" *) wire adc_clkx_n;
  (* SLEW = "SLOW" *) wire adc_clkx_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_da_ILA;
  wire [15:0]adc_data;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dax_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dax_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_db_ILA;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dbx_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dbx_p;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dco_ILA;
  wire adc_dco_delayed_ILA;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dcox_n;
  (* DIFF_TERM *) (* IBUF_LOW_PWR *) (* IOSTANDARD = "LVDS_25" *) wire adc_dcox_p;
  wire clk_100MHz;
  wire clk_200;
  wire fpga_clk;
  wire fpga_clk_inv;

  LUT1 #(
    .INIT(2'h1)) 
    fpga_clk_inv_INST_0
       (.I0(fpga_clk),
        .O(fpga_clk_inv));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_VETAL inst
       (.adc_clk_ILA(adc_clk_ILA),
        .adc_clkx_n(adc_clkx_n),
        .adc_clkx_p(adc_clkx_p),
        .adc_data(adc_data),
        .adc_dax_n(adc_dax_n),
        .adc_dax_p(adc_dax_p),
        .adc_dax_p_0(adc_da_ILA),
        .adc_dbx_n(adc_dbx_n),
        .adc_dbx_p(adc_dbx_p),
        .adc_dbx_p_0(adc_db_ILA),
        .adc_dco_ILA(adc_dco_ILA),
        .adc_dcox_n(adc_dcox_n),
        .adc_dcox_p(adc_dcox_p),
        .clk_100MHz(clk_100MHz),
        .clk_200(clk_200),
        .clk_200_0(adc_dco_delayed_ILA),
        .fpga_clk(fpga_clk));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
