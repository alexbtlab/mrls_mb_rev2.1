-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Mon Sep 14 16:53:44 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_LTC2318_16_1_0_stub.vhdl
-- Design      : design_1_LTC2318_16_1_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    clk_data_out : out STD_LOGIC;
    clk_data_out_inv : out STD_LOGIC;
    ADC_DATA_OUT : out STD_LOGIC_VECTOR ( 15 downto 0 );
    adc_dax_p : in STD_LOGIC;
    adc_dax_n : in STD_LOGIC;
    adc_dbx_p : in STD_LOGIC;
    adc_dbx_n : in STD_LOGIC;
    fpga_clk : in STD_LOGIC;
    clk_100 : in STD_LOGIC;
    adc_clkx_p : out STD_LOGIC;
    adc_clkx_n : out STD_LOGIC;
    adc_clk_inv_ila : out STD_LOGIC;
    adc_da_ila : out STD_LOGIC;
    adc_db_ila : out STD_LOGIC;
    adc_clk_ila : out STD_LOGIC;
    clk_200 : in STD_LOGIC;
    adc_clk_ila_delayed : out STD_LOGIC;
    cnt100_ila : out STD_LOGIC_VECTOR ( 2 downto 0 );
    cnt_pos_dco_ila : out STD_LOGIC_VECTOR ( 1 downto 0 );
    cnt_neg_dco_ila : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_data_out,clk_data_out_inv,ADC_DATA_OUT[15:0],adc_dax_p,adc_dax_n,adc_dbx_p,adc_dbx_n,fpga_clk,clk_100,adc_clkx_p,adc_clkx_n,adc_clk_inv_ila,adc_da_ila,adc_db_ila,adc_clk_ila,clk_200,adc_clk_ila_delayed,cnt100_ila[2:0],cnt_pos_dco_ila[1:0],cnt_neg_dco_ila[1:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "LTC2387_16,Vivado 2019.1";
begin
end;
