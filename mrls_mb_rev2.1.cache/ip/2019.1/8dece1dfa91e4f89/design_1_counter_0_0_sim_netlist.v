// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Tue Oct 13 12:32:12 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_counter_0_0_sim_netlist.v
// Design      : design_1_counter_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_counter
   (count,
    FrameSize,
    reset,
    clk);
  output [15:0]count;
  input [15:0]FrameSize;
  input reset;
  input clk;

  wire [15:0]FrameSize;
  wire clk;
  wire cnt;
  wire [15:1]cnt0;
  wire [15:1]cnt00_in;
  wire cnt0_carry__0_i_1_n_0;
  wire cnt0_carry__0_i_2_n_0;
  wire cnt0_carry__0_i_3_n_0;
  wire cnt0_carry__0_i_4_n_0;
  wire cnt0_carry__0_n_0;
  wire cnt0_carry__0_n_1;
  wire cnt0_carry__0_n_2;
  wire cnt0_carry__0_n_3;
  wire cnt0_carry__1_i_1_n_0;
  wire cnt0_carry__1_i_2_n_0;
  wire cnt0_carry__1_i_3_n_0;
  wire cnt0_carry__1_i_4_n_0;
  wire cnt0_carry__1_n_0;
  wire cnt0_carry__1_n_1;
  wire cnt0_carry__1_n_2;
  wire cnt0_carry__1_n_3;
  wire cnt0_carry__2_i_1_n_0;
  wire cnt0_carry__2_i_2_n_0;
  wire cnt0_carry__2_i_3_n_0;
  wire cnt0_carry__2_n_2;
  wire cnt0_carry__2_n_3;
  wire cnt0_carry_i_1_n_0;
  wire cnt0_carry_i_2_n_0;
  wire cnt0_carry_i_3_n_0;
  wire cnt0_carry_i_4_n_0;
  wire cnt0_carry_n_0;
  wire cnt0_carry_n_1;
  wire cnt0_carry_n_2;
  wire cnt0_carry_n_3;
  wire cnt1;
  wire cnt1_carry__0_i_1_n_0;
  wire cnt1_carry__0_i_2_n_0;
  wire cnt1_carry__0_n_3;
  wire cnt1_carry_i_1_n_0;
  wire cnt1_carry_i_2_n_0;
  wire cnt1_carry_i_3_n_0;
  wire cnt1_carry_i_4_n_0;
  wire cnt1_carry_n_0;
  wire cnt1_carry_n_1;
  wire cnt1_carry_n_2;
  wire cnt1_carry_n_3;
  wire \cnt[0]_i_1_n_0 ;
  wire \cnt[10]_i_1_n_0 ;
  wire \cnt[11]_i_1_n_0 ;
  wire \cnt[12]_i_1_n_0 ;
  wire \cnt[13]_i_1_n_0 ;
  wire \cnt[14]_i_1_n_0 ;
  wire \cnt[15]_i_2_n_0 ;
  wire \cnt[1]_i_1_n_0 ;
  wire \cnt[2]_i_1_n_0 ;
  wire \cnt[3]_i_1_n_0 ;
  wire \cnt[4]_i_1_n_0 ;
  wire \cnt[5]_i_1_n_0 ;
  wire \cnt[6]_i_1_n_0 ;
  wire \cnt[7]_i_1_n_0 ;
  wire \cnt[8]_i_1_n_0 ;
  wire \cnt[9]_i_1_n_0 ;
  wire \cnt_reg[12]_i_2_n_0 ;
  wire \cnt_reg[12]_i_2_n_1 ;
  wire \cnt_reg[12]_i_2_n_2 ;
  wire \cnt_reg[12]_i_2_n_3 ;
  wire \cnt_reg[15]_i_3_n_2 ;
  wire \cnt_reg[15]_i_3_n_3 ;
  wire \cnt_reg[4]_i_2_n_0 ;
  wire \cnt_reg[4]_i_2_n_1 ;
  wire \cnt_reg[4]_i_2_n_2 ;
  wire \cnt_reg[4]_i_2_n_3 ;
  wire \cnt_reg[8]_i_2_n_0 ;
  wire \cnt_reg[8]_i_2_n_1 ;
  wire \cnt_reg[8]_i_2_n_2 ;
  wire \cnt_reg[8]_i_2_n_3 ;
  wire [15:0]count;
  wire direct;
  wire p_0_in;
  wire reset;
  wire [3:2]NLW_cnt0_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_cnt0_carry__2_O_UNCONNECTED;
  wire [3:0]NLW_cnt1_carry_O_UNCONNECTED;
  wire [3:2]NLW_cnt1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_cnt1_carry__0_O_UNCONNECTED;
  wire [3:2]\NLW_cnt_reg[15]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_cnt_reg[15]_i_3_O_UNCONNECTED ;

  CARRY4 cnt0_carry
       (.CI(1'b0),
        .CO({cnt0_carry_n_0,cnt0_carry_n_1,cnt0_carry_n_2,cnt0_carry_n_3}),
        .CYINIT(count[0]),
        .DI(count[4:1]),
        .O(cnt0[4:1]),
        .S({cnt0_carry_i_1_n_0,cnt0_carry_i_2_n_0,cnt0_carry_i_3_n_0,cnt0_carry_i_4_n_0}));
  CARRY4 cnt0_carry__0
       (.CI(cnt0_carry_n_0),
        .CO({cnt0_carry__0_n_0,cnt0_carry__0_n_1,cnt0_carry__0_n_2,cnt0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(count[8:5]),
        .O(cnt0[8:5]),
        .S({cnt0_carry__0_i_1_n_0,cnt0_carry__0_i_2_n_0,cnt0_carry__0_i_3_n_0,cnt0_carry__0_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__0_i_1
       (.I0(count[8]),
        .O(cnt0_carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__0_i_2
       (.I0(count[7]),
        .O(cnt0_carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__0_i_3
       (.I0(count[6]),
        .O(cnt0_carry__0_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__0_i_4
       (.I0(count[5]),
        .O(cnt0_carry__0_i_4_n_0));
  CARRY4 cnt0_carry__1
       (.CI(cnt0_carry__0_n_0),
        .CO({cnt0_carry__1_n_0,cnt0_carry__1_n_1,cnt0_carry__1_n_2,cnt0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(count[12:9]),
        .O(cnt0[12:9]),
        .S({cnt0_carry__1_i_1_n_0,cnt0_carry__1_i_2_n_0,cnt0_carry__1_i_3_n_0,cnt0_carry__1_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__1_i_1
       (.I0(count[12]),
        .O(cnt0_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__1_i_2
       (.I0(count[11]),
        .O(cnt0_carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__1_i_3
       (.I0(count[10]),
        .O(cnt0_carry__1_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__1_i_4
       (.I0(count[9]),
        .O(cnt0_carry__1_i_4_n_0));
  CARRY4 cnt0_carry__2
       (.CI(cnt0_carry__1_n_0),
        .CO({NLW_cnt0_carry__2_CO_UNCONNECTED[3:2],cnt0_carry__2_n_2,cnt0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,count[14:13]}),
        .O({NLW_cnt0_carry__2_O_UNCONNECTED[3],cnt0[15:13]}),
        .S({1'b0,cnt0_carry__2_i_1_n_0,cnt0_carry__2_i_2_n_0,cnt0_carry__2_i_3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__2_i_1
       (.I0(count[15]),
        .O(cnt0_carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__2_i_2
       (.I0(count[14]),
        .O(cnt0_carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry__2_i_3
       (.I0(count[13]),
        .O(cnt0_carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry_i_1
       (.I0(count[4]),
        .O(cnt0_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry_i_2
       (.I0(count[3]),
        .O(cnt0_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry_i_3
       (.I0(count[2]),
        .O(cnt0_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cnt0_carry_i_4
       (.I0(count[1]),
        .O(cnt0_carry_i_4_n_0));
  CARRY4 cnt1_carry
       (.CI(1'b0),
        .CO({cnt1_carry_n_0,cnt1_carry_n_1,cnt1_carry_n_2,cnt1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_cnt1_carry_O_UNCONNECTED[3:0]),
        .S({cnt1_carry_i_1_n_0,cnt1_carry_i_2_n_0,cnt1_carry_i_3_n_0,cnt1_carry_i_4_n_0}));
  CARRY4 cnt1_carry__0
       (.CI(cnt1_carry_n_0),
        .CO({NLW_cnt1_carry__0_CO_UNCONNECTED[3:2],cnt1,cnt1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_cnt1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,cnt1_carry__0_i_1_n_0,cnt1_carry__0_i_2_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    cnt1_carry__0_i_1
       (.I0(FrameSize[15]),
        .I1(count[15]),
        .O(cnt1_carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt1_carry__0_i_2
       (.I0(count[12]),
        .I1(FrameSize[12]),
        .I2(FrameSize[14]),
        .I3(count[14]),
        .I4(FrameSize[13]),
        .I5(count[13]),
        .O(cnt1_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt1_carry_i_1
       (.I0(count[9]),
        .I1(FrameSize[9]),
        .I2(FrameSize[11]),
        .I3(count[11]),
        .I4(FrameSize[10]),
        .I5(count[10]),
        .O(cnt1_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt1_carry_i_2
       (.I0(count[6]),
        .I1(FrameSize[6]),
        .I2(FrameSize[8]),
        .I3(count[8]),
        .I4(FrameSize[7]),
        .I5(count[7]),
        .O(cnt1_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt1_carry_i_3
       (.I0(count[3]),
        .I1(FrameSize[3]),
        .I2(FrameSize[5]),
        .I3(count[5]),
        .I4(FrameSize[4]),
        .I5(count[4]),
        .O(cnt1_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt1_carry_i_4
       (.I0(count[0]),
        .I1(FrameSize[0]),
        .I2(FrameSize[2]),
        .I3(count[2]),
        .I4(FrameSize[1]),
        .I5(count[1]),
        .O(cnt1_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'hBF10)) 
    \cnt[0]_i_1 
       (.I0(cnt1),
        .I1(count[0]),
        .I2(reset),
        .I3(FrameSize[0]),
        .O(\cnt[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBF8FFFF0B080000)) 
    \cnt[10]_i_1 
       (.I0(cnt00_in[10]),
        .I1(direct),
        .I2(cnt1),
        .I3(cnt0[10]),
        .I4(reset),
        .I5(FrameSize[10]),
        .O(\cnt[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBF8FFFF0B080000)) 
    \cnt[11]_i_1 
       (.I0(cnt00_in[11]),
        .I1(direct),
        .I2(cnt1),
        .I3(cnt0[11]),
        .I4(reset),
        .I5(FrameSize[11]),
        .O(\cnt[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBF8FFFF0B080000)) 
    \cnt[12]_i_1 
       (.I0(cnt00_in[12]),
        .I1(direct),
        .I2(cnt1),
        .I3(cnt0[12]),
        .I4(reset),
        .I5(FrameSize[12]),
        .O(\cnt[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBF8FFFF0B080000)) 
    \cnt[13]_i_1 
       (.I0(cnt00_in[13]),
        .I1(direct),
        .I2(cnt1),
        .I3(cnt0[13]),
        .I4(reset),
        .I5(FrameSize[13]),
        .O(\cnt[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBF8FFFF0B080000)) 
    \cnt[14]_i_1 
       (.I0(cnt00_in[14]),
        .I1(direct),
        .I2(cnt1),
        .I3(cnt0[14]),
        .I4(reset),
        .I5(FrameSize[14]),
        .O(\cnt[14]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \cnt[15]_i_1 
       (.I0(direct),
        .I1(reset),
        .O(cnt));
  LUT6 #(
    .INIT(64'hFBF8FFFF0B080000)) 
    \cnt[15]_i_2 
       (.I0(cnt00_in[15]),
        .I1(direct),
        .I2(cnt1),
        .I3(cnt0[15]),
        .I4(reset),
        .I5(FrameSize[15]),
        .O(\cnt[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFBF8FFFF0B080000)) 
    \cnt[1]_i_1 
       (.I0(cnt00_in[1]),
        .I1(direct),
        .I2(cnt1),
        .I3(cnt0[1]),
        .I4(reset),
        .I5(FrameSize[1]),
        .O(\cnt[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBF8FFFF0B080000)) 
    \cnt[2]_i_1 
       (.I0(cnt00_in[2]),
        .I1(direct),
        .I2(cnt1),
        .I3(cnt0[2]),
        .I4(reset),
        .I5(FrameSize[2]),
        .O(\cnt[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBF8FFFF0B080000)) 
    \cnt[3]_i_1 
       (.I0(cnt00_in[3]),
        .I1(direct),
        .I2(cnt1),
        .I3(cnt0[3]),
        .I4(reset),
        .I5(FrameSize[3]),
        .O(\cnt[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBF8FFFF0B080000)) 
    \cnt[4]_i_1 
       (.I0(cnt00_in[4]),
        .I1(direct),
        .I2(cnt1),
        .I3(cnt0[4]),
        .I4(reset),
        .I5(FrameSize[4]),
        .O(\cnt[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBF8FFFF0B080000)) 
    \cnt[5]_i_1 
       (.I0(cnt00_in[5]),
        .I1(direct),
        .I2(cnt1),
        .I3(cnt0[5]),
        .I4(reset),
        .I5(FrameSize[5]),
        .O(\cnt[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBF8FFFF0B080000)) 
    \cnt[6]_i_1 
       (.I0(cnt00_in[6]),
        .I1(direct),
        .I2(cnt1),
        .I3(cnt0[6]),
        .I4(reset),
        .I5(FrameSize[6]),
        .O(\cnt[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBF8FFFF0B080000)) 
    \cnt[7]_i_1 
       (.I0(cnt00_in[7]),
        .I1(direct),
        .I2(cnt1),
        .I3(cnt0[7]),
        .I4(reset),
        .I5(FrameSize[7]),
        .O(\cnt[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBF8FFFF0B080000)) 
    \cnt[8]_i_1 
       (.I0(cnt00_in[8]),
        .I1(direct),
        .I2(cnt1),
        .I3(cnt0[8]),
        .I4(reset),
        .I5(FrameSize[8]),
        .O(\cnt[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBF8FFFF0B080000)) 
    \cnt[9]_i_1 
       (.I0(cnt00_in[9]),
        .I1(direct),
        .I2(cnt1),
        .I3(cnt0[9]),
        .I4(reset),
        .I5(FrameSize[9]),
        .O(\cnt[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[0]_i_1_n_0 ),
        .Q(count[0]),
        .R(cnt));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[10]_i_1_n_0 ),
        .Q(count[10]),
        .R(cnt));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[11]_i_1_n_0 ),
        .Q(count[11]),
        .R(cnt));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[12]_i_1_n_0 ),
        .Q(count[12]),
        .R(cnt));
  CARRY4 \cnt_reg[12]_i_2 
       (.CI(\cnt_reg[8]_i_2_n_0 ),
        .CO({\cnt_reg[12]_i_2_n_0 ,\cnt_reg[12]_i_2_n_1 ,\cnt_reg[12]_i_2_n_2 ,\cnt_reg[12]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(cnt00_in[12:9]),
        .S(count[12:9]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[13]_i_1_n_0 ),
        .Q(count[13]),
        .R(cnt));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[14]_i_1_n_0 ),
        .Q(count[14]),
        .R(cnt));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[15]_i_2_n_0 ),
        .Q(count[15]),
        .R(cnt));
  CARRY4 \cnt_reg[15]_i_3 
       (.CI(\cnt_reg[12]_i_2_n_0 ),
        .CO({\NLW_cnt_reg[15]_i_3_CO_UNCONNECTED [3:2],\cnt_reg[15]_i_3_n_2 ,\cnt_reg[15]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_cnt_reg[15]_i_3_O_UNCONNECTED [3],cnt00_in[15:13]}),
        .S({1'b0,count[15:13]}));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[1]_i_1_n_0 ),
        .Q(count[1]),
        .R(cnt));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[2]_i_1_n_0 ),
        .Q(count[2]),
        .R(cnt));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[3]_i_1_n_0 ),
        .Q(count[3]),
        .R(cnt));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[4]_i_1_n_0 ),
        .Q(count[4]),
        .R(cnt));
  CARRY4 \cnt_reg[4]_i_2 
       (.CI(1'b0),
        .CO({\cnt_reg[4]_i_2_n_0 ,\cnt_reg[4]_i_2_n_1 ,\cnt_reg[4]_i_2_n_2 ,\cnt_reg[4]_i_2_n_3 }),
        .CYINIT(count[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(cnt00_in[4:1]),
        .S(count[4:1]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[5]_i_1_n_0 ),
        .Q(count[5]),
        .R(cnt));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[6]_i_1_n_0 ),
        .Q(count[6]),
        .R(cnt));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[7]_i_1_n_0 ),
        .Q(count[7]),
        .R(cnt));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[8]_i_1_n_0 ),
        .Q(count[8]),
        .R(cnt));
  CARRY4 \cnt_reg[8]_i_2 
       (.CI(\cnt_reg[4]_i_2_n_0 ),
        .CO({\cnt_reg[8]_i_2_n_0 ,\cnt_reg[8]_i_2_n_1 ,\cnt_reg[8]_i_2_n_2 ,\cnt_reg[8]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(cnt00_in[8:5]),
        .S(count[8:5]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt[9]_i_1_n_0 ),
        .Q(count[9]),
        .R(cnt));
  LUT1 #(
    .INIT(2'h1)) 
    direct_i_1
       (.I0(direct),
        .O(p_0_in));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    direct_reg
       (.C(reset),
        .CE(1'b1),
        .D(p_0_in),
        .Q(direct),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_counter_0_0,counter,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "counter,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk,
    reset,
    FrameSize,
    ready,
    count,
    div_clk);
  input clk;
  input reset;
  input [15:0]FrameSize;
  output ready;
  output [15:0]count;
  output div_clk;

  wire [15:0]FrameSize;
  wire clk;
  wire [15:0]count;
  wire reset;

  assign div_clk = count[13];
  assign ready = clk;
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_counter inst
       (.FrameSize(FrameSize),
        .clk(clk),
        .count(count),
        .reset(reset));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
