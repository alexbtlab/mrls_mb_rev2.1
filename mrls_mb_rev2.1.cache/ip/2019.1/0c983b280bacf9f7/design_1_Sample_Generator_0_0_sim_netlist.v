// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Wed Oct  6 12:27:52 2021
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_Sample_Generator_0_0_sim_netlist.v
// Design      : design_1_Sample_Generator_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0
   (m00_axis_tvalid,
    m00_axis_tdata,
    m00_axis_tlast,
    m00_axis_aresetn,
    data_clk,
    m00_axis_aclk,
    data_in_IF1,
    data_in_IF2);
  output m00_axis_tvalid;
  output [31:0]m00_axis_tdata;
  output m00_axis_tlast;
  input m00_axis_aresetn;
  input data_clk;
  input m00_axis_aclk;
  input [15:0]data_in_IF1;
  input [15:0]data_in_IF2;

  wire [3:0]cnt100_reg;
  wire [15:0]cnt_10;
  wire cnt_100_carry__0_n_0;
  wire cnt_100_carry__0_n_1;
  wire cnt_100_carry__0_n_2;
  wire cnt_100_carry__0_n_3;
  wire cnt_100_carry__1_n_0;
  wire cnt_100_carry__1_n_1;
  wire cnt_100_carry__1_n_2;
  wire cnt_100_carry__1_n_3;
  wire cnt_100_carry__2_n_2;
  wire cnt_100_carry__2_n_3;
  wire cnt_100_carry_n_0;
  wire cnt_100_carry_n_1;
  wire cnt_100_carry_n_2;
  wire cnt_100_carry_n_3;
  wire \cnt_10_reg_n_0_[0] ;
  wire \cnt_10_reg_n_0_[10] ;
  wire \cnt_10_reg_n_0_[11] ;
  wire \cnt_10_reg_n_0_[12] ;
  wire \cnt_10_reg_n_0_[13] ;
  wire \cnt_10_reg_n_0_[14] ;
  wire \cnt_10_reg_n_0_[15] ;
  wire \cnt_10_reg_n_0_[1] ;
  wire \cnt_10_reg_n_0_[2] ;
  wire \cnt_10_reg_n_0_[3] ;
  wire \cnt_10_reg_n_0_[4] ;
  wire \cnt_10_reg_n_0_[5] ;
  wire \cnt_10_reg_n_0_[6] ;
  wire \cnt_10_reg_n_0_[7] ;
  wire \cnt_10_reg_n_0_[8] ;
  wire \cnt_10_reg_n_0_[9] ;
  wire [15:1]data0;
  wire data_clk;
  wire [15:0]data_in_IF1;
  wire [15:0]data_in_IF2;
  wire \data_sum_1_reg_n_0_[0] ;
  wire \data_sum_1_reg_n_0_[10] ;
  wire \data_sum_1_reg_n_0_[11] ;
  wire \data_sum_1_reg_n_0_[12] ;
  wire \data_sum_1_reg_n_0_[13] ;
  wire \data_sum_1_reg_n_0_[14] ;
  wire \data_sum_1_reg_n_0_[15] ;
  wire \data_sum_1_reg_n_0_[1] ;
  wire \data_sum_1_reg_n_0_[2] ;
  wire \data_sum_1_reg_n_0_[3] ;
  wire \data_sum_1_reg_n_0_[4] ;
  wire \data_sum_1_reg_n_0_[5] ;
  wire \data_sum_1_reg_n_0_[6] ;
  wire \data_sum_1_reg_n_0_[7] ;
  wire \data_sum_1_reg_n_0_[8] ;
  wire \data_sum_1_reg_n_0_[9] ;
  wire \data_sum_2_reg_n_0_[0] ;
  wire \data_sum_2_reg_n_0_[10] ;
  wire \data_sum_2_reg_n_0_[11] ;
  wire \data_sum_2_reg_n_0_[12] ;
  wire \data_sum_2_reg_n_0_[13] ;
  wire \data_sum_2_reg_n_0_[14] ;
  wire \data_sum_2_reg_n_0_[15] ;
  wire \data_sum_2_reg_n_0_[1] ;
  wire \data_sum_2_reg_n_0_[2] ;
  wire \data_sum_2_reg_n_0_[3] ;
  wire \data_sum_2_reg_n_0_[4] ;
  wire \data_sum_2_reg_n_0_[5] ;
  wire \data_sum_2_reg_n_0_[6] ;
  wire \data_sum_2_reg_n_0_[7] ;
  wire \data_sum_2_reg_n_0_[8] ;
  wire \data_sum_2_reg_n_0_[9] ;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire m00_axis_tlast_r0;
  wire m00_axis_tlast_r_i_2_n_0;
  wire m00_axis_tlast_r_i_3_n_0;
  wire m00_axis_tlast_r_i_4_n_0;
  wire m00_axis_tlast_r_i_5_n_0;
  wire m00_axis_tvalid;
  wire m00_axis_tvalid_r0;
  wire m00_axis_tvalid_r_i_2_n_0;
  wire m00_axis_tvalid_r_i_3_n_0;
  wire p_0_in;
  wire [3:0]p_0_in__0;
  wire reset_inner;
  wire [3:2]NLW_cnt_100_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_cnt_100_carry__2_O_UNCONNECTED;

  LUT1 #(
    .INIT(2'h1)) 
    \cnt100[0]_i_1 
       (.I0(cnt100_reg[0]),
        .O(p_0_in__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \cnt100[1]_i_1 
       (.I0(cnt100_reg[0]),
        .I1(cnt100_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \cnt100[2]_i_1 
       (.I0(cnt100_reg[1]),
        .I1(cnt100_reg[0]),
        .I2(cnt100_reg[2]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \cnt100[3]_i_1 
       (.I0(cnt100_reg[2]),
        .I1(cnt100_reg[0]),
        .I2(cnt100_reg[1]),
        .I3(cnt100_reg[3]),
        .O(p_0_in__0[3]));
  FDRE \cnt100_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_0_in__0[0]),
        .Q(cnt100_reg[0]),
        .R(data_clk));
  FDRE \cnt100_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_0_in__0[1]),
        .Q(cnt100_reg[1]),
        .R(data_clk));
  FDRE \cnt100_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_0_in__0[2]),
        .Q(cnt100_reg[2]),
        .R(data_clk));
  FDRE \cnt100_reg[3] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_0_in__0[3]),
        .Q(cnt100_reg[3]),
        .R(data_clk));
  CARRY4 cnt_100_carry
       (.CI(1'b0),
        .CO({cnt_100_carry_n_0,cnt_100_carry_n_1,cnt_100_carry_n_2,cnt_100_carry_n_3}),
        .CYINIT(\cnt_10_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S({\cnt_10_reg_n_0_[4] ,\cnt_10_reg_n_0_[3] ,\cnt_10_reg_n_0_[2] ,\cnt_10_reg_n_0_[1] }));
  CARRY4 cnt_100_carry__0
       (.CI(cnt_100_carry_n_0),
        .CO({cnt_100_carry__0_n_0,cnt_100_carry__0_n_1,cnt_100_carry__0_n_2,cnt_100_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S({\cnt_10_reg_n_0_[8] ,\cnt_10_reg_n_0_[7] ,\cnt_10_reg_n_0_[6] ,\cnt_10_reg_n_0_[5] }));
  CARRY4 cnt_100_carry__1
       (.CI(cnt_100_carry__0_n_0),
        .CO({cnt_100_carry__1_n_0,cnt_100_carry__1_n_1,cnt_100_carry__1_n_2,cnt_100_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S({\cnt_10_reg_n_0_[12] ,\cnt_10_reg_n_0_[11] ,\cnt_10_reg_n_0_[10] ,\cnt_10_reg_n_0_[9] }));
  CARRY4 cnt_100_carry__2
       (.CI(cnt_100_carry__1_n_0),
        .CO({NLW_cnt_100_carry__2_CO_UNCONNECTED[3:2],cnt_100_carry__2_n_2,cnt_100_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_cnt_100_carry__2_O_UNCONNECTED[3],data0[15:13]}),
        .S({1'b0,\cnt_10_reg_n_0_[15] ,\cnt_10_reg_n_0_[14] ,\cnt_10_reg_n_0_[13] }));
  LUT5 #(
    .INIT(32'h0100FFFF)) 
    \cnt_10[0]_i_1 
       (.I0(m00_axis_tlast_r_i_2_n_0),
        .I1(m00_axis_tlast_r_i_3_n_0),
        .I2(m00_axis_tlast_r_i_4_n_0),
        .I3(\cnt_10_reg_n_0_[13] ),
        .I4(\cnt_10_reg_n_0_[0] ),
        .O(cnt_10[0]));
  LUT6 #(
    .INIT(64'hFFFDFFFF00000000)) 
    \cnt_10[10]_i_1 
       (.I0(\cnt_10_reg_n_0_[13] ),
        .I1(m00_axis_tlast_r_i_4_n_0),
        .I2(m00_axis_tlast_r_i_3_n_0),
        .I3(m00_axis_tlast_r_i_2_n_0),
        .I4(\cnt_10_reg_n_0_[0] ),
        .I5(data0[10]),
        .O(cnt_10[10]));
  LUT6 #(
    .INIT(64'hFFFDFFFF00000000)) 
    \cnt_10[11]_i_1 
       (.I0(\cnt_10_reg_n_0_[13] ),
        .I1(m00_axis_tlast_r_i_4_n_0),
        .I2(m00_axis_tlast_r_i_3_n_0),
        .I3(m00_axis_tlast_r_i_2_n_0),
        .I4(\cnt_10_reg_n_0_[0] ),
        .I5(data0[11]),
        .O(cnt_10[11]));
  LUT6 #(
    .INIT(64'hFFFDFFFF00000000)) 
    \cnt_10[12]_i_1 
       (.I0(\cnt_10_reg_n_0_[13] ),
        .I1(m00_axis_tlast_r_i_4_n_0),
        .I2(m00_axis_tlast_r_i_3_n_0),
        .I3(m00_axis_tlast_r_i_2_n_0),
        .I4(\cnt_10_reg_n_0_[0] ),
        .I5(data0[12]),
        .O(cnt_10[12]));
  LUT6 #(
    .INIT(64'hFFFFFFFF00020000)) 
    \cnt_10[13]_i_1 
       (.I0(\cnt_10_reg_n_0_[13] ),
        .I1(m00_axis_tlast_r_i_4_n_0),
        .I2(m00_axis_tlast_r_i_3_n_0),
        .I3(m00_axis_tlast_r_i_2_n_0),
        .I4(\cnt_10_reg_n_0_[0] ),
        .I5(data0[13]),
        .O(cnt_10[13]));
  LUT6 #(
    .INIT(64'hFFFDFFFF00000000)) 
    \cnt_10[14]_i_1 
       (.I0(\cnt_10_reg_n_0_[13] ),
        .I1(m00_axis_tlast_r_i_4_n_0),
        .I2(m00_axis_tlast_r_i_3_n_0),
        .I3(m00_axis_tlast_r_i_2_n_0),
        .I4(\cnt_10_reg_n_0_[0] ),
        .I5(data0[14]),
        .O(cnt_10[14]));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_10[15]_i_1 
       (.I0(reset_inner),
        .O(p_0_in));
  LUT6 #(
    .INIT(64'hFFFDFFFF00000000)) 
    \cnt_10[15]_i_2 
       (.I0(\cnt_10_reg_n_0_[13] ),
        .I1(m00_axis_tlast_r_i_4_n_0),
        .I2(m00_axis_tlast_r_i_3_n_0),
        .I3(m00_axis_tlast_r_i_2_n_0),
        .I4(\cnt_10_reg_n_0_[0] ),
        .I5(data0[15]),
        .O(cnt_10[15]));
  LUT6 #(
    .INIT(64'hFFFDFFFF00000000)) 
    \cnt_10[1]_i_1 
       (.I0(\cnt_10_reg_n_0_[13] ),
        .I1(m00_axis_tlast_r_i_4_n_0),
        .I2(m00_axis_tlast_r_i_3_n_0),
        .I3(m00_axis_tlast_r_i_2_n_0),
        .I4(\cnt_10_reg_n_0_[0] ),
        .I5(data0[1]),
        .O(cnt_10[1]));
  LUT6 #(
    .INIT(64'hFFFDFFFF00000000)) 
    \cnt_10[2]_i_1 
       (.I0(\cnt_10_reg_n_0_[13] ),
        .I1(m00_axis_tlast_r_i_4_n_0),
        .I2(m00_axis_tlast_r_i_3_n_0),
        .I3(m00_axis_tlast_r_i_2_n_0),
        .I4(\cnt_10_reg_n_0_[0] ),
        .I5(data0[2]),
        .O(cnt_10[2]));
  LUT6 #(
    .INIT(64'hFFFDFFFF00000000)) 
    \cnt_10[3]_i_1 
       (.I0(\cnt_10_reg_n_0_[13] ),
        .I1(m00_axis_tlast_r_i_4_n_0),
        .I2(m00_axis_tlast_r_i_3_n_0),
        .I3(m00_axis_tlast_r_i_2_n_0),
        .I4(\cnt_10_reg_n_0_[0] ),
        .I5(data0[3]),
        .O(cnt_10[3]));
  LUT6 #(
    .INIT(64'hFFFDFFFF00000000)) 
    \cnt_10[4]_i_1 
       (.I0(\cnt_10_reg_n_0_[13] ),
        .I1(m00_axis_tlast_r_i_4_n_0),
        .I2(m00_axis_tlast_r_i_3_n_0),
        .I3(m00_axis_tlast_r_i_2_n_0),
        .I4(\cnt_10_reg_n_0_[0] ),
        .I5(data0[4]),
        .O(cnt_10[4]));
  LUT6 #(
    .INIT(64'hFFFDFFFF00000000)) 
    \cnt_10[5]_i_1 
       (.I0(\cnt_10_reg_n_0_[13] ),
        .I1(m00_axis_tlast_r_i_4_n_0),
        .I2(m00_axis_tlast_r_i_3_n_0),
        .I3(m00_axis_tlast_r_i_2_n_0),
        .I4(\cnt_10_reg_n_0_[0] ),
        .I5(data0[5]),
        .O(cnt_10[5]));
  LUT6 #(
    .INIT(64'hFFFDFFFF00000000)) 
    \cnt_10[6]_i_1 
       (.I0(\cnt_10_reg_n_0_[13] ),
        .I1(m00_axis_tlast_r_i_4_n_0),
        .I2(m00_axis_tlast_r_i_3_n_0),
        .I3(m00_axis_tlast_r_i_2_n_0),
        .I4(\cnt_10_reg_n_0_[0] ),
        .I5(data0[6]),
        .O(cnt_10[6]));
  LUT6 #(
    .INIT(64'hFFFDFFFF00000000)) 
    \cnt_10[7]_i_1 
       (.I0(\cnt_10_reg_n_0_[13] ),
        .I1(m00_axis_tlast_r_i_4_n_0),
        .I2(m00_axis_tlast_r_i_3_n_0),
        .I3(m00_axis_tlast_r_i_2_n_0),
        .I4(\cnt_10_reg_n_0_[0] ),
        .I5(data0[7]),
        .O(cnt_10[7]));
  LUT6 #(
    .INIT(64'hFFFDFFFF00000000)) 
    \cnt_10[8]_i_1 
       (.I0(\cnt_10_reg_n_0_[13] ),
        .I1(m00_axis_tlast_r_i_4_n_0),
        .I2(m00_axis_tlast_r_i_3_n_0),
        .I3(m00_axis_tlast_r_i_2_n_0),
        .I4(\cnt_10_reg_n_0_[0] ),
        .I5(data0[8]),
        .O(cnt_10[8]));
  LUT6 #(
    .INIT(64'hFFFDFFFF00000000)) 
    \cnt_10[9]_i_1 
       (.I0(\cnt_10_reg_n_0_[13] ),
        .I1(m00_axis_tlast_r_i_4_n_0),
        .I2(m00_axis_tlast_r_i_3_n_0),
        .I3(m00_axis_tlast_r_i_2_n_0),
        .I4(\cnt_10_reg_n_0_[0] ),
        .I5(data0[9]),
        .O(cnt_10[9]));
  FDRE \cnt_10_reg[0] 
       (.C(data_clk),
        .CE(1'b1),
        .D(cnt_10[0]),
        .Q(\cnt_10_reg_n_0_[0] ),
        .R(p_0_in));
  FDRE \cnt_10_reg[10] 
       (.C(data_clk),
        .CE(1'b1),
        .D(cnt_10[10]),
        .Q(\cnt_10_reg_n_0_[10] ),
        .R(p_0_in));
  FDRE \cnt_10_reg[11] 
       (.C(data_clk),
        .CE(1'b1),
        .D(cnt_10[11]),
        .Q(\cnt_10_reg_n_0_[11] ),
        .R(p_0_in));
  FDRE \cnt_10_reg[12] 
       (.C(data_clk),
        .CE(1'b1),
        .D(cnt_10[12]),
        .Q(\cnt_10_reg_n_0_[12] ),
        .R(p_0_in));
  FDRE \cnt_10_reg[13] 
       (.C(data_clk),
        .CE(1'b1),
        .D(cnt_10[13]),
        .Q(\cnt_10_reg_n_0_[13] ),
        .R(p_0_in));
  FDRE \cnt_10_reg[14] 
       (.C(data_clk),
        .CE(1'b1),
        .D(cnt_10[14]),
        .Q(\cnt_10_reg_n_0_[14] ),
        .R(p_0_in));
  FDRE \cnt_10_reg[15] 
       (.C(data_clk),
        .CE(1'b1),
        .D(cnt_10[15]),
        .Q(\cnt_10_reg_n_0_[15] ),
        .R(p_0_in));
  FDRE \cnt_10_reg[1] 
       (.C(data_clk),
        .CE(1'b1),
        .D(cnt_10[1]),
        .Q(\cnt_10_reg_n_0_[1] ),
        .R(p_0_in));
  FDRE \cnt_10_reg[2] 
       (.C(data_clk),
        .CE(1'b1),
        .D(cnt_10[2]),
        .Q(\cnt_10_reg_n_0_[2] ),
        .R(p_0_in));
  FDRE \cnt_10_reg[3] 
       (.C(data_clk),
        .CE(1'b1),
        .D(cnt_10[3]),
        .Q(\cnt_10_reg_n_0_[3] ),
        .R(p_0_in));
  FDRE \cnt_10_reg[4] 
       (.C(data_clk),
        .CE(1'b1),
        .D(cnt_10[4]),
        .Q(\cnt_10_reg_n_0_[4] ),
        .R(p_0_in));
  FDRE \cnt_10_reg[5] 
       (.C(data_clk),
        .CE(1'b1),
        .D(cnt_10[5]),
        .Q(\cnt_10_reg_n_0_[5] ),
        .R(p_0_in));
  FDRE \cnt_10_reg[6] 
       (.C(data_clk),
        .CE(1'b1),
        .D(cnt_10[6]),
        .Q(\cnt_10_reg_n_0_[6] ),
        .R(p_0_in));
  FDRE \cnt_10_reg[7] 
       (.C(data_clk),
        .CE(1'b1),
        .D(cnt_10[7]),
        .Q(\cnt_10_reg_n_0_[7] ),
        .R(p_0_in));
  FDRE \cnt_10_reg[8] 
       (.C(data_clk),
        .CE(1'b1),
        .D(cnt_10[8]),
        .Q(\cnt_10_reg_n_0_[8] ),
        .R(p_0_in));
  FDRE \cnt_10_reg[9] 
       (.C(data_clk),
        .CE(1'b1),
        .D(cnt_10[9]),
        .Q(\cnt_10_reg_n_0_[9] ),
        .R(p_0_in));
  FDRE \data_sum_1_reg[0] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF1[0]),
        .Q(\data_sum_1_reg_n_0_[0] ),
        .R(p_0_in));
  FDRE \data_sum_1_reg[10] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF1[10]),
        .Q(\data_sum_1_reg_n_0_[10] ),
        .R(p_0_in));
  FDRE \data_sum_1_reg[11] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF1[11]),
        .Q(\data_sum_1_reg_n_0_[11] ),
        .R(p_0_in));
  FDRE \data_sum_1_reg[12] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF1[12]),
        .Q(\data_sum_1_reg_n_0_[12] ),
        .R(p_0_in));
  FDRE \data_sum_1_reg[13] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF1[13]),
        .Q(\data_sum_1_reg_n_0_[13] ),
        .R(p_0_in));
  FDRE \data_sum_1_reg[14] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF1[14]),
        .Q(\data_sum_1_reg_n_0_[14] ),
        .R(p_0_in));
  FDRE \data_sum_1_reg[15] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF1[15]),
        .Q(\data_sum_1_reg_n_0_[15] ),
        .R(p_0_in));
  FDRE \data_sum_1_reg[1] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF1[1]),
        .Q(\data_sum_1_reg_n_0_[1] ),
        .R(p_0_in));
  FDRE \data_sum_1_reg[2] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF1[2]),
        .Q(\data_sum_1_reg_n_0_[2] ),
        .R(p_0_in));
  FDRE \data_sum_1_reg[3] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF1[3]),
        .Q(\data_sum_1_reg_n_0_[3] ),
        .R(p_0_in));
  FDRE \data_sum_1_reg[4] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF1[4]),
        .Q(\data_sum_1_reg_n_0_[4] ),
        .R(p_0_in));
  FDRE \data_sum_1_reg[5] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF1[5]),
        .Q(\data_sum_1_reg_n_0_[5] ),
        .R(p_0_in));
  FDRE \data_sum_1_reg[6] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF1[6]),
        .Q(\data_sum_1_reg_n_0_[6] ),
        .R(p_0_in));
  FDRE \data_sum_1_reg[7] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF1[7]),
        .Q(\data_sum_1_reg_n_0_[7] ),
        .R(p_0_in));
  FDRE \data_sum_1_reg[8] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF1[8]),
        .Q(\data_sum_1_reg_n_0_[8] ),
        .R(p_0_in));
  FDRE \data_sum_1_reg[9] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF1[9]),
        .Q(\data_sum_1_reg_n_0_[9] ),
        .R(p_0_in));
  FDRE \data_sum_2_reg[0] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF2[0]),
        .Q(\data_sum_2_reg_n_0_[0] ),
        .R(p_0_in));
  FDRE \data_sum_2_reg[10] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF2[10]),
        .Q(\data_sum_2_reg_n_0_[10] ),
        .R(p_0_in));
  FDRE \data_sum_2_reg[11] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF2[11]),
        .Q(\data_sum_2_reg_n_0_[11] ),
        .R(p_0_in));
  FDRE \data_sum_2_reg[12] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF2[12]),
        .Q(\data_sum_2_reg_n_0_[12] ),
        .R(p_0_in));
  FDRE \data_sum_2_reg[13] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF2[13]),
        .Q(\data_sum_2_reg_n_0_[13] ),
        .R(p_0_in));
  FDRE \data_sum_2_reg[14] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF2[14]),
        .Q(\data_sum_2_reg_n_0_[14] ),
        .R(p_0_in));
  FDRE \data_sum_2_reg[15] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF2[15]),
        .Q(\data_sum_2_reg_n_0_[15] ),
        .R(p_0_in));
  FDRE \data_sum_2_reg[1] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF2[1]),
        .Q(\data_sum_2_reg_n_0_[1] ),
        .R(p_0_in));
  FDRE \data_sum_2_reg[2] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF2[2]),
        .Q(\data_sum_2_reg_n_0_[2] ),
        .R(p_0_in));
  FDRE \data_sum_2_reg[3] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF2[3]),
        .Q(\data_sum_2_reg_n_0_[3] ),
        .R(p_0_in));
  FDRE \data_sum_2_reg[4] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF2[4]),
        .Q(\data_sum_2_reg_n_0_[4] ),
        .R(p_0_in));
  FDRE \data_sum_2_reg[5] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF2[5]),
        .Q(\data_sum_2_reg_n_0_[5] ),
        .R(p_0_in));
  FDRE \data_sum_2_reg[6] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF2[6]),
        .Q(\data_sum_2_reg_n_0_[6] ),
        .R(p_0_in));
  FDRE \data_sum_2_reg[7] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF2[7]),
        .Q(\data_sum_2_reg_n_0_[7] ),
        .R(p_0_in));
  FDRE \data_sum_2_reg[8] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF2[8]),
        .Q(\data_sum_2_reg_n_0_[8] ),
        .R(p_0_in));
  FDRE \data_sum_2_reg[9] 
       (.C(data_clk),
        .CE(1'b1),
        .D(data_in_IF2[9]),
        .Q(\data_sum_2_reg_n_0_[9] ),
        .R(p_0_in));
  FDRE \m00_axis_tdata_r_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_1_reg_n_0_[0] ),
        .Q(m00_axis_tdata[0]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[10] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_1_reg_n_0_[10] ),
        .Q(m00_axis_tdata[10]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[11] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_1_reg_n_0_[11] ),
        .Q(m00_axis_tdata[11]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[12] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_1_reg_n_0_[12] ),
        .Q(m00_axis_tdata[12]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[13] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_1_reg_n_0_[13] ),
        .Q(m00_axis_tdata[13]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[14] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_1_reg_n_0_[14] ),
        .Q(m00_axis_tdata[14]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[15] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_1_reg_n_0_[15] ),
        .Q(m00_axis_tdata[15]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[16] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_2_reg_n_0_[0] ),
        .Q(m00_axis_tdata[16]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[17] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_2_reg_n_0_[1] ),
        .Q(m00_axis_tdata[17]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[18] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_2_reg_n_0_[2] ),
        .Q(m00_axis_tdata[18]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[19] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_2_reg_n_0_[3] ),
        .Q(m00_axis_tdata[19]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_1_reg_n_0_[1] ),
        .Q(m00_axis_tdata[1]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[20] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_2_reg_n_0_[4] ),
        .Q(m00_axis_tdata[20]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[21] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_2_reg_n_0_[5] ),
        .Q(m00_axis_tdata[21]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[22] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_2_reg_n_0_[6] ),
        .Q(m00_axis_tdata[22]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[23] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_2_reg_n_0_[7] ),
        .Q(m00_axis_tdata[23]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[24] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_2_reg_n_0_[8] ),
        .Q(m00_axis_tdata[24]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[25] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_2_reg_n_0_[9] ),
        .Q(m00_axis_tdata[25]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[26] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_2_reg_n_0_[10] ),
        .Q(m00_axis_tdata[26]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[27] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_2_reg_n_0_[11] ),
        .Q(m00_axis_tdata[27]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[28] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_2_reg_n_0_[12] ),
        .Q(m00_axis_tdata[28]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[29] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_2_reg_n_0_[13] ),
        .Q(m00_axis_tdata[29]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_1_reg_n_0_[2] ),
        .Q(m00_axis_tdata[2]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[30] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_2_reg_n_0_[14] ),
        .Q(m00_axis_tdata[30]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[31] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_2_reg_n_0_[15] ),
        .Q(m00_axis_tdata[31]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[3] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_1_reg_n_0_[3] ),
        .Q(m00_axis_tdata[3]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_1_reg_n_0_[4] ),
        .Q(m00_axis_tdata[4]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[5] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_1_reg_n_0_[5] ),
        .Q(m00_axis_tdata[5]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[6] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_1_reg_n_0_[6] ),
        .Q(m00_axis_tdata[6]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[7] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_1_reg_n_0_[7] ),
        .Q(m00_axis_tdata[7]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[8] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_1_reg_n_0_[8] ),
        .Q(m00_axis_tdata[8]),
        .R(1'b0));
  FDRE \m00_axis_tdata_r_reg[9] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\data_sum_1_reg_n_0_[9] ),
        .Q(m00_axis_tdata[9]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h01000000)) 
    m00_axis_tlast_r_i_1
       (.I0(m00_axis_tlast_r_i_2_n_0),
        .I1(m00_axis_tlast_r_i_3_n_0),
        .I2(m00_axis_tlast_r_i_4_n_0),
        .I3(\cnt_10_reg_n_0_[13] ),
        .I4(m00_axis_tlast_r_i_5_n_0),
        .O(m00_axis_tlast_r0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    m00_axis_tlast_r_i_2
       (.I0(\cnt_10_reg_n_0_[14] ),
        .I1(\cnt_10_reg_n_0_[15] ),
        .I2(\cnt_10_reg_n_0_[11] ),
        .I3(\cnt_10_reg_n_0_[12] ),
        .I4(\cnt_10_reg_n_0_[2] ),
        .I5(\cnt_10_reg_n_0_[1] ),
        .O(m00_axis_tlast_r_i_2_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_axis_tlast_r_i_3
       (.I0(\cnt_10_reg_n_0_[4] ),
        .I1(\cnt_10_reg_n_0_[3] ),
        .I2(\cnt_10_reg_n_0_[6] ),
        .I3(\cnt_10_reg_n_0_[5] ),
        .O(m00_axis_tlast_r_i_3_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_axis_tlast_r_i_4
       (.I0(\cnt_10_reg_n_0_[8] ),
        .I1(\cnt_10_reg_n_0_[7] ),
        .I2(\cnt_10_reg_n_0_[10] ),
        .I3(\cnt_10_reg_n_0_[9] ),
        .O(m00_axis_tlast_r_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000001000000000)) 
    m00_axis_tlast_r_i_5
       (.I0(cnt100_reg[2]),
        .I1(cnt100_reg[3]),
        .I2(cnt100_reg[0]),
        .I3(cnt100_reg[1]),
        .I4(\cnt_10_reg_n_0_[0] ),
        .I5(reset_inner),
        .O(m00_axis_tlast_r_i_5_n_0));
  FDRE m00_axis_tlast_r_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(m00_axis_tlast_r0),
        .Q(m00_axis_tlast),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h000200020002AAAA)) 
    m00_axis_tvalid_r_i_1
       (.I0(m00_axis_tvalid_r_i_2_n_0),
        .I1(\cnt_10_reg_n_0_[15] ),
        .I2(\cnt_10_reg_n_0_[14] ),
        .I3(\cnt_10_reg_n_0_[13] ),
        .I4(m00_axis_tvalid_r_i_3_n_0),
        .I5(\cnt_10_reg_n_0_[0] ),
        .O(m00_axis_tvalid_r0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00020000)) 
    m00_axis_tvalid_r_i_2
       (.I0(cnt100_reg[0]),
        .I1(cnt100_reg[1]),
        .I2(cnt100_reg[2]),
        .I3(cnt100_reg[3]),
        .I4(reset_inner),
        .O(m00_axis_tvalid_r_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    m00_axis_tvalid_r_i_3
       (.I0(m00_axis_tlast_r_i_4_n_0),
        .I1(\cnt_10_reg_n_0_[4] ),
        .I2(\cnt_10_reg_n_0_[3] ),
        .I3(\cnt_10_reg_n_0_[6] ),
        .I4(\cnt_10_reg_n_0_[5] ),
        .I5(m00_axis_tlast_r_i_2_n_0),
        .O(m00_axis_tvalid_r_i_3_n_0));
  FDRE m00_axis_tvalid_r_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(m00_axis_tvalid_r0),
        .Q(m00_axis_tvalid),
        .R(1'b0));
  FDRE reset_inner_reg
       (.C(data_clk),
        .CE(1'b1),
        .D(m00_axis_aresetn),
        .Q(reset_inner),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_Sample_Generator_0_0,Sample_Generator_v3_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "Sample_Generator_v3_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (data_clk,
    data_in_IF1,
    data_in_IF2,
    m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tvalid,
    m00_axis_tready,
    m00_axis_aclk,
    m00_axis_config_tvalid,
    m00_axis_config_tdata,
    m00_axis_config_tready,
    m00_axis_aresetn);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 data_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME data_clk, FREQ_HZ 10000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_2_clk_out1, INSERT_VIP 0" *) input data_clk;
  input [15:0]data_in_IF1;
  input [15:0]data_in_IF2;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TDATA" *) output [31:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB" *) output [3:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0" *) input m00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS_CONFIG TVALID" *) output m00_axis_config_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS_CONFIG TDATA" *) output [31:0]m00_axis_config_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M_AXIS_CONFIG TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M_AXIS_CONFIG, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_config_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input m00_axis_aresetn;

  wire \<const0> ;
  wire \<const1> ;
  wire data_clk;
  wire [15:0]data_in_IF1;
  wire [15:0]data_in_IF2;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire m00_axis_tvalid;

  assign m00_axis_config_tdata[31] = \<const0> ;
  assign m00_axis_config_tdata[30] = \<const0> ;
  assign m00_axis_config_tdata[29] = \<const0> ;
  assign m00_axis_config_tdata[28] = \<const0> ;
  assign m00_axis_config_tdata[27] = \<const0> ;
  assign m00_axis_config_tdata[26] = \<const0> ;
  assign m00_axis_config_tdata[25] = \<const0> ;
  assign m00_axis_config_tdata[24] = \<const0> ;
  assign m00_axis_config_tdata[23] = \<const0> ;
  assign m00_axis_config_tdata[22] = \<const0> ;
  assign m00_axis_config_tdata[21] = \<const0> ;
  assign m00_axis_config_tdata[20] = \<const0> ;
  assign m00_axis_config_tdata[19] = \<const0> ;
  assign m00_axis_config_tdata[18] = \<const0> ;
  assign m00_axis_config_tdata[17] = \<const0> ;
  assign m00_axis_config_tdata[16] = \<const0> ;
  assign m00_axis_config_tdata[15] = \<const0> ;
  assign m00_axis_config_tdata[14] = \<const0> ;
  assign m00_axis_config_tdata[13] = \<const0> ;
  assign m00_axis_config_tdata[12] = \<const0> ;
  assign m00_axis_config_tdata[11] = \<const0> ;
  assign m00_axis_config_tdata[10] = \<const0> ;
  assign m00_axis_config_tdata[9] = \<const0> ;
  assign m00_axis_config_tdata[8] = \<const0> ;
  assign m00_axis_config_tdata[7] = \<const0> ;
  assign m00_axis_config_tdata[6] = \<const0> ;
  assign m00_axis_config_tdata[5] = \<const0> ;
  assign m00_axis_config_tdata[4] = \<const0> ;
  assign m00_axis_config_tdata[3] = \<const0> ;
  assign m00_axis_config_tdata[2] = \<const0> ;
  assign m00_axis_config_tdata[1] = \<const0> ;
  assign m00_axis_config_tdata[0] = \<const0> ;
  assign m00_axis_config_tvalid = \<const0> ;
  assign m00_axis_tstrb[3] = \<const1> ;
  assign m00_axis_tstrb[2] = \<const1> ;
  assign m00_axis_tstrb[1] = \<const1> ;
  assign m00_axis_tstrb[0] = \<const1> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0 inst
       (.data_clk(data_clk),
        .data_in_IF1(data_in_IF1),
        .data_in_IF2(data_in_IF2),
        .m00_axis_aclk(m00_axis_aclk),
        .m00_axis_aresetn(m00_axis_aresetn),
        .m00_axis_tdata(m00_axis_tdata),
        .m00_axis_tlast(m00_axis_tlast),
        .m00_axis_tvalid(m00_axis_tvalid));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
