-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Wed Dec  2 13:47:41 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_Sample_Generator_0_0_sim_netlist.vhdl
-- Design      : design_1_Sample_Generator_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0 is
  port (
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 33 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    data_in_IF1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_in_IF2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_clk : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    clk_5MHz : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0 is
  signal clear : STD_LOGIC;
  signal cnt10 : STD_LOGIC;
  signal \cnt100[0]_i_2_n_0\ : STD_LOGIC;
  signal cnt100_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt100_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_1_n_7\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \cnt50_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \cnt50_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \cnt50_carry__0_i_3_n_2\ : STD_LOGIC;
  signal \cnt50_carry__0_i_3_n_3\ : STD_LOGIC;
  signal \cnt50_carry__0_n_3\ : STD_LOGIC;
  signal cnt50_carry_i_1_n_0 : STD_LOGIC;
  signal cnt50_carry_i_2_n_0 : STD_LOGIC;
  signal cnt50_carry_i_3_n_0 : STD_LOGIC;
  signal cnt50_carry_i_4_n_0 : STD_LOGIC;
  signal cnt50_carry_i_5_n_0 : STD_LOGIC;
  signal cnt50_carry_i_5_n_1 : STD_LOGIC;
  signal cnt50_carry_i_5_n_2 : STD_LOGIC;
  signal cnt50_carry_i_5_n_3 : STD_LOGIC;
  signal cnt50_carry_i_6_n_0 : STD_LOGIC;
  signal cnt50_carry_i_6_n_1 : STD_LOGIC;
  signal cnt50_carry_i_6_n_2 : STD_LOGIC;
  signal cnt50_carry_i_6_n_3 : STD_LOGIC;
  signal cnt50_carry_i_7_n_0 : STD_LOGIC;
  signal cnt50_carry_i_7_n_1 : STD_LOGIC;
  signal cnt50_carry_i_7_n_2 : STD_LOGIC;
  signal cnt50_carry_i_7_n_3 : STD_LOGIC;
  signal cnt50_carry_n_0 : STD_LOGIC;
  signal cnt50_carry_n_1 : STD_LOGIC;
  signal cnt50_carry_n_2 : STD_LOGIC;
  signal cnt50_carry_n_3 : STD_LOGIC;
  signal cnt51 : STD_LOGIC_VECTOR ( 16 downto 1 );
  signal \cnt5[0]_i_2_n_0\ : STD_LOGIC;
  signal cnt5_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt5_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt5_reg[0]_i_1_n_1\ : STD_LOGIC;
  signal \cnt5_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \cnt5_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal \cnt5_reg[0]_i_1_n_4\ : STD_LOGIC;
  signal \cnt5_reg[0]_i_1_n_5\ : STD_LOGIC;
  signal \cnt5_reg[0]_i_1_n_6\ : STD_LOGIC;
  signal \cnt5_reg[0]_i_1_n_7\ : STD_LOGIC;
  signal \cnt5_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt5_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt5_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt5_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt5_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt5_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt5_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt5_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt5_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt5_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt5_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt5_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt5_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt5_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt5_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt5_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt5_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt5_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt5_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt5_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt5_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt5_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt5_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal data_even_1 : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal data_even_2 : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal data_sum_10 : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \data_sum_1[11]_i_2_n_0\ : STD_LOGIC;
  signal \data_sum_1[11]_i_3_n_0\ : STD_LOGIC;
  signal \data_sum_1[11]_i_4_n_0\ : STD_LOGIC;
  signal \data_sum_1[11]_i_5_n_0\ : STD_LOGIC;
  signal \data_sum_1[15]_i_2_n_0\ : STD_LOGIC;
  signal \data_sum_1[15]_i_3_n_0\ : STD_LOGIC;
  signal \data_sum_1[15]_i_4_n_0\ : STD_LOGIC;
  signal \data_sum_1[15]_i_5_n_0\ : STD_LOGIC;
  signal \data_sum_1[16]_i_2_n_0\ : STD_LOGIC;
  signal \data_sum_1[3]_i_2_n_0\ : STD_LOGIC;
  signal \data_sum_1[3]_i_3_n_0\ : STD_LOGIC;
  signal \data_sum_1[3]_i_4_n_0\ : STD_LOGIC;
  signal \data_sum_1[3]_i_5_n_0\ : STD_LOGIC;
  signal \data_sum_1[7]_i_2_n_0\ : STD_LOGIC;
  signal \data_sum_1[7]_i_3_n_0\ : STD_LOGIC;
  signal \data_sum_1[7]_i_4_n_0\ : STD_LOGIC;
  signal \data_sum_1[7]_i_5_n_0\ : STD_LOGIC;
  signal \data_sum_1_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \data_sum_1_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \data_sum_1_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \data_sum_1_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \data_sum_1_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \data_sum_1_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \data_sum_1_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \data_sum_1_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \data_sum_1_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \data_sum_1_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \data_sum_1_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \data_sum_1_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \data_sum_1_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \data_sum_1_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \data_sum_1_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \data_sum_1_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal data_sum_20 : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \data_sum_2[11]_i_2_n_0\ : STD_LOGIC;
  signal \data_sum_2[11]_i_3_n_0\ : STD_LOGIC;
  signal \data_sum_2[11]_i_4_n_0\ : STD_LOGIC;
  signal \data_sum_2[11]_i_5_n_0\ : STD_LOGIC;
  signal \data_sum_2[15]_i_2_n_0\ : STD_LOGIC;
  signal \data_sum_2[15]_i_3_n_0\ : STD_LOGIC;
  signal \data_sum_2[15]_i_4_n_0\ : STD_LOGIC;
  signal \data_sum_2[15]_i_5_n_0\ : STD_LOGIC;
  signal \data_sum_2[16]_i_3_n_0\ : STD_LOGIC;
  signal \data_sum_2[3]_i_2_n_0\ : STD_LOGIC;
  signal \data_sum_2[3]_i_3_n_0\ : STD_LOGIC;
  signal \data_sum_2[3]_i_4_n_0\ : STD_LOGIC;
  signal \data_sum_2[3]_i_5_n_0\ : STD_LOGIC;
  signal \data_sum_2[7]_i_2_n_0\ : STD_LOGIC;
  signal \data_sum_2[7]_i_3_n_0\ : STD_LOGIC;
  signal \data_sum_2[7]_i_4_n_0\ : STD_LOGIC;
  signal \data_sum_2[7]_i_5_n_0\ : STD_LOGIC;
  signal \data_sum_2_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \data_sum_2_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \data_sum_2_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \data_sum_2_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \data_sum_2_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \data_sum_2_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \data_sum_2_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \data_sum_2_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \data_sum_2_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \data_sum_2_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \data_sum_2_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \data_sum_2_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \data_sum_2_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \data_sum_2_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \data_sum_2_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \data_sum_2_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \i__carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i__carry_i_1_n_0\ : STD_LOGIC;
  signal \i__carry_i_2_n_0\ : STD_LOGIC;
  signal \i__carry_i_3_n_0\ : STD_LOGIC;
  signal \i__carry_i_4_n_0\ : STD_LOGIC;
  signal \i__carry_i_5_n_0\ : STD_LOGIC;
  signal \i__carry_i_6_n_0\ : STD_LOGIC;
  signal \i__carry_i_7_n_0\ : STD_LOGIC;
  signal \i__carry_i_8_n_0\ : STD_LOGIC;
  signal \^m00_axis_tlast\ : STD_LOGIC;
  signal m00_axis_tlast_r0 : STD_LOGIC;
  signal \m00_axis_tlast_r0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tlast_r0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \m00_axis_tlast_r0_carry__0_n_3\ : STD_LOGIC;
  signal m00_axis_tlast_r0_carry_i_1_n_0 : STD_LOGIC;
  signal m00_axis_tlast_r0_carry_i_2_n_0 : STD_LOGIC;
  signal m00_axis_tlast_r0_carry_i_3_n_0 : STD_LOGIC;
  signal m00_axis_tlast_r0_carry_i_4_n_0 : STD_LOGIC;
  signal m00_axis_tlast_r0_carry_n_0 : STD_LOGIC;
  signal m00_axis_tlast_r0_carry_n_1 : STD_LOGIC;
  signal m00_axis_tlast_r0_carry_n_2 : STD_LOGIC;
  signal m00_axis_tlast_r0_carry_n_3 : STD_LOGIC;
  signal m00_axis_tlast_r_i_1_n_0 : STD_LOGIC;
  signal m00_axis_tlast_r_i_3_n_0 : STD_LOGIC;
  signal m00_axis_tlast_r_i_4_n_0 : STD_LOGIC;
  signal m00_axis_tlast_r_i_5_n_0 : STD_LOGIC;
  signal \^m00_axis_tvalid\ : STD_LOGIC;
  signal m00_axis_tvalid_r1 : STD_LOGIC;
  signal m00_axis_tvalid_r13_out : STD_LOGIC;
  signal \m00_axis_tvalid_r1_inferred__0/i__carry__0_n_1\ : STD_LOGIC;
  signal \m00_axis_tvalid_r1_inferred__0/i__carry__0_n_2\ : STD_LOGIC;
  signal \m00_axis_tvalid_r1_inferred__0/i__carry__0_n_3\ : STD_LOGIC;
  signal \m00_axis_tvalid_r1_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \m00_axis_tvalid_r1_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \m00_axis_tvalid_r1_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \m00_axis_tvalid_r1_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal m00_axis_tvalid_r_i_1_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_2_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_3_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_4_n_0 : STD_LOGIC;
  signal m00_axis_tvalid_r_i_5_n_0 : STD_LOGIC;
  signal p_1_in : STD_LOGIC;
  signal sel : STD_LOGIC;
  signal \NLW_cnt100_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_cnt50_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cnt50_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_cnt50_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cnt50_carry__0_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_cnt50_carry__0_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_cnt5_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_data_sum_1_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_data_sum_1_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_data_sum_2_reg[16]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_data_sum_2_reg[16]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal NLW_m00_axis_tlast_r0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_m00_axis_tlast_r0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_m00_axis_tlast_r0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_m00_axis_tvalid_r1_inferred__0/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_m00_axis_tvalid_r1_inferred__0/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
  m00_axis_tlast <= \^m00_axis_tlast\;
  m00_axis_tvalid <= \^m00_axis_tvalid\;
\cnt100[0]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt100_reg(0),
      O => \cnt100[0]_i_2_n_0\
    );
\cnt100_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[0]_i_1_n_7\,
      Q => cnt100_reg(0),
      R => cnt10
    );
\cnt100_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt100_reg[0]_i_1_n_0\,
      CO(2) => \cnt100_reg[0]_i_1_n_1\,
      CO(1) => \cnt100_reg[0]_i_1_n_2\,
      CO(0) => \cnt100_reg[0]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt100_reg[0]_i_1_n_4\,
      O(2) => \cnt100_reg[0]_i_1_n_5\,
      O(1) => \cnt100_reg[0]_i_1_n_6\,
      O(0) => \cnt100_reg[0]_i_1_n_7\,
      S(3 downto 1) => cnt100_reg(3 downto 1),
      S(0) => \cnt100[0]_i_2_n_0\
    );
\cnt100_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[8]_i_1_n_5\,
      Q => cnt100_reg(10),
      R => cnt10
    );
\cnt100_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[8]_i_1_n_4\,
      Q => cnt100_reg(11),
      R => cnt10
    );
\cnt100_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[12]_i_1_n_7\,
      Q => cnt100_reg(12),
      R => cnt10
    );
\cnt100_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt100_reg[8]_i_1_n_0\,
      CO(3) => \NLW_cnt100_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt100_reg[12]_i_1_n_1\,
      CO(1) => \cnt100_reg[12]_i_1_n_2\,
      CO(0) => \cnt100_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt100_reg[12]_i_1_n_4\,
      O(2) => \cnt100_reg[12]_i_1_n_5\,
      O(1) => \cnt100_reg[12]_i_1_n_6\,
      O(0) => \cnt100_reg[12]_i_1_n_7\,
      S(3 downto 0) => cnt100_reg(15 downto 12)
    );
\cnt100_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[12]_i_1_n_6\,
      Q => cnt100_reg(13),
      R => cnt10
    );
\cnt100_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[12]_i_1_n_5\,
      Q => cnt100_reg(14),
      R => cnt10
    );
\cnt100_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[12]_i_1_n_4\,
      Q => cnt100_reg(15),
      R => cnt10
    );
\cnt100_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[0]_i_1_n_6\,
      Q => cnt100_reg(1),
      R => cnt10
    );
\cnt100_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[0]_i_1_n_5\,
      Q => cnt100_reg(2),
      R => cnt10
    );
\cnt100_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[0]_i_1_n_4\,
      Q => cnt100_reg(3),
      R => cnt10
    );
\cnt100_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[4]_i_1_n_7\,
      Q => cnt100_reg(4),
      R => cnt10
    );
\cnt100_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt100_reg[0]_i_1_n_0\,
      CO(3) => \cnt100_reg[4]_i_1_n_0\,
      CO(2) => \cnt100_reg[4]_i_1_n_1\,
      CO(1) => \cnt100_reg[4]_i_1_n_2\,
      CO(0) => \cnt100_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt100_reg[4]_i_1_n_4\,
      O(2) => \cnt100_reg[4]_i_1_n_5\,
      O(1) => \cnt100_reg[4]_i_1_n_6\,
      O(0) => \cnt100_reg[4]_i_1_n_7\,
      S(3 downto 0) => cnt100_reg(7 downto 4)
    );
\cnt100_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[4]_i_1_n_6\,
      Q => cnt100_reg(5),
      R => cnt10
    );
\cnt100_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[4]_i_1_n_5\,
      Q => cnt100_reg(6),
      R => cnt10
    );
\cnt100_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[4]_i_1_n_4\,
      Q => cnt100_reg(7),
      R => cnt10
    );
\cnt100_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[8]_i_1_n_7\,
      Q => cnt100_reg(8),
      R => cnt10
    );
\cnt100_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt100_reg[4]_i_1_n_0\,
      CO(3) => \cnt100_reg[8]_i_1_n_0\,
      CO(2) => \cnt100_reg[8]_i_1_n_1\,
      CO(1) => \cnt100_reg[8]_i_1_n_2\,
      CO(0) => \cnt100_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt100_reg[8]_i_1_n_4\,
      O(2) => \cnt100_reg[8]_i_1_n_5\,
      O(1) => \cnt100_reg[8]_i_1_n_6\,
      O(0) => \cnt100_reg[8]_i_1_n_7\,
      S(3 downto 0) => cnt100_reg(11 downto 8)
    );
\cnt100_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[8]_i_1_n_6\,
      Q => cnt100_reg(9),
      R => cnt10
    );
cnt10_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt10,
      O => p_1_in
    );
cnt10_reg: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => '1',
      D => p_1_in,
      Q => cnt10,
      R => '0'
    );
cnt50_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => cnt50_carry_n_0,
      CO(2) => cnt50_carry_n_1,
      CO(1) => cnt50_carry_n_2,
      CO(0) => cnt50_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => B"1111",
      O(3 downto 0) => NLW_cnt50_carry_O_UNCONNECTED(3 downto 0),
      S(3) => cnt50_carry_i_1_n_0,
      S(2) => cnt50_carry_i_2_n_0,
      S(1) => cnt50_carry_i_3_n_0,
      S(0) => cnt50_carry_i_4_n_0
    );
\cnt50_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => cnt50_carry_n_0,
      CO(3 downto 2) => \NLW_cnt50_carry__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => sel,
      CO(0) => \cnt50_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0011",
      O(3 downto 0) => \NLW_cnt50_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \cnt50_carry__0_i_1_n_0\,
      S(0) => \cnt50_carry__0_i_2_n_0\
    );
\cnt50_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"21"
    )
        port map (
      I0 => cnt51(15),
      I1 => cnt51(16),
      I2 => cnt5_reg(15),
      O => \cnt50_carry__0_i_1_n_0\
    );
\cnt50_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => cnt5_reg(13),
      I1 => cnt51(13),
      I2 => cnt5_reg(12),
      I3 => cnt51(12),
      I4 => cnt51(14),
      I5 => cnt5_reg(14),
      O => \cnt50_carry__0_i_2_n_0\
    );
\cnt50_carry__0_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => cnt50_carry_i_5_n_0,
      CO(3) => cnt51(16),
      CO(2) => \NLW_cnt50_carry__0_i_3_CO_UNCONNECTED\(2),
      CO(1) => \cnt50_carry__0_i_3_n_2\,
      CO(0) => \cnt50_carry__0_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_cnt50_carry__0_i_3_O_UNCONNECTED\(3),
      O(2 downto 0) => cnt51(15 downto 13),
      S(3) => '1',
      S(2 downto 0) => FrameSize(15 downto 13)
    );
cnt50_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => cnt5_reg(10),
      I1 => cnt51(10),
      I2 => cnt5_reg(9),
      I3 => cnt51(9),
      I4 => cnt51(11),
      I5 => cnt5_reg(11),
      O => cnt50_carry_i_1_n_0
    );
cnt50_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => cnt5_reg(7),
      I1 => cnt51(7),
      I2 => cnt5_reg(6),
      I3 => cnt51(6),
      I4 => cnt51(8),
      I5 => cnt5_reg(8),
      O => cnt50_carry_i_2_n_0
    );
cnt50_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => cnt5_reg(4),
      I1 => cnt51(4),
      I2 => cnt5_reg(3),
      I3 => cnt51(3),
      I4 => cnt51(5),
      I5 => cnt5_reg(5),
      O => cnt50_carry_i_3_n_0
    );
cnt50_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000900990090000"
    )
        port map (
      I0 => cnt5_reg(1),
      I1 => cnt51(1),
      I2 => cnt5_reg(2),
      I3 => cnt51(2),
      I4 => FrameSize(0),
      I5 => cnt5_reg(0),
      O => cnt50_carry_i_4_n_0
    );
cnt50_carry_i_5: unisim.vcomponents.CARRY4
     port map (
      CI => cnt50_carry_i_6_n_0,
      CO(3) => cnt50_carry_i_5_n_0,
      CO(2) => cnt50_carry_i_5_n_1,
      CO(1) => cnt50_carry_i_5_n_2,
      CO(0) => cnt50_carry_i_5_n_3,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => cnt51(12 downto 9),
      S(3 downto 0) => FrameSize(12 downto 9)
    );
cnt50_carry_i_6: unisim.vcomponents.CARRY4
     port map (
      CI => cnt50_carry_i_7_n_0,
      CO(3) => cnt50_carry_i_6_n_0,
      CO(2) => cnt50_carry_i_6_n_1,
      CO(1) => cnt50_carry_i_6_n_2,
      CO(0) => cnt50_carry_i_6_n_3,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => cnt51(8 downto 5),
      S(3 downto 0) => FrameSize(8 downto 5)
    );
cnt50_carry_i_7: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => cnt50_carry_i_7_n_0,
      CO(2) => cnt50_carry_i_7_n_1,
      CO(1) => cnt50_carry_i_7_n_2,
      CO(0) => cnt50_carry_i_7_n_3,
      CYINIT => FrameSize(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => cnt51(4 downto 1),
      S(3 downto 0) => FrameSize(4 downto 1)
    );
\cnt5[0]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt5_reg(0),
      O => \cnt5[0]_i_2_n_0\
    );
\cnt5_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_5MHz,
      CE => sel,
      D => \cnt5_reg[0]_i_1_n_7\,
      Q => cnt5_reg(0),
      R => clear
    );
\cnt5_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt5_reg[0]_i_1_n_0\,
      CO(2) => \cnt5_reg[0]_i_1_n_1\,
      CO(1) => \cnt5_reg[0]_i_1_n_2\,
      CO(0) => \cnt5_reg[0]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt5_reg[0]_i_1_n_4\,
      O(2) => \cnt5_reg[0]_i_1_n_5\,
      O(1) => \cnt5_reg[0]_i_1_n_6\,
      O(0) => \cnt5_reg[0]_i_1_n_7\,
      S(3 downto 1) => cnt5_reg(3 downto 1),
      S(0) => \cnt5[0]_i_2_n_0\
    );
\cnt5_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_5MHz,
      CE => sel,
      D => \cnt5_reg[8]_i_1_n_5\,
      Q => cnt5_reg(10),
      R => clear
    );
\cnt5_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_5MHz,
      CE => sel,
      D => \cnt5_reg[8]_i_1_n_4\,
      Q => cnt5_reg(11),
      R => clear
    );
\cnt5_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_5MHz,
      CE => sel,
      D => \cnt5_reg[12]_i_1_n_7\,
      Q => cnt5_reg(12),
      R => clear
    );
\cnt5_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt5_reg[8]_i_1_n_0\,
      CO(3) => \NLW_cnt5_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt5_reg[12]_i_1_n_1\,
      CO(1) => \cnt5_reg[12]_i_1_n_2\,
      CO(0) => \cnt5_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt5_reg[12]_i_1_n_4\,
      O(2) => \cnt5_reg[12]_i_1_n_5\,
      O(1) => \cnt5_reg[12]_i_1_n_6\,
      O(0) => \cnt5_reg[12]_i_1_n_7\,
      S(3 downto 0) => cnt5_reg(15 downto 12)
    );
\cnt5_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_5MHz,
      CE => sel,
      D => \cnt5_reg[12]_i_1_n_6\,
      Q => cnt5_reg(13),
      R => clear
    );
\cnt5_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_5MHz,
      CE => sel,
      D => \cnt5_reg[12]_i_1_n_5\,
      Q => cnt5_reg(14),
      R => clear
    );
\cnt5_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_5MHz,
      CE => sel,
      D => \cnt5_reg[12]_i_1_n_4\,
      Q => cnt5_reg(15),
      R => clear
    );
\cnt5_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_5MHz,
      CE => sel,
      D => \cnt5_reg[0]_i_1_n_6\,
      Q => cnt5_reg(1),
      R => clear
    );
\cnt5_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_5MHz,
      CE => sel,
      D => \cnt5_reg[0]_i_1_n_5\,
      Q => cnt5_reg(2),
      R => clear
    );
\cnt5_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_5MHz,
      CE => sel,
      D => \cnt5_reg[0]_i_1_n_4\,
      Q => cnt5_reg(3),
      R => clear
    );
\cnt5_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_5MHz,
      CE => sel,
      D => \cnt5_reg[4]_i_1_n_7\,
      Q => cnt5_reg(4),
      R => clear
    );
\cnt5_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt5_reg[0]_i_1_n_0\,
      CO(3) => \cnt5_reg[4]_i_1_n_0\,
      CO(2) => \cnt5_reg[4]_i_1_n_1\,
      CO(1) => \cnt5_reg[4]_i_1_n_2\,
      CO(0) => \cnt5_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt5_reg[4]_i_1_n_4\,
      O(2) => \cnt5_reg[4]_i_1_n_5\,
      O(1) => \cnt5_reg[4]_i_1_n_6\,
      O(0) => \cnt5_reg[4]_i_1_n_7\,
      S(3 downto 0) => cnt5_reg(7 downto 4)
    );
\cnt5_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_5MHz,
      CE => sel,
      D => \cnt5_reg[4]_i_1_n_6\,
      Q => cnt5_reg(5),
      R => clear
    );
\cnt5_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_5MHz,
      CE => sel,
      D => \cnt5_reg[4]_i_1_n_5\,
      Q => cnt5_reg(6),
      R => clear
    );
\cnt5_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_5MHz,
      CE => sel,
      D => \cnt5_reg[4]_i_1_n_4\,
      Q => cnt5_reg(7),
      R => clear
    );
\cnt5_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_5MHz,
      CE => sel,
      D => \cnt5_reg[8]_i_1_n_7\,
      Q => cnt5_reg(8),
      R => clear
    );
\cnt5_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt5_reg[4]_i_1_n_0\,
      CO(3) => \cnt5_reg[8]_i_1_n_0\,
      CO(2) => \cnt5_reg[8]_i_1_n_1\,
      CO(1) => \cnt5_reg[8]_i_1_n_2\,
      CO(0) => \cnt5_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt5_reg[8]_i_1_n_4\,
      O(2) => \cnt5_reg[8]_i_1_n_5\,
      O(1) => \cnt5_reg[8]_i_1_n_6\,
      O(0) => \cnt5_reg[8]_i_1_n_7\,
      S(3 downto 0) => cnt5_reg(11 downto 8)
    );
\cnt5_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_5MHz,
      CE => sel,
      D => \cnt5_reg[8]_i_1_n_6\,
      Q => cnt5_reg(9),
      R => clear
    );
\data_even_1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF1(0),
      Q => data_even_1(0),
      R => clear
    );
\data_even_1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF1(10),
      Q => data_even_1(10),
      R => clear
    );
\data_even_1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF1(11),
      Q => data_even_1(11),
      R => clear
    );
\data_even_1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF1(12),
      Q => data_even_1(12),
      R => clear
    );
\data_even_1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF1(13),
      Q => data_even_1(13),
      R => clear
    );
\data_even_1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF1(14),
      Q => data_even_1(14),
      R => clear
    );
\data_even_1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF1(15),
      Q => data_even_1(16),
      R => clear
    );
\data_even_1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF1(1),
      Q => data_even_1(1),
      R => clear
    );
\data_even_1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF1(2),
      Q => data_even_1(2),
      R => clear
    );
\data_even_1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF1(3),
      Q => data_even_1(3),
      R => clear
    );
\data_even_1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF1(4),
      Q => data_even_1(4),
      R => clear
    );
\data_even_1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF1(5),
      Q => data_even_1(5),
      R => clear
    );
\data_even_1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF1(6),
      Q => data_even_1(6),
      R => clear
    );
\data_even_1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF1(7),
      Q => data_even_1(7),
      R => clear
    );
\data_even_1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF1(8),
      Q => data_even_1(8),
      R => clear
    );
\data_even_1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF1(9),
      Q => data_even_1(9),
      R => clear
    );
\data_even_2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF2(0),
      Q => data_even_2(0),
      R => clear
    );
\data_even_2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF2(10),
      Q => data_even_2(10),
      R => clear
    );
\data_even_2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF2(11),
      Q => data_even_2(11),
      R => clear
    );
\data_even_2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF2(12),
      Q => data_even_2(12),
      R => clear
    );
\data_even_2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF2(13),
      Q => data_even_2(13),
      R => clear
    );
\data_even_2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF2(14),
      Q => data_even_2(14),
      R => clear
    );
\data_even_2_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF2(15),
      Q => data_even_2(16),
      R => clear
    );
\data_even_2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF2(1),
      Q => data_even_2(1),
      R => clear
    );
\data_even_2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF2(2),
      Q => data_even_2(2),
      R => clear
    );
\data_even_2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF2(3),
      Q => data_even_2(3),
      R => clear
    );
\data_even_2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF2(4),
      Q => data_even_2(4),
      R => clear
    );
\data_even_2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF2(5),
      Q => data_even_2(5),
      R => clear
    );
\data_even_2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF2(6),
      Q => data_even_2(6),
      R => clear
    );
\data_even_2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF2(7),
      Q => data_even_2(7),
      R => clear
    );
\data_even_2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF2(8),
      Q => data_even_2(8),
      R => clear
    );
\data_even_2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => p_1_in,
      D => data_in_IF2(9),
      Q => data_even_2(9),
      R => clear
    );
\data_sum_1[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_1(11),
      I1 => data_in_IF1(11),
      O => \data_sum_1[11]_i_2_n_0\
    );
\data_sum_1[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_1(10),
      I1 => data_in_IF1(10),
      O => \data_sum_1[11]_i_3_n_0\
    );
\data_sum_1[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_1(9),
      I1 => data_in_IF1(9),
      O => \data_sum_1[11]_i_4_n_0\
    );
\data_sum_1[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_1(8),
      I1 => data_in_IF1(8),
      O => \data_sum_1[11]_i_5_n_0\
    );
\data_sum_1[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_in_IF1(15),
      I1 => data_even_1(16),
      O => \data_sum_1[15]_i_2_n_0\
    );
\data_sum_1[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_1(14),
      I1 => data_in_IF1(14),
      O => \data_sum_1[15]_i_3_n_0\
    );
\data_sum_1[15]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_1(13),
      I1 => data_in_IF1(13),
      O => \data_sum_1[15]_i_4_n_0\
    );
\data_sum_1[15]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_1(12),
      I1 => data_in_IF1(12),
      O => \data_sum_1[15]_i_5_n_0\
    );
\data_sum_1[16]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_in_IF1(15),
      I1 => data_even_1(16),
      O => \data_sum_1[16]_i_2_n_0\
    );
\data_sum_1[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_1(3),
      I1 => data_in_IF1(3),
      O => \data_sum_1[3]_i_2_n_0\
    );
\data_sum_1[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_1(2),
      I1 => data_in_IF1(2),
      O => \data_sum_1[3]_i_3_n_0\
    );
\data_sum_1[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_1(1),
      I1 => data_in_IF1(1),
      O => \data_sum_1[3]_i_4_n_0\
    );
\data_sum_1[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_1(0),
      I1 => data_in_IF1(0),
      O => \data_sum_1[3]_i_5_n_0\
    );
\data_sum_1[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_1(7),
      I1 => data_in_IF1(7),
      O => \data_sum_1[7]_i_2_n_0\
    );
\data_sum_1[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_1(6),
      I1 => data_in_IF1(6),
      O => \data_sum_1[7]_i_3_n_0\
    );
\data_sum_1[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_1(5),
      I1 => data_in_IF1(5),
      O => \data_sum_1[7]_i_4_n_0\
    );
\data_sum_1[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_1(4),
      I1 => data_in_IF1(4),
      O => \data_sum_1[7]_i_5_n_0\
    );
\data_sum_1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_10(0),
      Q => m00_axis_tdata(0),
      R => clear
    );
\data_sum_1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_10(10),
      Q => m00_axis_tdata(10),
      R => clear
    );
\data_sum_1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_10(11),
      Q => m00_axis_tdata(11),
      R => clear
    );
\data_sum_1_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \data_sum_1_reg[7]_i_1_n_0\,
      CO(3) => \data_sum_1_reg[11]_i_1_n_0\,
      CO(2) => \data_sum_1_reg[11]_i_1_n_1\,
      CO(1) => \data_sum_1_reg[11]_i_1_n_2\,
      CO(0) => \data_sum_1_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => data_even_1(11 downto 8),
      O(3 downto 0) => data_sum_10(11 downto 8),
      S(3) => \data_sum_1[11]_i_2_n_0\,
      S(2) => \data_sum_1[11]_i_3_n_0\,
      S(1) => \data_sum_1[11]_i_4_n_0\,
      S(0) => \data_sum_1[11]_i_5_n_0\
    );
\data_sum_1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_10(12),
      Q => m00_axis_tdata(12),
      R => clear
    );
\data_sum_1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_10(13),
      Q => m00_axis_tdata(13),
      R => clear
    );
\data_sum_1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_10(14),
      Q => m00_axis_tdata(14),
      R => clear
    );
\data_sum_1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_10(15),
      Q => m00_axis_tdata(15),
      R => clear
    );
\data_sum_1_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \data_sum_1_reg[11]_i_1_n_0\,
      CO(3) => \data_sum_1_reg[15]_i_1_n_0\,
      CO(2) => \data_sum_1_reg[15]_i_1_n_1\,
      CO(1) => \data_sum_1_reg[15]_i_1_n_2\,
      CO(0) => \data_sum_1_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => data_in_IF1(15),
      DI(2 downto 0) => data_even_1(14 downto 12),
      O(3 downto 0) => data_sum_10(15 downto 12),
      S(3) => \data_sum_1[15]_i_2_n_0\,
      S(2) => \data_sum_1[15]_i_3_n_0\,
      S(1) => \data_sum_1[15]_i_4_n_0\,
      S(0) => \data_sum_1[15]_i_5_n_0\
    );
\data_sum_1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_10(16),
      Q => m00_axis_tdata(16),
      R => clear
    );
\data_sum_1_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \data_sum_1_reg[15]_i_1_n_0\,
      CO(3 downto 0) => \NLW_data_sum_1_reg[16]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_data_sum_1_reg[16]_i_1_O_UNCONNECTED\(3 downto 1),
      O(0) => data_sum_10(16),
      S(3 downto 1) => B"000",
      S(0) => \data_sum_1[16]_i_2_n_0\
    );
\data_sum_1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_10(1),
      Q => m00_axis_tdata(1),
      R => clear
    );
\data_sum_1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_10(2),
      Q => m00_axis_tdata(2),
      R => clear
    );
\data_sum_1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_10(3),
      Q => m00_axis_tdata(3),
      R => clear
    );
\data_sum_1_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \data_sum_1_reg[3]_i_1_n_0\,
      CO(2) => \data_sum_1_reg[3]_i_1_n_1\,
      CO(1) => \data_sum_1_reg[3]_i_1_n_2\,
      CO(0) => \data_sum_1_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => data_even_1(3 downto 0),
      O(3 downto 0) => data_sum_10(3 downto 0),
      S(3) => \data_sum_1[3]_i_2_n_0\,
      S(2) => \data_sum_1[3]_i_3_n_0\,
      S(1) => \data_sum_1[3]_i_4_n_0\,
      S(0) => \data_sum_1[3]_i_5_n_0\
    );
\data_sum_1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_10(4),
      Q => m00_axis_tdata(4),
      R => clear
    );
\data_sum_1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_10(5),
      Q => m00_axis_tdata(5),
      R => clear
    );
\data_sum_1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_10(6),
      Q => m00_axis_tdata(6),
      R => clear
    );
\data_sum_1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_10(7),
      Q => m00_axis_tdata(7),
      R => clear
    );
\data_sum_1_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \data_sum_1_reg[3]_i_1_n_0\,
      CO(3) => \data_sum_1_reg[7]_i_1_n_0\,
      CO(2) => \data_sum_1_reg[7]_i_1_n_1\,
      CO(1) => \data_sum_1_reg[7]_i_1_n_2\,
      CO(0) => \data_sum_1_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => data_even_1(7 downto 4),
      O(3 downto 0) => data_sum_10(7 downto 4),
      S(3) => \data_sum_1[7]_i_2_n_0\,
      S(2) => \data_sum_1[7]_i_3_n_0\,
      S(1) => \data_sum_1[7]_i_4_n_0\,
      S(0) => \data_sum_1[7]_i_5_n_0\
    );
\data_sum_1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_10(8),
      Q => m00_axis_tdata(8),
      R => clear
    );
\data_sum_1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_10(9),
      Q => m00_axis_tdata(9),
      R => clear
    );
\data_sum_2[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_2(11),
      I1 => data_in_IF2(11),
      O => \data_sum_2[11]_i_2_n_0\
    );
\data_sum_2[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_2(10),
      I1 => data_in_IF2(10),
      O => \data_sum_2[11]_i_3_n_0\
    );
\data_sum_2[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_2(9),
      I1 => data_in_IF2(9),
      O => \data_sum_2[11]_i_4_n_0\
    );
\data_sum_2[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_2(8),
      I1 => data_in_IF2(8),
      O => \data_sum_2[11]_i_5_n_0\
    );
\data_sum_2[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_in_IF2(15),
      I1 => data_even_2(16),
      O => \data_sum_2[15]_i_2_n_0\
    );
\data_sum_2[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_2(14),
      I1 => data_in_IF2(14),
      O => \data_sum_2[15]_i_3_n_0\
    );
\data_sum_2[15]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_2(13),
      I1 => data_in_IF2(13),
      O => \data_sum_2[15]_i_4_n_0\
    );
\data_sum_2[15]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_2(12),
      I1 => data_in_IF2(12),
      O => \data_sum_2[15]_i_5_n_0\
    );
\data_sum_2[16]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => m00_axis_aresetn,
      O => clear
    );
\data_sum_2[16]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_in_IF2(15),
      I1 => data_even_2(16),
      O => \data_sum_2[16]_i_3_n_0\
    );
\data_sum_2[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_2(3),
      I1 => data_in_IF2(3),
      O => \data_sum_2[3]_i_2_n_0\
    );
\data_sum_2[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_2(2),
      I1 => data_in_IF2(2),
      O => \data_sum_2[3]_i_3_n_0\
    );
\data_sum_2[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_2(1),
      I1 => data_in_IF2(1),
      O => \data_sum_2[3]_i_4_n_0\
    );
\data_sum_2[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_2(0),
      I1 => data_in_IF2(0),
      O => \data_sum_2[3]_i_5_n_0\
    );
\data_sum_2[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_2(7),
      I1 => data_in_IF2(7),
      O => \data_sum_2[7]_i_2_n_0\
    );
\data_sum_2[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_2(6),
      I1 => data_in_IF2(6),
      O => \data_sum_2[7]_i_3_n_0\
    );
\data_sum_2[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_2(5),
      I1 => data_in_IF2(5),
      O => \data_sum_2[7]_i_4_n_0\
    );
\data_sum_2[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => data_even_2(4),
      I1 => data_in_IF2(4),
      O => \data_sum_2[7]_i_5_n_0\
    );
\data_sum_2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_20(0),
      Q => m00_axis_tdata(17),
      R => clear
    );
\data_sum_2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_20(10),
      Q => m00_axis_tdata(27),
      R => clear
    );
\data_sum_2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_20(11),
      Q => m00_axis_tdata(28),
      R => clear
    );
\data_sum_2_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \data_sum_2_reg[7]_i_1_n_0\,
      CO(3) => \data_sum_2_reg[11]_i_1_n_0\,
      CO(2) => \data_sum_2_reg[11]_i_1_n_1\,
      CO(1) => \data_sum_2_reg[11]_i_1_n_2\,
      CO(0) => \data_sum_2_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => data_even_2(11 downto 8),
      O(3 downto 0) => data_sum_20(11 downto 8),
      S(3) => \data_sum_2[11]_i_2_n_0\,
      S(2) => \data_sum_2[11]_i_3_n_0\,
      S(1) => \data_sum_2[11]_i_4_n_0\,
      S(0) => \data_sum_2[11]_i_5_n_0\
    );
\data_sum_2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_20(12),
      Q => m00_axis_tdata(29),
      R => clear
    );
\data_sum_2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_20(13),
      Q => m00_axis_tdata(30),
      R => clear
    );
\data_sum_2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_20(14),
      Q => m00_axis_tdata(31),
      R => clear
    );
\data_sum_2_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_20(15),
      Q => m00_axis_tdata(32),
      R => clear
    );
\data_sum_2_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \data_sum_2_reg[11]_i_1_n_0\,
      CO(3) => \data_sum_2_reg[15]_i_1_n_0\,
      CO(2) => \data_sum_2_reg[15]_i_1_n_1\,
      CO(1) => \data_sum_2_reg[15]_i_1_n_2\,
      CO(0) => \data_sum_2_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => data_in_IF2(15),
      DI(2 downto 0) => data_even_2(14 downto 12),
      O(3 downto 0) => data_sum_20(15 downto 12),
      S(3) => \data_sum_2[15]_i_2_n_0\,
      S(2) => \data_sum_2[15]_i_3_n_0\,
      S(1) => \data_sum_2[15]_i_4_n_0\,
      S(0) => \data_sum_2[15]_i_5_n_0\
    );
\data_sum_2_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_20(16),
      Q => m00_axis_tdata(33),
      R => clear
    );
\data_sum_2_reg[16]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \data_sum_2_reg[15]_i_1_n_0\,
      CO(3 downto 0) => \NLW_data_sum_2_reg[16]_i_2_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_data_sum_2_reg[16]_i_2_O_UNCONNECTED\(3 downto 1),
      O(0) => data_sum_20(16),
      S(3 downto 1) => B"000",
      S(0) => \data_sum_2[16]_i_3_n_0\
    );
\data_sum_2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_20(1),
      Q => m00_axis_tdata(18),
      R => clear
    );
\data_sum_2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_20(2),
      Q => m00_axis_tdata(19),
      R => clear
    );
\data_sum_2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_20(3),
      Q => m00_axis_tdata(20),
      R => clear
    );
\data_sum_2_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \data_sum_2_reg[3]_i_1_n_0\,
      CO(2) => \data_sum_2_reg[3]_i_1_n_1\,
      CO(1) => \data_sum_2_reg[3]_i_1_n_2\,
      CO(0) => \data_sum_2_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => data_even_2(3 downto 0),
      O(3 downto 0) => data_sum_20(3 downto 0),
      S(3) => \data_sum_2[3]_i_2_n_0\,
      S(2) => \data_sum_2[3]_i_3_n_0\,
      S(1) => \data_sum_2[3]_i_4_n_0\,
      S(0) => \data_sum_2[3]_i_5_n_0\
    );
\data_sum_2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_20(4),
      Q => m00_axis_tdata(21),
      R => clear
    );
\data_sum_2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_20(5),
      Q => m00_axis_tdata(22),
      R => clear
    );
\data_sum_2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_20(6),
      Q => m00_axis_tdata(23),
      R => clear
    );
\data_sum_2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_20(7),
      Q => m00_axis_tdata(24),
      R => clear
    );
\data_sum_2_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \data_sum_2_reg[3]_i_1_n_0\,
      CO(3) => \data_sum_2_reg[7]_i_1_n_0\,
      CO(2) => \data_sum_2_reg[7]_i_1_n_1\,
      CO(1) => \data_sum_2_reg[7]_i_1_n_2\,
      CO(0) => \data_sum_2_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => data_even_2(7 downto 4),
      O(3 downto 0) => data_sum_20(7 downto 4),
      S(3) => \data_sum_2[7]_i_2_n_0\,
      S(2) => \data_sum_2[7]_i_3_n_0\,
      S(1) => \data_sum_2[7]_i_4_n_0\,
      S(0) => \data_sum_2[7]_i_5_n_0\
    );
\data_sum_2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_20(8),
      Q => m00_axis_tdata(25),
      R => clear
    );
\data_sum_2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => data_clk,
      CE => cnt10,
      D => data_sum_20(9),
      Q => m00_axis_tdata(26),
      R => clear
    );
\i__carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => FrameSize(14),
      I1 => cnt5_reg(14),
      I2 => cnt5_reg(15),
      I3 => FrameSize(15),
      O => \i__carry__0_i_1_n_0\
    );
\i__carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => FrameSize(12),
      I1 => cnt5_reg(12),
      I2 => cnt5_reg(13),
      I3 => FrameSize(13),
      O => \i__carry__0_i_2_n_0\
    );
\i__carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => FrameSize(10),
      I1 => cnt5_reg(10),
      I2 => cnt5_reg(11),
      I3 => FrameSize(11),
      O => \i__carry__0_i_3_n_0\
    );
\i__carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => FrameSize(8),
      I1 => cnt5_reg(8),
      I2 => cnt5_reg(9),
      I3 => FrameSize(9),
      O => \i__carry__0_i_4_n_0\
    );
\i__carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => cnt5_reg(14),
      I1 => FrameSize(14),
      I2 => FrameSize(15),
      I3 => cnt5_reg(15),
      O => \i__carry__0_i_5_n_0\
    );
\i__carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => cnt5_reg(12),
      I1 => FrameSize(12),
      I2 => cnt5_reg(13),
      I3 => FrameSize(13),
      O => \i__carry__0_i_6_n_0\
    );
\i__carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => cnt5_reg(10),
      I1 => FrameSize(10),
      I2 => cnt5_reg(11),
      I3 => FrameSize(11),
      O => \i__carry__0_i_7_n_0\
    );
\i__carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => cnt5_reg(8),
      I1 => FrameSize(8),
      I2 => cnt5_reg(9),
      I3 => FrameSize(9),
      O => \i__carry__0_i_8_n_0\
    );
\i__carry_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => FrameSize(6),
      I1 => cnt5_reg(6),
      I2 => cnt5_reg(7),
      I3 => FrameSize(7),
      O => \i__carry_i_1_n_0\
    );
\i__carry_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => FrameSize(4),
      I1 => cnt5_reg(4),
      I2 => cnt5_reg(5),
      I3 => FrameSize(5),
      O => \i__carry_i_2_n_0\
    );
\i__carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => FrameSize(2),
      I1 => cnt5_reg(2),
      I2 => cnt5_reg(3),
      I3 => FrameSize(3),
      O => \i__carry_i_3_n_0\
    );
\i__carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => FrameSize(0),
      I1 => cnt5_reg(0),
      I2 => cnt5_reg(1),
      I3 => FrameSize(1),
      O => \i__carry_i_4_n_0\
    );
\i__carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => cnt5_reg(6),
      I1 => FrameSize(6),
      I2 => cnt5_reg(7),
      I3 => FrameSize(7),
      O => \i__carry_i_5_n_0\
    );
\i__carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => cnt5_reg(4),
      I1 => FrameSize(4),
      I2 => cnt5_reg(5),
      I3 => FrameSize(5),
      O => \i__carry_i_6_n_0\
    );
\i__carry_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => cnt5_reg(2),
      I1 => FrameSize(2),
      I2 => cnt5_reg(3),
      I3 => FrameSize(3),
      O => \i__carry_i_7_n_0\
    );
\i__carry_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => FrameSize(0),
      I1 => cnt5_reg(0),
      I2 => cnt5_reg(1),
      I3 => FrameSize(1),
      O => \i__carry_i_8_n_0\
    );
m00_axis_tlast_r0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => m00_axis_tlast_r0_carry_n_0,
      CO(2) => m00_axis_tlast_r0_carry_n_1,
      CO(1) => m00_axis_tlast_r0_carry_n_2,
      CO(0) => m00_axis_tlast_r0_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_m00_axis_tlast_r0_carry_O_UNCONNECTED(3 downto 0),
      S(3) => m00_axis_tlast_r0_carry_i_1_n_0,
      S(2) => m00_axis_tlast_r0_carry_i_2_n_0,
      S(1) => m00_axis_tlast_r0_carry_i_3_n_0,
      S(0) => m00_axis_tlast_r0_carry_i_4_n_0
    );
\m00_axis_tlast_r0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => m00_axis_tlast_r0_carry_n_0,
      CO(3 downto 2) => \NLW_m00_axis_tlast_r0_carry__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => m00_axis_tlast_r0,
      CO(0) => \m00_axis_tlast_r0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_m00_axis_tlast_r0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \m00_axis_tlast_r0_carry__0_i_1_n_0\,
      S(0) => \m00_axis_tlast_r0_carry__0_i_2_n_0\
    );
\m00_axis_tlast_r0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => FrameSize(15),
      I1 => cnt5_reg(15),
      O => \m00_axis_tlast_r0_carry__0_i_1_n_0\
    );
\m00_axis_tlast_r0_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => FrameSize(14),
      I1 => cnt5_reg(14),
      I2 => FrameSize(13),
      I3 => cnt5_reg(13),
      I4 => cnt5_reg(12),
      I5 => FrameSize(12),
      O => \m00_axis_tlast_r0_carry__0_i_2_n_0\
    );
m00_axis_tlast_r0_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => FrameSize(11),
      I1 => cnt5_reg(11),
      I2 => FrameSize(10),
      I3 => cnt5_reg(10),
      I4 => cnt5_reg(9),
      I5 => FrameSize(9),
      O => m00_axis_tlast_r0_carry_i_1_n_0
    );
m00_axis_tlast_r0_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => FrameSize(8),
      I1 => cnt5_reg(8),
      I2 => FrameSize(7),
      I3 => cnt5_reg(7),
      I4 => cnt5_reg(6),
      I5 => FrameSize(6),
      O => m00_axis_tlast_r0_carry_i_2_n_0
    );
m00_axis_tlast_r0_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => FrameSize(5),
      I1 => cnt5_reg(5),
      I2 => FrameSize(4),
      I3 => cnt5_reg(4),
      I4 => cnt5_reg(3),
      I5 => FrameSize(3),
      O => m00_axis_tlast_r0_carry_i_3_n_0
    );
m00_axis_tlast_r0_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => FrameSize(2),
      I1 => cnt5_reg(2),
      I2 => FrameSize(1),
      I3 => cnt5_reg(1),
      I4 => FrameSize(0),
      I5 => cnt5_reg(0),
      O => m00_axis_tlast_r0_carry_i_4_n_0
    );
m00_axis_tlast_r_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => m00_axis_tlast_r0,
      I1 => \^m00_axis_tlast\,
      I2 => m00_axis_tvalid_r13_out,
      O => m00_axis_tlast_r_i_1_n_0
    );
m00_axis_tlast_r_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4000000000000000"
    )
        port map (
      I0 => cnt100_reg(1),
      I1 => cnt100_reg(0),
      I2 => m00_axis_aresetn,
      I3 => m00_axis_tlast_r_i_3_n_0,
      I4 => m00_axis_tlast_r_i_4_n_0,
      I5 => m00_axis_tlast_r_i_5_n_0,
      O => m00_axis_tvalid_r13_out
    );
m00_axis_tlast_r_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => cnt100_reg(5),
      I1 => cnt100_reg(4),
      I2 => cnt100_reg(3),
      I3 => cnt100_reg(2),
      O => m00_axis_tlast_r_i_3_n_0
    );
m00_axis_tlast_r_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => cnt100_reg(9),
      I1 => cnt100_reg(8),
      I2 => cnt100_reg(7),
      I3 => cnt100_reg(6),
      O => m00_axis_tlast_r_i_4_n_0
    );
m00_axis_tlast_r_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => cnt100_reg(10),
      I1 => cnt100_reg(11),
      I2 => cnt100_reg(12),
      I3 => cnt100_reg(13),
      I4 => cnt100_reg(15),
      I5 => cnt100_reg(14),
      O => m00_axis_tlast_r_i_5_n_0
    );
m00_axis_tlast_r_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => m00_axis_tlast_r_i_1_n_0,
      Q => \^m00_axis_tlast\,
      R => '0'
    );
\m00_axis_tvalid_r1_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \m00_axis_tvalid_r1_inferred__0/i__carry_n_0\,
      CO(2) => \m00_axis_tvalid_r1_inferred__0/i__carry_n_1\,
      CO(1) => \m00_axis_tvalid_r1_inferred__0/i__carry_n_2\,
      CO(0) => \m00_axis_tvalid_r1_inferred__0/i__carry_n_3\,
      CYINIT => '1',
      DI(3) => \i__carry_i_1_n_0\,
      DI(2) => \i__carry_i_2_n_0\,
      DI(1) => \i__carry_i_3_n_0\,
      DI(0) => \i__carry_i_4_n_0\,
      O(3 downto 0) => \NLW_m00_axis_tvalid_r1_inferred__0/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry_i_5_n_0\,
      S(2) => \i__carry_i_6_n_0\,
      S(1) => \i__carry_i_7_n_0\,
      S(0) => \i__carry_i_8_n_0\
    );
\m00_axis_tvalid_r1_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \m00_axis_tvalid_r1_inferred__0/i__carry_n_0\,
      CO(3) => m00_axis_tvalid_r1,
      CO(2) => \m00_axis_tvalid_r1_inferred__0/i__carry__0_n_1\,
      CO(1) => \m00_axis_tvalid_r1_inferred__0/i__carry__0_n_2\,
      CO(0) => \m00_axis_tvalid_r1_inferred__0/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \i__carry__0_i_1_n_0\,
      DI(2) => \i__carry__0_i_2_n_0\,
      DI(1) => \i__carry__0_i_3_n_0\,
      DI(0) => \i__carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_m00_axis_tvalid_r1_inferred__0/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry__0_i_5_n_0\,
      S(2) => \i__carry__0_i_6_n_0\,
      S(1) => \i__carry__0_i_7_n_0\,
      S(0) => \i__carry__0_i_8_n_0\
    );
m00_axis_tvalid_r_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE00000"
    )
        port map (
      I0 => m00_axis_tvalid_r_i_2_n_0,
      I1 => m00_axis_tvalid_r_i_3_n_0,
      I2 => m00_axis_tvalid_r1,
      I3 => \^m00_axis_tvalid\,
      I4 => m00_axis_tvalid_r13_out,
      O => m00_axis_tvalid_r_i_1_n_0
    );
m00_axis_tvalid_r_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => cnt5_reg(5),
      I1 => cnt5_reg(6),
      I2 => cnt5_reg(7),
      I3 => cnt5_reg(8),
      I4 => m00_axis_tvalid_r_i_4_n_0,
      O => m00_axis_tvalid_r_i_2_n_0
    );
m00_axis_tvalid_r_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => cnt5_reg(13),
      I1 => cnt5_reg(14),
      I2 => cnt5_reg(15),
      I3 => cnt5_reg(0),
      I4 => m00_axis_tvalid_r_i_5_n_0,
      O => m00_axis_tvalid_r_i_3_n_0
    );
m00_axis_tvalid_r_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt5_reg(4),
      I1 => cnt5_reg(3),
      I2 => cnt5_reg(2),
      I3 => cnt5_reg(1),
      O => m00_axis_tvalid_r_i_4_n_0
    );
m00_axis_tvalid_r_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt5_reg(12),
      I1 => cnt5_reg(11),
      I2 => cnt5_reg(10),
      I3 => cnt5_reg(9),
      O => m00_axis_tvalid_r_i_5_n_0
    );
m00_axis_tvalid_r_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => m00_axis_tvalid_r_i_1_n_0,
      Q => \^m00_axis_tvalid\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_clk : in STD_LOGIC;
    clk_5MHz : in STD_LOGIC;
    data_in_IF1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_in_IF2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 47 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 5 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_config_tvalid : out STD_LOGIC;
    m00_axis_config_tdata : out STD_LOGIC_VECTOR ( 47 downto 0 );
    m00_axis_config_tready : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_Sample_Generator_0_0,Sample_Generator_v3_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "Sample_Generator_v3_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^m00_axis_tdata\ : STD_LOGIC_VECTOR ( 40 downto 0 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of data_clk : signal is "xilinx.com:signal:clock:1.0 data_clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of data_clk : signal is "XIL_INTERFACENAME data_clk, FREQ_HZ 10000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_2_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK";
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_config_tready : signal is "xilinx.com:interface:axis:1.0 M_AXIS_CONFIG TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_config_tready : signal is "XIL_INTERFACENAME M_AXIS_CONFIG, TDATA_NUM_BYTES 6, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_config_tvalid : signal is "xilinx.com:interface:axis:1.0 M_AXIS_CONFIG TVALID";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 6, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN /clk_wiz_1_clk_out1, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TVALID";
  attribute X_INTERFACE_INFO of m00_axis_config_tdata : signal is "xilinx.com:interface:axis:1.0 M_AXIS_CONFIG TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB";
begin
  m00_axis_config_tdata(47) <= \<const0>\;
  m00_axis_config_tdata(46) <= \<const0>\;
  m00_axis_config_tdata(45) <= \<const0>\;
  m00_axis_config_tdata(44) <= \<const0>\;
  m00_axis_config_tdata(43) <= \<const0>\;
  m00_axis_config_tdata(42) <= \<const0>\;
  m00_axis_config_tdata(41) <= \<const0>\;
  m00_axis_config_tdata(40) <= \<const0>\;
  m00_axis_config_tdata(39) <= \<const0>\;
  m00_axis_config_tdata(38) <= \<const0>\;
  m00_axis_config_tdata(37) <= \<const0>\;
  m00_axis_config_tdata(36) <= \<const0>\;
  m00_axis_config_tdata(35) <= \<const0>\;
  m00_axis_config_tdata(34) <= \<const0>\;
  m00_axis_config_tdata(33) <= \<const0>\;
  m00_axis_config_tdata(32) <= \<const0>\;
  m00_axis_config_tdata(31) <= \<const0>\;
  m00_axis_config_tdata(30) <= \<const0>\;
  m00_axis_config_tdata(29) <= \<const0>\;
  m00_axis_config_tdata(28) <= \<const0>\;
  m00_axis_config_tdata(27) <= \<const0>\;
  m00_axis_config_tdata(26) <= \<const0>\;
  m00_axis_config_tdata(25) <= \<const0>\;
  m00_axis_config_tdata(24) <= \<const0>\;
  m00_axis_config_tdata(23) <= \<const0>\;
  m00_axis_config_tdata(22) <= \<const0>\;
  m00_axis_config_tdata(21) <= \<const0>\;
  m00_axis_config_tdata(20) <= \<const0>\;
  m00_axis_config_tdata(19) <= \<const0>\;
  m00_axis_config_tdata(18) <= \<const0>\;
  m00_axis_config_tdata(17) <= \<const0>\;
  m00_axis_config_tdata(16) <= \<const0>\;
  m00_axis_config_tdata(15) <= \<const0>\;
  m00_axis_config_tdata(14) <= \<const0>\;
  m00_axis_config_tdata(13) <= \<const0>\;
  m00_axis_config_tdata(12) <= \<const0>\;
  m00_axis_config_tdata(11) <= \<const0>\;
  m00_axis_config_tdata(10) <= \<const0>\;
  m00_axis_config_tdata(9) <= \<const0>\;
  m00_axis_config_tdata(8) <= \<const0>\;
  m00_axis_config_tdata(7) <= \<const0>\;
  m00_axis_config_tdata(6) <= \<const0>\;
  m00_axis_config_tdata(5) <= \<const0>\;
  m00_axis_config_tdata(4) <= \<const0>\;
  m00_axis_config_tdata(3) <= \<const0>\;
  m00_axis_config_tdata(2) <= \<const0>\;
  m00_axis_config_tdata(1) <= \<const0>\;
  m00_axis_config_tdata(0) <= \<const0>\;
  m00_axis_config_tvalid <= \<const0>\;
  m00_axis_tdata(47) <= \<const0>\;
  m00_axis_tdata(46) <= \<const0>\;
  m00_axis_tdata(45) <= \<const0>\;
  m00_axis_tdata(44) <= \<const0>\;
  m00_axis_tdata(43) <= \<const0>\;
  m00_axis_tdata(42) <= \<const0>\;
  m00_axis_tdata(41) <= \<const0>\;
  m00_axis_tdata(40 downto 24) <= \^m00_axis_tdata\(40 downto 24);
  m00_axis_tdata(23) <= \<const0>\;
  m00_axis_tdata(22) <= \<const0>\;
  m00_axis_tdata(21) <= \<const0>\;
  m00_axis_tdata(20) <= \<const0>\;
  m00_axis_tdata(19) <= \<const0>\;
  m00_axis_tdata(18) <= \<const0>\;
  m00_axis_tdata(17) <= \<const0>\;
  m00_axis_tdata(16 downto 0) <= \^m00_axis_tdata\(16 downto 0);
  m00_axis_tstrb(5) <= \<const0>\;
  m00_axis_tstrb(4) <= \<const0>\;
  m00_axis_tstrb(3) <= \<const1>\;
  m00_axis_tstrb(2) <= \<const1>\;
  m00_axis_tstrb(1) <= \<const1>\;
  m00_axis_tstrb(0) <= \<const1>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Sample_Generator_v3_0
     port map (
      FrameSize(15 downto 0) => FrameSize(15 downto 0),
      clk_5MHz => clk_5MHz,
      data_clk => data_clk,
      data_in_IF1(15 downto 0) => data_in_IF1(15 downto 0),
      data_in_IF2(15 downto 0) => data_in_IF2(15 downto 0),
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_aresetn => m00_axis_aresetn,
      m00_axis_tdata(33 downto 17) => \^m00_axis_tdata\(40 downto 24),
      m00_axis_tdata(16 downto 0) => \^m00_axis_tdata\(16 downto 0),
      m00_axis_tlast => m00_axis_tlast,
      m00_axis_tvalid => m00_axis_tvalid
    );
end STRUCTURE;
