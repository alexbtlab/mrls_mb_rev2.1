// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Fri Sep 25 18:28:29 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_VETAL_0_0_stub.v
// Design      : design_1_ADC_VETAL_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "ADC_VETAL,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(fpga_clk, clk_200, clk_100MHz, fpga_clk_inv, 
  adc_data, adc_dax_p, adc_dax_n, adc_dbx_p, adc_dbx_n, adc_dcox_p, adc_dcox_n, adc_clkx_p, 
  adc_clkx_n)
/* synthesis syn_black_box black_box_pad_pin="fpga_clk,clk_200,clk_100MHz,fpga_clk_inv,adc_data[15:0],adc_dax_p,adc_dax_n,adc_dbx_p,adc_dbx_n,adc_dcox_p,adc_dcox_n,adc_clkx_p,adc_clkx_n" */;
  input fpga_clk;
  input clk_200;
  input clk_100MHz;
  output fpga_clk_inv;
  output [15:0]adc_data;
  input adc_dax_p;
  input adc_dax_n;
  input adc_dbx_p;
  input adc_dbx_n;
  input adc_dcox_p;
  input adc_dcox_n;
  output adc_clkx_p;
  output adc_clkx_n;
endmodule
