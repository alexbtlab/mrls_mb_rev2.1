`timescale 1 ns / 1 ps

module Sample_Generator_v3_0 #
(
parameter integer C_M00_AXIS_TDATA_WIDTH = 32,
parameter integer C_M00_AXIS_START_COUNT = 32
)
(
    input  wire         data_clk,
    input  wire [15:0]  data_in_IF1,
    input  wire [15:0]  data_in_IF2,
    
    //input  wire [15:0]  val_set_valid,
    
    input  wire         m00_axis_aresetn,
    output wire         m00_axis_tvalid,
    output wire         reset_constrict,
    output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0]     m00_axis_tdata,
    output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
    output wire         m00_axis_tlast,
    input  wire         m00_axis_tready,
    input  wire         m00_axis_aclk,
    output wire         m00_axis_config_tvalid,
    output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_config_tdata,
    input  wire         m00_axis_config_tready
);


localparam FrameSize = 8192;
   reg m00_axis_tvalid_r;
assign m00_axis_tvalid = m00_axis_tvalid_r;
   reg m00_axis_tlast_r;
assign m00_axis_tlast = m00_axis_tlast_r;

reg [3:0] cnt100;
reg [15:0] cnt_10;  
reg [15:0] data_sum_1;
reg [15:0] data_sum_2;
reg reset_cnt;
reg reset_inner;
assign reset_constrict = reset_inner;
//assign m00_axis_tdata = {data_sum_2, data_sum_1};
reg [31 : 0] m00_axis_tdata_r;
assign m00_axis_tdata = m00_axis_tdata_r;
assign m00_axis_tstrb  = 4'hF;
assign m00_axis_config_tdata  = 16'h0;
assign m00_axis_config_tvalid  = 1'h0;

    /*-------------------------------------------------------------------------------------------------------------*/
    always @ (posedge data_clk) begin
        if(reset_inner) begin
                data_sum_1 <= {data_in_IF1};
                data_sum_2 <= {data_in_IF2};
                if(cnt_10 == FrameSize+1)   cnt_10 <= FrameSize+1;
                else                        cnt_10 <= cnt_10 + 1;
        end
        else begin
           data_sum_1 <= 0;
           data_sum_2 <= 0;
           cnt_10 <= 0;
        end  
//        if(m00_axis_aresetn)       reset_inner <= 1;
//        else                       reset_inner <= 0;
    end
    always @ (posedge m00_axis_aclk) begin
        if(m00_axis_aresetn)       reset_inner <= 1;
        else                       reset_inner <= 0;
    end
    /*-------------------------------------------------------------------------------------------------------------*/
//    always @ (negedge data_clk) begin
//      if(reset_inner) begin
//            if(cnt_10 == FrameSize+1)   cnt_10 <= FrameSize+1;
//            else                        cnt_10 <= cnt_10 + 1;
//      end
//      else begin
//        cnt_10 <= 0;
//      end
//    end
    /*-------------------------------------------------------------------------------------------------------------*/
    localparam val_set_valid = 3;
    always @ (posedge m00_axis_aclk) begin
    
        m00_axis_tdata_r <= {data_sum_2, data_sum_1};
        
        if(~data_clk) begin
            cnt100 <= cnt100 + 1;
            if(cnt100 == 0)       reset_cnt <= 1;
            else                  reset_cnt <= 0;
        end
        else begin
            reset_cnt <= 1;
            cnt100 <= 0;
        end
        if(cnt100 == val_set_valid & reset_inner & (cnt_10 >= 0) & (cnt_10 <= FrameSize))                m00_axis_tvalid_r <= 1;
        else                                                                                             m00_axis_tvalid_r <= 0;
        if(cnt100 == val_set_valid & (cnt_10 == FrameSize) & reset_inner)                                m00_axis_tlast_r <= 1;
        else                                                                                             m00_axis_tlast_r <= 0;      
    end
    endmodule