//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Tue Oct 12 12:17:00 2021
//Host        : zl-04 running 64-bit major release  (build 9200)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (ADC_MODE_PDn,
    ATTEN,
    DDR3_addr,
    DDR3_ba,
    DDR3_cas_n,
    DDR3_ck_n,
    DDR3_ck_p,
    DDR3_cke,
    DDR3_cs_n,
    DDR3_dm,
    DDR3_dq,
    DDR3_dqs_n,
    DDR3_dqs_p,
    DDR3_odt,
    DDR3_ras_n,
    DDR3_reset_n,
    DDR3_we_n,
    ETH_REF,
    ETH_REF_CLK_EN_and_RST,
    FPGA_CLK,
    IF1_CH1_AMP123,
    IF2_CH1_AMP123,
    LED,
    MDIO_0_mdc,
    MDIO_0_mdio_io,
    PAMP_EN_FPGA,
    PLL_ROW_EN_FPGA,
    Pmod_out_pin10_io,
    Pmod_out_pin1_io,
    Pmod_out_pin2_io,
    Pmod_out_pin3_io,
    Pmod_out_pin4_io,
    Pmod_out_pin7_io,
    Pmod_out_pin8_io,
    Pmod_out_pin9_io,
    RMII_PHY_M_crs_dv,
    RMII_PHY_M_rx_er,
    RMII_PHY_M_rxd,
    RMII_PHY_M_tx_en,
    RMII_PHY_M_txd,
    SW12_IF12_CH1,
    SW_IN_IF1_IF2,
    SW_OUT_CH1_IF1_IF2,
    Vaux1_0_v_n,
    Vaux1_0_v_p,
    Vaux9_0_v_n,
    Vaux9_0_v_p,
    adc_clkx_0_n,
    adc_clkx_0_p,
    adc_clkx_1_n,
    adc_clkx_1_p,
    adc_dax_0_n,
    adc_dax_0_p,
    adc_dax_1_n,
    adc_dax_1_p,
    adc_dbx_0_n,
    adc_dbx_0_p,
    adc_dbx_1_n,
    adc_dbx_1_p,
    adc_dcox_0_n,
    adc_dcox_0_p,
    adc_dcox_1_n,
    adc_dcox_1_p,
    clk_cs,
    clk_mosi,
    clk_reset,
    clk_sclk,
    clk_sync,
    intr_encoder,
    pll_cen,
    pll_ld_sdo,
    pll_mosi,
    pll_sck,
    pll_sen,
    pll_trig,
    spi_rtl_io0_io,
    spi_rtl_io1_io,
    spi_rtl_io2_io,
    spi_rtl_io3_io,
    spi_rtl_ss_io,
    sys_clk,
    uart_rtl_0_rxd,
    uart_rtl_0_txd,
    uart_rtl_1_rxd,
    uart_rtl_1_txd);
  output [5:0]ADC_MODE_PDn;
  output [5:0]ATTEN;
  output [13:0]DDR3_addr;
  output [2:0]DDR3_ba;
  output DDR3_cas_n;
  output [0:0]DDR3_ck_n;
  output [0:0]DDR3_ck_p;
  output [0:0]DDR3_cke;
  output [0:0]DDR3_cs_n;
  output [1:0]DDR3_dm;
  inout [15:0]DDR3_dq;
  inout [1:0]DDR3_dqs_n;
  inout [1:0]DDR3_dqs_p;
  output [0:0]DDR3_odt;
  output DDR3_ras_n;
  output DDR3_reset_n;
  output DDR3_we_n;
  input ETH_REF;
  output [1:0]ETH_REF_CLK_EN_and_RST;
  input FPGA_CLK;
  output [2:0]IF1_CH1_AMP123;
  output [2:0]IF2_CH1_AMP123;
  output LED;
  output MDIO_0_mdc;
  inout MDIO_0_mdio_io;
  output [0:0]PAMP_EN_FPGA;
  output [0:0]PLL_ROW_EN_FPGA;
  inout Pmod_out_pin10_io;
  inout Pmod_out_pin1_io;
  inout Pmod_out_pin2_io;
  inout Pmod_out_pin3_io;
  inout Pmod_out_pin4_io;
  inout Pmod_out_pin7_io;
  inout Pmod_out_pin8_io;
  inout Pmod_out_pin9_io;
  input RMII_PHY_M_crs_dv;
  input RMII_PHY_M_rx_er;
  input [1:0]RMII_PHY_M_rxd;
  output RMII_PHY_M_tx_en;
  output [1:0]RMII_PHY_M_txd;
  output [3:0]SW12_IF12_CH1;
  output [3:0]SW_IN_IF1_IF2;
  output [3:0]SW_OUT_CH1_IF1_IF2;
  input Vaux1_0_v_n;
  input Vaux1_0_v_p;
  input Vaux9_0_v_n;
  input Vaux9_0_v_p;
  output adc_clkx_0_n;
  output adc_clkx_0_p;
  output adc_clkx_1_n;
  output adc_clkx_1_p;
  input adc_dax_0_n;
  input adc_dax_0_p;
  input adc_dax_1_n;
  input adc_dax_1_p;
  input adc_dbx_0_n;
  input adc_dbx_0_p;
  input adc_dbx_1_n;
  input adc_dbx_1_p;
  input adc_dcox_0_n;
  input adc_dcox_0_p;
  input adc_dcox_1_n;
  input adc_dcox_1_p;
  output clk_cs;
  output clk_mosi;
  output clk_reset;
  output clk_sclk;
  output clk_sync;
  input [0:0]intr_encoder;
  output pll_cen;
  input pll_ld_sdo;
  output pll_mosi;
  output pll_sck;
  output pll_sen;
  output pll_trig;
  inout spi_rtl_io0_io;
  inout spi_rtl_io1_io;
  inout spi_rtl_io2_io;
  inout spi_rtl_io3_io;
  inout [0:0]spi_rtl_ss_io;
  input sys_clk;
  input uart_rtl_0_rxd;
  output uart_rtl_0_txd;
  input uart_rtl_1_rxd;
  output uart_rtl_1_txd;

  wire [5:0]ADC_MODE_PDn;
  wire [5:0]ATTEN;
  wire [13:0]DDR3_addr;
  wire [2:0]DDR3_ba;
  wire DDR3_cas_n;
  wire [0:0]DDR3_ck_n;
  wire [0:0]DDR3_ck_p;
  wire [0:0]DDR3_cke;
  wire [0:0]DDR3_cs_n;
  wire [1:0]DDR3_dm;
  wire [15:0]DDR3_dq;
  wire [1:0]DDR3_dqs_n;
  wire [1:0]DDR3_dqs_p;
  wire [0:0]DDR3_odt;
  wire DDR3_ras_n;
  wire DDR3_reset_n;
  wire DDR3_we_n;
  wire ETH_REF;
  wire [1:0]ETH_REF_CLK_EN_and_RST;
  wire FPGA_CLK;
  wire [2:0]IF1_CH1_AMP123;
  wire [2:0]IF2_CH1_AMP123;
  wire LED;
  wire MDIO_0_mdc;
  wire MDIO_0_mdio_i;
  wire MDIO_0_mdio_io;
  wire MDIO_0_mdio_o;
  wire MDIO_0_mdio_t;
  wire [0:0]PAMP_EN_FPGA;
  wire [0:0]PLL_ROW_EN_FPGA;
  wire Pmod_out_pin10_i;
  wire Pmod_out_pin10_io;
  wire Pmod_out_pin10_o;
  wire Pmod_out_pin10_t;
  wire Pmod_out_pin1_i;
  wire Pmod_out_pin1_io;
  wire Pmod_out_pin1_o;
  wire Pmod_out_pin1_t;
  wire Pmod_out_pin2_i;
  wire Pmod_out_pin2_io;
  wire Pmod_out_pin2_o;
  wire Pmod_out_pin2_t;
  wire Pmod_out_pin3_i;
  wire Pmod_out_pin3_io;
  wire Pmod_out_pin3_o;
  wire Pmod_out_pin3_t;
  wire Pmod_out_pin4_i;
  wire Pmod_out_pin4_io;
  wire Pmod_out_pin4_o;
  wire Pmod_out_pin4_t;
  wire Pmod_out_pin7_i;
  wire Pmod_out_pin7_io;
  wire Pmod_out_pin7_o;
  wire Pmod_out_pin7_t;
  wire Pmod_out_pin8_i;
  wire Pmod_out_pin8_io;
  wire Pmod_out_pin8_o;
  wire Pmod_out_pin8_t;
  wire Pmod_out_pin9_i;
  wire Pmod_out_pin9_io;
  wire Pmod_out_pin9_o;
  wire Pmod_out_pin9_t;
  wire RMII_PHY_M_crs_dv;
  wire RMII_PHY_M_rx_er;
  wire [1:0]RMII_PHY_M_rxd;
  wire RMII_PHY_M_tx_en;
  wire [1:0]RMII_PHY_M_txd;
  wire [3:0]SW12_IF12_CH1;
  wire [3:0]SW_IN_IF1_IF2;
  wire [3:0]SW_OUT_CH1_IF1_IF2;
  wire Vaux1_0_v_n;
  wire Vaux1_0_v_p;
  wire Vaux9_0_v_n;
  wire Vaux9_0_v_p;
  wire adc_clkx_0_n;
  wire adc_clkx_0_p;
  wire adc_clkx_1_n;
  wire adc_clkx_1_p;
  wire adc_dax_0_n;
  wire adc_dax_0_p;
  wire adc_dax_1_n;
  wire adc_dax_1_p;
  wire adc_dbx_0_n;
  wire adc_dbx_0_p;
  wire adc_dbx_1_n;
  wire adc_dbx_1_p;
  wire adc_dcox_0_n;
  wire adc_dcox_0_p;
  wire adc_dcox_1_n;
  wire adc_dcox_1_p;
  wire clk_cs;
  wire clk_mosi;
  wire clk_reset;
  wire clk_sclk;
  wire clk_sync;
  wire [0:0]intr_encoder;
  wire pll_cen;
  wire pll_ld_sdo;
  wire pll_mosi;
  wire pll_sck;
  wire pll_sen;
  wire pll_trig;
  wire spi_rtl_io0_i;
  wire spi_rtl_io0_io;
  wire spi_rtl_io0_o;
  wire spi_rtl_io0_t;
  wire spi_rtl_io1_i;
  wire spi_rtl_io1_io;
  wire spi_rtl_io1_o;
  wire spi_rtl_io1_t;
  wire spi_rtl_io2_i;
  wire spi_rtl_io2_io;
  wire spi_rtl_io2_o;
  wire spi_rtl_io2_t;
  wire spi_rtl_io3_i;
  wire spi_rtl_io3_io;
  wire spi_rtl_io3_o;
  wire spi_rtl_io3_t;
  wire [0:0]spi_rtl_ss_i_0;
  wire [0:0]spi_rtl_ss_io_0;
  wire [0:0]spi_rtl_ss_o_0;
  wire spi_rtl_ss_t;
  wire sys_clk;
  wire uart_rtl_0_rxd;
  wire uart_rtl_0_txd;
  wire uart_rtl_1_rxd;
  wire uart_rtl_1_txd;

  IOBUF MDIO_0_mdio_iobuf
       (.I(MDIO_0_mdio_o),
        .IO(MDIO_0_mdio_io),
        .O(MDIO_0_mdio_i),
        .T(MDIO_0_mdio_t));
  IOBUF Pmod_out_pin10_iobuf
       (.I(Pmod_out_pin10_o),
        .IO(Pmod_out_pin10_io),
        .O(Pmod_out_pin10_i),
        .T(Pmod_out_pin10_t));
  IOBUF Pmod_out_pin1_iobuf
       (.I(Pmod_out_pin1_o),
        .IO(Pmod_out_pin1_io),
        .O(Pmod_out_pin1_i),
        .T(Pmod_out_pin1_t));
  IOBUF Pmod_out_pin2_iobuf
       (.I(Pmod_out_pin2_o),
        .IO(Pmod_out_pin2_io),
        .O(Pmod_out_pin2_i),
        .T(Pmod_out_pin2_t));
  IOBUF Pmod_out_pin3_iobuf
       (.I(Pmod_out_pin3_o),
        .IO(Pmod_out_pin3_io),
        .O(Pmod_out_pin3_i),
        .T(Pmod_out_pin3_t));
  IOBUF Pmod_out_pin4_iobuf
       (.I(Pmod_out_pin4_o),
        .IO(Pmod_out_pin4_io),
        .O(Pmod_out_pin4_i),
        .T(Pmod_out_pin4_t));
  IOBUF Pmod_out_pin7_iobuf
       (.I(Pmod_out_pin7_o),
        .IO(Pmod_out_pin7_io),
        .O(Pmod_out_pin7_i),
        .T(Pmod_out_pin7_t));
  IOBUF Pmod_out_pin8_iobuf
       (.I(Pmod_out_pin8_o),
        .IO(Pmod_out_pin8_io),
        .O(Pmod_out_pin8_i),
        .T(Pmod_out_pin8_t));
  IOBUF Pmod_out_pin9_iobuf
       (.I(Pmod_out_pin9_o),
        .IO(Pmod_out_pin9_io),
        .O(Pmod_out_pin9_i),
        .T(Pmod_out_pin9_t));
  design_1 design_1_i
       (.ADC_MODE_PDn(ADC_MODE_PDn),
        .ATTEN(ATTEN),
        .DDR3_addr(DDR3_addr),
        .DDR3_ba(DDR3_ba),
        .DDR3_cas_n(DDR3_cas_n),
        .DDR3_ck_n(DDR3_ck_n),
        .DDR3_ck_p(DDR3_ck_p),
        .DDR3_cke(DDR3_cke),
        .DDR3_cs_n(DDR3_cs_n),
        .DDR3_dm(DDR3_dm),
        .DDR3_dq(DDR3_dq),
        .DDR3_dqs_n(DDR3_dqs_n),
        .DDR3_dqs_p(DDR3_dqs_p),
        .DDR3_odt(DDR3_odt),
        .DDR3_ras_n(DDR3_ras_n),
        .DDR3_reset_n(DDR3_reset_n),
        .DDR3_we_n(DDR3_we_n),
        .ETH_REF(ETH_REF),
        .ETH_REF_CLK_EN_and_RST(ETH_REF_CLK_EN_and_RST),
        .FPGA_CLK(FPGA_CLK),
        .IF1_CH1_AMP123(IF1_CH1_AMP123),
        .IF2_CH1_AMP123(IF2_CH1_AMP123),
        .LED(LED),
        .MDIO_0_mdc(MDIO_0_mdc),
        .MDIO_0_mdio_i(MDIO_0_mdio_i),
        .MDIO_0_mdio_o(MDIO_0_mdio_o),
        .MDIO_0_mdio_t(MDIO_0_mdio_t),
        .PAMP_EN_FPGA(PAMP_EN_FPGA),
        .PLL_ROW_EN_FPGA(PLL_ROW_EN_FPGA),
        .Pmod_out_pin10_i(Pmod_out_pin10_i),
        .Pmod_out_pin10_o(Pmod_out_pin10_o),
        .Pmod_out_pin10_t(Pmod_out_pin10_t),
        .Pmod_out_pin1_i(Pmod_out_pin1_i),
        .Pmod_out_pin1_o(Pmod_out_pin1_o),
        .Pmod_out_pin1_t(Pmod_out_pin1_t),
        .Pmod_out_pin2_i(Pmod_out_pin2_i),
        .Pmod_out_pin2_o(Pmod_out_pin2_o),
        .Pmod_out_pin2_t(Pmod_out_pin2_t),
        .Pmod_out_pin3_i(Pmod_out_pin3_i),
        .Pmod_out_pin3_o(Pmod_out_pin3_o),
        .Pmod_out_pin3_t(Pmod_out_pin3_t),
        .Pmod_out_pin4_i(Pmod_out_pin4_i),
        .Pmod_out_pin4_o(Pmod_out_pin4_o),
        .Pmod_out_pin4_t(Pmod_out_pin4_t),
        .Pmod_out_pin7_i(Pmod_out_pin7_i),
        .Pmod_out_pin7_o(Pmod_out_pin7_o),
        .Pmod_out_pin7_t(Pmod_out_pin7_t),
        .Pmod_out_pin8_i(Pmod_out_pin8_i),
        .Pmod_out_pin8_o(Pmod_out_pin8_o),
        .Pmod_out_pin8_t(Pmod_out_pin8_t),
        .Pmod_out_pin9_i(Pmod_out_pin9_i),
        .Pmod_out_pin9_o(Pmod_out_pin9_o),
        .Pmod_out_pin9_t(Pmod_out_pin9_t),
        .RMII_PHY_M_crs_dv(RMII_PHY_M_crs_dv),
        .RMII_PHY_M_rx_er(RMII_PHY_M_rx_er),
        .RMII_PHY_M_rxd(RMII_PHY_M_rxd),
        .RMII_PHY_M_tx_en(RMII_PHY_M_tx_en),
        .RMII_PHY_M_txd(RMII_PHY_M_txd),
        .SW12_IF12_CH1(SW12_IF12_CH1),
        .SW_IN_IF1_IF2(SW_IN_IF1_IF2),
        .SW_OUT_CH1_IF1_IF2(SW_OUT_CH1_IF1_IF2),
        .Vaux1_0_v_n(Vaux1_0_v_n),
        .Vaux1_0_v_p(Vaux1_0_v_p),
        .Vaux9_0_v_n(Vaux9_0_v_n),
        .Vaux9_0_v_p(Vaux9_0_v_p),
        .adc_clkx_0_n(adc_clkx_0_n),
        .adc_clkx_0_p(adc_clkx_0_p),
        .adc_clkx_1_n(adc_clkx_1_n),
        .adc_clkx_1_p(adc_clkx_1_p),
        .adc_dax_0_n(adc_dax_0_n),
        .adc_dax_0_p(adc_dax_0_p),
        .adc_dax_1_n(adc_dax_1_n),
        .adc_dax_1_p(adc_dax_1_p),
        .adc_dbx_0_n(adc_dbx_0_n),
        .adc_dbx_0_p(adc_dbx_0_p),
        .adc_dbx_1_n(adc_dbx_1_n),
        .adc_dbx_1_p(adc_dbx_1_p),
        .adc_dcox_0_n(adc_dcox_0_n),
        .adc_dcox_0_p(adc_dcox_0_p),
        .adc_dcox_1_n(adc_dcox_1_n),
        .adc_dcox_1_p(adc_dcox_1_p),
        .clk_cs(clk_cs),
        .clk_mosi(clk_mosi),
        .clk_reset(clk_reset),
        .clk_sclk(clk_sclk),
        .clk_sync(clk_sync),
        .intr_encoder(intr_encoder),
        .pll_cen(pll_cen),
        .pll_ld_sdo(pll_ld_sdo),
        .pll_mosi(pll_mosi),
        .pll_sck(pll_sck),
        .pll_sen(pll_sen),
        .pll_trig(pll_trig),
        .spi_rtl_io0_i(spi_rtl_io0_i),
        .spi_rtl_io0_o(spi_rtl_io0_o),
        .spi_rtl_io0_t(spi_rtl_io0_t),
        .spi_rtl_io1_i(spi_rtl_io1_i),
        .spi_rtl_io1_o(spi_rtl_io1_o),
        .spi_rtl_io1_t(spi_rtl_io1_t),
        .spi_rtl_io2_i(spi_rtl_io2_i),
        .spi_rtl_io2_o(spi_rtl_io2_o),
        .spi_rtl_io2_t(spi_rtl_io2_t),
        .spi_rtl_io3_i(spi_rtl_io3_i),
        .spi_rtl_io3_o(spi_rtl_io3_o),
        .spi_rtl_io3_t(spi_rtl_io3_t),
        .spi_rtl_ss_i(spi_rtl_ss_i_0),
        .spi_rtl_ss_o(spi_rtl_ss_o_0),
        .spi_rtl_ss_t(spi_rtl_ss_t),
        .sys_clk(sys_clk),
        .uart_rtl_0_rxd(uart_rtl_0_rxd),
        .uart_rtl_0_txd(uart_rtl_0_txd),
        .uart_rtl_1_rxd(uart_rtl_1_rxd),
        .uart_rtl_1_txd(uart_rtl_1_txd));
  IOBUF spi_rtl_io0_iobuf
       (.I(spi_rtl_io0_o),
        .IO(spi_rtl_io0_io),
        .O(spi_rtl_io0_i),
        .T(spi_rtl_io0_t));
  IOBUF spi_rtl_io1_iobuf
       (.I(spi_rtl_io1_o),
        .IO(spi_rtl_io1_io),
        .O(spi_rtl_io1_i),
        .T(spi_rtl_io1_t));
  IOBUF spi_rtl_io2_iobuf
       (.I(spi_rtl_io2_o),
        .IO(spi_rtl_io2_io),
        .O(spi_rtl_io2_i),
        .T(spi_rtl_io2_t));
  IOBUF spi_rtl_io3_iobuf
       (.I(spi_rtl_io3_o),
        .IO(spi_rtl_io3_io),
        .O(spi_rtl_io3_i),
        .T(spi_rtl_io3_t));
  IOBUF spi_rtl_ss_iobuf_0
       (.I(spi_rtl_ss_o_0),
        .IO(spi_rtl_ss_io[0]),
        .O(spi_rtl_ss_i_0),
        .T(spi_rtl_ss_t));
endmodule
