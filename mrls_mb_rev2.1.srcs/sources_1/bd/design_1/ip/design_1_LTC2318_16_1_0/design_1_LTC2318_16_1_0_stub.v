// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Mon Oct 11 18:58:29 2021
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top design_1_LTC2318_16_1_0 -prefix
//               design_1_LTC2318_16_1_0_ design_1_LTC2318_16_0_0_stub.v
// Design      : design_1_LTC2318_16_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "LTC2387_16,Vivado 2019.1" *)
module design_1_LTC2318_16_1_0(ADC_DATA, fpga_clk_async, fpga_clk_sync, 
  adc_dax_p, adc_dax_n, adc_dbx_p, adc_dbx_n, adc_dcox_p, adc_dcox_n, clk_100, adc_clkx_p, 
  adc_clkx_n, clk_200)
/* synthesis syn_black_box black_box_pad_pin="ADC_DATA[15:0],fpga_clk_async,fpga_clk_sync,adc_dax_p,adc_dax_n,adc_dbx_p,adc_dbx_n,adc_dcox_p,adc_dcox_n,clk_100,adc_clkx_p,adc_clkx_n,clk_200" */;
  output [15:0]ADC_DATA;
  input fpga_clk_async;
  output fpga_clk_sync;
  input adc_dax_p;
  input adc_dax_n;
  input adc_dbx_p;
  input adc_dbx_n;
  input adc_dcox_p;
  input adc_dcox_n;
  input clk_100;
  output adc_clkx_p;
  output adc_clkx_n;
  input clk_200;
endmodule
