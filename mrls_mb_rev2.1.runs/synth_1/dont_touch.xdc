# This file is automatically generated.
# It contains project source information necessary for synthesis and implementation.

# XDC: C:/project/mrls_mb_rev2.1/mrls.xdc

# Block Designs: bd/design_1/design_1.bd
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1 || ORIG_REF_NAME==design_1} -quiet] -quiet

# IP: bd/design_1/ip/design_1_microblaze_0_0/design_1_microblaze_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_microblaze_0_0 || ORIG_REF_NAME==design_1_microblaze_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_dlmb_v10_0/design_1_dlmb_v10_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_dlmb_v10_0 || ORIG_REF_NAME==design_1_dlmb_v10_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_ilmb_v10_0/design_1_ilmb_v10_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_ilmb_v10_0 || ORIG_REF_NAME==design_1_ilmb_v10_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_dlmb_bram_if_cntlr_0/design_1_dlmb_bram_if_cntlr_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_dlmb_bram_if_cntlr_0 || ORIG_REF_NAME==design_1_dlmb_bram_if_cntlr_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_ilmb_bram_if_cntlr_0/design_1_ilmb_bram_if_cntlr_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_ilmb_bram_if_cntlr_0 || ORIG_REF_NAME==design_1_ilmb_bram_if_cntlr_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_lmb_bram_0/design_1_lmb_bram_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_lmb_bram_0 || ORIG_REF_NAME==design_1_lmb_bram_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_microblaze_0_axi_intc_0/design_1_microblaze_0_axi_intc_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_microblaze_0_axi_intc_0 || ORIG_REF_NAME==design_1_microblaze_0_axi_intc_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_microblaze_0_xlconcat_0/design_1_microblaze_0_xlconcat_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_microblaze_0_xlconcat_0 || ORIG_REF_NAME==design_1_microblaze_0_xlconcat_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_mdm_1_0/design_1_mdm_1_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_mdm_1_0 || ORIG_REF_NAME==design_1_mdm_1_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_clk_wiz_1_0/design_1_clk_wiz_1_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_clk_wiz_1_0 || ORIG_REF_NAME==design_1_clk_wiz_1_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_rst_clk_wiz_1_100M_0/design_1_rst_clk_wiz_1_100M_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_rst_clk_wiz_1_100M_0 || ORIG_REF_NAME==design_1_rst_clk_wiz_1_100M_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_AD9508_0_1/design_1_AD9508_0_1.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_AD9508_0_1 || ORIG_REF_NAME==design_1_AD9508_0_1} -quiet] -quiet

# IP: bd/design_1/ip/design_1_HMC769_0_0/design_1_HMC769_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_HMC769_0_0 || ORIG_REF_NAME==design_1_HMC769_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_xlconstant_0_0/design_1_xlconstant_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_xlconstant_0_0 || ORIG_REF_NAME==design_1_xlconstant_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_Sample_Generator_0_0/design_1_Sample_Generator_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_Sample_Generator_0_0 || ORIG_REF_NAME==design_1_Sample_Generator_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_axi_dma_0_0/design_1_axi_dma_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_axi_dma_0_0 || ORIG_REF_NAME==design_1_axi_dma_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_mig_7series_0_0/design_1_mig_7series_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_mig_7series_0_0 || ORIG_REF_NAME==design_1_mig_7series_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_axi_smc_0/design_1_axi_smc_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_axi_smc_0 || ORIG_REF_NAME==design_1_axi_smc_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_rst_mig_7series_0_100M_0/design_1_rst_mig_7series_0_100M_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_rst_mig_7series_0_100M_0 || ORIG_REF_NAME==design_1_rst_mig_7series_0_100M_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_axi_ethernetlite_0_0/design_1_axi_ethernetlite_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_axi_ethernetlite_0_0 || ORIG_REF_NAME==design_1_axi_ethernetlite_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_mii_to_rmii_0_0/design_1_mii_to_rmii_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_mii_to_rmii_0_0 || ORIG_REF_NAME==design_1_mii_to_rmii_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_axi_timer_0_0/design_1_axi_timer_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_axi_timer_0_0 || ORIG_REF_NAME==design_1_axi_timer_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_axi_uartlite_0_0/design_1_axi_uartlite_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_axi_uartlite_0_0 || ORIG_REF_NAME==design_1_axi_uartlite_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_LTC2318_16_0_0/design_1_LTC2318_16_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_LTC2318_16_0_0 || ORIG_REF_NAME==design_1_LTC2318_16_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_LTC2318_16_1_0/design_1_LTC2318_16_1_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_LTC2318_16_1_0 || ORIG_REF_NAME==design_1_LTC2318_16_1_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_xfft_0_0/design_1_xfft_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_xfft_0_0 || ORIG_REF_NAME==design_1_xfft_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_constrict_AXIS_0_0/design_1_constrict_AXIS_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_constrict_AXIS_0_0 || ORIG_REF_NAME==design_1_constrict_AXIS_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_configReciever_0_0/design_1_configReciever_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_configReciever_0_0 || ORIG_REF_NAME==design_1_configReciever_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_xadc_wiz_0_0/design_1_xadc_wiz_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_xadc_wiz_0_0 || ORIG_REF_NAME==design_1_xadc_wiz_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_axi_quad_spi_0_0/design_1_axi_quad_spi_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_axi_quad_spi_0_0 || ORIG_REF_NAME==design_1_axi_quad_spi_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_axi_uartlite_1_0/design_1_axi_uartlite_1_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_axi_uartlite_1_0 || ORIG_REF_NAME==design_1_axi_uartlite_1_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_PmodSD_0_0/design_1_PmodSD_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_PmodSD_0_0 || ORIG_REF_NAME==design_1_PmodSD_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_axi_timer_3_0/design_1_axi_timer_3_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_axi_timer_3_0 || ORIG_REF_NAME==design_1_axi_timer_3_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_xlconstant_1_0/design_1_xlconstant_1_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_xlconstant_1_0 || ORIG_REF_NAME==design_1_xlconstant_1_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_xbar_1/design_1_xbar_1.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_xbar_1 || ORIG_REF_NAME==design_1_xbar_1} -quiet] -quiet

# IP: bd/design_1/ip/design_1_axi_interconnect_0_0/design_1_axi_interconnect_0_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_axi_interconnect_0_0 || ORIG_REF_NAME==design_1_axi_interconnect_0_0} -quiet] -quiet

# IP: bd/design_1/ip/design_1_auto_pc_0/design_1_auto_pc_0.xci
set_property DONT_TOUCH TRUE [get_cells -hier -filter {REF_NAME==design_1_auto_pc_0 || ORIG_REF_NAME==design_1_auto_pc_0} -quiet] -quiet

# XDC: bd/design_1/design_1_ooc.xdc
