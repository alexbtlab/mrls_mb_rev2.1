
`timescale 1 ns / 1 ps

	module HMC769_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line

		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 6
	)
	(
	    //input wire  FPGA_clk,
	    //input wire  sel_clk,
	
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready,
		output wire  pll_sen,
		output wire  pll_sck,
		output wire  pll_mosi,
		input wire   pll_ld_sdo,
		output wire  pll_cen,
		output wire  pll_trig,
		output wire [5:0] ATTEN,
        output wire start_adc_count,
        //input wire [15:0]  sweep_val,
//        input wire [15:0] shift_front,
        input wire   azimut_0 
//        input wire [15:0] sweep_val, 
//        input wire [15:0] guard_val
          
	);
	     wire [32-1:0]	slv_reg0;
         wire [32-1:0]	slv_reg1;
         wire [32-1:0]	slv_reg2;
         wire [32-1:0]	slv_reg3;
         wire [32-1:0]	slv_reg4;
         wire [32-1:0]	slv_reg5;
         wire [32-1:0]	slv_reg6;
         wire [32-1:0]	slv_reg7;
        
         wire [32-1:0]	ip2mb_reg0;
         wire [32-1:0]	ip2mb_reg1;
         wire [32-1:0]	ip2mb_reg2;
         wire [32-1:0]	ip2mb_reg3;
         wire [32-1:0]	ip2mb_reg4;
         wire [32-1:0]	ip2mb_reg5;
         wire [32-1:0]	ip2mb_reg6;
         wire [32-1:0]	ip2mb_reg7;
	
top_PLL_control top_PLL_control_i(
    .clk_100MHz(s00_axi_aclk),
//    .shift_front(shift_front),
    .pll_sen(pll_sen),
    .pll_sck(pll_sck),
    .pll_mosi(pll_mosi),
    .pll_ld_sdo(pll_ld_sdo),
    .pll_cen(pll_cen),
    .pll_trig(pll_trig),
    .ATTEN(ATTEN),
    .start_adc_count(start_adc_count),
    
    .azimut_0(azimut_0),
    //.sweep_val(sweep_val),
    .slv_reg0(slv_reg0),
    .slv_reg1(slv_reg1),
    .slv_reg2(slv_reg2),
    .slv_reg3(slv_reg3),
    .slv_reg4(slv_reg4),
    .slv_reg5(slv_reg5),
    .slv_reg6(slv_reg6),
    .slv_reg7(slv_reg7),
    .ip2mb_reg0(ip2mb_reg0),
    .ip2mb_reg1(ip2mb_reg1),
    .ip2mb_reg2(ip2mb_reg2),
    .ip2mb_reg3(ip2mb_reg3),
    .ip2mb_reg4(ip2mb_reg4),
    .ip2mb_reg5(ip2mb_reg5),
    .ip2mb_reg6(ip2mb_reg6),
    .ip2mb_reg7(ip2mb_reg7)
    
//    .FPGA_clk(FPGA_clk),
//     .sel_clk(sel_clk)
//    .sweep_val(sweep_val),
//    .guard_val(guard_val)
);	
// Instantiation of Axi Bus Interface S00_AXI
	HMC769_v1_0_S00_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) HMC769_v1_0_S00_AXI_inst (
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready),
		
	    .slv_reg0(slv_reg0),
		.slv_reg1(slv_reg1),
		.slv_reg2(slv_reg2),
		.slv_reg3(slv_reg3),
		.slv_reg4(slv_reg4),
		.slv_reg5(slv_reg5),
		.slv_reg6(slv_reg6),
		.slv_reg7(slv_reg7),
		.ip2mb_reg0(ip2mb_reg0),
		.ip2mb_reg1(ip2mb_reg1),
		.ip2mb_reg2(ip2mb_reg2),
		.ip2mb_reg3(ip2mb_reg3),
		.ip2mb_reg4(ip2mb_reg4),  
	    .ip2mb_reg5(ip2mb_reg5),
		.ip2mb_reg6(ip2mb_reg6),
		.ip2mb_reg7(ip2mb_reg7)  
	       
	);
endmodule


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
module pll_send(
    input wire clk,
    input wire [23:0] pll_data,
    input wire [4:0] reg_address,
    input wire start,
    output wire pll_clk,
    output wire pll_mosi,
    output wire pll_cs    
    );
    reg is_clk_running = 0;
    reg [5:0] bits_send = 0;
    reg start_prev = 0;
    reg pll_mosi_reg = 0, pll_cs_reg=0;
    
    assign pll_clk = clk & is_clk_running;    
    assign pll_cs = pll_cs_reg;
    assign pll_mosi = pll_mosi_reg;    
        
    always @(negedge clk)
    begin
        start_prev <= start;
        if ((start_prev == 1'b0)&(start==1'b1))
        begin 
            bits_send <= 0;            
            pll_mosi_reg <= pll_data[23]; 
            is_clk_running <= 1;           
        end        
        else if (is_clk_running==1)
        begin
            bits_send <= bits_send + 1;
            if (bits_send < 23)
            begin
                pll_mosi_reg <= pll_data[22-bits_send];
            end
            else if (bits_send <28)
            begin
                pll_mosi_reg <= reg_address>>(27-bits_send);
            end
            else if (bits_send < 31)
            begin
                pll_mosi_reg <= 0;
            end
            else if (bits_send == 31)
            begin                
                bits_send<=0;
                pll_cs_reg <= 1;
                is_clk_running <= 0;
            end
        end 
        else 
        begin
            pll_cs_reg <= 0;
            pll_mosi_reg <= 0;            
        end
    end
endmodule
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
module pll_receive(
    input wire clk,
    input wire [7:0] reg_address,
    input wire start,
    output wire pll_clk,
    output wire pll_mosi,
    input wire pll_miso,
    output wire pll_cs,
    output wire [23:0] read_data,
    output wire data_ready
    );
    
    reg is_first_cycle_going = 0;
    reg is_second_cycle_going = 0;
    reg [5:0] bits_send = 0;
    reg start_prev = 0;
    reg pll_mosi_reg = 0, pll_cs_reg=0;
    reg [23:0] read_data_reg = 0; 
    reg data_ready_reg = 0;
      
    assign data_ready = data_ready_reg;
    assign pll_clk = clk & (is_first_cycle_going|is_second_cycle_going);    
    assign pll_cs = pll_cs_reg;
    assign pll_mosi = pll_mosi_reg;
    assign read_data = read_data_reg;
        
    always @(negedge clk)
    begin
        start_prev <= start;
        if ((start_prev == 1'b0)&(start==1'b1))
        begin
            read_data_reg <= 0;
            data_ready_reg <= 0;
            bits_send <= 0;            
            pll_mosi_reg <= 0; //read operation 
            is_first_cycle_going <= 1;           
        end        
        else if (is_first_cycle_going==1)
        begin
            bits_send <= bits_send + 1;
            if (bits_send < 19)
            begin
                pll_mosi_reg <= 0;
            end
            else if (bits_send <23)
            begin
                pll_mosi_reg <= reg_address>>(22-bits_send);
            end
            else if (bits_send < 31)
            begin
                pll_mosi_reg <= 0;
            end
            else if (bits_send == 31)
            begin                
                bits_send<=0;
                pll_cs_reg <= 1;
                is_first_cycle_going <=0;
                is_second_cycle_going <=1;
            end
        end                
        else if (is_second_cycle_going==1)
        begin
            if (pll_cs_reg == 1) pll_cs_reg <= 0;
            bits_send <= bits_send + 1;
            if (bits_send < 19)
            begin
                pll_mosi_reg <= 0;
                read_data_reg <= read_data_reg | (pll_miso<<(23-bits_send));
            end
            else if (bits_send < 24)
            begin
                pll_mosi_reg <= reg_address>>(22-bits_send); 
                read_data_reg <= read_data_reg | (pll_miso<<(23-bits_send));                
            end
            else if (bits_send < 31)
            begin
                pll_mosi_reg <= 0;
            end
             else if (bits_send ==31)
            begin               
                is_second_cycle_going<=0;
                bits_send<=0;
                pll_cs_reg <= 1;                
            end
        end
        else 
        begin
            pll_cs_reg <= 0;
            pll_mosi_reg <= 0;
            data_ready_reg <= 1;
        end
    end
endmodule