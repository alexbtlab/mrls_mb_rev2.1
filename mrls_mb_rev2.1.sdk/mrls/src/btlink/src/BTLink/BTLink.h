/** @file
 *  @brief MAVLink comm protocol generated from BTLink.xml
 *  @see http://mavlink.org
 */
#pragma once
#ifndef MAVLINK_BTLINK_H
#define MAVLINK_BTLINK_H

#ifndef MAVLINK_H
    #error Wrong include order: MAVLINK_BTLINK.H MUST NOT BE DIRECTLY USED. Include mavlink.h from the same directory instead or set ALL AND EVERY defines from MAVLINK.H manually accordingly, including the #define MAVLINK_H call.
#endif

#undef MAVLINK_THIS_XML_IDX
#define MAVLINK_THIS_XML_IDX 0

#ifdef __cplusplus
extern "C" {
#endif

// MESSAGE LENGTHS AND CRCS

#ifndef MAVLINK_MESSAGE_LENGTHS
#define MAVLINK_MESSAGE_LENGTHS {}
#endif

#ifndef MAVLINK_MESSAGE_CRCS
#define MAVLINK_MESSAGE_CRCS {{0, 254, 5, 5, 0, 0, 0}, {1, 72, 10, 10, 3, 8, 9}, {2, 119, 32, 32, 0, 0, 0}, {3, 67, 136, 136, 0, 0, 0}, {4, 28, 255, 255, 0, 0, 0}, {5, 149, 196, 196, 0, 0, 0}, {6, 225, 68, 68, 3, 66, 67}, {7, 89, 68, 68, 3, 66, 67}, {8, 189, 4, 4, 0, 0, 0}, {9, 14, 4, 4, 3, 2, 3}, {10, 159, 2, 2, 3, 0, 1}, {11, 206, 18, 18, 3, 0, 1}, {12, 65, 20, 20, 3, 2, 3}, {13, 120, 29, 29, 3, 10, 11}, {14, 64, 45, 45, 0, 0, 0}, {15, 142, 28, 28, 0, 0, 0}, {16, 75, 20, 20, 0, 0, 0}, {17, 235, 10, 10, 0, 0, 0}, {18, 39, 6, 6, 0, 0, 0}, {19, 208, 6, 6, 0, 0, 0}, {20, 197, 5, 5, 0, 0, 0}, {32, 120, 139, 139, 0, 0, 0}, {33, 148, 16, 16, 0, 0, 0}, {34, 68, 17, 17, 0, 0, 0}, {35, 21, 45, 45, 0, 0, 0}}
#endif

#include "../protocol.h"

#define MAVLINK_ENABLED_BTLINK

// ENUM DEFINITIONS


/** @brief  . */
#ifndef HAVE_ENUM_BT_STATE
#define HAVE_ENUM_BT_STATE
typedef enum BT_STATE
{
   STATE_BOOT=0, /*  . | */
   STATE_CALIBRATING=1, /*  . | */
   STATE_STANDBY=2, /*  | */
   STATE_ACTIVE=3, /*  . | */
   STATE_CRITICAL=4, /*    . | */
   STATE_NEED_REBOOT=5, /*   . | */
   STATE_BACKGROUND_COLLECTION=6, /*   . | */
   BT_STATE_ENUM_END=7, /*  | */
} BT_STATE;
#endif

/** @brief  . */
#ifndef HAVE_ENUM_BT_CMD_TYPE
#define HAVE_ENUM_BT_CMD_TYPE
typedef enum BT_CMD_TYPE
{
   CUSTOM=0, /*  . |User defined| User defined| User defined| User defined| User defined| User defined| User defined| User defined|  */
   REBOOT=1, /*  . |User defined| User defined| User defined| User defined| User defined| User defined| User defined| User defined|  */
   RESET_PARAM=2, /*  . |User defined| User defined| User defined| User defined| User defined| User defined| User defined| User defined|  */
   BT_CMD_TYPE_ENUM_END=3, /*  | */
} BT_CMD_TYPE;
#endif

/** @brief ACK / NACK / ERROR values as a result of BT_CMDs and for mission item transmission. */
#ifndef HAVE_ENUM_BT_CMD_ACK_TYPE
#define HAVE_ENUM_BT_CMD_ACK_TYPE
typedef enum BT_CMD_ACK_TYPE
{
   CMD_ACK_OK=0, /* Command is ok. | */
   CMD_ACK_FAILED=1, /* Generic error message if none of the other reasons fails or if no detailed error reporting is implemented. | */
   CMD_ACK_ACCESS_DENIED=2, /* The system is refusing to accept this command from this source / communication partner. | */
   CMD_ACK_NOT_SUPPORTED=3, /* Command is not supported, other commands would be accepted. | */
   CMD_ACK_IN_PROGRESS=4, /* Parameter value received but not yet validated or set. A subsequent PARAM_EXT_ACK will follow once operation is completed with the actual result. These are for parameters that may take longer to set. Instead of waiting for an ACK and potentially timing out, you will immediately receive this response to let you know it was received. | */
   BT_CMD_ACK_TYPE_ENUM_END=5, /*  | */
} BT_CMD_ACK_TYPE;
#endif

/** @brief Specifies the datatype of a BTLink parameter. */
#ifndef HAVE_ENUM_BT_PARAM_TYPE
#define HAVE_ENUM_BT_PARAM_TYPE
typedef enum BT_PARAM_TYPE
{
   PARAM_TYPE_UINT8=0, /* 8-bit unsigned integer. | */
   PARAM_TYPE_INT8=1, /* 8-bit signed integer. | */
   PARAM_TYPE_UINT16=2, /* 16-bit unsigned integer. | */
   PARAM_TYPE_INT16=3, /* 16-bit signed integer. | */
   PARAM_TYPE_UINT32=4, /* 32-bit unsigned integer. | */
   PARAM_TYPE_INT32=5, /* 32-bit signed integer. | */
   PARAM_TYPE_UINT64=6, /* 64-bit unsigned integer. | */
   PARAM_TYPE_INT64=7, /* 64-bit signed integer. | */
   PARAM_TYPE_REAL32=8, /* 32-bit floating-point. | */
   PARAM_TYPE_REAL64=9, /* 64-bit floating-point. | */
   PARAM_TYPE_BIT=10, /* 1-bit (boolean). | */
   PARAM_TYPE_BITS8=11, /* 8-bit array. | */
   PARAM_TYPE_BITS16=12, /* 16-bit array. | */
   PARAM_TYPE_BITS32=13, /* 32-bit array. | */
   PARAM_TYPE_BITS64=14, /* 64-bit array. | */
   PARAM_TYPE_IP=253, /* IP address. | */
   MAV_PARAM_TYPE_CUSTOM=254, /* Custom Type | */
   BT_PARAM_TYPE_ENUM_END=255, /*  | */
} BT_PARAM_TYPE;
#endif

/** @brief Result from a PARAM_EXT_SET message. */
#ifndef HAVE_ENUM_BT_PARAM_ACK_TYPE
#define HAVE_ENUM_BT_PARAM_ACK_TYPE
typedef enum BT_PARAM_ACK_TYPE
{
   PARAM_ACK_OK=0, /* Parameter value ACCEPTED and SET | */
   PARAM_ACK_FAILED=1, /* Parameter failed to set | */
   PARAM_ACK_ACCESS_DENIED=2, /* The system is refusing to accept this parameter from this source / communication partner. | */
   PARAM_ACK_VALUE_UNSUPPORTED=3, /* Parameter value UNKNOWN/UNSUPPORTED | */
   PARAM_ACK_IN_PROGRESS=4, /* Parameter value received but not yet validated or set. A subsequent PARAM_EXT_ACK will follow once operation is completed with the actual result. These are for parameters that may take longer to set. Instead of waiting for an ACK and potentially timing out, you will immediately receive this response to let you know it was received. | */
   BT_PARAM_ACK_TYPE_ENUM_END=5, /*  | */
} BT_PARAM_ACK_TYPE;
#endif

/** @brief Type of GPS fix */
#ifndef HAVE_ENUM_BT_GPS_FIX_TYPE
#define HAVE_ENUM_BT_GPS_FIX_TYPE
typedef enum BT_GPS_FIX_TYPE
{
   GPS_FIX_TYPE_NO_GPS=0, /* No GPS connected | */
   GPS_FIX_TYPE_NO_FIX=1, /* No position information, GPS is connected | */
   GPS_FIX_TYPE_2D_FIX=2, /* 2D position | */
   GPS_FIX_TYPE_3D_FIX=3, /* 3D position | */
   GPS_FIX_TYPE_DGPS=4, /* DGPS/SBAS aided 3D position | */
   GPS_FIX_TYPE_RTK_FLOAT=5, /* RTK float, 3D position | */
   GPS_FIX_TYPE_RTK_FIXED=6, /* RTK Fixed, 3D position | */
   GPS_FIX_TYPE_STATIC=7, /* Static fixed, typically used for base stations | */
   GPS_FIX_TYPE_PPP=8, /* PPP, 3D position. | */
   GPS_FIX_TYPE_MANUAL=9, /* Manual or fixed position. | */
   BT_GPS_FIX_TYPE_ENUM_END=10, /*  | */
} BT_GPS_FIX_TYPE;
#endif

// MAVLINK VERSION

#ifndef MAVLINK_VERSION
#define MAVLINK_VERSION 3
#endif

#if (MAVLINK_VERSION == 0)
#undef MAVLINK_VERSION
#define MAVLINK_VERSION 3
#endif

// MESSAGE DEFINITIONS
#include "./mavlink_msg_heartbeat.h"
#include "./mavlink_msg_ping.h"
#include "./mavlink_msg_auth_key.h"
#include "./mavlink_msg_error.h"
#include "./mavlink_msg_text.h"
#include "./mavlink_msg_debug.h"
#include "./mavlink_msg_command_int.h"
#include "./mavlink_msg_command_long.h"
#include "./mavlink_msg_command_ack.h"
#include "./mavlink_msg_command_cancel.h"
#include "./mavlink_msg_param_request_list.h"
#include "./mavlink_msg_param_request_group.h"
#include "./mavlink_msg_param_request_read.h"
#include "./mavlink_msg_param_set.h"
#include "./mavlink_msg_param_value.h"
#include "./mavlink_msg_param_set_ack.h"
#include "./mavlink_msg_gps_raw.h"
#include "./mavlink_msg_temp_raw.h"
#include "./mavlink_msg_amplifier.h"
#include "./mavlink_msg_drive.h"
#include "./mavlink_msg_power.h"
#include "./mavlink_msg_raw_data.h"
#include "./mavlink_msg_target_data.h"
#include "./mavlink_msg_target_id_data.h"
#include "./mavlink_msg_target_id_data_from_model.h"

// base include


#undef MAVLINK_THIS_XML_IDX
#define MAVLINK_THIS_XML_IDX 0

#if MAVLINK_THIS_XML_IDX == MAVLINK_PRIMARY_XML_IDX
# define MAVLINK_MESSAGE_INFO {MAVLINK_MESSAGE_INFO_HEARTBEAT, MAVLINK_MESSAGE_INFO_PING, MAVLINK_MESSAGE_INFO_AUTH_KEY, MAVLINK_MESSAGE_INFO_ERROR, MAVLINK_MESSAGE_INFO_TEXT, MAVLINK_MESSAGE_INFO_DEBUG, MAVLINK_MESSAGE_INFO_COMMAND_INT, MAVLINK_MESSAGE_INFO_COMMAND_LONG, MAVLINK_MESSAGE_INFO_COMMAND_ACK, MAVLINK_MESSAGE_INFO_COMMAND_CANCEL, MAVLINK_MESSAGE_INFO_PARAM_REQUEST_LIST, MAVLINK_MESSAGE_INFO_PARAM_REQUEST_GROUP, MAVLINK_MESSAGE_INFO_PARAM_REQUEST_READ, MAVLINK_MESSAGE_INFO_PARAM_SET, MAVLINK_MESSAGE_INFO_PARAM_VALUE, MAVLINK_MESSAGE_INFO_PARAM_SET_ACK, MAVLINK_MESSAGE_INFO_GPS_RAW, MAVLINK_MESSAGE_INFO_TEMP_RAW, MAVLINK_MESSAGE_INFO_AMPLIFIER, MAVLINK_MESSAGE_INFO_DRIVE, MAVLINK_MESSAGE_INFO_POWER, MAVLINK_MESSAGE_INFO_RAW_DATA, MAVLINK_MESSAGE_INFO_TARGET_DATA, MAVLINK_MESSAGE_INFO_TARGET_ID_DATA, MAVLINK_MESSAGE_INFO_TARGET_ID_DATA_FROM_MODEL}
# define MAVLINK_MESSAGE_NAMES {{ "AMPLIFIER", 18 }, { "AUTH_KEY", 2 }, { "COMMAND_ACK", 8 }, { "COMMAND_CANCEL", 9 }, { "COMMAND_INT", 6 }, { "COMMAND_LONG", 7 }, { "DEBUG", 5 }, { "DRIVE", 19 }, { "ERROR", 3 }, { "GPS_RAW", 16 }, { "HEARTBEAT", 0 }, { "PARAM_REQUEST_GROUP", 11 }, { "PARAM_REQUEST_LIST", 10 }, { "PARAM_REQUEST_READ", 12 }, { "PARAM_SET", 13 }, { "PARAM_SET_ACK", 15 }, { "PARAM_VALUE", 14 }, { "PING", 1 }, { "POWER", 20 }, { "RAW_DATA", 32 }, { "TARGET_DATA", 33 }, { "TARGET_ID_DATA", 34 }, { "TARGET_ID_DATA_FROM_MODEL", 35 }, { "TEMP_RAW", 17 }, { "TEXT", 4 }}
# if MAVLINK_COMMAND_24BIT
#  include "../mavlink_get_info.h"
# endif
#endif

#ifdef __cplusplus
}
#endif // __cplusplus
#endif // MAVLINK_BTLINK_H
