#pragma once
// MESSAGE AMPLIFIER PACKING

#define MAVLINK_MSG_ID_AMPLIFIER 18


typedef struct __mavlink_amplifier_t {
 uint32_t time; /*<  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.*/
 int8_t power; /*<  .*/
 int8_t temp; /*<  .*/
} mavlink_amplifier_t;

#define MAVLINK_MSG_ID_AMPLIFIER_LEN 6
#define MAVLINK_MSG_ID_AMPLIFIER_MIN_LEN 6
#define MAVLINK_MSG_ID_18_LEN 6
#define MAVLINK_MSG_ID_18_MIN_LEN 6

#define MAVLINK_MSG_ID_AMPLIFIER_CRC 39
#define MAVLINK_MSG_ID_18_CRC 39



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_AMPLIFIER { \
    18, \
    "AMPLIFIER", \
    3, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_amplifier_t, time) }, \
         { "power", NULL, MAVLINK_TYPE_INT8_T, 0, 4, offsetof(mavlink_amplifier_t, power) }, \
         { "temp", NULL, MAVLINK_TYPE_INT8_T, 0, 5, offsetof(mavlink_amplifier_t, temp) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_AMPLIFIER { \
    "AMPLIFIER", \
    3, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_amplifier_t, time) }, \
         { "power", NULL, MAVLINK_TYPE_INT8_T, 0, 4, offsetof(mavlink_amplifier_t, power) }, \
         { "temp", NULL, MAVLINK_TYPE_INT8_T, 0, 5, offsetof(mavlink_amplifier_t, temp) }, \
         } \
}
#endif

/**
 * @brief Pack a amplifier message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param power  .
 * @param temp  .
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_amplifier_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, int8_t power, int8_t temp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_AMPLIFIER_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int8_t(buf, 4, power);
    _mav_put_int8_t(buf, 5, temp);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_AMPLIFIER_LEN);
#else
    mavlink_amplifier_t packet;
    packet.time = time;
    packet.power = power;
    packet.temp = temp;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_AMPLIFIER_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_AMPLIFIER;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_AMPLIFIER_MIN_LEN, MAVLINK_MSG_ID_AMPLIFIER_LEN, MAVLINK_MSG_ID_AMPLIFIER_CRC);
}

/**
 * @brief Pack a amplifier message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param power  .
 * @param temp  .
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_amplifier_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,int8_t power,int8_t temp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_AMPLIFIER_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int8_t(buf, 4, power);
    _mav_put_int8_t(buf, 5, temp);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_AMPLIFIER_LEN);
#else
    mavlink_amplifier_t packet;
    packet.time = time;
    packet.power = power;
    packet.temp = temp;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_AMPLIFIER_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_AMPLIFIER;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_AMPLIFIER_MIN_LEN, MAVLINK_MSG_ID_AMPLIFIER_LEN, MAVLINK_MSG_ID_AMPLIFIER_CRC);
}

/**
 * @brief Encode a amplifier struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param amplifier C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_amplifier_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_amplifier_t* amplifier)
{
    return mavlink_msg_amplifier_pack(system_id, component_id, msg, amplifier->time, amplifier->power, amplifier->temp);
}

/**
 * @brief Encode a amplifier struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param amplifier C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_amplifier_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_amplifier_t* amplifier)
{
    return mavlink_msg_amplifier_pack_chan(system_id, component_id, chan, msg, amplifier->time, amplifier->power, amplifier->temp);
}

/**
 * @brief Send a amplifier message
 * @param chan MAVLink channel to send the message
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param power  .
 * @param temp  .
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_amplifier_send(mavlink_channel_t chan, uint32_t time, int8_t power, int8_t temp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_AMPLIFIER_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int8_t(buf, 4, power);
    _mav_put_int8_t(buf, 5, temp);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_AMPLIFIER, buf, MAVLINK_MSG_ID_AMPLIFIER_MIN_LEN, MAVLINK_MSG_ID_AMPLIFIER_LEN, MAVLINK_MSG_ID_AMPLIFIER_CRC);
#else
    mavlink_amplifier_t packet;
    packet.time = time;
    packet.power = power;
    packet.temp = temp;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_AMPLIFIER, (const char *)&packet, MAVLINK_MSG_ID_AMPLIFIER_MIN_LEN, MAVLINK_MSG_ID_AMPLIFIER_LEN, MAVLINK_MSG_ID_AMPLIFIER_CRC);
#endif
}

/**
 * @brief Send a amplifier message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_amplifier_send_struct(mavlink_channel_t chan, const mavlink_amplifier_t* amplifier)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_amplifier_send(chan, amplifier->time, amplifier->power, amplifier->temp);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_AMPLIFIER, (const char *)amplifier, MAVLINK_MSG_ID_AMPLIFIER_MIN_LEN, MAVLINK_MSG_ID_AMPLIFIER_LEN, MAVLINK_MSG_ID_AMPLIFIER_CRC);
#endif
}

#if MAVLINK_MSG_ID_AMPLIFIER_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_amplifier_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, int8_t power, int8_t temp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int8_t(buf, 4, power);
    _mav_put_int8_t(buf, 5, temp);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_AMPLIFIER, buf, MAVLINK_MSG_ID_AMPLIFIER_MIN_LEN, MAVLINK_MSG_ID_AMPLIFIER_LEN, MAVLINK_MSG_ID_AMPLIFIER_CRC);
#else
    mavlink_amplifier_t *packet = (mavlink_amplifier_t *)msgbuf;
    packet->time = time;
    packet->power = power;
    packet->temp = temp;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_AMPLIFIER, (const char *)packet, MAVLINK_MSG_ID_AMPLIFIER_MIN_LEN, MAVLINK_MSG_ID_AMPLIFIER_LEN, MAVLINK_MSG_ID_AMPLIFIER_CRC);
#endif
}
#endif

#endif

// MESSAGE AMPLIFIER UNPACKING


/**
 * @brief Get field time from amplifier message
 *
 * @return  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 */
static inline uint32_t mavlink_msg_amplifier_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field power from amplifier message
 *
 * @return  .
 */
static inline int8_t mavlink_msg_amplifier_get_power(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int8_t(msg,  4);
}

/**
 * @brief Get field temp from amplifier message
 *
 * @return  .
 */
static inline int8_t mavlink_msg_amplifier_get_temp(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int8_t(msg,  5);
}

/**
 * @brief Decode a amplifier message into a struct
 *
 * @param msg The message to decode
 * @param amplifier C-struct to decode the message contents into
 */
static inline void mavlink_msg_amplifier_decode(const mavlink_message_t* msg, mavlink_amplifier_t* amplifier)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    amplifier->time = mavlink_msg_amplifier_get_time(msg);
    amplifier->power = mavlink_msg_amplifier_get_power(msg);
    amplifier->temp = mavlink_msg_amplifier_get_temp(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_AMPLIFIER_LEN? msg->len : MAVLINK_MSG_ID_AMPLIFIER_LEN;
        memset(amplifier, 0, MAVLINK_MSG_ID_AMPLIFIER_LEN);
    memcpy(amplifier, _MAV_PAYLOAD(msg), len);
#endif
}
