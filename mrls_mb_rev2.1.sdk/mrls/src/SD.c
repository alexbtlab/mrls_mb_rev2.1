#include "SD.h"
#include <stdio.h>
#include <stdlib.h>

u64 posFile = 0;

char dhcp_str[256];		 	 char* dhcp_ptr = dhcp_str;
char ip_str[256];			 char* ip_ptr = ip_str;
char nm_str[256];			 char* nm_ptr = nm_str;
char gw_str[256] = {0};		 char* gw_ptr = gw_str;
char port_str[256] = {0};	 char* port_ptr = port_str;
char cable_use_str[256];	 char* cable_use_ptr = cable_use_str;

extern bool useCable;

static int handler(void* user, const char* section, const char* name,
                   const char* value)
{
    //configuration* pconfig = (configuration*)user;

         if(strcmp("ip",		name) == 0)  	memcpy(ip_str,	 		value, strlen(value));
    else if(strcmp("gw", 		name) == 0)    	memcpy(gw_str,	 		value, strlen(value));
    else if(strcmp("nm", 		name) == 0)    	memcpy(nm_str,	 		value, strlen(value));
	else if(strcmp("port", 		name) == 0)		memcpy(port_str,	 	value, strlen(value));
	else if(strcmp("dhcp",  	name) == 0)		memcpy(dhcp_str, 		value, strlen(value));
	else if(strcmp("cable_use", name) == 0)		memcpy(cable_use_str, 	value, strlen(value));

    return 1;
}
int WriteSD_file(char* file_name, char* data, u64 offset_file){

	// The drive to mount the SD volume to.
		  // Options are: "0:", "1:", "2:", "3:", "4:"
		  static const char szDriveNbr[] = "0:";

		  DXSPISDVOL disk(XPAR_PMODSD_0_AXI_LITE_SPI_BASEADDR,
		                  XPAR_PMODSD_0_AXI_LITE_SDCS_BASEADDR);
		  DFILE file;

		  FRESULT fr;
		  //u32 bytesWritten = 0;
		  u32 bytesRead, totalBytesRead, currentTotalBytesRead;
		  static char buff[128];
		  //static char* buffptr = "qwerty";
//		  xil_printf_BLE("INFO: Disk start mounted for write\r\n");
		  // Mount the disk
		  DFATFS::fsmount(disk, szDriveNbr, 1);
//		  xil_printf_BLE("INFO: Disk mounted comleted\r\n");

		  fr = file.fsopen(file_name, FA_WRITE);
		  if (fr == FR_OK)
		  {
		    xil_printf_BLE("INFO: Open %s\r\n", file_name);

		    totalBytesRead = 0;

		    file.fslseek(offset_file);

		    do{
		    	currentTotalBytesRead = totalBytesRead;
		    	fr = file.fswrite(data, 1, &bytesRead);
		    	data++;
		    	if(*data == '\0')
		    		break;
		    	totalBytesRead += bytesRead;
		    } while ((currentTotalBytesRead != totalBytesRead) && fr == FR_OK);


		    if (fr == FR_OK){
		      xil_printf_BLE("INFO: Write successful:\r\n");
		      buff[totalBytesRead] = 0;
		    }
		    else{
		      xil_printf_BLE("Read failed\r\n");
		    }

		    	  fr = file.fsclose();
		    	  if (fr == FR_OK){
		    	 	      xil_printf_BLE("INFO: file.fsclose\r\n");
		    	 	      buff[totalBytesRead] = 0;
		    	  }
		    	  else{
		    	 	      xil_printf_BLE("file.fsclose failed\r\n");
		    	  }
				    xil_printf_BLE("cnt-%d\r\n", totalBytesRead);
				    //while(1);
			return totalBytesRead + 1 + offset_file;
		 }
		 else{
			 return -1;
		 }
}
int WriteSD_log(char* data){

	// The drive to mount the SD volume to.
		  // Options are: "0:", "1:", "2:", "3:", "4:"
		  static const char szDriveNbr[] = "0:";

		  DXSPISDVOL disk(XPAR_PMODSD_0_AXI_LITE_SPI_BASEADDR,
		                  XPAR_PMODSD_0_AXI_LITE_SDCS_BASEADDR);
		  DFILE file;
		  FRESULT fr;

		  u32 bytesRead, totalBytesRead, currentTotalBytesRead;
		  static char buff[128];
		  // Mount the disk
		  DFATFS::fsmount(disk, szDriveNbr, 1);

		  fr = file.fsopen("logging.log", FA_WRITE);
		  if (fr == FR_OK)
		  {
		    totalBytesRead = 0;
		    file.fslseek(posFile);

		    do{
		    	currentTotalBytesRead = totalBytesRead;
		    	fr = file.fswrite(data, 1, &bytesRead);
		    	data++;
		    	if(*data == '\0')
		    		break;
		    	totalBytesRead += bytesRead;
		    } while ((currentTotalBytesRead != totalBytesRead) && fr == FR_OK);

		    if (fr == FR_OK){
		      buff[totalBytesRead] = 0;
		    }
		    else{
		    	 return -1;
		    }
		    	  fr = file.fsclose();
		    	  if (fr == FR_OK){
		    	 	      buff[totalBytesRead] = 0;
		    	  }
		    	  else{
		    		  return -1;
		    	  }
			return posFile += totalBytesRead + 1;
		 }
		 else{
			 return -1;
		 }
}
int WriteSD_setNetData(ip4_addr_t ip, ip4_addr_t nm, ip4_addr_t gw, u16 port, u8 dhcp_use, bool useCable){

	//configuration config;

	// The drive to mount the SD volume to.
	// Options are: "0:", "1:", "2:", "3:", "4:"
	const char szDriveNbr[] = "0:";
	DXSPISDVOL disk(XPAR_PMODSD_0_AXI_LITE_SPI_BASEADDR,
			                  XPAR_PMODSD_0_AXI_LITE_SDCS_BASEADDR);
	DFILE file;
	FRESULT fr;
	u32 bytesWritten = 0;

	u32  totalBytesRead ;
	char buff[1024] = {0};
	char new_str[256] = {0};
	char* pnew_str = new_str;

	DFATFS::fsmount(disk, szDriveNbr, 1);

	ip4addr_ntoa_r(&ip, ip_ptr, 16);
	ip4addr_ntoa_r(&nm, nm_ptr, 16);
	ip4addr_ntoa_r(&gw, gw_ptr, 16);

	snprintf(new_str, 1024, \
	  "[netconfig]\r\ndhcp = %s\r\nip = %s\r\nnm = %s\r\ngw = %s\r\nport = %s\r\ncable_use = %s\r\n", \
	  dhcp_use ? "1":"0",
	  ip_ptr,
	  nm_ptr,
	  gw_ptr,
	  utoa(port, port_str, 10),
	  useCable ? "1":"0");

	  fr = file.fsopen("netconfig.ini", FA_WRITE);
		  if (fr == FR_OK)
		  {
			totalBytesRead = 0;

			do{
				file.fswrite (pnew_str, 1, &bytesWritten);
			}while(*(pnew_str++) != '\0');

			if (fr == FR_OK)
				buff[totalBytesRead] = 0;
			else
				xil_printf_BLE("Read failed\r\n");

			fr = file.fsclose();
			if (fr == FR_OK)
				buff[totalBytesRead] = 0;
			else
				xil_printf_BLE("file.fsclose failed\r\n");
		   }
	  return 0;
}
bool ReadNetConfigFromSD(ip_addr_t *ipaddr, ip_addr_t *netmask, ip_addr_t *gw, u16* port){

	configuration config;
	bool dhcp_use = false;
    char  end[128];
    char*  p_end = end;


  DXSPISDVOL disk(XPAR_PMODSD_0_AXI_LITE_SPI_BASEADDR,
                  XPAR_PMODSD_0_AXI_LITE_SDCS_BASEADDR);
  DFILE file;

  // The drive to mount the SD volume to.
  // Options are: "0:", "1:", "2:", "3:", "4:"
  const char szDriveNbr[] = "0:";
  FRESULT fr;
  u32 bytesRead, totalBytesRead, currentTotalBytesRead = 0;

  char buff[150] = {0};
  char *buffptr = buff;

  // Mount the disk
  DFATFS::fsmount(disk, szDriveNbr, 1);

  fr = file.fsopen("netconfig.ini", FA_READ);
  if (fr == FR_OK)
  {
		totalBytesRead = 0;
		do
		{
		  currentTotalBytesRead = totalBytesRead;
		  fr = file.fsread(buffptr, 1, &bytesRead);
		  if(*buffptr == '\0')
			  break;
		  buffptr++;
		  totalBytesRead += bytesRead;
		}while ((currentTotalBytesRead != totalBytesRead) && fr == FR_OK);

    if (fr == FR_OK){
      buff[totalBytesRead] = 0;
    }

	if (file.fsclose() == FR_OK){}				//xil_printf_BLE("INFO: %s-Closed file\r\n", file_name);
	else
		xil_printf_BLE("ERROR: file is not close\r\n");

	ini_parse_string(buff, handler, &config);

	*port    = strtol (port_str, 			 &p_end, 10);
    dhcp_use = strtol((char *)dhcp_str, 	 &p_end, 10) ? 1:0;
    useCable = strtol((char *)cable_use_str, &p_end, 10) ? 1:0;

    if(dhcp_use){
     	ipaddr->addr = 0;
		gw->addr = 0;
		netmask->addr = 0;
    }

    ip4addr_aton(ip_str,    ipaddr);
    ip4addr_aton(nm_str, 	netmask);
    ip4addr_aton(gw_str,	gw);

    return dhcp_use;
  }
  else{
	  xil_printf_BLE("ERR_SD: Error opened file");
	  return 0;
  }
}
//int ini_parse_stream(ini_reader reader, void* stream, ini_handler handler,
//                     void* user);

//char* my_ini_fgets(char* str, int num, void* stream)
//{
//	(FRESULT*)(stream)->fsgets(str, num);
//	return str;
//}


